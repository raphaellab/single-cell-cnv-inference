## Copy Number Calling of Cancer Single Cell Sequencing Technology: README ##
This repository contains code, write-up, and model notes. Data can be found on Brown: /nbu/compbio/aconard/ and /data/compbio/aconard/SCS document, and Princeton: /n/fs/ragr-data/projects/scDNA/data/ and /n/fs/ragr-data/datasets. Results can be found on Brown: /data/compbio/aconard/SCS and Princeton: /n/fs/ragr-data/projects/scDNA/results. 
This is a new repo created June 12th as last repo reached space limit.

