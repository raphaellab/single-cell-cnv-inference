# Created By: Ashley Conard
# Last Modified : April 9th, 2017
# AneuFinder commands - T5

args <- commandArgs(trailingOnly = TRUE)
if (length(args) < 4){
    stop("Usage: Rscript aeuFinder_sims.r <FULL_PATH_TO_DATA> <FULL_OUTPUT_FOLDER> <BIN_SIZE> <FULL_PATH_TO_REF_AND_REF>", call.=FALSE)
}

#source("https://bioconductor.org/biocLite.R")
#biocLite("BSgenome.Hsapiens.UCSC.hg38")
#biocLite("AneuFinder")
library(BSgenome.Hsapiens.UCSC.hg38)
library(AneuFinder)

# Server Folders
var.width.ref <- args[4]
datafolder <- args[1] #"/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/bed/"#"/n/fs/ragr-data/projects/scDNA/aneuFinder/data/bam_T6_normal_not2512_not2498_not2324/"
outputfolder <- args[2] #"/n/fs/ragr-data/projects/scDNA/methods/aneuFinder/outputs/clone1_sims_10kb/"

# Initialize copy-number states and call AneuFinder.
cnv.states <- c('zero-inflation', paste0(1:20, '-somy'))
# c(1000000,500000,250000,50000,10000,5000,1000,500)
Aneufinder(inputfolder = datafolder, outputfolder = outputfolder, numCPU = 10, binsizes = c(as.numeric(args[3])), variable.width.reference = var.width.ref, chromosomes = paste0('chr', c(1:22,'X','Y')), states = cnv.states, correction.method = 'GC', GC.BSgenome = BSgenome.Hsapiens.UCSC.hg38,num.trials = 1, assembly = 'hg38')

