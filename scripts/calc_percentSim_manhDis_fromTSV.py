#!/usr/bin/python
#############################
#
# comparing_methods.py: compares aneuFinder and Ginkgo copy-number results and outputs
# created by : Ashley Conard
# last updated: May 15, 2017
#
# e.g. python comparing_methods.py ~/Desktop/compare_methods/test_1965/aneu_1965.bed ~/Desktop/compare_methods/test_1965/ginkgo_1965.csv 50000 tester_50000_chr19_again
#
#############################
from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import collections as c
import csv
import matplotlib.pyplot
import os
import sys
import re

def dif_01(row):
  if row['ANEUF'] == row['GINKGO']:
      val = 1
  else:
      val = 0
  return val

def compare_calls(mast_df):
  #mast_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'ANEUF', 'GINKGO')) # initializing a dataframe for current cell
  mast_df['manh'] = abs(mast_df['ANEUF'] - mast_df['GINKGO'])
  mast_df['dif_01'] = mast_df.apply(dif_01, axis=1)

  return mast_df

###########################
# USAGE
if len(sys.argv) != 4:
  sys.stderr.write("Usage: " + sys.argv[0] + " DIRECTORY_OF_TSV_FILES(cols: CHR START END ANEUF GINKGO) PATIENT_NAME OUTPUT_FILE_NAME\n")
  sys.exit(1)
###########################

# ARGUMENTS
DIR = sys.argv[1]
PATIENT = sys.argv[2]
OUTPUT_FILE_NAME = sys.argv[3]

if __name__ == '__main__':
  with open(OUTPUT_FILE_NAME+'.csv', 'wb') as csvfile:
    f = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

#f = open(OUTPUT_FILE_NAME+".csv", 'w')
#f = csv.write() 
    f.writerow(('CELL','PERCENT_SIM','MANH_DIST','NUM_BINS','PATIENT'))

    for file in os.listdir(DIR):
      if file.endswith(".csv"): 
        print file

        # Read .csv file into df
        df = pd.read_csv(DIR+file, sep=',')

        # Create percent similarity column and manhattan distance column to sum later 
        m_df = compare_calls(df)

        # Compare the Percent Similarity and Manhattan 
        #f.write("%s,',',%d,',',%d,',', %d ,',', %s\n" % (file.strip("'"),(m_df['dif_01'].sum()/(len(m_df)))*100, m_df['manh'].sum(), len(df), PATIENT.strip('"') )) 
        f.writerow((file,(m_df['dif_01'].sum()/(len(m_df)))*100, m_df['manh'].sum(), len(df), PATIENT))

      else:
          continue
    #f.close() 
