#!/usr/bin/python
#############################
# create_heatmap_CVS_input.py: creates row sorted heatmap of CSV file input. 
# created by : Ashley Conard
# last updated: March 31st, 2017
#
# data used : /Desktop/singleCell/code_others/ginkgo/ginkgo_outputs_t5_gao
# main resource used: http://stackoverflow.com/questions/24997926/making-a-custom-colormap-using-matplotlib-in-python
#############################
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import numpy as np
from matplotlib import colors
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os

###########################
# USAGE
if len(sys.argv) != 4:
  sys.stderr.write("Usage: " + sys.argv[0] + " GINKGO_OUPUT_CSV_FILE(COPY-NUM) "+"clustermap (type  1) or heatmap (type 0)" +" OUTPUT_FILE_NAME.png\n")
  sys.exit(1)
###########################

# ARGUMENTS
CSV_FILE = sys.argv[1]
CLUSTERMAP = sys.argv[2]
OUTPUT_FILE = str(sys.argv[3])

if __name__ == '__main__':

# Read CSV
  df = pd.read_csv(CSV_FILE).sort_index()

# Remove unwanted columns
  df.drop(df.columns[[0,1,2]], axis=1, inplace=True)

# turn into np.array
  df_nparray = df.as_matrix()
  #print "Total Number of Cells: ", len(list(df)), " shape: ", df_nparray.shape

# sort np.array
  df_nparray_s = df_nparray[df_nparray[:,1].argsort()]

# set up colormap
  vmax = 6.0 #float(np.amax(df_nparray_s))
  print "Max set to : ", vmax
  cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'), (2 / vmax, 'white'), (vmax / vmax, 'red')])

  fig, ax = plt.subplots(1)
  #fig.set_size_inches(30, 20)

# plot using sns.heatmap
  if CLUSTERMAP == 1:
    im = sns.clustermap(df_nparray_s.transpose(), col_cluster=False, robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=0, vmax=5)

  else:
    im = sns.heatmap(df_nparray_s.transpose(), robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)
  
# specific to plot
  ax.set(frame_on=False, xticks=[], yticks=[])
  plt.yticks(rotation='horizontal')
  
  # when saving, specify the DPI
  plt.savefig(OUTPUT_FILE, dpi = 200)
  #ax.invert_yaxis()

########### If Needed to work in pandas df space - see what I ahve already tried, below ############
#print df.ix[1,1]
#print "before", list(df)

#df_nparray_s = df.sort()

#a = [[2,2,2,1,1,1,2],[4,4,3,2,1,2,2],[1,1,1,1,1,1,1],[4,3,2,1,2,3,3],[1,1,1,1,1,1,1],[4,4,3,2,1,2,2]]
#y=np.array([np.array(xi) for xi in a])
#df_nparray_s=y[y[:,1].argsort()]

#im = ax.pcolor(df, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')   
#fig = plt.gcf() # get current figure

#im = imshow(df_nparray_s.transpose(), cmap=cmap, interpolation='nearest')#, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)


#im.set_yticks([])
#im.set_xticks([])
#cax = plt.gcf().axes[-1]
#cax.tick_params(labelsize=20)
#confused = ax.pcolor(im, cmap=cmap)
#cbar = fig.colorbar(im)#, cax=cax) #colorbar(im[4], cax=cax)
#cbar.set_ticks(range(7)) # Integer colorbar tick locations
#ax.set(frame_on=False, aspect=1, xticks=[], yticks=[])

#cbar = ax.collections[0].colorbar
#cbar.set_ticks([0,1,2,3,4,5])

#cbar.set_ticks(range(4)) # Integer colorbar tick locations

# cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'),
#                                                     (1 / vmax, 'white'),
#                                                     (3 / vmax, 'red')]
#                                       )

#fig, ax = plt.subplots()
#im = ax.pcolor(data, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')
#cbar = fig.colorbar(im)

