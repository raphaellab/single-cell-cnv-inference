#!/usr/bin/python

# gather_joint_indep_results.py : Get Maximum Posterior Probability from S and compute manhattan distance
#								  Gather Joint Distance matrix.
# Ashley Conard

from __future__ import division
from datadiff import diff
import seaborn as sns
import ast
import matplotlib.pyplot as plt
import math as m
import sys
from scipy.special import gammaln
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import subprocess
from scipy.cluster.hierarchy import fcluster
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

def manhattan_distance(x,y):
    return sum(abs(a-b) for a,b in zip(x,y))

def get_max(S_matrix):
	max_dict={}
	for cell,bins in S_matrix.items():
		#print "cell ", cell
		max_dict[cell]={}
		l = []
		for b in bins:
			#print "\nb", b
			b_max_val= max(b,key=b.get)
			#print "b_max_val", b_max_val
			l.append(b_max_val)
		max_dict[cell] = l
	return max_dict

def normalized_manhattan_distance(inf,tru):
    assert len(inf)==len(tru)# make sure same number of cells
    assert len(inf[0])==len(tru[0])# make sure same number of bins
    full_manh = 0
    #print "m*n", len(inf), '*', len(inf[0])
    for i in range(0,len(inf)):
        full_manh+=manhattan_distance(inf[i],tru[i])
    L1_final_dist = full_manh/(len(inf)*len(inf[0])) # manhattan distance / (m * n)
    return L1_final_dist

def read_file(input_file):
    dict_max_posteriors = OrderedDict() #= {}
    alpha_dict = {}#OrderedDict() 

    alpha = 0
    with open(input_file,'r') as f:
		for line in f:
			if "a  " in line:
				x = line
				x = [i for i in x.split()]
				if alpha==0:
					alpha = int(x[1])
					ms = int(x[3])
					ks = int(x[5])
				elif int(x[1])!=alpha:
					#print "new alpha ", int(x[1])
					dict_max_posteriors[alpha] = alpha_dict
					#print dict_max_posteriors
					alpha_dict = {}#OrderedDict()
					alpha = int(x[1])
					ms = int(x[3])
					ks = int(x[5])
				else:
					alpha = int(x[1])
					ms = int(x[3])
					ks = int(x[5])

				#print "\nalph", alpha, "m ", ms, "k ", ks
			
			elif "Soft_CNPs" in line: 
				#print "soft_cnps", ms, ks
				soft_matrix = ast.literal_eval(line.split('Soft_CNPs', 1)[-1].strip(" "))
				s_max_prof = get_max(soft_matrix)
				#print "s_max_prof", s_max_prof
				#print "soft ", soft_matrix
				#for k,v in soft_matrix.items():
				#	sys.stderr.write(str(k)+"\n")
				#	for val in v:
				#		sys.stderr.write(str(val)+"\n")
			elif "True_CNPs" in line:
				#print "true_cnps", ms, ks 
				true_cnps = ast.literal_eval(line.split('True_CNPs', 1)[-1].strip(" "))
				#print "true_cnps", true_cnps
				
				alpha_dict[(ms,ks)] = normalized_manhattan_distance(s_max_prof,true_cnps)
				#print "alpha dict: ", alpha_dict
			elif line.startswith("Alpha Heat Map "):
				joint_infered = ast.literal_eval(line.split('Alpha Heat Map Dictionary ',1)[-1].strip(" "))
				#print "joint_CNPs", joint_infered
		dict_max_posteriors[alpha] = alpha_dict
		#print dict_max_posteriors
    joint_infered = OrderedDict(sorted(joint_infered.items()))#[OrderedDict(v) for k,v in joint_infered.items()]
    return dict(dict_max_posteriors), dict(joint_infered)

###########################
# USAGE
if (len(sys.argv) !=2):
    sys.stderr.write("Usage: " + sys.argv[0] +" #alphas_#ms_#ks_output.txt"+"\n")
    sys.exit(1)

# ASSIGNMENTS
FILE = str(sys.argv[1])

if __name__ == '__main__':
   	distance_s_indep, distance_s_joint = read_file(FILE)
   	print distance_s_joint
   	print distance_s_indep


##### NOTES #####

	#s_max = {0: [3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 3, 3, 1, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2], 1: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 4, 5, 2, 2, 2, 2, 4, 5, 5, 3, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4, 3, 4, 4, 5, 5, 4, 3, 2, 2, 3, 5, 2, 1, 4, 4], 2: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 2, 3, 4, 3, 3, 3, 4, 4, 5, 1, 3, 4, 3, 3, 4, 4, 4, 3, 3, 2, 2, 0, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 1, 2], 3: [2, 2, 2, 2, 2, 2, 1, 1, 3, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 2, 3, 3, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2], 4: [1, 2, 0, 3, 3, 3, 3, 1, 1, 1, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 4, 4, 4, 4, 5, 5, 1, 1, 0, 0, 1, 2, 1, 1, 3, 4, 2, 3, 3, 3, 3, 1, 2, 1, 2, 2, 3, 2, 4], 5: [3, 4, 3, 3, 4, 1, 0, 0, 1, 1, 3, 3, 3, 3, 3, 4, 4, 5, 4, 3, 3, 4, 3, 3, 2, 4, 4, 3, 3, 2, 2, 0, 1, 4, 1, 2, 3, 2, 2, 2, 2, 3, 4, 4, 3, 3, 4, 4, 1, 2], 6: [3, 1, 0, 2, 2, 3, 2, 2, 0, 3, 2, 2, 1, 0, 0, 4, 3, 2, 2, 2, 3, 3, 5, 4, 2, 2, 1, 3, 1, 2, 1, 2, 4, 5, 5, 5, 5, 4, 5, 5, 3, 1, 1, 2, 3, 4, 4, 1, 4, 4], 7: [2, 1, 0, 1, 1, 2, 1, 2, 0, 2, 2, 2, 1, 0, 0, 4, 5, 2, 2, 2, 3, 5, 5, 3, 2, 2, 1, 2, 2, 2, 1, 2, 3, 3, 4, 4, 5, 3, 4, 3, 3, 3, 2, 2, 3, 3, 3, 2, 3, 2], 8: [2, 2, 2, 2, 1, 2, 3, 3, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 3, 2, 2, 1, 2, 3, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 1, 2], 9: [2, 2, 0, 4, 4, 4, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 5, 4, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 2, 2, 1, 2, 4, 3, 3, 1, 2, 1, 2, 3, 4, 1, 4], 10: [3, 3, 4, 4, 4, 1, 0, 0, 1, 2, 2, 3, 3, 4, 3, 3, 4, 4, 5, 2, 1, 3, 4, 4, 1, 3, 3, 3, 2, 2, 2, 0, 1, 4, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3, 2, 4, 5, 4, 1, 1], 11: [2, 0, 0, 2, 1, 2, 0, 2, 1, 1, 0, 0, 0, 1, 1, 2, 4, 5, 5, 5, 3, 3, 3, 3, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 0, 1, 2, 3, 3, 5, 5, 1, 2, 1, 1, 1, 4, 4, 3, 3], 12: [2, 2, 0, 4, 4, 4, 5, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 2, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 2, 1, 4], 13: [3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 3, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3, 2], 14: [2, 2, 3, 4, 5, 1, 0, 0, 1, 2, 3, 2, 3, 3, 3, 4, 5, 5, 5, 3, 3, 5, 4, 3, 2, 3, 5, 1, 2, 2, 2, 0, 1, 4, 3, 3, 2, 2, 2, 3, 2, 3, 3, 2, 2, 3, 4, 4, 1, 2], 15: [2, 2, 3, 4, 4, 1, 0, 0, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 4, 4, 4, 3, 3, 3, 3, 3, 3, 2, 0, 1, 4, 2, 2, 3, 3, 1, 2, 2, 3, 3, 3, 3, 3, 5, 5, 2, 2], 16: [3, 2, 0, 2, 3, 4, 3, 2, 1, 2, 2, 1, 1, 2, 3, 4, 3, 4, 4, 2, 2, 3, 3, 5, 5, 3, 2, 1, 1, 0, 0, 2, 2, 1, 1, 3, 2, 1, 4, 3, 3, 2, 2, 2, 1, 3, 3, 3, 1, 3], 17: [2, 2, 0, 4, 4, 3, 4, 1, 1, 4, 4, 1, 1, 2, 3, 3, 4, 4, 3, 4, 4, 4, 4, 4, 5, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 4, 4, 1, 2, 3, 3, 4, 1, 2, 2, 1, 3, 4, 1, 4], 18: [2, 1, 0, 1, 2, 2, 1, 1, 0, 3, 2, 2, 2, 0, 0, 4, 4, 2, 2, 2, 2, 3, 4, 4, 1, 2, 1, 3, 2, 2, 3, 3, 4, 4, 5, 4, 4, 5, 5, 4, 2, 2, 3, 2, 3, 3, 3, 1, 2, 3], 19: [2, 2, 0, 1, 2, 2, 1, 1, 0, 2, 2, 2, 1, 0, 0, 4, 5, 4, 2, 2, 2, 4, 4, 3, 2, 2, 1, 3, 2, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 3, 3, 1, 1, 3, 4, 4, 1, 4, 3]}
	#truth = {0: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 1: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 5, 5, 3, 2, 2, 2, 5, 5, 4, 2, 2, 1, 3, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 5, 3, 3, 2, 2, 3, 5, 3, 1, 4, 4], 2: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 5, 2, 2, 5, 4, 3, 2, 4, 5, 3, 3, 2, 2, 0, 1, 4, 2, 2, 3, 3, 2, 2, 2, 3, 3, 4, 2, 3, 5, 4, 1, 2], 3: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 4: [2, 2, 0, 4, 4, 3, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 4, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 3, 1, 4], 5: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 5, 2, 2, 5, 4, 3, 2, 4, 5, 3, 3, 2, 2, 0, 1, 4, 2, 2, 3, 3, 2, 2, 2, 3, 3, 4, 2, 3, 5, 4, 1, 2], 6: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 5, 5, 3, 2, 2, 2, 5, 5, 4, 2, 2, 1, 3, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 5, 3, 3, 2, 2, 3, 5, 3, 1, 4, 4], 7: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 5, 5, 3, 2, 2, 2, 5, 5, 4, 2, 2, 1, 3, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 5, 3, 3, 2, 2, 3, 5, 3, 1, 4, 4], 8: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 9: [2, 2, 0, 4, 4, 3, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 4, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 3, 1, 4], 10: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 5, 2, 2, 5, 4, 3, 2, 4, 5, 3, 3, 2, 2, 0, 1, 4, 2, 2, 3, 3, 2, 2, 2, 3, 3, 4, 2, 3, 5, 4, 1, 2], 11: [2, 0, 0, 2, 1, 2, 0, 2, 1, 1, 0, 0, 0, 1, 1, 1, 5, 5, 5, 4, 2, 3, 4, 2, 3, 1, 2, 3, 2, 2, 2, 4, 1, 2, 0, 1, 2, 5, 3, 4, 5, 2, 2, 1, 1, 1, 4, 4, 4, 3], 12: [2, 2, 0, 4, 4, 3, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 4, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 3, 1, 4], 13: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 14: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 5, 2, 2, 5, 4, 3, 2, 4, 5, 3, 3, 2, 2, 0, 1, 4, 2, 2, 3, 3, 2, 2, 2, 3, 3, 4, 2, 3, 5, 4, 1, 2], 15: [2, 3, 3, 3, 4, 1, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 5, 2, 2, 5, 4, 3, 2, 4, 5, 3, 3, 2, 2, 0, 1, 4, 2, 2, 3, 3, 2, 2, 2, 3, 3, 4, 2, 3, 5, 4, 1, 2], 16: [2, 2, 0, 4, 4, 3, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 4, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 3, 1, 4], 17: [2, 2, 0, 4, 4, 3, 4, 1, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 4, 3, 4, 3, 3, 4, 4, 5, 5, 1, 1, 0, 0, 2, 2, 1, 2, 3, 4, 1, 3, 3, 3, 3, 1, 2, 2, 2, 2, 3, 1, 4], 18: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 5, 5, 3, 2, 2, 2, 5, 5, 4, 2, 2, 1, 3, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 5, 3, 3, 2, 2, 3, 5, 3, 1, 4, 4], 19: [2, 1, 0, 1, 2, 2, 1, 2, 0, 3, 2, 2, 1, 0, 0, 5, 5, 3, 2, 2, 2, 5, 5, 4, 2, 2, 1, 3, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5, 5, 5, 3, 3, 2, 2, 3, 5, 3, 1, 4, 4]}
   	#ans = create_dist(s_max, truth)
   	#print "ans: ", ans
   	
   	#infer = {0: [2, 2, 2, 2, 3, 4, 5, 4, 4, 3], 1: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 2: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 3: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 4: [2, 2, 2, 2, 3, 4, 5, 4, 4, 3], 5: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 6: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 7: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 8: [2, 2, 1, 1, 4, 4, 4, 2, 4, 3]}
   	#truth = {0: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 1: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 2: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 3: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 4: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 5: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 6: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 7: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 8: [2, 2, 1, 1, 4, 3, 4, 2, 4, 2]}
   	#so_far = {(10, 5): 0.19, (20, 4): 0.12, (10, 4): 0.15, (20, 3): 0.07, (15, 3): 0.136, (20, 5): 0.126, (15, 4): 0.25066666666666665, (15, 5): 0.17733333333333334, (10, 3): 0.098}
   	#heatmap(so_far)


   	#     if "ALPHA" in line:
#         ALPHA = str(f.readline().split('=', 1)[-1].strip(" "))
# #    elif "M_CELLS" in line:
#         M_CELLS = int(f.readline().split('=', 1)[-1].strip(" "))
# #    elif "N_BINS" in line:
#         N_BINS = int(f.readline().split('=', 1)[-1].strip(" "))
# #    elif "K_CLONES" in line:
#         K_CLONES = int(f.readline().split('=', 1)[-1].strip(" "))
# #    elif "NUM_CLUST" in line:
#         NUM_CLUST = int(f.readline().split('=', 1)[-1].strip(" "))
# #    elif "clone_matrix" in line:
#         stri = f.readline().split('=', 1)[-1].strip(" ")
#         strs = stri.replace('[','').split('],')
#         clone_matrix = [map(int, s.replace(']','').split(',')) for s in strs]
# #    elif "geno_length" in line:
#         stri = f.readline().split('=', 1)[-1].strip(" ")
#         strs = stri.replace('[','').split('],')
#         geno_length = [map(int, s.replace(']','').split(',')) for s in strs][0]
# #    elif "assign_cells" in line:
#         stri = f.readline().split('=', 1)[-1].strip(" ")
#         strs = stri.replace('[','').split('],')
#         str_list = [map(int, s.replace(']','').split(',')) for s in strs][0]
#         assign_cells = np.array(str_list)
# #    elif "cell_chr_tot_num_reads" in line:
#         stri = f.readline().split('=', 1)[-1].strip(" ")
#         strs = stri.replace('[','').split('],')
#         str_list = [map(int, s.replace(']','').split(',')) for s in strs][0]
#         cell_chr_tot_num_reads = np.array(str_list)
# #    elif "read_matrix" in line:
#         stri = f.readline().split('=', 1)[-1].strip(" ")
#         strs = stri.replace('[','').split('],')
#         read_matrix = [map(int, s.replace(']','').split(',')) for s in strs]
# #    elif "mu_e_emis_matrix" in line:
#         mu_e_emis_matrix = ast.literal_eval(f.readline().split('=', 1)[-1].strip(" "))

