#!/usr/bin/python
#############################
# produce_nbCNV_input.py: creates vector of normalized read counts from "Genome-wide copy number analysis of single-cells" - Baslan et al. 2012. 
# created by : Ashley Conard
# last updated: April 3st, 2017
#
# TO RUN FOR ALL IN A FILE:
# for i in *.txt;do python ~/Desktop/singleCell/single-cell-cn/scripts/produce_nbCNV_input.py $i ${i%.txt}.csv ;done 
#############################
import pandas as pd
import sys
import os

###########################
# USAGE
if len(sys.argv) != 3:
  sys.stderr.write("Usage: " + sys.argv[0] + " BASLAN_STEP_84_VARBIN.50K.txt OUTPUT_FILE_NAME.csv\n")
  sys.exit(1)
###########################

# ARGUMENTS
CSV_FILE = sys.argv[1]
OUTPUT_FILE = str(sys.argv[2])

if __name__ == '__main__':

# Read CSV
  df = pd.read_csv(CSV_FILE, sep='\t')
# Remove unwanted columns
  df.drop(df.columns[[0,1,2,3,4,5,7]], axis=1, inplace=True)
  df.to_csv(OUTPUT_FILE,sep=',', index=False, header=None)

