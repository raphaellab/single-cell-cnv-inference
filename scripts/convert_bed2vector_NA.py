#!/usr/bin/python
#############################
#
# convert_bed2vector.py: compares aneuFinder and Ginkgo copy-number results and outputs
# created by : Ashley Conard
# last updated: July 10, 2017
#
#
#############################
from __future__ import division
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import re
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from os import listdir
from os.path import isfile, join
from collections import OrderedDict

# FILE POSITIONS - ANEUFINDER
CHR_A = 0
START = 1
END = 2
CN_A = 4

def create_normalized_vector(a_pd,cell_name):#, cur_cell):
  mast_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'ANEUF'))
  sorted_chroms = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX']
  idx = -1 # Get windows given KB from user
  
  # Iterate through the sorted list of chromosomes
  for i in sorted_chroms:
    cur_chrome = df[df["Chr"] == i]
    start_window = 1 # initialize the start for each

    # Get normalized coverage in a bin for range from 1 - End of chrom (coverage: , normalization: (coverage in bin) / (total reads))
    for end_window in range(KB, cur_chrome.loc[cur_chrome['End'].idmax()].End, KB):
      print "start and end ", start_window, end_window
      idx+=1
      #print "info passed to get_ functions: ", idx, ch, start_window, end_window
      mast_df.loc[idx] = [ch, start_window, end_window, get_aneu(cur_cell, ch, a_dict, start_window, end_window), get_ginkgo(cur_cell, ch, g_dict, start_window, end_window)] #get_aneu(aneu) ,get_ginkgo()] # grab the value in that range
      start_window = end_window+1 # next start position


    for end_window in range(KB, min(a_chr_end_list[ch], g_chr_end_list[ch]), KB):
      idx+=1
      #print "info passed to get_ functions: ", idx, ch, start_window, end_window
      mast_df.loc[idx] = [ch, start_window, end_window, get_aneu(cur_cell, ch, a_dict, start_window, end_window), get_ginkgo(cur_cell, ch, g_dict, start_window, end_window)] #get_aneu(aneu) ,get_ginkgo()] # grab the value in that range
      start_window = end_window+1 # next start position
  
    # Check to see that they are the same length
  if len(mast_df['ANEUF'].tolist()) != len(mast_df['GINKGO'].tolist()):
    sys.error('ERROR: Length of AneuFinder profile and Ginkgo profile are not equal.')

  # Get Percent Similarity and Manhattan 
  manh_dist, count_sim = percent_sim_manh_dist(mast_df['ANEUF'].tolist(), mast_df['GINKGO'].tolist()) # here there are two (AneuF and Ginkgo) profiles for the same cell
  perc_sim = (count_sim/len(mast_df['ANEUF'].tolist())*100)
  
  # Calculate the ploidy calls for each cell for Ginkgo and AneuFinder.
  ploidy_aneuf, ploidy_gink = calculate_ploidy(mast_df['ANEUF'].tolist(), mast_df['GINKGO'].tolist())

  # Outputs to percentSim_manhDist csv file
  print cur_cell,'\t',perc_sim,'\t',manh_dist,'\t', len(mast_df['ANEUF']), '\t', ploidy_aneuf, '\t', ploidy_gink ,'\t', PATIENT
  
  # Output to sderr
  sys.stderr.write("Percent similarity ((+1 for same cn in a bin) / all bins) is "+ str(count_sim)+"/"+str(len(mast_df['ANEUF'].tolist()))+"*100 ="+str(perc_sim)+"%"+'\n') 
  sys.stderr.write("Manhattan distance of: "+str(manh_dist)+" out of sequence length "+str(len(mast_df['ANEUF'].tolist()))+'\n') 
  mast_df.to_csv(OUTPUT_FILE+"_"+cur_cell+'.csv')
  #mast_df.to_csv(OUTPUT_FILE+'.csv')

###########################
# USAGE
if len(sys.argv) != 4:
  sys.stderr.write("Usage: " + sys.argv[0] + " DIRECTORY_INPUT_FILES(.csv files from compare_methods.py)  CELL_ID KB (e.g. 50000)  OUTPUT_FILE_NAME\n")
  sys.exit(1)
###########################

# ARGUMENTS
FILEPATH = sys.argv[1]
KB = int(sys.argv[2])
OUTPUT_FILE = str(sys.argv[3])

if __name__ == '__main__':
  onlyfiles = [f for f in listdir(FILEPATH) if isfile(join(FILEPATH, f))]
  for f in onlyfiles:
    print "Reading .bed for cell: ", FILEPATH+f # file and filepath
    df = pd.read_csv(FILEPATH+f, sep='\t', header=None)
    df.columns = ["Chr", "Start", "End", "Name", "Coverage", "Strand"] 
    #print list(df.columns.values)
    aneuFinder_processing(df)
