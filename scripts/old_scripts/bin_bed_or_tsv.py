#!/usr/bin/python
#############################
#
# bin_bed_or_tsv.py: bins .bed or .tsv files
# created by : Ashley Conard
# last updated: Aug. 7, 2017
#
#############################
from __future__ import division
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import gzip
import string
import re
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from collections import OrderedDict

def aneuFinder_processing(aneu_file): # returns 3 part dictionary : [cell:[chrom:[(start,end): CN,...],...]]
  freq_X_somy_bins_df = pd.DataFrame(columns=('ID', 'CELL_NAME', 'NUM_X-SOMY_BINS', 'NUM_TOT_BINS', 'NORM_X-SOMY_BINS'))
  after_first_cell = False
  c_idx = 0 # keep track of the number of cells

  # Read in .bed.gz for each cell
  with gzip.open(aneu_file) as f:
    for line in f:
    
      # FOR EACH CELL
      if (line.startswith("track") and after_first_cell):# AFTER THE FIRST CELL

            # Calling bin_data method to bin data with KB bin size (output is 1 file per cell).
            print "\nCalling bin_data() for: ", cell_name
            output_bin_df = bin_data(data_df, cell_name) # bin the current cell
            
            # Calling calc_num_X_somy_bins to get normalized number of X-somy bins for each cell (output is 1 fil for all cells)
            print "\nCalling calc_num_X-somy_bins() for: ", cell_name
            c_idx+=1
            calc_num_X_somy_bins(output_bin_df, freq_X_somy_bins_df, c_idx, cell_name) #c_idx used to index the output freq_X-somy_bins_df
            
            # Restart with another cell
            l = line.split(" ")
            cell_name = string.replace(l[4], "\"","")
            print "\nReading in next cell: ", cell_name
            data_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'CN'))
            idx=-1 # restart index
            
      elif (line.startswith("track") and not after_first_cell): # FOR THE FIRST CELL
            after_first_cell = True
            l = line.split(" ")
            cell_name = string.replace(l[4], "\"","")
            print "Reading in: ", cell_name
            data_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'CN'))
            idx=-1 # restart index
        
      # Gather copy-number information for that cell
      elif line.startswith('chr'): # it is a chromosome
        idx+=1
        l_s = line.split('\t')
        data_df.loc[idx] = [l_s[0], l_s[1], l_s[2], l_s[3]]
  
  freq_X_somy_bins_df.to_csv(OUTPUT_FILE+"_ID_"+ID+"_frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv")
  print "Saved Aneuf. freq. X-somy .csv file as: ", OUTPUT_FILE+"_ID_"+ID+"_frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv"
  os.remove('intermediate.csv')

def get_cn_gink(chrom, ist, ien):
    #print "chrom: ", chrom
    ist_int = chrom[(chrom.START <= ist) & (ist < chrom.END)].index[0]
    ien_int = chrom[(chrom.START <= ien) & (ien < chrom.END)].index[0]
    #print "ist_int: ", ist_int
    #print "ien_int: ", ien_int

    if ist_int != ien_int:
        ien_int+=1
        interval_chrom = chrom.iloc[ist_int:ien_int,:]
        #print "interval_chrom: ", interval_chrom
        interval_chrom['diff'] = interval_chrom.END - interval_chrom.START # make the difference between the start of the ist and its chrom.END and end of ien and its chrom.START  
        #print "\nmultiple", ist, ien, "\n", interval_chrom 
        #cn = interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]
        #print "\nCN: ", cn
        return string.replace(str(interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]), ".0", "")
        
    elif ist_int == ien_int:
        if len(chrom[(chrom.START <= ist) & (ien <= chrom.END)])==1:
            n = chrom[(chrom.START <= ist) & (ien <= chrom.END)]
            #print "in", ist, ien, "\n", n
            return string.replace(str(n.iloc[0].CN),".0","")#n.iloc[0].CN

        else:
            sys.exit("IST_INT == IEN_INT but not in interval...")
    
    else:
        sys.exit("NOT AN OPTION", ist, ien, ist_int, ien_int)

def get_cn(chrom, ist, ien):
    ist_int = chrom[(chrom.START <= ist) & (ist < chrom.END)].index[0]
    ien_int = chrom[(chrom.START <= ien) & (ien < chrom.END)].index[0]
    #print "ist_int: ", ist_int
    #print "ien_int: ", ien_int

    if ist_int != ien_int:
        ien_int+=1
        interval_chrom = chrom.iloc[ist_int:ien_int,:]
        #print "interval_chrom: ", interval_chrom
        interval_chrom['diff'] = interval_chrom.END - interval_chrom.START # make the difference between the start of the ist and its chrom.END and end of ien and its chrom.START  
        #print "\nmultiple", ist, ien, "\n", interval_chrom 
        #print "\nCN: ", string.replace((interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]), "-somy", "")
        return string.replace((interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]), "-somy", "")
        
    elif ist_int == ien_int:
        if len(chrom[(chrom.START <= ist) & (ien <= chrom.END)])==1:
            n = chrom[(chrom.START <= ist) & (ien <= chrom.END)]
            #print "in", ist, ien, "\n", n
            return string.replace(n.iloc[0].CN,"-somy","")
        
        else:
            sys.exit("IST_INT == IEN_INT but not in interval...")
    
    else:
        sys.exit("NOT AN OPTION", ist, ien, ist_int, ien_int)

def bin_data(b_df, cell_na): 
  output_binned_df =  pd.DataFrame(columns=('CHR', 'START', 'END', 'ID', 'CELL_NAME', 'CN'))
  sorted_chroms = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX']

  idx = -1 # Index used to fill in coverage for all cells
  skip = KB

  # Iterate through the sorted list of chromosomes to build the profiles for comparison
  for chrm in sorted_chroms: 
      current_df = b_df[b_df['CHR']==chrm]   
      current_df.to_csv("intermediate.csv")
      current_df = pd.read_csv("intermediate.csv")

      start = 1
      end = int(current_df['END'].max())

      print "\nCurrent chromosome: ", chrm
      print "end ", end
    
      for i in range(start, end, skip):
          idx+=1
          if i+skip > end:
            print "Finished binning"
          
          else:    
            #print "\nIntervals: ", i, i+skip-1, type(i), type(i+skip-1)
            if ANEU_OR_GINK == 0:
                  cn = get_cn(current_df, i, i+skip-1)
                  output_binned_df.loc[idx] = [chrm, i, i+skip-1, ID, cell_na, cn]
            elif ANEU_OR_GINK == 1:
                  cn = get_cn_gink(current_df, i, i+skip-1)
                  output_binned_df.loc[idx] = [chrm, i, i+skip-1, ID, cell_na, cn]              
            else:
                  sys.exit("ERROR IN BIN_DATA - MUST HAVE 0 OR 1 TO PROCESS BINNING DATA")
    
  output_binned_df.to_csv(OUTPUT_FILE+"_ID_"+ID+"_cell_"+cell_na+"_"+str(KB)+"size__bin_bed_or_tsv.csv")
  return output_binned_df

def calc_num_X_somy_bins(out_bin_df, freq_df, index, cell_n):
    state_count = out_bin_df[out_bin_df["CN"] == str(STATE)].count().CN 
    total_bins = out_bin_df.shape[0]

    freq_df.loc[index] = [ID, cell_n, state_count, total_bins, (state_count/total_bins)]
    #print "freq_bf: ", freq_df

###########################
# USAGE
if len(sys.argv) != 7:
  sys.stderr.write("Usage: " + sys.argv[0] + " <0 or 1 (0-ANEUFINDER_INPUT_BED_FILE,  1-GINKGO_INPUT_TSV_FILE_LIST, e.g. '../file1.tsv,../file2.tsv,../file3.tsv')>  <DIR_INPUT_FILE> <KB (e.g. 50000)> <ID (for merge_dnacopy_dnahmm_ginkgofixed_ginkgovariable.py)>  <X-somy (e.g. 2.0)>  <OUTPUT_FILE_NAME>\n")
  sys.exit(1)
###########################

# ARGUMENTS
ANEU_OR_GINK = int(sys.argv[1])
DATA_DIR_FILE = sys.argv[2]
KB = int(sys.argv[3])
ID = sys.argv[4]
STATE = str(sys.argv[5])
#if ".0" not in STATE:
#      STATE = STATE+".0"
OUTPUT_FILE = str(sys.argv[6])

if __name__ == '__main__':
  # .BED.GZ (ANEUF) PROCESSING
  if ANEU_OR_GINK == 0: 
    print "Processing .bed.gz: ", DATA_DIR_FILE 
    aneuFinder_processing(DATA_DIR_FILE)
  
  # .TSV (GINKGO) PROCESSING
  elif ANEU_OR_GINK == 1: 
    c_idx = 0 # keep track of the number of cells for the freq_X-somy file.
    freq_X_somy_bins_df_gink = pd.DataFrame(columns=('ID', 'CELL_NAME', 'NUM_X-SOMY_BINS', 'NUM_TOT_BINS', 'NORM_X-SOMY_BINS'))
    
    # Get .tsv into pandas dataframe
    print "Processing .tsv: ", DATA_DIR_FILE
    big_ginkgo_df = pd.read_csv(DATA_DIR_FILE,sep="\t")

    # List of cells
    list_cells = list(big_ginkgo_df)
    del list_cells[:3]

    for cell_name_g in list_cells:
          c_idx+=1
          # Get cell specific dataframe and change cell_name col to "CN"
          cell_ginkgo_df = big_ginkgo_df[['CHR', 'START', 'END', cell_name_g]] # create dataframe for the cell
          cell_ginkgo_df= cell_ginkgo_df.rename(columns = {cell_name_g: 'CN'})

          print "Binning cell: ", cell_name_g
          output_bin_df = bin_data(cell_ginkgo_df, cell_name_g) # bin current cell
            
          # Calling calc_num_X_somy_bins to get normalized number of X-somy bins for each cell (output is 1 fil for all cells)
          print "\nCalling calc_num_X-somy_bins() for: ", cell_name_g
          calc_num_X_somy_bins(output_bin_df, freq_X_somy_bins_df_gink, c_idx, cell_name_g) #c_idx used to index the output freq_X-somy_bins_df
    freq_X_somy_bins_df_gink.to_csv(OUTPUT_FILE+"_ID_"+ID+"_frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv")
    print "Saved Ginkgo freq. X-somy .csv file as: ", OUTPUT_FILE+"_ID_"+ID+"_frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv"
    os.remove('intermediate.csv')
          
  else:
        sys.exit("MUST ENTER 0 or 1 (0-ANEUFINDER_INPUT_BED_FILE,  1-GINKGO_INPUT_TSV_FILE_LIST, e.g. '../file1.tsv,../file2.tsv,../file3.tsv') ")

    # # DF to fill with vectors of normalized read counts across all cells in the input directory
    #binned_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'ID', 'CELL_NAME', 'CN')
    #freq_X-somy_bins_df = pd.DataFrame(columns=('ID', 'CELL_NAME', 'NUM_X-SOMY_BINS', 'NUM_TOT_BINS'))
    
    # text_w = '%s/data_binned_RD_%s_%s_fetch.csv'%(OUTPUT_DIR,CELL_BAM_NAME,BIN_SKIP)
    #binned_df = index_master_df(mast_df, dict_start_end_chroms, CHR_LIST)


################## Notes ##################

# def get_cn(chrom, ist, ien):
#     #& ien <= chrom.END
#     #check_multi_overlaps = chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]
#     #chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]
#     #print "\nck: ", check_multi_overlaps
#     ist_int = chrom[(chrom.START <= ist) & (ist < chrom.END)].index[0]
#     ien_int = chrom[(chrom.START <= ien) & (ien < chrom.END)].index[0]
#     print "ist: ", ist_int
#     print "ien: ", ien_int

#     if ist_int != ien_int:
#         ien_int+=1
#         interval_chrom = chrom.iloc[ist_int:ien_int,:]
#         print "interval_chrom: ", interval_chrom
#         interval_chrom['diff'] = interval_chrom.END - interval_chrom.START # make the difference between the start of the ist and its chrom.END and end of ien and its chrom.START  
#         print "multiple", ist, ien, "\n", interval_chrom 
#         return string.replace((interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]), "-somy", "")
        
#     elif ist_int == ien_int:
#         if len(chrom[(chrom.START <= ist) & (ien <= chrom.END)])==1:
#             n = chrom[(chrom.START <= ist) & (ien <= chrom.END)]
#             print "in", ist, ien, "\n", n
#             #print "n.CN", n.iloc[0].CN
#             st = string.replace(n.iloc[0].CN,"-somy","")
#             print "st: ", st
#             return st
        
#         else:
#             sys.exit("IST_INT == IEN_INT but not in interval...")
    
#     else:
#         sys.exit("NOT AN OPTION", ist, ien, ist_int, ien_int)
    
#     if not check_multi_overlaps.empty: # if the dataframe is not empty
#         next_bin_row = chrom.iloc[[check_multi_overlaps.index[0]+1]] # get index of current 
#         print "\n End and next bin row end: ", ien, next_bin_row.END
#         while (ist >= next_bin_row.START) and (ien <= next_bin_row.END):
#             print "add interval", 
    #|-*---|----|----|--*-|
    #while #if len(check_multiple_overlaps)>1:
        
#     if len(chrom[(chrom.START <= ist) & (ien <= chrom.END)])==1:
#         n = chrom[(chrom.START <= ist) & (ien <= chrom.END)]
#         print "in", ist, ien,chrom[(chrom.START <= ist) & (ien <= chrom.END)]
#         return string.replace(n.CN[0],"-somy","")
    
#     elif len(chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]) ==1:
#         pot_int = chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]

#         if (pot_int.END[0] - ist) > (ien - pot_int.END[0]):
#             n = chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]
#             print "choose current",ist, ien, chrom[(chrom.START <= ist) & (ist < chrom.END ) & (ien >= chrom.END)]
#             return string.replace(n.CN[0],"-somy","")
            
#         elif (pot_int.END[0] - ist) < (ien - pot_int.END[0]):
#             n = chrom[(chrom.START[0] <= ist) & (ist < chrom.END[0] ) & (ien >= chrom.END)]
#             loc_next = n.index[0]+1
#             print "choose next", ist, ien, chrom.iloc[[loc_next]]
#             return string.replace(loc_next.CN[0],"-somy","")
        
#     elif len((chrom.END <= en) & (chrom.END <= en))>1:
#         mask = (chrom.END <= en) & (chrom.END <= en)
#         pot_int = chrom.loc[mask]
#         pot_int['diff'] = pot_int.END - pot_int.START
#         print "multiple", ist, ien, pot_int 
#         return string.replace((pot_int.loc[pot_int['diff']== pot_int['diff'].max(), 'CN'].iloc[0]), "-somy", "")

#     else:
#         print "NOT AN OPTION", ist, ien

