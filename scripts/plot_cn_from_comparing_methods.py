#!/usr/bin/python
#
# plot_cn_from_comparing_methods.py 
#	
# Ashley Conard

from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import pandas as pd
import os
import numpy as np
from pandas import DataFrame
import warnings

###########################
# USAGE
if len(sys.argv) != 3:
  sys.stderr.write("Usage: " + sys.argv[0] + " PATIENT_CELL_FILE.csv(from comparing_methods.py output) PDF_FILE_OUTPUTNAME\n")
  sys.exit(1)

###########################
# ASSIGNMENTS
FILE = str(sys.argv[1])
CELL_NAME = str(sys.argv[2])

###########################

if __name__ == '__main__':
	df = pd.read_csv(FILE,sep=',')
	fig,ax=plt.subplots(1,figsize=(12,3))
	for index,row in df.iterrows():
		#plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (row["GINKGO"],row["GINKGO"]), "|-", c="g", markersize=10, linewidth=.1)
	for index,row in df.iterrows():
		#plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (row["ANEUF"],row["ANEUF"]), "|-", c="b", markersize=10, linewidth=.1)
	
#	df.GINKGO.plot(style='g-', label='Ginkgo')
#	df.ANEUF.plot(style='b-', label='AneuFinder')
#	df.ANEUF.plot(style='_', c='b', label='AneuFinder', markersize=4)
#	df.GINKGO.plot(style='_', c='g', label='Ginkgo', markersize=4)
	plt.legend(loc='upper right')
	plt.ylim(-0.9, 10)
	plt.xlim(0)
	plt.title("%s Copy Number Profiles" % (CELL_NAME))
	plt.xlabel('Bin')
	plt.ylabel('Integer Copy-Number')
	plt.savefig('%s_copy-number-profiles_points.pdf' % (CELL_NAME))
	plt.close(fig)

	
	fig,ax=plt.subplots(1,figsize=(12,3))
	for index,row in df.iterrows():
	#	plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (abs(row["GINKGO"]-row["ANEUF"]),abs(row["GINKGO"]-row["ANEUF"])), "|-", c="r", markersize=10, linewidth=.1)
		#plt.plot((index-0.5, index+0.5), (abs(row["GINKGO"]-row["ANEUF"]),abs(row["GINKGO"]-row["ANEUF"])), "--", c="r", linewidth=6)
	# df.GINKGO.plot(style='g-', label='Ginkgo')
	# df.ANEUF.plot(style='b-', label='AneuFinder')
#	df.ANEUF.plot(style='_', c='b', label='AneuFinder', markersize=4)
#	df.GINKGO.plot(style='_', c='g', label='Ginkgo', markersize=4)
	plt.legend(loc='upper right')
	plt.ylim(-0.9, 10)
	plt.xlim(0)
	plt.title("%s Copy Number Profiles" % (CELL_NAME))
	plt.xlabel('Bin')
	plt.ylabel('Integer Copy-Number')
	plt.savefig('%s_overlap_copy-number-profiles_points.pdf' % (CELL_NAME))
	plt.close(fig)
