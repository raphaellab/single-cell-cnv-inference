#!/usr/bin/python

from __future__ import division
import seaborn as sns
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import re
import pandas as pd
import sys
import os
import matplotlib
from scipy.stats import mode
from IPython.display import HTML
import seaborn as sns
import scipy as sp
from pylab import rcParams

###########################
# USAGE
if len(sys.argv) != 2:
  sys.stderr.write("Usage: " + sys.argv[0] + " COPY_NUMBER (must be int)\n")
  sys.exit(1)
###########################

# ARGUMENTS
COPY_NUMBER = int(sys.argv[1])

if __name__ == '__main__':
    # Dataframe
    df = pd.read_csv("../data/SegCopy.tsv", sep='\t')
    df.replace({r'\r\n': ''},regex=True);

    # Get list of cells
    cells_list = df.columns.values.tolist()
    cells_list=cells_list[3:]
    
    # Create dataframe of frequency of all copy-numbers for each bin
    df_new = df[cells_list].apply(pd.Series.value_counts,axis=1).fillna(0)
    df_new["INDEX"] = df["START"]

    # Create plot for input copy-number 
    ax = df_new[[COPY_NUMBER, 'INDEX']]
    print ax
    
    fig = ax.set_index('INDEX').plot(kind="bar", color="lightblue", position=0, width=1.0, rot=0, ec='black')
    plt.tick_params(axis='both', which='major', labelsize=40)
    plt.tick_params(axis='both', which='minor', labelsize=40)
    plt.xlabel('Bin', fontsize=30)
    plt.ylabel('Frequency', fontsize=30)
    plt.title('Genomic Bin Locations with Copy-Number %s Across 38 Normal Patient T5 Cells' % COPY_NUMBER,  fontsize = 50)
    plt.grid(b=True, which='major', color='grey', linestyle='--')
    plt.xticks( rotation = 45)
    plt.yticks(range(0,int(ax_0[0].max())+1))
    rcParams['figure.figsize'] = 100, 50
    plt.locator_params(tight=True, nbins= 100)
    plt.savefig("/Users/ashleymaeconard/Desktop/cn_0_T5_normal_cells.pdf")
    plt.show()

## Small example ##
    # small_ax_0 = df_new[['INDEX', 0]].head()
    # print small_ax_0
    # print list(small_ax_0['INDEX'])

    # small_ax_0.set_index('INDEX').plot(kind="bar", color="lightblue", position=0, width=1.0, rot=0, ec='black')
    # plt.tick_params(axis='both', which='major', labelsize=10)
    # plt.tick_params(axis='both', which='minor', labelsize=10)
    # plt.xlabel('Bin', fontsize=18)
    # plt.ylabel('Frequency', fontsize=18)
    # plt.title('Genomic Bin Locations with Copy-Number 0 Across 38 Normal Patient T5 Cells')
    # #plt.xticks(np.arange(min(ax_0['INDEX']),max(ax_0['INDEX']),100), np.arange(min(ax_0['INDEX']),max(ax_0['INDEX']),100))
    # plt.grid(b=True, which='major', color='grey', linestyle='--')