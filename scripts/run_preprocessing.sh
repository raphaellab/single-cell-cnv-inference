#!/bin/bash
# created by : Ashley Conard 
# last modified : April 5th, 2017

# Usage
if [ ${#@} == 0 ]; then
	#if [ $# -ne 1 ]; then
	    echo $0: Usage: ./run_preprocessing.sh FULL_PATH_DATA_DIR FASTQ_FILE
	        exit 1
	fi

echo "Running Steps 1, 2, 3 preprocessing from fastq to bam to bed file."
echo " "
echo "1. Bowtie for " $2
/research/compbio/software/bowtie2/bin/bowtie2 -x /research/compbio/software/bowtie2/data/hg38 -U $1/$2 -S "$1/$2.sam"
echo "completed $2.sam "
echo "2. Samtools"
samtools view -Sb "$1/$2.sam" -q 20 -o "$1/$2.bam"
echo "completed  $2.bam "
rm -f $2.sam
echo "Sorting and Indexing $1.bam"
samtools sort "$1/$2.bam" "$1/$2.bam.sorted"
samtools index "$1/$2.bam.sorted.bam"
echo "3. BamToBed"
/research/compbio/software/bedtools2/bin/bamToBed -i "$1/$2.bam" > "$1/$2.bed"
echo "done"
