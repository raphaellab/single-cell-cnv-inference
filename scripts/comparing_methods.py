#!/usr/bin/python
#############################
#
# comparing_methods.py: compares aneuFinder and Ginkgo copy-number results and outputs
# created by : Ashley Conard
# last updated: May 15, 2017
#
# e.g. python comparing_methods.py ~/Desktop/compare_methods/test_1965/aneu_1965.bed ~/Desktop/compare_methods/test_1965/ginkgo_1965.csv 50000 tester_50000_chr19_again
#
#############################
from __future__ import division
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import re
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from collections import OrderedDict

##
#  Idea 
#   get Ginkgo pd. Then input and iterate through all aneuFinder and call all from there. 
#   therefore - find df['A'] if aneuFinder has 'A' (cell) etc.
#   NOTE : T5 had issues in AneuFinder claiming that ChrY was too small (below bin size of 220kb). 
#          In this case, the comparison will not compare bins in that region and just stop after ChrX

# FILE POSITIONS (BOTH ANEUFINDER AND GINKGO FOLLOW THIS FORMAT FOR RELEVANT COLUMNS)
CHR_A = 0
START = 1
END = 2
CN_A = 3

def aneuFinder_processing(aneu_file, big_ginkgo_df): # returns 3 part dictionary : [cell:[chrom:[(start,end): CN,...],...]]
  print "CELL\tPERCENT_SIM\tMANH_DIST\tNUM_BINS\tPLOIDY_ANEUF\tPLOIDY_GINK\tPATIENT\n"
  cells_a_dict = c.defaultdict(list) # dict of cells
  current_chrom = 'chr1' # keeping track of all chromosomes in file for each cell
  after_first_cell = 0 # switch for first cell processing or any other

  # Read in .bed
  with gzip.open(aneu_file) as f:
    for line in f:
      #print 'line', line
      # FOR EACH CELL
      if (line.startswith("track")):

        if after_first_cell:
          # Take current cell and compare with Ginkgo IF IN GINKGO DATASET
          # print "after first cell:, call compare_aneu_ginkgo_each_cell for cell", cell_a
          if cell_a in list(big_ginkgo_df):
            one_cell_ginkgo_df=big_ginkgo_df[['CHR', 'START', 'END', cell_a]]
            compare_aneu_ginkgo_each_cell(cells_a_dict, one_cell_ginkgo_df, cell_a) # pass aneufinder dict, cell_a, part big_ginkgo_df

            # Next cell
            cell_a = re.sub('\.bed$', '',(line.split()[4].strip("\""))) # name of the cell
            sys.stderr.write("\nNext AneuFinder cell: "+str(cell_a)+'\n')
            cells_a_dict = c.defaultdict(list) # dict of cells
            cells_a_dict[cell_a] = OrderedDict() # dict for that cell
            cells_a_dict[cell_a][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell

          # IF CELL NOT IN GINKGO DATASET - move to next cell
          else:
            # Next cell
            cell_a = re.sub('\.bed$', '',(line.split()[4].strip("\""))) # name of the cell
            sys.stderr.write("\nNext AneuFinder cell: "+str(cell_a)+'\n')
            cells_a_dict = c.defaultdict(list) # dict of cells
            cells_a_dict[cell_a] = OrderedDict() # dict for that cell
            cells_a_dict[cell_a][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell

        else:
          after_first_cell = 1 # next cell and on will be processed above
          cell_a = re.sub('\.bed$', '',(line.split()[4].strip("\""))) # name of the cell
          
          sys.stderr.write("\nAneuFinder cell: "+str(cell_a)+'\n')

          cells_a_dict[cell_a] = OrderedDict() # dict for that cell
          cells_a_dict[cell_a][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell
      
      # Gather copy-number information for that cell
      elif line.startswith('chr'): # it is a chromosome
        l_s = line.split('\t')
        copy_num_a = int(l_s[CN_A].strip('-somy')) # get chromosome number
        
        if str(current_chrom) != str(l_s[CHR_A]): # new chromosome!
          current_chrom = l_s[CHR_A]
          cells_a_dict[cell_a][l_s[CHR_A]] = OrderedDict() # dict of chromosomes
          cells_a_dict[cell_a][l_s[CHR_A]][(int(l_s[START]),int(l_s[END]))] = copy_num_a

        else:
          cells_a_dict[cell_a][l_s[CHR_A]][(int(l_s[START]),int(l_s[END]))] = copy_num_a # add another interval and copy-number for chrom.
      
      # There is an error in the file
      else:
        sys.exit('ERROR in processing file (lines should start with track or chr...)')

    # Print dict of dict of dicts
    # for key,val in cells_a_dict.items():
    #   print key
    #   for k,v in val.items():
    #     print k
    #     for ks,vs in v.items():
    #      print ks, vs

    else: # process the last cell information
      # Take current cell and compare with Ginkgo
      sys.stderr.write( "Processing last cell:, call compare_aneu_ginkgo_each_cell for cell"+str(cell_a)+'\n')
      
      if cell_a in list(big_ginkgo_df):
        one_cell_ginkgo_df=big_ginkgo_df[['CHR', 'START', 'END', cell_a]]
        compare_aneu_ginkgo_each_cell(cells_a_dict, one_cell_ginkgo_df, cell_a) # pass aneufinder dict, cell_a, part big_ginkgo_df

  #return cells_a_dict, a_end_chrom_list, cell_a

def ginkgo_processing(g_df):  # specific to that cell passed in!
  cells_g_dict = c.defaultdict(list) # dict of cells
  current_chrom = 'chr1' # keeping track of all chromosomes in file for each cell (start with chrom 1)
  g_end_chrom_list = {} # keep track of the end of the chromosome

  # Prepare cell
  cell_g = list(g_df)[3] # For all cells in the file
  sys.stderr.write("Ginkgo cell: "+str(cell_g))
  cells_g_dict[cell_g] = OrderedDict() # dict for that cell
  cells_g_dict[cell_g][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell

  for idx, row in g_df.iterrows():
    # Make a new dictionary for a new chromosome and add copy-number information for that chromosome
    if row['CHR'] != current_chrom:
      current_chrom = row['CHR'] # set new chrom number
      cells_g_dict[cell_g][current_chrom] = OrderedDict() # dict of chromosomes
      cells_g_dict[cell_g][current_chrom][(int(row['START']),int(row['END']))] = row[cell_g] # add new chromosome and new (start,end):cn for that cell

    else: # Add new (start,end):cn dict entry for current chromosome
      cells_g_dict[cell_g][current_chrom][(int(row['START']),int(row['END']))] = row[cell_g] # add another interval and copy-number for chrom.

    # Get the chromsome and the length (given by the end of the last segment)
    for key,val in cells_g_dict.items():
      for k,v in val.items():
        g_end_chrom_list[k] = max([item[1] for item in v.keys()])# chrome and (start and end) 

    # Print dict of dict of dicts
    # for key,val in cells_a_dict.items():
    #   print key
    #   for k,v in val.items():
    #     print k
    #     for ks,vs in v.items():
    #      print ks, vs
    #print g_end_chrom_list

  return cells_g_dict, g_end_chrom_list

def compare_aneu_ginkgo_each_cell(aneu_dict_one_cell, ginkgo_df_one_cell, current_cell):
  a_end_chrom_list = {} # keep track of the size of the chromosomes
  # Get the chromsome and the length (given by the end of the last segment)
  for key,val in aneu_dict_one_cell.items():
    for k,v in val.items():
      a_end_chrom_list[k] = max([item[1] for item in v.keys()])# chrome and (start and end) 

  gink_dict_one_cell, g_end_chrom_list = ginkgo_processing(ginkgo_df_one_cell) # create dictionary of current cell and list of size of chromosomes
  compare_calls(aneu_dict_one_cell, gink_dict_one_cell, a_end_chrom_list, g_end_chrom_list, current_cell) 

def get_aneu(cell_cur_a, chro_cur_a, aneu_dict, start_cur, end_cur): # cell, chrom, aneu_dict, range to check
  for k,v in aneu_dict[cell_cur_a][chro_cur_a].items():
    #print '\n get_aneu ',k[0], start_cur, end_cur, k[1], v
    if (k[0] <= start_cur) and (end_cur <= k[1]): # k[0][--start_cur|--end_cur|--]k[1] INTERVAL IS COMPLETELY INSIDE
      #print 'IN  ', k[0], start_cur, end_cur, k[1], v
      return v
    elif (end_cur <= k[0]):
      #print "interval moved "
      break

  # Overlap with two intervals    
  for k,v in aneu_dict[cell_cur_a][chro_cur_a].items():
    if (k[0] <= start_cur) and (start_cur <= k[1]) and (end_cur >= k[1]): # k[0][--start_cur|--k[1]|--]end_cur  INTERVAL OVERLAPS WITH RIGHT SIDE 
      #print 'RIGHT ', k[0], start_cur, end_cur, k[1], v
      link_prev, link_next, key = aneu_dict[cell_cur_a][chro_cur_a]._OrderedDict__map[k] # http://stackoverflow.com/questions/28035490/in-python-how-can-i-get-the-next-and-previous-keyvalue-of-a-particular-key-in
      #print 'nextKey: ', link_next[2], 'value for next:', aneu_dict[cell_cur_a][chro_cur_a][link_next[2]], "val:", v
      
      # check the two copy-numbers 
      #print "end_cur", end_cur, "start_cur", start_cur, "k", k, "link_next[2]", link_next[2]

      # if the two copy numbers are the same in the two intervals
      if (link_next[2][0] <= end_cur <= link_next[2][1]) and (aneu_dict[cell_cur_a][chro_cur_a][link_next[2]] == v) :
        return v
      # if they are different, take the one which overlaps the majority of the interval
      elif (link_next[2][0] <= end_cur <= link_next[2][1]) and ((end_cur - link_next[2][0])< (k[1]-start_cur)):
        #print "taking v: ", v
        return v
      else:
        #print "taking new value ", aneu_dict[cell_cur_a][chro_cur_a][link_next[2]]
        return aneu_dict[cell_cur_a][chro_cur_a][link_next[2]]

    # SANITY CHECK - THIS SHOULD NOT HAPPEN
    elif (k[0] >= start_cur) and (end_cur <= k[1]): # start_cur[--k[0]|--end_cur|--]k[1]  INTERVAL OVERLAPS WITH LEFT SIDE
      print 'LEFT ', k[0], start_cur, end_cur, k[1], v
      return v
  
  # SANITY CHECK - THIS SHOULD NOT HAPPEN  
  # Did not match any interval or instance of overlap
  print "Did not match any interval: ", k[0], start_cur, end_cur, k[1]
  return 1000 # IF INTERVALS OF X KB ARE NOT FULLY IN INTERVAL FROM METHOD, RETURN LARGE NUMBER

def get_ginkgo(cell_cur_g, chro_cur_g, gin_dict, start_cur, end_cur): # cell, chrom, aneu_dict, range to check
  # Inside one interval  
  for k,v in gin_dict[cell_cur_g][chro_cur_g].items():
    #print "\n get_ginkgo ", k[0], start_cur, end_cur, k[1]
    if (k[0] <= start_cur) and (end_cur <= k[1]): 
      #print 'IN  ', k[0], start_cur, end_cur, k[1], v
      return v
    elif (end_cur <= k[0]):
      #print "interval moved "
      break

  # Overlap with two intervals    
  for k,v in gin_dict[cell_cur_g][chro_cur_g].items():
    if (k[0] <= start_cur) and (start_cur <= k[1]) and (end_cur >= k[1]): # k[0][--start_cur|--k[1]|--]end_cur  INTERVAL OVERLAPS WITH RIGHT SIDE 
     #print 'RIGHT ', k[0], start_cur, end_cur, k[1], v
      link_prev, link_next, key = gin_dict[cell_cur_g][chro_cur_g]._OrderedDict__map[k] # http://stackoverflow.com/questions/28035490/in-python-how-can-i-get-the-next-and-previous-keyvalue-of-a-particular-key-in
      #print 'nextKey: ', link_next[2], 'value for next:', gin_dict[cell_cur_g][chro_cur_g][link_next[2]], "val:", v
      
      # check the two copy-numbers 
      #print "end_cur", end_cur, "start_cur", start_cur, "k", k, "link_next[2]", link_next[2]

      # if the two copy numbers are the same in the two intervals
      if (link_next[2][0] <= end_cur <= link_next[2][1]) and (gin_dict[cell_cur_g][chro_cur_g][link_next[2]] == v) :
        return v
      # if they are different, take the one which overlaps the majority of the interval

      elif (link_next[2][0] <= end_cur <= link_next[2][1]) and ((end_cur - link_next[2][0])< (k[1]-start_cur)):
        #print "taking v: ", v
        return v
      else:
        #print "taking new value ", gin_dict[cell_cur_g][chro_cur_g][link_next[2]]
        return gin_dict[cell_cur_g][chro_cur_g][link_next[2]]

    # SANITY CHECK - THIS SHOULD NOT HAPPEN
    elif (k[0] >= start_cur) and (end_cur <= k[1]): # start_cur[--k[0]|--end_cur|--]k[1]  INTERVAL OVERLAPS WITH LEFT SIDE
      print 'LEFT', k[0], start_cur, end_cur, k[1], v
      return v
  
  # SANITY CHECK - THIS SHOULD NOT HAPPEN
  # Did not match any interval or instance of overlap
  print "Did not match any interval: ", k[0], start_cur, end_cur, k[1]
  return 1000 # IF INTERVALS OF X KB ARE NOT FULLY IN INTERVAL FROM METHOD, RETURN LARGE NUMBER

def compare_calls(a_dict, g_dict, a_chr_end_list, g_chr_end_list, cur_cell):
  mast_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'ANEUF', 'GINKGO')) # initializing a dataframe for current cell
  
  # Iterate through the sorted list of chromosomes to build the profiles for comparision
  sorted_chroms = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY']
  
  # Make sure list of chromosomes are equal
  if ("chrY" not in a_chr_end_list.keys()) or ("chrY" not in g_chr_end_list.keys()):
    sys.stderr.write("Chromosome Y is not present in AneuFinder data. Possibly not present in Ginkgo. Must check.")
    sorted_chroms.remove('chrY')
    
  elif a_chr_end_list.keys()!=g_chr_end_list.keys():
    sys.exit('\nERROR: list of chromosomes are not the same for both methods'+str(a_chr_end_list.keys())+str(g_chr_end_list.keys())+'\n')
  elif a_dict.keys()!=g_dict.keys():
    sys.exit('\nERROR: list of cells are not the same for both methods')

  sys.stderr.write("\nCurrent cell to compare "+ str(a_dict.keys())+"\n")

  # Get windows given KB from user
  idx = -1

  # Iterate through all chromosomes
  for ch in sorted_chroms:
    start_window = 1 # initialize the start for each
    sys.stderr.write('chrom lengths : aneu vs. ginkgo: '+str(a_chr_end_list[ch])+" "+str(g_chr_end_list[ch])+" min: "+str(min(a_chr_end_list[ch], g_chr_end_list[ch]))+"\n")

    for end_window in range(KB, min(a_chr_end_list[ch], g_chr_end_list[ch]), KB):
      idx+=1
      #print "info passed to get_ functions: ", idx, ch, start_window, end_window
      mast_df.loc[idx] = [ch, start_window, end_window, get_aneu(cur_cell, ch, a_dict, start_window, end_window), get_ginkgo(cur_cell, ch, g_dict, start_window, end_window)] #get_aneu(aneu) ,get_ginkgo()] # grab the value in that range
      start_window = end_window+1 # next start position
  
    # Check to see that they are the same length
  if len(mast_df['ANEUF'].tolist()) != len(mast_df['GINKGO'].tolist()):
    sys.error('ERROR: Length of AneuFinder profile and Ginkgo profile are not equal.')

  # Get Percent Similarity and Manhattan 
  manh_dist, count_sim = percent_sim_manh_dist(mast_df['ANEUF'].tolist(), mast_df['GINKGO'].tolist()) # here there are two (AneuF and Ginkgo) profiles for the same cell
  perc_sim = (count_sim/len(mast_df['ANEUF'].tolist())*100)
  
  # Calculate the ploidy calls for each cell for Ginkgo and AneuFinder.
  ploidy_aneuf, ploidy_gink = calculate_ploidy(mast_df['ANEUF'].tolist(), mast_df['GINKGO'].tolist())

  # Outputs to percentSim_manhDist csv file
  print cur_cell,'\t',perc_sim,'\t',manh_dist,'\t', len(mast_df['ANEUF']), '\t', ploidy_aneuf, '\t', ploidy_gink ,'\t', PATIENT
  
  # Output to sderr
  sys.stderr.write("Percent similarity ((+1 for same cn in a bin) / all bins) is "+ str(count_sim)+"/"+str(len(mast_df['ANEUF'].tolist()))+"*100 ="+str(perc_sim)+"%"+'\n') 
  sys.stderr.write("Manhattan distance of: "+str(manh_dist)+" out of sequence length "+str(len(mast_df['ANEUF'].tolist()))+'\n') 
  mast_df.to_csv(OUTPUT_FILE+"_"+cur_cell+'.csv')
  #mast_df.to_csv(OUTPUT_FILE+'.csv')

def percent_sim_manh_dist(x,y):
    counter = 0
    total_cns = 0
    for i,j in zip(x,y):
      if i==j:
        counter+=1
    return sum(abs(a-b) for a,b in zip(x,y)), counter # returns tuple of (manhattan_dist, percent_sim) between inferred copy-number profiles for each cell

def calculate_ploidy(x,y):
  return np.mean(x), np.mean(y)

###########################
# USAGE
if len(sys.argv) != 6:
  sys.stderr.write("Usage: " + sys.argv[0] + " ANEUFINDER_INPUT_BED_FILE  GINKGO_INPUT_TSV_FILE  KB (e.g. 50000)  OUTPUT_FILE_NAME  PATIENT_NAME(e.g. T5)  >  OUTDIR.TSV\n")
  sys.exit(1)
###########################

# ARGUMENTS
ANEU = sys.argv[1]
GINK = sys.argv[2]
KB = int(sys.argv[3])
OUTPUT_FILE = str(sys.argv[4])
PATIENT = sys.argv[5]

if __name__ == '__main__':
  # Read ginkgo .tsv file into df
  gin_df = pd.read_csv(GINK, sep='\t') #print list(g_df)[3], g_df[g_df.columns[3]] # print all after chrom, start, end
  aneuFinder_processing(ANEU, gin_df)

############################
# # Remove unwanted columns
#   df.drop(df.columns[[0,1,2]], axis=1, inplace=True)

# # turn into np.array
#   df_nparray = df.as_matrix()
#   #print "Total Number of Cells: ", len(list(df)), " shape: ", df_nparray.shape

# # sort np.array
#   df_nparray_s = df_nparray[df_nparray[:,1].argsort()]

# # set up colormap
#   vmax = 6.0 #float(np.amax(df_nparray_s))
#   print "Max set to : ", vmax
#   cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'), (2 / vmax, 'white'), (vmax / vmax, 'red')])

#   fig, ax = plt.subplots(1)
#   #fig.set_size_inches(30, 20)

# # plot using sns.heatmap
#   if CLUSTERMAP == 1:
#     im = sns.clustermap(df_nparray_s.transpose(), col_cluster=False, robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=0, vmax=5)
#   else:
#     im = sns.heatmap(df_nparray_s.transpose(), robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)
  
# # specific to plot
#   ax.set(frame_on=False, xticks=[], yticks=[])
#   plt.yticks(rotation='horizontal')
  
#   # when saving, specify the DPI
#   plt.savefig(OUTPUT_FILE, dpi = 200)
#   #ax.invert_yaxis()


########### If Needed to work in pandas df space - see what I have already tried, below ############
#print df.ix[1,1]
#print "before", list(df)

#df_nparray_s = df.sort()

#a = [[2,2,2,1,1,1,2],[4,4,3,2,1,2,2],[1,1,1,1,1,1,1],[4,3,2,1,2,3,3],[1,1,1,1,1,1,1],[4,4,3,2,1,2,2]]
#y=np.array([np.array(xi) for xi in a])
#df_nparray_s=y[y[:,1].argsort()]

#im = ax.pcolor(df, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')   
#fig = plt.gcf() # get current figure

#im = imshow(df_nparray_s.transpose(), cmap=cmap, interpolation='nearest')#, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)

#im.set_yticks([])
#im.set_xticks([])
#cax = plt.gcf().axes[-1]
#cax.tick_params(labelsize=20)
#confused = ax.pcolor(im, cmap=cmap)
#cbar = fig.colorbar(im)#, cax=cax) #colorbar(im[4], cax=cax)
#cbar.set_ticks(range(7)) # Integer colorbar tick locations
#ax.set(frame_on=False, aspect=1, xticks=[], yticks=[])

#cbar = ax.collections[0].colorbar
#cbar.set_ticks([0,1,2,3,4,5])

#cbar.set_ticks(range(4)) # Integer colorbar tick locations

# cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'),
#                                                     (1 / vmax, 'white'),
#                                                     (3 / vmax, 'red')]
#                                       )

#fig, ax = plt.subplots()
#im = ax.pcolor(data, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')
#cbar = fig.colorbar(im)

