#!/usr/bin/python
import sys
import csv
import collections as c
from collections import Counter
import itertools
import numpy as np
from scipy import stats
import plotly.plotly as py
import plotly.graph_objs as go
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plot
import seaborn as sns
import operator

# Ashley Mae
# Created Aug 25, 2016
#####
#
# plot_cn.py: copy number plots for each cell in ginkgo's output "copy number" file
#
#####

# BIOMART INTERVAL FILE
CHROM = 1
GENE_START = 2
GENE_END = 3
STRAND = 4
GC_BIAS = 5
HGNC = 6

def open_copy_number_tsv(cn_tsv_file):
	table = pd.read_csv(cn_tsv_file, sep='\t')
	header = list(table.columns.values)

	#print "Number of Cells: ", (len(list(table.columns.values))-4) # start counting at 1 and remove chrom, start, end info

	# for every cell in dataset
	for index, i in enumerate(list(table.columns.values)):
		
		# Get interval values
		if index == 0:
			interval_vals = table[i].tolist() # create list of column

		# Get copy number information for each cell
		# elif index >= 4:
		# 	copy_number_cell_i = table[i].tolist()
		# 	plot.scatter(interval_vals, copy_number_cell_i)#, c={2: "red", 3: "green", 1: "blue"})
		# 	plot.title("Copy Number Profile : Cell " + str(header[index]) )
		# 	plot.xlabel("Chromosome Position bins: ~ 50,000 bp")
		# 	plot.ylabel("Copy Number")
		# 	plot.show()
	return table, header

def open_gene_interval_list(gene_intr_file):
	genes = c.defaultdict(list)
	genes_indexing = {}
	iterator = -1

	with open(gene_intr_file) as f:
	        header = next(f)
        	for line in f:
        		iterator+=1
            		s = line.rstrip("\r\n").split(',')
            		e = [a.replace(' ','') for a in s]
            		genes[e[HGNC]] = ([e[GENE_START], e[GENE_END], e[CHROM]])
            		genes_indexing[e[HGNC]] = iterator
	return genes, genes_indexing

def neg_one_listmaker(n): # fill integer copy number matrix initially with -1
    neg_one = [-1] * n
    return neg_one

def cn_integer_matrix(table_cn, header_cn_table, gene_interval_dict, gene_indexed):
	int_cn_matrix = c.defaultdict(list)
	sorted_x = sorted(gene_indexed.items(), key=operator.itemgetter(1))

	for ind,i in enumerate(header_cn_table): # indexing and iterating through each cell
		if ind >= 4: # only go through the cells (skipping : #, CHR, START, END)
			#print "\n"
			#print "Cell: ", i
			#print "\n"

			# Initialize integer matrix
			int_cn_matrix[i] = neg_one_listmaker(len(gene_indexed)) # start counting at 1 and remove chrom, start, end info)
			
			for index,row in table_cn.iterrows(): # for each interval
				#print row['CHR'], row['START'], row['END'], row[i]
				for k,v in gene_interval_dict.items(): # for each gene
					#print k#, int(v[0]), int(v[1]), v[2]

					# Definitions
					g_start= int(v[0])
					g_end= int(v[1])
					interval_start = row['START']
					interval_end = row['END']
					interval_chromo = str(row['CHR'])
					g_chromo = ('chr'+v[2])

					# If the gene is completely within the copy number bp interval	
					if ( interval_start <= g_start <= g_end <= interval_end) and interval_chromo == g_chromo:
						#print k, "yes", row['START'], int(v[0]), int(v[1]), row['END'], row[i]
						int_cn_matrix[i][gene_indexed[k]] = (row[i]) # int_cn_matrix[ cell ][ gene ], gene is given by the gene_index dictionary (based on input)
			
					# If the gene spans an interval and belongs to the left-most
					# elif (interval_start <= g_start) and not(interval_end <= g_end):
					# 	overlap = (interval_end - g_start) / (g_end- g_start)
					# 	if overlap >= 0.5:
					# 		int_cn_matrix[i][gene_indexed[k]] = row[i] # int_cn_matrix[ cell ][ gene ], gene is given by the gene_index dictionary (based on input)

					# # If the gene spans an interval and belongs to the right-most
					# elif not (interval_start <= g_start) and (interval_end <= g_end):
					# 	overlap = (g_end - interval_start) / (g_end - g_start)
					# 	if overlap >= 0.5:
					# 		int_cn_matrix[i][gene_indexed[k]] = row[i] # int_cn_matrix[ cell ][ gene ], gene is given by the gene_index dictionary (based on input)

	return int_cn_matrix

# USAGE
if len(sys.argv) != 3:
    sys.stderr.write("Usage: " + sys.argv[0] + "<COPY_NUMBER_FILE> <GENE_INTERVAL_FILE>")
    sys.exit(1)

# ASSIGNMENT
CN_FILE = sys.argv[1]
GENE_INTERVAL_FILE = sys.argv[2]

##################################################################################
# METHOD CALLS

gene_intervals, gene_index = open_gene_interval_list(GENE_INTERVAL_FILE)
cn_table, cn_header = open_copy_number_tsv(CN_FILE)
copy_number_integer_matrix = cn_integer_matrix(cn_table,cn_header,gene_intervals, gene_index)

# PRINTING
sorted_list = gene_index.keys()
sorted_list.sort()
print "CELL_NAME",",", sorted_list
#print "CELL_NAME",",",'MUTYH',",",'TEX38',",",'DUSP12',",",'DNM3',",",'PLXNA2',",",'TRIM58',",",'TRIB2',",",'BTLA',",",'ROPN1B',",",'PIK3CA',",",'LSG1',",",'CASP3',",",'MARCH11',",",'PRDM9',",",'FBN2',",",'PANK3',",",'TCP11',",",'CALD1',",",'FAM214B',",",'FUBP3',",",'PITRM1',",",'FGFR2',",",'CABP2',",",'FCHSD2',",",'TECTA',",",'PPP2R5E',",",'KNSTRN',",",'SEC11A',",",'ITGAD',",",'CFAP52',",",'CXXC1',",",'ADGRG2',",",'DCAF8L1'#,gene_index.keys()
for key,value in copy_number_integer_matrix.items():
	print key ,",", value #['\t'.join(v) for v in value]


##################################################################################
