def plot_readDepth(chrm,start,end):
	reads = pysam.AlignmentFile("/data/compbio/projects/10x_SVs/data/Chromium/NA12878/NA12878_WGS_phased_possorted_bam.bam","rb")
	skip = 1000
	x = []
	y = []
	for i in range(start,end,skip):
		depth = len(list(reads.fetch(chrm,i,i+skip)))/float(skip)*100 #
		print i,i+skip,depth
		#print list(reads.fetch(chrm,i,i+skip))[0],depth
		x.append(i)
		y.append(depth)
	f, ax = plt.subplots()
	ax.plot(x,y)
	ax.set_title('%s %s-%s'%(chrm,str(start),str(end)))
	ax.set_ylim(0,100)
	text = 'data/plots/%s_%s_%s.png'%(chrm,str(start),str(end))
	f.savefig(text,dpi=300, format='png',bbox_inches='tight')