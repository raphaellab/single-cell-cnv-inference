#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import pysam as pys
from subprocess import Popen, PIPE
import pandas as pd
from time import sleep
from multiprocessing import Pool
from multiprocessing import Process
from random import randint

# Ashley Mae
# Last Modified Sept 6, 2017
#####
#
# get_total_reads_n_coverage.py
#
####

def bam_read_count(dir_cell_bam):
    """ Return a tuple of the number of mapped and unmapped reads in a bam file """
    p = Popen(['samtools', 'idxstats', dir_cell_bam], stdout=PIPE)
    ps = Popen(['samtools', 'flagstat', dir_cell_bam], stdout=PIPE)
    print "Stats for ", dir_cell_bam, "\n", ps
    for i in ps.stdout:
	print i
    mapped = 0
    unmapped = 0
    for line in p.stdout:
	rname, rlen, nm, nu = line.rstrip().split()
        mapped += int(nm)
        unmapped += int(nu)
    print "total unmapped reads: ", unmapped
    print  "total mapped reads: ",  mapped


# USAGE
if len(sys.argv)!=2:
   sys.stderr.write("Usage: python "+ sys.argv[0] + "DATA_DIR/CELL_BAM_FILE.bam")

# ARGUMENTS
DATA_DIR_CELL = sys.argv[1]

if __name__ == '__main__':
   bam_read_count(DATA_DIR_CELL)
