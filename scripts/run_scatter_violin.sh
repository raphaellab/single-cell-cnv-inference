#!/bin/bash
# @Author: ashleymaeconard
# @Date:   2017-01-28 16:23:49
# @Last Modified by:   ashleymaeconard
# @Last Modified time: 2017-01-29 09:37:09


display_usage() { 
	echo "Please provide arguments 1.Folder of all #m-#k-#a-#n.txt  2. # cells 3. # clones 4. # bins" 
	#echo -e "\nUsage:\n__g5_token588d3785655ad [arguments] \n" 
	} 

# if less than two arguments supplied, display usage 
if [  $# -le 1 ] 
then 
	display_usage
	exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ( $# == "--help") ||  $# == "-h" ]] 
then 
	display_usage
	exit 0
fi 

# Enter into folder and begin
cd $1
# Create output directory
if [ -e output_files ]
then
	echo "output_files exists"
	exit 0
fi

mkdir output_files

for f in $2*.txt;
do
	filename=$f
	echo "file:" $f
	python /Users/ashleymaeconard/Desktop/FB_algo/scripts/create_scatter.py "$f" "output_files/${f}_output" 2> "output_files/${f}_violin.txt"
done

cd output_files
cat *_violin.txt >> all_$2_$3_$4.txt 
#rm *_violin.txt

python /Users/ashleymaeconard/Desktop/FB_algo/scripts/make_violin.py all_$2_$3_$4.txt $2 $3 $4


