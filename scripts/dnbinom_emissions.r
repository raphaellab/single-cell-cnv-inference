# dnbinom from exomeCopy Michael Love 2016

options(echo=FALSE) # Change to TRUE if you want to see commands in the output file
args <- commandArgs(trailingOnly = TRUE) # trailingOnly=TRUE means that only your arguments are returned, check:
#print(args) # print(commandsArgs(trailingOnly=FALSE))

#arg[1] is the read depth r_i
#arg[2] is mu value (nbCNV is 28.3)
#arg[3] is overdispersion value (nbCNV is 11.29)

a <- dnbinom(as.numeric(args[1]), mu = as.numeric(args[2]) , size = as.numeric(args[3])) #(as.numeric(args[2])/(as.numeric(args[3])-1)))
cat(a)
quit(save = "no", status = 0, runLast = FALSE)
#11111111