#!/usr/bin/python
from __future__ import division
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import sys,os,itertools, argparse
import matplotlib.pyplot as plt 
import plotly.plotly as py
import collections as c
import numpy as np
import pysam as pys

# Ashley Mae
# Created Oct 3, 2016
#####
#
# PCA_all_cells.py: creates X matrix from individual cells and performs PCA
# reference: http://sebastianraschka.com/Articles/2014_pca_step_by_step.html#sc_matrix
# why doesnt work: http://stats.stackexchange.com/questions/28909/pca-when-the-dimensionality-is-greater-than-the-number-of-samples
#####

# ASSIGNMENTS
CHR = 0
START_BIN = 1
END_BIN = 2
DEPTH = 3

def add_to_X_matrix(x_l, file):
	bin_limit = 414 # limit to number of bins (n)
	cell_list = [] # create list of cells to be added
	iterator = 1

	with open(file) as f:
		for line in f:
			if iterator <= bin_limit:
				s = line.rstrip("\r\n").split(' ')
				e = [a.replace(' ','') for a in s]
				cell_list.append(float(e[DEPTH]))
				iterator+=1

	x_l.append(cell_list)
	return x_l

def plot_3D(xMatrix,nCells):
	print xMatrix
	fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111, projection='3d')
	plt.rcParams['legend.fontsize'] = 10   
	ax.plot(xMatrix[0,:], xMatrix[1,:], xMatrix[2,:], 'o', markersize=8, color='blue', alpha=0.5)
	plt.title('Samples from '+str(nCells)+' cells')
	ax.legend(loc='upper right')

	plt.show()

def d_dim_mean_vector(xMatrix):
	mean_x = np.mean(xMatrix[0,:])
	mean_y = np.mean(xMatrix[1,:])
	mean_z = np.mean(xMatrix[2,:])
	mean_vector = np.array([[mean_x],[mean_y],[mean_z]])
	print 'Mean Vector:','\n', mean_vector
	return mean_vector, mean_x, mean_y, mean_z

def create_scatter_matrix(xMatrix, m_vec):
	scatter_matrix = np.zeros((NUM_FILES-2,NUM_FILES-2))
	print "scat: ", scatter_matrix

	for i in range(xMatrix.shape[1]):
		scatter_matrix += (xMatrix[:,i].reshape(NUM_FILES-2,1) - m_vec).dot((xMatrix[:,i].reshape(NUM_FILES-2,1) - m_vec).T)
	
	print('Scatter Matrix:','\n', scatter_matrix)
	return scatter_matrix

def eigens(scat_matrix):
	# eigenvectors and eigenvalues for the from the scatter matrix
	eig_val_sc, eig_vec_sc = np.linalg.eig(scat_matrix)
	
	# for loop over all eigen values 
	for i in range(len(eig_val_sc)):
	    eigvec_sc = eig_vec_sc[:,i].reshape(1,NUM_FILES-2).T

	    print('Eigenvector {}: \n{}'.format(i+1, eigvec_sc))
	    print('Eigenvalue {} from scatter matrix: {}'.format(i+1, eig_val_sc[i]))

    	return eig_val_sc, eig_vec_sc, eigvec_sc

def check_eigenvector_eigenvalue_calc(e_val_sc, e_vec_sc, scat_matrix):
	for i in range(len(e_val_sc)):
	    eigv = e_vec_sc[:,i].reshape(1,NUM_FILES-2).T
	    np.testing.assert_array_almost_equal(scat_matrix.dot(eigv), e_val_sc[i] * eigv, decimal=6, err_msg='', verbose=True)

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

def visual_eigens(xMatrix, scat_matrix, e_vec_sc, x_m, y_m, z_m):
	fig = plt.figure(figsize=(7,7))
	ax = fig.add_subplot(111, projection='3d')
	ax.plot(xMatrix[0,:], xMatrix[1,:], xMatrix[2,:], 'o', markersize=8, color='green', alpha=0.2)
	ax.plot([x_m], [y_m], [z_m], 'o', markersize=10, color='red', alpha=0.5)
	for v in e_vec_sc.T:
	    a = Arrow3D([x_m, v[0]], [y_m, v[1]], [z_m, v[2]], mutation_scale=20, lw=3, arrowstyle="-|>", color="r")
	    ax.add_artist(a)
	ax.set_xlabel('x_cell_values')
	ax.set_ylabel('y_cell_values')
	ax.set_zlabel('z_cell_values')

	plt.title('Eigenvectors')

	plt.show()

def sorting(e_vec_sc, e_val_sc):
	for ev in e_vec_sc:
    		np.testing.assert_array_almost_equal(1.0, np.linalg.norm(ev))

	# Make a list of (eigenvalue, eigenvector) tuples
	e_pairs = [(np.abs(e_val_sc[i]), e_vec_sc[:,i]) for i in range(len(e_val_sc))]

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	e_pairs.sort(key=lambda x: x[0], reverse=True)

	# Visually confirm that the list is correctly sorted by decreasing eigenvalues
	for i in e_pairs:
	    print(i[0])
	return e_pairs

def choose_k_eigenvectors_with_largest_eigenvalues(e_pairs):
	matrix_w = np.hstack((e_pairs[0][1].reshape(NUM_FILES-2,1), e_pairs[1][1].reshape(NUM_FILES-2,1)))
	print('Matrix W:','\n', matrix_w)
	return matrix_w

def transformed_samples(xMatrix, mat_w):
	transformed = mat_w.T.dot(xMatrix)
	#assert transformed.shape == (2,40), "The matrix is not 2x40 dimensional."
	plt.plot(transformed[0,0:414], transformed[1,0:414], 'o', markersize=7, color='blue', alpha=0.5, label='3 cells')
	plt.xlim([-4,4])
	plt.ylim([-4,4])
	plt.xlabel('x_values')
	plt.ylabel('y_values')
	plt.legend()
	plt.title('Transformed samples with class labels')

	plt.show()


######################################
# USAGE
if type(len(sys.argv[1])) != int:
    sys.stderr.write("Usage: " + sys.argv[0] + " <NUM_FILES> <data_binned_RD_SRR308173... file>\n")
    sys.exit(1)

######################################
# ASSIGNMENT
NUM_FILES = int(sys.argv[1])
x_list = []
num_cells = 0

######################################
# METHODS

# Iterate through cell files
for i in range(2,NUM_FILES): # starting with 2nd argument
    num_cells+=1
    print "cell (file): ", sys.argv[i]
    x_list = add_to_X_matrix(x_list, sys.argv[i])
print "done"

# Created starting matrix X
x_matrix = np.array((x_list))
#print x_matrix

# Plot 3D
plot_3D(x_matrix, num_cells) 

# Computing d-dimensional mean vector
mean_vec, m_x, m_y, m_z = d_dim_mean_vector(x_matrix)

# Computing Scatter Matrix=
scat_mat = create_scatter_matrix(x_matrix, mean_vec)

# Computing eigenvectors and eigenvalues
eig_value_sc, eig_vector_sc, eigvector_sc = eigens(scat_mat)

# Checking eigenvector calculation
check_eigenvector_eigenvalue_calc(eig_value_sc, eig_vector_sc, scat_mat)

# Visualizing eigenvectors
visual_eigens(x_matrix, scat_mat, eig_vector_sc, m_x, m_y, m_z)

# Sorting
eig_pairs = sorting(eig_vector_sc, eig_value_sc)

# Choosing the k eigenvectors with the largest eigenvalues
w_matrix = choose_k_eigenvectors_with_largest_eigenvalues(eig_pairs)

# Transforming samples onto the new subspace
transformed_samples(x_matrix, w_matrix)

