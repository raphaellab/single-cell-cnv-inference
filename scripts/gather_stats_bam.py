#!/usr/bin/python
#
# gather_stats_bam.py : Collect basic statistics from .bam files
#                                 
# Ashley Conard

def coverage(samfile):
    cov = []
    for col in samfile.pileup("CHROM"):
        cov.append(col.n)
    return [samfile.mapped, samfile.unmapped, len(cov), np.median(cov), cov]

def get_stats(alignments):
    coverage_stats = []
    for alignment in alignments:
        coverage_stats.append(coverage(alignment[0]))
        
    data = {}
    data['cell'] = []
    data['breadth'] = []
    data['median_cov'] = []
    data['reads'] = []
    data['mapped_reads'] = []
    for (i,alignment) in enumerate(alignments):
        data['cell'].append(alignment[1].rstrip(".sorted.bam")[-3:])
        data['breadth'].append(coverage_stats[i][2] / float(genome_length) * 100)
        data['median_cov'].append(coverage_stats[i][3])
        data['reads'].append(coverage_stats[i][0] + coverage_stats[i][1])
        data['mapped_reads'].append(coverage_stats[i][0] / float(coverage_stats[i][0] + coverage_stats[i][1]) * 100)
        
    return coverage_stats, pd.DataFrame(data=data)

genome_length = 3088286401 # DIPLOID - 6,176,572,802 ? HAPLOID - 3088286401?

alignments = []
for bam_file in glob.glob("*.bam"):
    alignments.append((pysam.AlignmentFile(bam_file, "rb"),bam_file))
    
stats, df = get_stats(alignments)
df

