#!/bin/sh
# Iterate through all bin sizes to produce AneuFinder results
# Ashley Conard
# Last Modified: Nov. 8 2017

if [ ${#@} -lt 5 ]; then
        echo ERROR- Usage: ./aneuFinder_run_difBinSizes.sh  NUM_CPUS  FULL_PATH_TO_DATA  FULL_PATH_TO_OUTPUT_FOLDER  END_OUTPUT_FOLDER_NAME  "'"BIN_SIZE1 BIN_SIZE2 ..."'" FULL_PATH_TO_REF_AND_REF
        exit 1
fi
parallel -j $1 Rscript /n/fs/ragr-data/projects/scDNA/scripts/aneuFinder_scripts/aneuFinder_sims_setRef.r $2 $3/{1}_$4 {1} $6 ::: $5
