# Ashley Conard
# rsvsim_genome - simulates structural variation events in the genome (hg19 reference)
# Last update : June 2nd
# Fantastic Tutorial for RSVSim : https://rdrr.io/bioc/RSVSim/man/simulateSV.html#heading-8

# To install
#source("https://bioconductor.org/biocLite.R")
#biocLite("RSVSim")
#biocLite("BSgenome.Hsapiens.UCSC.hg19") # Default genome is hg19

# Libraries
library(RSVSim)
library("BSgenome.Hsapiens.UCSC.hg19")
library("BSgenome.Hsapiens.UCSC.hg19.masked")

# size of events (drawn from beta with estimateSVSizes)
size_del = estimateSVSizes(n=60, minSize=1000, maxSize=1000000, hist=TRUE, default="deletions")
size_dup = estimateSVSizes(n=40, minSize=10000, maxSize=1000000, hist=TRUE, default='insertions')
size_ins = estimateSVSizes(n=50, minSize=10000, maxSize=1000000, hist=TRUE, default="insertions")

# deletions (drawn from beta), insertions (drawn from beta), duplication (drawn from beta)
sim = simulateSV(output=".", dels=60, ins=50, dups=40, sizeDels=size_del, sizeIns=size_ins, sizeDups=size_ins)# percCopiedIns=0.5)
#sim
metadata(sim)

## Setting the weights for SV formation mechanism and repeat biases demands a given data.frame structure
## The following weights are the default settings
## Please make sure your data.frames have the same row and column names, when setting your own weights
data(weightsMechanisms, package="RSVSim")
weightsMechanisms
data(weightsRepeats, package="RSVSim")
