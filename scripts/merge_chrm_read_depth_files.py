#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from subprocess import call
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import pysam as pys
from subprocess import Popen, PIPE
import pandas as pd
from time import sleep
from multiprocessing import Pool
from multiprocessing import Process
from random import randint

# Ashley Mae
# Created Aug 31, 2017
#####s
# merge_chrm_read_depth_files.py: merges all chromosome read depth files
#####

# USAGE
if len(sys.argv) != 6:
    sys.stderr.write("Usage: " + sys.argv[0] + " <DATA_DIR>  <CELL_NAME (e.g. cell_1 as it appears from read_depth_plots.py>  <PATTERN 1 TO LOOK FOR BEFORE CHRM (e.g. _ )> <PATTERN 2 TO LOOK FOR AFTER CHRM (e.g. _ )> <OUTPUT_DIR>\n")
    sys.exit(1)

# ARGUMENTS
DATA_DIR = sys.argv[1]
CELL = sys.argv[2]
PATTERN = sys.argv[3] # pattern to look for to detect chrom
PATTERN2 = sys.argv[4]
OUTPUT_DIR = sys.argv[5]

if __name__ == '__main__':
    # Create list of all .bam files to transform into vectors of normalized read counts
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and CELL in f] # Merge chrom read depth files for one cell at a time.
    print "\nMerging: ", onlyfiles
    CHROM = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','X']
    
    for chrm in CHROM:
	chrmo = PATTERN+chrm+PATTERN2	
        print "chrom to search: ", chrmo
         
        if chrm == '1':
            print "Chrom is 1", chrm
            #file_to_merge = [s for s in onlyfiles if ("_"+chrm+"chrm_") in s][0]
            file_to_merge = [s for s in onlyfiles if chrmo in s] 
            print file_to_merge
            if len(file_to_merge) > 1:
            	sys.exit("There are too many files matching the pattern for the chromosome", file_to_merge)
            df = pd.read_csv(DATA_DIR+"/"+file_to_merge[0])

        elif chrm in CHROM:
            print "Chrom: ", chrm
            file_to_merge = [s for s in onlyfiles if chrmo in s]
	    print file_to_merge
	    #file_to_merge = [s for s in onlyfiles if (chrm+"chrm_") in s][0]
            if len(file_to_merge) > 1:
		sys.exit("There are too many files matching the pattern for the chromosome", file_to_merge)
	    elif len(file_to_merge) < 1:
		print "Chr", chrm, "has no files: ", file_to_merge
	    else: 
	    	df2 = pd.read_csv(DATA_DIR+"/"+file_to_merge[0])
            	df = df.append(df2)
	
	else:
	    print "ERROR -", chrmo," NOT PRESENT IN DATA."
   
    print "merged dataframes for ",len(CHROM) ," chromosomes. (NOT Y)."
    df.to_csv(OUTPUT_DIR+"/"+CELL+"__merged_chrm_read_depth_files.csv")
    #print "listDIR", listdir(OUTPUT_DIR)
    
    files_merged = [f for f in listdir(OUTPUT_DIR) if isfile(join(OUTPUT_DIR, f)) and 'merged_chrm_read_depth_files.csv' in f and CELL in f] 
    print "\nOutput merged files: ", files_merged
    print "File(s) have these sizes: "
    os.system("for i in *merged_chrm_read_depth_files.csv; do echo $i; cat $i | wc; done")   
    print "\nChrom files merged: ", onlyfiles
    #delete_or_not = raw_input("\nShould I erase the chromosome files for this/these sample(s)? : ('yes' or 'no')")
    #if delete_or_not == 'yes':
    for i in onlyfiles:
    	os.remove(DATA_DIR+"/"+i)
