#!/usr/bin/python
#############################
#
# comparing_methods.py: compares aneuFinder and Ginkgo copy-number results 
# created by : Ashley Conard
# last updated: May 15, 2017
#
# e.g. python comparing_methods.py ~/Desktop/compare_methods/test_1965/aneu_1965.bed ~/Desktop/compare_methods/test_1965/ginkgo_1965.csv 50000 tester_50000_chr19_again
#
#############################
from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from collections import OrderedDict

# FILE POSITIONS (BOTH ANEUFINDER AND GINKGO FOLLOW THIS FORMAT FOR RELEVANT COLUMNS)
CHR_A = 0
START = 1
END = 2
CN_A = 3

def find(key_element):
    return [value for key, value in a.iteritems() if len(key) > 1 and key[1] == key_element]

def aneuFinder_processing(aneu_file): # returns 3 part dictionary : [cell:[chrom:[(start,end): CN,...],...]]
  cells_a_dict = c.defaultdict(list) # dict of cells
  current_chrom = 'chr1' # keeping track of all chromosomes in file for each cell
  a_end_chrom_list = {} # keep track of the end of the chromosome

  # Read in .bed
  with open(aneu_file) as f:
    for line in f:
      # Prepare a new cell
      if line.startswith("track"):
        cell_a = line.split()[4].strip("\"") # name of the cell
        cells_a_dict[cell_a] = OrderedDict() # dict for that cell
        cells_a_dict[cell_a][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell
      
      # Gather copy-number information for that cell
      elif line.startswith('chr'):
        l_s = line.split('\t')
        copy_num_a = int(l_s[CN_A].strip('-somy')) # get chromosome number
        
        if str(current_chrom) != str(l_s[CHR_A]): # new chromosome!
          current_chrom = l_s[CHR_A]
          cells_a_dict[cell_a][l_s[CHR_A]] = OrderedDict() # dict of chromosomes
          cells_a_dict[cell_a][l_s[CHR_A]][(int(l_s[START]),int(l_s[END]))] = copy_num_a
        
        else:
          cells_a_dict[cell_a][l_s[CHR_A]][(int(l_s[START]),int(l_s[END]))] = copy_num_a # add another interval and copy-number for chrom.
      
      # There is an error
      else:
        sys.exit('ERROR in processing file (lines should start with track or chr...)')
    
    # Get the chromsome and the length (given by the end of the last segment)
    for key,val in cells_a_dict.items():
      for k,v in val.items():
        a_end_chrom_list[k] = max([item[1] for item in v.keys()])# chrome and (start and end) 

    # Print dict of dict of dicts
    # for key,val in cells_a_dict.items():
    #   print key
    #   for k,v in val.items():
    #     print k
    #     for ks,vs in v.items():
    #      print ks, vs

  return cells_a_dict, a_end_chrom_list

def ginkgo_processing(gink_file):  
  cells_g_dict = c.defaultdict(list) # dict of cells
  current_chrom = 'chr1' # keeping track of all chromosomes in file for each cell (start with chrom 1)
  g_end_chrom_list = {} # keep track of the end of the chromosome

  # Read in .tsv into df
  g_df = pd.read_csv(gink_file, sep='\t') #print list(g_df)[3], g_df[g_df.columns[3]] # print all after chrom, start, end

  # Prepare a new cell
  for cell_g in list(g_df)[3:]: # For all cells in the file
    cell_g_bed = cell_g+'.bed'
    cells_g_dict[cell_g_bed] = OrderedDict() # dict for that cell
    cells_g_dict[cell_g_bed][current_chrom] = OrderedDict() # set up 'chr1' for first chromosome in cell

    for idx, row in g_df.iterrows():      
      # Make a new dictionary for a new chromosome and add copy-number information for that chromosome
      if row['CHR'] != current_chrom:
        current_chrom = row['CHR'] # set new chrom number
        cells_g_dict[cell_g_bed][current_chrom] = OrderedDict() # dict of chromosomes
        cells_g_dict[cell_g_bed][current_chrom][(int(row['START']),int(row['END']))] = row[cell_g] # add new chromosome and new (start,end):cn for that cell

      else: # Add new (start,end):cn dict entry for current chromosome
        cells_g_dict[cell_g_bed][current_chrom][(int(row['START']),int(row['END']))] = row[cell_g] # add another interval and copy-number for chrom.

    # Get the chromsome and the length (given by the end of the last segment)
    for key,val in cells_g_dict.items():
      for k,v in val.items():
        g_end_chrom_list[k] = max([item[1] for item in v.keys()])# chrome and (start and end) 

    # Print dict of dict of dicts
    # for key,val in cells_a_dict.items():
    #   print key
    #   for k,v in val.items():
    #     print k
    #     for ks,vs in v.items():
    #      print ks, vs
    #print g_end_chrom_list

  return cells_g_dict, g_end_chrom_list

def get_aneu(cell_cur_a, chro_cur_a, aneu_dict, start_cur, end_cur): # cell, chrom, aneu_dict, range to check
  for k,v in aneu_dict[cell_cur_a][chro_cur_a].items():
    #print '\n get_aneu ',k[0], start_cur, end_cur, k[1], v
    if (k[0] <= start_cur) and (end_cur <= k[1]): # k[0][--start_cur|--end_cur|--]k[1] INTERVAL IS COMPLETELY INSIDE
      #print 'IN  ', k[0], start_cur, end_cur, k[1], v
      return v
    elif (end_cur <= k[0]):
      #print "interval moved "
      break

  # Overlap with two intervals    
  for k,v in aneu_dict[cell_cur_a][chro_cur_a].items():
    if (k[0] <= start_cur) and (start_cur <= k[1]) and (end_cur >= k[1]): # k[0][--start_cur|--k[1]|--]end_cur  INTERVAL OVERLAPS WITH RIGHT SIDE 
      #print 'RIGHT ', k[0], start_cur, end_cur, k[1], v
      link_prev, link_next, key = aneu_dict[cell_cur_a][chro_cur_a]._OrderedDict__map[k] # http://stackoverflow.com/questions/28035490/in-python-how-can-i-get-the-next-and-previous-keyvalue-of-a-particular-key-in
      #print 'nextKey: ', link_next[2], 'value for next:', aneu_dict[cell_cur_a][chro_cur_a][link_next[2]], "val:", v
      
      # check the two copy-numbers 
      #print "end_cur", end_cur, "start_cur", start_cur, "k", k, "link_next[2]", link_next[2]

      # if the two copy numbers are the same in the two intervals
      if (link_next[2][0] <= end_cur <= link_next[2][1]) and (aneu_dict[cell_cur_a][chro_cur_a][link_next[2]] == v) :
        return v
      # if they are different, take the one which overlaps the majority of the interval
      elif (link_next[2][0] <= end_cur <= link_next[2][1]) and ((end_cur - link_next[2][0])< (k[1]-start_cur)):
        #print "taking v: ", v
        return v
      else:
        #print "taking new value ", aneu_dict[cell_cur_a][chro_cur_a][link_next[2]]
        return aneu_dict[cell_cur_a][chro_cur_a][link_next[2]]

    elif (k[0] >= start_cur) and (end_cur <= k[1]): # start_cur[--k[0]|--end_cur|--]k[1]  INTERVAL OVERLAPS WITH LEFT SIDE
      print 'LEFT ', k[0], start_cur, end_cur, k[1], v
      return v
    
  # Did not match any interval or instance of overlap
  #print "Did not match any interval: ", k[0], start_cur, end_cur, k[1]
  return 1000 # IF INTERVALS OF X KB ARE NOT FULLY IN INTERVAL FROM METHOD, RETURN LARGE NUMBER

def get_ginkgo(cell_cur_g, chro_cur_g, gin_dict, start_cur, end_cur): # cell, chrom, aneu_dict, range to check
  # Inside one interval  
  for k,v in gin_dict[cell_cur_g][chro_cur_g].items():
    #print "\n get_ginkgo ", k[0], start_cur, end_cur, k[1]
    if (k[0] <= start_cur) and (end_cur <= k[1]): 
      #print 'IN  ', k[0], start_cur, end_cur, k[1], v
      return v
    elif (end_cur <= k[0]):
      #print "interval moved "
      break

  # Overlap with two intervals    
  for k,v in gin_dict[cell_cur_g][chro_cur_g].items():
    if (k[0] <= start_cur) and (start_cur <= k[1]) and (end_cur >= k[1]): # k[0][--start_cur|--k[1]|--]end_cur  INTERVAL OVERLAPS WITH RIGHT SIDE 
     #print 'RIGHT ', k[0], start_cur, end_cur, k[1], v
      link_prev, link_next, key = gin_dict[cell_cur_g][chro_cur_g]._OrderedDict__map[k] # http://stackoverflow.com/questions/28035490/in-python-how-can-i-get-the-next-and-previous-keyvalue-of-a-particular-key-in
      #print 'nextKey: ', link_next[2], 'value for next:', gin_dict[cell_cur_g][chro_cur_g][link_next[2]], "val:", v
      
      # check the two copy-numbers 
      #print "end_cur", end_cur, "start_cur", start_cur, "k", k, "link_next[2]", link_next[2]

      # if the two copy numbers are the same in the two intervals
      if (link_next[2][0] <= end_cur <= link_next[2][1]) and (gin_dict[cell_cur_g][chro_cur_g][link_next[2]] == v) :
        return v
      # if they are different, take the one which overlaps the majority of the interval

      elif (link_next[2][0] <= end_cur <= link_next[2][1]) and ((end_cur - link_next[2][0])< (k[1]-start_cur)):
        #print "taking v: ", v
        return v
      else:
        #print "taking new value ", gin_dict[cell_cur_g][chro_cur_g][link_next[2]]
        return gin_dict[cell_cur_g][chro_cur_g][link_next[2]]

    elif (k[0] >= start_cur) and (end_cur <= k[1]): # start_cur[--k[0]|--end_cur|--]k[1]  INTERVAL OVERLAPS WITH LEFT SIDE
      print 'LEFT ', k[0], start_cur, end_cur, k[1], v
      return v
    
  # Did not match any interval or instance of overlap
  print "Did not match any interval: ", k[0], start_cur, end_cur, k[1]
  return 1000 # IF INTERVALS OF X KB ARE NOT FULLY IN INTERVAL FROM METHOD, RETURN LARGE NUMBER

def compare_calls(a_dict, g_dict, a_chr_end_list, g_chr_end_list):
  mast_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'ANEUF', 'GINKGO')) # make one for each cells
  #print "a_chr: ", a_chr_end_list
  #print "g_chr: ", g_chr_end_list
  # Make sure list of chromosomes are equal
  if a_chr_end_list.keys()!=g_chr_end_list.keys():
    sys.exit('ERROR: list of chromosomes are not the same for both methods')
  elif a_dict.keys()!=g_dict.keys():
    sys.exit('ERROR: list of cells are not the same for both methods')

  print "cell", a_dict.keys() # Use the same dict but pump out the file after processing each cell

  # Iterate through the sorted list of chromosomes to build the profiles for comparision
  sorted_chroms = ['chr22']#['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY']
  
  for cel in a_dict.keys(): # iterate through all cells!
    # Get windows given KB from user
    idx = -1
    for ch in sorted_chroms:
      start_window = 1 # initialize the start for each
      for end_window in range(KB, min(a_chr_end_list[ch], g_chr_end_list[ch]), KB):
        idx+=1
        #print "info passed to get_ functions: ", idx, ch, start_window, end_window
        mast_df.loc[idx] = [ch, start_window, end_window, get_aneu(cel, ch, a_dict, start_window, end_window), get_ginkgo(cel, ch, g_dict, start_window, end_window)] #get_aneu(aneu) ,get_ginkgo()] # grab the value in that range
        start_window = end_window+1 # next start position
  
    # Check to see that they are the same length
    if len(mast_df['ANEUF'].tolist()) != len(mast_df['GINKGO'].tolist()):
      sys.error('ERROR: Length of AneuFinder profile and Ginkgo profile are not equal.')

    # Compare the Percent Similarity and Manhattan 
    dist, count_sim = manhattan_distance(mast_df['ANEUF'].tolist(), mast_df['GINKGO'].tolist())
    print "Percent similarity ((+1 for same cn in a bin) / all bins) is ", count_sim,"/",len(mast_df['ANEUF'].tolist()),"*100 =", (count_sim/len(mast_df['ANEUF'].tolist())*100), "%"    
    print "Manhattan distance of: ", dist, " out of sequence length ", len(mast_df['ANEUF'].tolist()) 
    mast_df.to_csv(OUTPUT_FILE+"_"+cel+'_.csv')

def plot_cnp(clos):
  alpha_cl = clos.keys()[0][0]
  cells_cl = clos.keys()[0][1]
  clones_cl = clos.keys()[0][2] 
  N_BINS = len(clos.values()[0][0])

  ind = range(1,N_BINS+1)

  for idx, i in enumerate(clos.values()[0]):
    cl = i
    plt.scatter(ind,cl)
    plt.plot(ind,cl)
    plt.title("Clone %d CNP: %d Cells, %d Clones, %d Bins" % (idx,cells_cl,clones_cl,N_BINS))
    plt.xlabel('Bin')
    plt.ylabel('Integer Copy-Number')
    plt.savefig(OUTDIR+'/cnp_clone%d_of%dclones_%dcells_%dbins.pdf' % (idx,clones_cl,cells_cl,N_BINS))
    #plt.show()
    plt.clf()
  return alpha_cl, cells_cl, clones_cl, N_BINS

def manhattan_distance(x,y):
    #print "dist truth ", x
    #print "dist other ", y
    counter = 0
    for i,j in zip(x,y):
      if i==j:
        counter+=1
    return sum(abs(a-b) for a,b in zip(x,y)), counter

def normalized_manhattan_distance(inf,tru):
    assert len(inf)==len(tru)# make sure same number of cells
    assert len(inf[0])==len(tru[0])# make sure same number of bins
    full_manh = 0
    #print "m*n", len(inf), '*', len(inf[0])
    for i in range(0,len(inf)):
        full_manh+=manhattan_distance(inf[i],tru[i])
    L1_final_dist = full_manh/(len(inf)*len(inf[0])) # manhattan distance / (m * n)
    return L1_final_dist

###########################
# USAGE
if len(sys.argv) != 5:
  sys.stderr.write("Usage: " + sys.argv[0] + " ANEUFINDER_INPUT_BED_FILE  GINKGO_INPUT_TSV_FILE  KB (e.g. 50000)  OUTPUT_FILE_NAME\n")
  sys.exit(1)
###########################

# ARGUMENTS
ANEU = sys.argv[1]
GINK = sys.argv[2]
KB = int(sys.argv[3])
OUTPUT_FILE = str(sys.argv[4])

if __name__ == '__main__':
  aneu_dict, a_chrom_end_list = aneuFinder_processing(ANEU)
  gink_dict, g_chrom_end_list = ginkgo_processing(GINK)
  compare_calls(aneu_dict, gink_dict, a_chrom_end_list, g_chrom_end_list) # contains chr, window start, window end, A cn, G cn


############################
# # Remove unwanted columns
#   df.drop(df.columns[[0,1,2]], axis=1, inplace=True)

# # turn into np.array
#   df_nparray = df.as_matrix()
#   #print "Total Number of Cells: ", len(list(df)), " shape: ", df_nparray.shape

# # sort np.array
#   df_nparray_s = df_nparray[df_nparray[:,1].argsort()]

# # set up colormap
#   vmax = 6.0 #float(np.amax(df_nparray_s))
#   print "Max set to : ", vmax
#   cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'), (2 / vmax, 'white'), (vmax / vmax, 'red')])

#   fig, ax = plt.subplots(1)
#   #fig.set_size_inches(30, 20)

# # plot using sns.heatmap
#   if CLUSTERMAP == 1:
#     im = sns.clustermap(df_nparray_s.transpose(), col_cluster=False, robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=0, vmax=5)
#   else:
#     im = sns.heatmap(df_nparray_s.transpose(), robust=True, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)
  
# # specific to plot
#   ax.set(frame_on=False, xticks=[], yticks=[])
#   plt.yticks(rotation='horizontal')
  
#   # when saving, specify the DPI
#   plt.savefig(OUTPUT_FILE, dpi = 200)
#   #ax.invert_yaxis()


########### If Needed to work in pandas df space - see what I have already tried, below ############
#print df.ix[1,1]
#print "before", list(df)

#df_nparray_s = df.sort()

#a = [[2,2,2,1,1,1,2],[4,4,3,2,1,2,2],[1,1,1,1,1,1,1],[4,3,2,1,2,3,3],[1,1,1,1,1,1,1],[4,4,3,2,1,2,2]]
#y=np.array([np.array(xi) for xi in a])
#df_nparray_s=y[y[:,1].argsort()]

#im = ax.pcolor(df, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')   
#fig = plt.gcf() # get current figure

#im = imshow(df_nparray_s.transpose(), cmap=cmap, interpolation='nearest')#, vmin=0, vmax=vmax, edgecolors='black') #fmt="f", cmap='RdBu_r', vmin=-2, vmax=6)


#im.set_yticks([])
#im.set_xticks([])
#cax = plt.gcf().axes[-1]
#cax.tick_params(labelsize=20)
#confused = ax.pcolor(im, cmap=cmap)
#cbar = fig.colorbar(im)#, cax=cax) #colorbar(im[4], cax=cax)
#cbar.set_ticks(range(7)) # Integer colorbar tick locations
#ax.set(frame_on=False, aspect=1, xticks=[], yticks=[])

#cbar = ax.collections[0].colorbar
#cbar.set_ticks([0,1,2,3,4,5])

#cbar.set_ticks(range(4)) # Integer colorbar tick locations

# cmap = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'),
#                                                     (1 / vmax, 'white'),
#                                                     (3 / vmax, 'red')]
#                                       )

#fig, ax = plt.subplots()
#im = ax.pcolor(data, cmap=cmap, vmin=0, vmax=vmax, edgecolors='black')
#cbar = fig.colorbar(im)

