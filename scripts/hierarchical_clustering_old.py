#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse, scipy
from plotly.tools import FigureFactory as FF
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt 
import plotly.plotly as py
import collections as c
import numpy as np
import pysam as pys
import re

# Ashley Mae
# Created Oct 10, 2016
#####
#
# hierarchical_clustering.py: creates K clusters for individual cells
# reference: http://blog.nextgenetics.net/?e=43
# reference for distance functions: http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html#scipy.spatial.distance.pdist
#
#####

# FILE ASSIGNMENTS
CHR = 0
START_BIN = 1
END_BIN = 2
DEPTH = 3

def add_to_cell_matrix(x_l, file, sample_name):
	bin_limit = 414 # limit to number of bins (n)
	cell_list = [] # create list of cells to be added
	iterator = 1

	with open(file) as f:
		for line in f:
			if iterator <= bin_limit:
				s = line.rstrip("\r\n").split(' ')
				e = [a.replace(' ','') for a in s]
				cell_list.append(float(e[DEPTH]))
				iterator+=1

	x_l.append(cell_list)
	return x_l

def distance_measures(data, names):
	#print "data: ", data
	# Calculate a distance matrix
	distMatrix = dist.pdist(data, 'chebyshev') # 'cityblock' (manhattan), 'hamming', 'jaccard'

	# Convert distance matrix to square form. The distance matrix 
	# 	calculated above contains no redundancies, you want a square form 
	# 	matrix where the data is mirrored across the diagonal.
	distSquareMatrix = dist.squareform(distMatrix)
	#print "distsq: ", distSquareMatrix

	# Calculate the linkage matrix 
	linkageMatrix = hier.linkage(distSquareMatrix)
	#print "linkage matrix", linkageMatrix

	dendro = hier.dendrogram(linkageMatrix,labels=names, leaf_rotation=90)
	#dendro = FF.create_dendrogram(linkageMatrix, orientation='left', labels=names)
	#print "dendro: ", dendro
	
	# Get the order of rows according to the dendrogram 
	leaves = dendro['leaves']
	#print "leaves", leaves
	#leaves = names
	#print leaves

	# Reorder the original data according to the order of the 
	#	dendrogram. Note that this slice notation is numpy specific.
	#	It just means for every row specified in the 'leaves' array,
	#	get all the columns. So it effectively remakes the data matrix
	#	using the order from the 'leaves' array.
	transformedData = data[leaves,:]
	#print "trans ", transformedData
	
	plt.title("Patient T5 "+ str(NUM_FILES) +" Cells - Dendrogram")
	plt.xlabel("Cells")
	plt.ylabel("Pairwise Distances")
	plt.show()

 
def cluster_points(X, mu):
    clusters  = {}
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                    for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters
 
def reevaluate_centers(mu, clusters):
    newmu = []
    keys = sorted(clusters.keys())
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
    return newmu
 
def has_converged(mu, oldmu):
    return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))
 
def find_centers(X, K):
    # Initialize to K random centers
    oldmu = random.sample(X, K)
    mu = random.sample(X, K)
    while not has_converged(mu, oldmu):
        oldmu = mu
        # Assign all points in X to clusters
        clusters = cluster_points(X, mu)
        # Reevaluate centers
        mu = reevaluate_centers(oldmu, clusters)
    return(mu, clusters)

######################################
# ASSIGNMENTS
NUM_FILES = int(sys.argv[1])
cell_list = []
name = str
all_names = []

######################################
# USAGE
if len(sys.argv) != NUM_FILES+2:
    sys.stderr.write("Usage: " + sys.argv[0] + " <NUM_FILES_TO_ITERATE_OVER> <data_binned_RD_SRR308173... file>\n")
    sys.exit(1)

######################################
# METHODS

# Iterate through cell files
for i in range(2,int(NUM_FILES+2)): # starting with 2nd argument
    text = sys.argv[i]

    print "cell data file: ", text
    
    # If normal cell - add name with "N"
    if 'normal' in sys.argv[i]:
    	m = re.search('_RD_(.+?)_1', text)
    	if m:
    		found = m.group(1)
    		name = str('N_'+found)
    	#print name
   
    # If tumor cell - add name with "T"
    elif 'tumor' in sys.argv[i]:
    	m = re.search('_RD_(.+?)_1', text)
    	if m:
    		found = m.group(1)
    		name = str('T_'+found)
    	#print name
    
    # Create cell list
    cell_list = add_to_cell_matrix(cell_list, text, name)
    all_names.append(name)

print "done"
print all_names

# Create cell matrix (cells X bins)
cell_matrix = np.array((cell_list))

# Create hierarchical clustering dendrogram
distance_measures(cell_matrix, all_names)

