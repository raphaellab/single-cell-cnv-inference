#!/bin/sh

# Gathers data from each .bam file (SORTED) generated and passes it to a python script to make a file of vectors of normalized read depth
# Ashley Conard
# Last Modifined July 20, 2017

#ARGUMENTS
# 1: Directory of data, example: ../data/read_depth_data/
# 2: Desired name of cell .bam file to create chrom_len.txt (used to have end of chromosome for fetching within a certain window)
# 3: Desired output directory
# 4: Bin (skip) size desired
# 5: Read length 76 (Gao), which is single-end
# 6: Path to read_depth_plot.py, example: ~/Desktop/singleCell/single-cell-cnv-inference/scripts/scripts_plot_readDepth_normal/read_depth_plot.py


# OLDER Example: ./gather_read_depth_data.sh chr17 ../data/read_depth_data/ SRR3082145_1.fastq.gz.SORTED.bam ../results/ 200000
# Example2: ./gather_read_depth_data.sh ~/Desktop/singleCell/data/read_depth_data/ SRR3082145_1.fastq.gz.SORTED.bam ~/Desktop/results_gather/ 250000 76 ~/Desktop/singleCell/single-cell-cnv-inference/scripts/scripts_plot_readDepth_normal/ 

# Example3: ~/Desktop/singleCell/single-cell-cnv-inference/scripts/bam2vector_readcounts/gather_read_depth_data.sh 
#				/Users/ashleymaeconard/Desktop/singleCell/data/T5_tumor_bam_files/ SRR3081691_1.fastq.gz.bam.sorted.bam 
#				~/Desktop/results_gather/gather/ 250000 76 ~/Desktop/singleCell/single-cell-cnv-inference/scripts/bam2vector_readcounts/

#METHOD CALLS
# echo "Calling Samtools for $2"

echo "Stats: "
samtools flagstat $1$2

echo "For single-end, I want # mapped alignments, not those marked as secondary or supplementary. so number of mapped alignments: " 
samtools view -F 0x904 -c $1$2

echo "Another tool using samtools view -c -F 4: " 
samtools view -c -F 4 $1$2 

echo "Another tool using samtools view -c -q 20: "
samtools view -c $1$2 -q 20

echo "Another tool using samtools view -c -F 4 -q 20: "
samtools view -c -F 4 $1$2 -q 20

# # Create chromosome length list to parse in read_depth.py. Used to determine the end of each chromosome.
#samtools view $1$2 | awk '{print $3 "\t" $4 "\t" $4+length($10)-1}' | sed '/_/d' > $3$2_chrom_len.txt # NOTE sed is removing all secondary and supplementary mapped alignments

# Calling Read Depth 
echo "Calling python with $2 as chrom location file in $3"
#python "$7"read_depth_plot.py "$2$3" "$4$3_$1_depth_loci.txt" "$3" "$4" "$5" "$6"
python "$6"read_depth_plot.py "$1" "$3$2_use_for_chrom_len.txt" "$3" "$4" "$5" # "$3$2_all_test_depth_loci.txt"

################################################

## Getting average and stdev for read depth of each cell
#echo "Cell average read depth and standard deviation:"
#samtools depth $2$3 |  awk '{sum+=$3; sumsq+=$3*$3} END { print "Average = ",sum/NR; print "Stdev = ",sqrt(sumsq/NR - (sum/NR)**2)}'

## Get each locus depth
# for i in 'chr1' 'chr2' 'chr3' 'chr4' 'chr5' 'chr6' 'chr7' 'chr8' 'chr9' 'chr10' 'chr11' 'chr12' 'chr13' 'chr14' 'chr15' 'chr16' 'chr17' 'chr18' 'chr19' 'chr20' 'chr21' 'chr22' 'chrX'
# do
#     echo "chr: $i"
#     samtools depth $1$2 -r $i >> $2_all_test_depth_loci.txt
# done
#samtools depth $1$2 -r "chr17" > $2_all_test_depth_loci.txt

#mv $3_$1_depth_loci.txt $3/
#mv $2_all_test_depth_loci.txt $3
