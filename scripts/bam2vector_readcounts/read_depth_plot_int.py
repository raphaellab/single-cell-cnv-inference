#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import pysam as pys
from subprocess import Popen, PIPE
import pandas as pd
from time import sleep
from multiprocessing import Pool
from multiprocessing import Process
from random import randint

# Ashley Mae
# Created Sept 27, 2016
# Last Modified Aug. 23, 2017
#####s
# read_depth_plot.py: plots the read depth
# e.g.: python read_depth_plot.py DIR(data)/ DIR2(output)/FILE_chr_depth_loci.txt  OUTPUT_DIR SKIP(WINDOW) READ_LEN CELL_BAM_NAME
#  (NOTE: FILE_chr_depth_loci.txt could be FILE_use_for_chrom_len.txt)
#####

# ASSIGNMENTS FOR DICTIONARY OF CHROMOSOME START AND END
START = 0
END = 1

def get_start_end_chrom(chr_df, chr_list): # bam file as df
    chrom_start_end_dict = c.defaultdict()

    #Iterate through all chromosomes to make dictionary
    for i in chr_list:
        cur_chrome = chr_df[chr_df["Chr"] == i]
        chrom_start_end_dict[i]=(cur_chrome['Start'].min(), cur_chrome['End'].max())
    
    chrom_start_end_dict['chrY']=(530000,57227415)
    #print "dict ", chrom_start_end_dict
    chrom_start_end_dict = {k.replace('chr',''): v for k,v in chrom_start_end_dict.items()}
    
    return chrom_start_end_dict

def bam_read_count(dir_cell_bam):
    """ Return a tuple of the number of mapped and unmapped reads in a bam file """
    p = Popen(['samtools', 'idxstats', dir_cell_bam], stdout=PIPE)
    mapped = 0
    unmapped = 0
    for line in p.stdout:
        rname, rlen, nm, nu = line.rstrip().split()
        mapped += int(nm)
        unmapped += int(nu)
    print  "total reads: ",  mapped
    return (mapped, unmapped)

def index_master_df(m_df, dict_chr_start_end, chr_list): # Index master df with the chr, start, end. Then all cell RD columns will be appended.
    idx = -1 # Index used to fill in coverage for all cells
    skip = BIN_SKIP

    for chrm in chr_list:   
        start = 1
        print "chrm: ", chrm
        end = dict_chr_start_end[chrm][END]

        for i in range(start, end, skip):
            idx+=1
            m_df.loc[idx] = [chrm, i, i+skip] # CHR, START, END
    
    return m_df

def get_readDepth(chrm_list, dict_chr_start_end, dir_bam_file, NAME_CELL): # with FETCH command on .bam file
    #print "bam: ", bam_file
    df = pd.DataFrame(columns=('INDEX', NAME_CELL))
    reads = pys.AlignmentFile(dir_bam_file,"rb")
    chr_start_list=[] # keeping track of the start of chromosomes for plot lines

    idx = -1 # Index used to find and merge coverage for all cells with master df

    # Example of how to print the lines where each read begins
    # iter = reads.fetch('chr1', 0, 84478)
    # for i in iter:
    #     print (str(i))

    skip = BIN_SKIP
    x = [] # x axis
    y = [] # y axis

    print "Starting to count read depth for skip ", skip
    print "Getting total reads for: ", dir_bam_file
    total_read = bam_read_count(dir_bam_file)[0]
        
    for chrm in chrm_list: # Iterate through chromosomes
        start = 1
        end = dict_chr_start_end[chrm][END]

        for i in range(start,end,skip):
            idx+=1
            #print chrm, i, i+skip #, depth
            if NORMALIZED_OR_DEPTH == 'rawcounts':
                raw_counts = len(list(reads.fetch(chrm,i,i+skip)))
                df.loc[idx] = [idx , raw_counts]
                x.append(idx)
                if i == 1:
                    print "starting chrom", chrm," at pos:",  i
                    chr_start_list.append(idx) # keep the index to mark the beginning of a chromosome in the read depth plot
                y.append(raw_counts)
            
            elif NORMALIZED_OR_DEPTH == 'depth':
                raw_counts = len(list(reads.fetch(chrm,i,i+skip)))
                depth = raw_counts/float(skip)*READ_LEN # calc read depth
                x.append(idx)
                if i == 1:
                    print "starting chrom", chrm," at pos:",  i
                    chr_start_list.append(idx) # keep the index to mark the beginning of a chromosome in the read depth plot
                y.append(depth)
            
            elif NORMALIZED_OR_DEPTH == 'depthnormalized':
                raw_counts = len(list(reads.fetch(chrm,i,i+skip)))
                depth = raw_counts/float(skip)*READ_LEN # calc read depth
                normalized_depth = depth / total_read
                x.append(idx)
                if i == 1:
                    print "starting chrom", chrm," at pos:",  i
                    chr_start_list.append(idx) # keep the index to mark the beginning of a chromosome in the read depth plot
                y.append(normalized_depth)
            
            elif NORMALIZED_OR_DEPTH == 'rawcountsnormalized':
                raw_counts = len(list(reads.fetch(chrm,i,i+skip)))
                raw_counts_normalized = raw_counts/total_read
                df.loc[idx] = [idx , raw_counts_normalized]
                x.append(idx)
                if i == 1:
                    print "starting chrom", chrm," at pos:",  i
                    chr_start_list.append(idx) # keep the index to mark the beginning of a chromosome in the read depth plot
                y.append(raw_counts_normalized)
            else:
                sys.exit("ERROR - please type one of these for the last command: raw counts normalized, raw counts, raw counts normalized, depth, depth normalized")

                #fp.write('{},{},{},{}\n'.format(chrm, i, i+skip, depth))
                #myfile.write((str(chrm),str(i),str(i+skip),str(depth)))
                #print i,i+skip,depth
                #print list(reads.fetch(chrm,i,i+skip))[0],depth
    
    print "End of count read depth"

    if PLOTS: # plot the read depths of all cells
        f, ax = plt.subplots(figsize=(15, 6))
        ax.set_ylim([-1,2000])
        #ax.set_xlim([0,12200])
        #ax.plot(x,y)
        ax.scatter(x,y, c="b", linewidth=1, s=3)#,color=".3")
        ax.set_xticks(chr_start_list, minor=True)
        ax.xaxis.grid(True, which='minor')
        #plt.xticks(x, x)
        ax.set_title('%s for %s, bin size %s'%(NORMALIZED_OR_DEPTH, NAME_CELL, BIN_SKIP))
        text_b = '%s/binned_RD_counts_%s_%s_fetch.pdf'%(OUTPUT_DIR,NAME_CELL,BIN_SKIP)
        #f.set_figheight(5)
        #f.set_figwidth(10)
        #plt.figure(figsize=(10, 5))
        f.savefig(text_b,dpi=600,format='pdf')#,bbox_inches='tight')

    return df

# USAGE
if len(sys.argv) != 10:
    sys.stderr.write("Usage: " + sys.argv[0] + " <SORTED_BAM_FILE_DIR (with .bai index files)> <CHR_START_END_FILE_NAME> <OUTPUT_DIR> <BIN_SIZE> <READ_LEN> <CELL_BAM_NAME> <LIST_CELLS_NOT_TO_CLUSTER (e.g. cell1,cell3 or NA> <IF_YOU_WANT_PLOT_RD_FOR_EACH_CELL(0 or 1)> <COUNTS_OR_DEPTH ('rawcounts' OR 'rawcountsnormalized' OR 'depth' OR 'depthnormalized')>\n") # <LOCI_IN_FILE (FROM DEPTH COMMAND)> 
    sys.exit(1)

# ASSIGNMENT
CHR_LIST_NUM = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','X']
CHR_LIST = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 
           'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY'] #CHR_LIST = sys.argv[1] # NOT GATHERING ANY OTHER CHROMOSOMES BESIDES 
#BAM_FILE = sys.argv[1] 
DATA_DIR = sys.argv[1]
CHR_START_END_FILE = sys.argv[2] #LOCI_IN_FILE = sys.argv[2]
OUTPUT_DIR = sys.argv[3]
BIN_SKIP = int(sys.argv[4])
READ_LEN = int(sys.argv[5])
OUTPUT_CELL_BAM_NAME = sys.argv[6]
DONOT_INCLUDE = str(sys.argv[7])
PLOTS = int(sys.argv[8])
NORMALIZED_OR_DEPTH = sys.argv[9]
#CHR_LIST = str(sys.argv[10])

if __name__ == '__main__':
    counter = 0 # keeping track of the number of cells which have been processed.
    
    # Create list of all .bam files to transform into vectors of normalized read counts
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and not ".bai" in f]
    #print "only files: ", len(onlyfiles)
    print "Those cells not to include: ", DONOT_INCLUDE, type(DONOT_INCLUDE)
    
    # Filtering
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store")
    if DONOT_INCLUDE == "NA":
        print "Use all cells."
    elif "," not in DONOT_INCLUDE:
        print "Use a comma to separate cells."
        sys.exit()
    elif DONOT_INCLUDE != "NA":
        DONOT_INCLUDE = DONOT_INCLUDE.split(",")
        print "Do not include: ", DONOT_INCLUDE
        print "only files: ", onlyfiles
        for i in DONOT_INCLUDE:
            onlyfiles.remove(i) # remove cells not to include in clustering (because they are too different from others and should not be included.)
    else:
        sys.exit("ERROR TO PROCESS INPUT LIST OF CELLS TO CONSIDER.")

    #print "after: ", len(onlyfiles)
    # # Create dictionary of all start and ends of chromosomes 
    chr_start_end_df = pd.read_csv(CHR_START_END_FILE, sep='\t', header=None)
    chr_start_end_df.columns = ["Chr", "Start", "End"] 
    dict_start_end_chroms = get_start_end_chrom(chr_start_end_df, CHR_LIST)
    print "dictionary of integer chromosomes: ", dict_start_end_chroms 
    
    # # DF to fill with vectors of normalized read counts across all cells in the input directory
    mast_df = pd.DataFrame(columns=('CHR', 'START', 'END'))
    # text_w = '%s/data_binned_RD_%s_%s_fetch.csv'%(OUTPUT_DIR,CELL_BAM_NAME,BIN_SKIP)
    master_df = index_master_df(mast_df, dict_start_end_chroms, CHR_LIST_NUM)
    #master_df = pd.read_csv("test_mast_df_indices.csv")
    #cell_df_to_merge = pd.read_csv('cell_df_to_merge.csv')

    # Get read depth for each cell and each bin
    for cell_name in onlyfiles:
        counter+=1
        print "Cell number ", counter ," DIR+name: ", DATA_DIR+cell_name # file and filepath
        cell_df_to_merge = get_readDepth(CHR_LIST_NUM, dict_start_end_chroms, DATA_DIR+cell_name, cell_name)
        #cell_df_to_merge.to_csv('cell_df_to_merge.csv')
        master_df[cell_name]=cell_df_to_merge[cell_name]

    # Save master_df
    master_df.to_csv(OUTPUT_DIR+"/"+OUTPUT_CELL_BAM_NAME+"_"+str(BIN_SKIP)+"_binsXcells__read_depth_plot.csv")

#python read_depth_plot.py DIR(data) DIR2(output)/FILE_chr_depth_loci.txt DIR(data) OUTPUT_DIR SKIP(WINDOW) READ_LEN

# PRINTING HISTOGRAM
# plt.hist(dic, bins=len(dic))
# plt.title("Read Depth Histogram")
# plt.xlabel("Bin")
# plt.ylabel("Frequency")
# plt.show()

# fig = plt.gcf()
# plot_url = py.plot_mpl(fig, filename='genome_read_depth')

