# !/bin/sh
# Runs scripts through all files in /nbu/compbio/aconard/
# Ashley Conard
# Oct 3rd, 2016

for i in *.bam.sorted.bam 
do
	echo "Processing $i"
	./gather_read_depth_data.sh chr17 /nbu/compbio/aconard/BAM_SAM_BED_Gao_TNBC_normal_T5/ $i results_normal_gao_t5/ 200000 76
	echo "Done processing $i"
done

