# Ashley Conard
# plot_divisiveClust_diana.r - performs divisive clustering given .csv files where last column is count data.
# Last update : Sept 19, 2017
# Rscript plot_divisiveClust_diana.r FULL_INPUT_DIR/(will all __read_depth_plot.csv files) NAME_CELLS(ID for output plots)

# Load library for Diana
library(cluster)

# Allow commandline arguments
args <- commandArgs(trailingOnly = TRUE)
if (length(args) == 0){
    stop("Please type: Rscript plot_divisiveClust_diana.r FULL_INPUT_DIR/(will all __read_depth_plot.csv files) NAME_CELLS(ID for output plots) SMALL_EVENT_ID(ID such as difseed or 1a1...) BIN_SIZE  READ_LENGTH", call.=FALSE)
}

# Arguments
NAME_CELLS = args[2]
SMALL_EVENT_ID = args[3]
BIN_SIZE = as.numeric(args[4])
READ_LEN = as.numeric(args[5])

# Get files
temp = list.files(path=args[1], pattern="*_read_depth", full.names=T, recursive=FALSE)
print("Files:")
print(temp)
myfiles = lapply(temp, read.csv)
idx=0

# Concatenate all to make data matrix for diana
for (f in myfiles){
    idx=idx+1
    
    if (idx == 1){ # create matrix
        print(" ") 
        print("First file")
        #print(colnames(f[,ncol(f)])
        #mat = f[,ncol(f)]
        mat = rev(f)[1]
        print(colnames(mat))
        #print(mat)

        # Determine if cell has small event
        if (grepl(SMALL_EVENT_ID, colnames(mat))){
            print("Small Event")
            list_col_name = strsplit(colnames(mat), split="_")
            
            # if the name starts with 'cell', take that
            if (grepl("cell",list_col_name[[1]][1])){
                cell_name = paste(c(list_col_name[[1]][1],"g"),collapse="")
            }
            # otherwise find where 'cell' is and use that
            else{
                #print("Find where cell is")
                #print(list_col_name)
                for (i in list_col_name[[1]]){
                    if (grepl("cell", i)){
                        #print("yes")
                        if (grepl("[.]", i)){
                            peri = strsplit(i, split="[.]")
                            #print("peri")
                            #print(peri)
                            for (j in peri[[1]]){
                                if (grepl("cell", j)){
                                    cell_name = paste(c(j,"g"), collapse="")
                                    print(cell_name)
                                }
                            }
                        }      
                    }
                    else{
                        cell_name = paste(c(i,"g"),collapse="")
                    }
                }
            }
        } 
        # Determine if cell has just large event
        else{
            list_col_name = strsplit(colnames(mat), split="_")
            print("just large event")

            for (i in list_col_name[[1]]){
                if (grepl("[.]", i)){
                    peri = strsplit(i, split="[.]")
                    for (j in peri[[1]]){
                        if (grepl("cell", j)){
                            cell_name = j
                            print(cell_name)
                        }
                    }
                }
                # otherwise look before the last part of name
                else if (grepl("cell", i)){
                    #print("cell here")
                    cell_name = i
                    print(cell_name)
                }
            }
        }
        
        list_names = c(cell_name) # create list of column (cell) names
        print("First Changed to")
        print(cell_name)
    }
    else{# concatenate columns to current matrix c
        print(" ") 
        print("Next")
        mat = cbind(mat,rev(f)[1])
        name = colnames(rev(f)[1]) # get the name of the cell
        print(colnames(mat))
        print("Name to change: ")
        print(name)

        # Determine if cell has small event
        if (grepl(SMALL_EVENT_ID, name )){ # colnames(f[5])
            print("smallevent")
            list_col_name = strsplit(name, split="_")
            
            # if the name starts with 'cell', take that
            if (grepl("cell",list_col_name[[1]][1])){
                cell_n = paste(c(list_col_name[[1]][1],"g"),collapse="")
                print(cell_n)
            }
            # otherwise find where 'cell' is and use that
            else{
                #print("Find where cell is")
                for (i in list_col_name[[1]]){
                    if (grepl("cell", i)){
                        #print("yes")
                        if (grepl("[.]", i)){
                            peri = strsplit(i, split="[.]")
                            #print("peri")
                            #print(peri)
                            for (j in peri[[1]]){
                                if (grepl("cell", j)){
                                    cell_n = paste(c(j,"g"), collapse="")
                                    print(cell_n)
                                }
                            }
                        }
                    }
                    else{
                        cell_n = paste(c(i,"g"),collapse="")
                        print(cell_n)
                    }
                }
            }
        } 

        # Determine if cell has just large event
        else{
            list_col_name = strsplit(name, split="_", fixed=TRUE)
            print("just large event")

            for (i in list_col_name[[1]]){
                # check last name to extract cell name
                if (grepl("[.]", i)){
                    peri = strsplit(i, split="[.]")
                    for (j in peri[[1]]){
                        if (grepl("cell", j)){
                            cell_n = j
                            print(cell_n)
                        }
                    }
                }
                # otherwise look before the last part of name
                else if (grepl("cell", i)){
                    print("cell here")
                    cell_n = i
                    print(cell_n)
                }
            }
        }

        list_names = c(list_names,cell_n) # append more short cell names
        print("Changed to")
        print(cell_n)
    }    
}

# Change column (cell) names
colnames(mat)<- c(list_names)
# See dimensions
print("Dimensions:")
print(dim(mat))

# Transpose matrix for diana format
mat = t(mat)

# Convert to coverage
#(function(x)(x/BIN_SIZE*READ_LEN))(mat)
#apply(mat, 1:(dim(mat)[2]), function(x) (x/BIN_SIZE*READ_LEN))

# DIANA - euclidean
de <- diana(mat, diss=inherits(mat, "dist"), metric="euclidean")
pdf(paste(c("diana_eucl_",NAME_CELLS,"__diana.pdf"), collapse=""))
plot(de, main=paste("Divisive Clustering Dendro., Euclidean: ", NAME_CELLS))
dev.off()

# DIANA - manhattan
dm <- diana(mat, diss=inherits(mat, "dist"), metric="manhattan")
pdf(paste(c("diana_manh_",NAME_CELLS,"__diana.pdf"), collapse=""))
plot(dm, main=paste("Divisive Clustering Dendro., Manhattan:", NAME_CELLS))
dev.off()