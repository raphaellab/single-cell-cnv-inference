#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse, scipy
from plotly.tools import FigureFactory as FF
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt 
from os import listdir
from scipy.cluster.hierarchy import dendrogram, linkage
from os.path import isfile, join
import pysam as pys
import pandas as pd
import plotly.plotly as py
import collections as c
import numpy as np
import pysam as pys
import re

# Ashley Mae
# Created Oct 10, 2016
#####
#
# hierarchical_clustering.py: creates K clusters for individual cells
# reference: http://blog.nextgenetics.net/?e=43
# reference: https://stackoverflow.com/questions/11917779/how-to-plot-and-annotate-hierarchical-clustering-dendrograms-in-scipy-matplotlib
# reference for distance functions: http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html#scipy.spatial.distance.pdist
#
#####

def reformat_df(df, cell_col_na, cell_na):
    #master_df = master_df[[cell_col_name]]
    #master_df = master_df.rename(index=str, columns={cell_col_name: cell_name})

    print "List column names: ", list(df)
    #cell_col_name = [a for a in list(df) if cell_n in a][0]
    #df.reset_index()
    #len_rows = df.shape[0]
    #df['CELL_NAME'] = [cell_n] * len_rows
    df.rename(columns={cell_col_na:'READ_COUNTS'}, inplace=True)
    df[cell_na] = df.apply(calc_coverage, axis=1) # add column for coverage
    #df = df[[cell_na]]
    return df
    
def calc_coverage(row):
    return (row['READ_COUNTS']/BIN_SIZE*READ_LEN)

def distance_measures(data, names, typeDist):
    # Calculate a distance matrix
    distMatrix = dist.pdist(data, typeDist) 
    print distMatrix
    # Convert distance matrix to square form. The distance matrix 
    # 	calculated above contains no redundancies, you want a square form 
    # 	matrix where the data is mirrored across the diagonal.
    #distSquareMatrix = dist.squareform(distMatrix)
    #print "distsq: ", distSquareMatrix

    # Calculate the linkage matrix 
    linkageMatrix = hier.linkage(distMatrix, "complete") # how do you compare groups of cells? ()
    print "linkage matrix", linkageMatrix

    dendro = hier.dendrogram(linkageMatrix,labels=names, leaf_rotation=45, color_threshold=0)
    #dendro = FF.create_dendrogram(linkageMatrix, orientation='left', labels=names)
    print "dendro: ", dendro

    # Get the order of rows according to the dendrogram 
    leaves = dendro['leaves']

    # Reorder the original data according to the order of the 
    #	dendrogram. Note that this slice notation is numpy specific.
    #	It just means for every row specified in the 'leaves' array,
    #	get all the columns. So it effectively remakes the data matrix
    #	using the order from the 'leaves' array.
    transformedData = data[leaves,:]
    
    plt.title("Dendrogram - %s"%(typeDist))
    plt.xlabel("Cells")
    plt.ylabel("Distance")
    plt.savefig(typeDist+"dendro__hierarchical_clustering.pdf",format='pdf')#,bbox_inches='tight')
    plt.show()
    plt.close()

def cluster_points(X, mu):
    clusters  = {}
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                    for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters
 
def reevaluate_centers(mu, clusters):
    newmu = []
    keys = sorted(clusters.keys())
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
    return newmu
 
def has_converged(mu, oldmu):
    return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))
 
def find_centers(X, K):
    # Initialize to K random centers
    oldmu = random.sample(X, K)
    mu = random.sample(X, K)
    while not has_converged(mu, oldmu):
        oldmu = mu
        # Assign all points in X to clusters
        clusters = cluster_points(X, mu)
        # Reevaluate centers
        mu = reevaluate_centers(oldmu, clusters)
    return(mu, clusters)

def plot_rd(df_read_depth_plot, cell_n):   
    #print "df plotting: ", len(df_read_depth_plot) # 13507
    #print "index: ", df_read_depth_plot.index
    #df_read_depth_plot.plot(kind='scatter', x='col1', y='col2', s=df.col3)  
    f, ax = plt.subplots(figsize=(15, 6))
    #ax.set_ylim([-1,10000])
    #ax.set_xlim([0,12200])
    #ax.plot(x,y)
    #print "df ", df_read_depth_plot, cell_n, list(df_read_depth_plot)
    #ax.scatter(x,y, c="b", linewidth=1, s=3)#,color=".3")
    #plt.xticks(x, x)

    if CHR == 'NA': # plot full read depth of cell
        ax.set_title('%s plot for Cell: %s'%(COUNTS_COV ,cell_n))
        text_b = '%s_read_depth_plot__hierarchical_cluster.pdf'%(cell_n)
        plt.xticks(np.arange(min(df_read_depth_plot.index), max(df_read_depth_plot.index)+1, 10000.0))
    else: # plot for a chromosome
        df_read_depth_plot = df_read_depth_plot.reset_index(drop=True)
        ax.set_title('Chrom %s, %s plot for Cell: %s'%(CHR, COUNTS_COV ,cell_n))
        text_b = '%s_chrom_%s_read_depth_plot__hierarchical_cluster.pdf'%(cell_n, CHR)
        plt.xticks(np.arange(min(df_read_depth_plot.index), max(df_read_depth_plot.index)+1, 100.0))
        #plt.xticks(np.arange(1, len(df_read_depth_plot)+1, 100.0))
    plt.tick_params(axis='x', which='major', labelsize=5)
    #plt.tick_params(axis='both', which='minor', labelsize=8)   
    ax.scatter(df_read_depth_plot.index, df_read_depth_plot[cell_n], marker='o',linewidth=1, s=2)#, c="b"
    plt.xticks(rotation=70)
    plt.xlabel("Bin")
    plt.ylabel(COUNTS_COV)
    #f.set_figheight(5)
    #f.set_figwidth(10)
    #plt.figure(figsize=(10, 5))
    f.savefig(text_b,dpi=600,format='pdf')#,bbox_inches='tight')
    plt.show()
    plt.close()

######################################
# USAGE
if len(sys.argv) != 6:
    sys.stderr.write("Usage: python" + sys.argv[0] + " <DATA_DIR> <BIN_SIZE> <READ_LENGTH> <PLOT ('no' or 'yes')> <CHR (NA or chromosome as it appears in .bam file>\n")
    sys.exit(1)

######################################
# ARGUMENTS
DATA_DIR = sys.argv[1]
COUNTS_COV = 'READCOUNTS'
BIN_SIZE = int(sys.argv[2])
READ_LEN = int(sys.argv[3])
PLOT_Y_N = sys.argv[4]
CHR = sys.argv[5]
#TYPE_DIST = sys.argv[2] # 'cityblock' (manhattan- L1), 'hamming', 'jaccard', 'chebyshev' (L inf), 'euclidean' (L2),)
#OUTPUT_DIR = sys.argv[3]

######################################
# MAIN
if __name__ == '__main__':
    idx = 0
    
    filter_sym = raw_input("Please enter the desired ending for files to consider: ")

    # Create list of all .bam files to transform into vectors of normalized read counts
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and filter_sym in f] # Merge chrom read depth files for one cell at a time.
    print "\nFiles to merge: ", onlyfiles
    
    for i in onlyfiles:
        idx+=1
        if idx == 1: # Process first cell
            print "\nFirst File", idx ,"/", len(onlyfiles),", cell ", i,". Processing ", DATA_DIR+i ,"\n"
            # Get name of the cell to process
            cell_name = raw_input("\nPlease follow order of files listed and enter desired cell name for dataframe and cluster plot: ")
            #cell_name = 'cell0g'
             
            # Create df and get the cell name
            master_df = pd.read_csv(DATA_DIR+"/"+i)
            master_df.reset_index()
            cell_col_name = list(master_df)[-1] #[a for a in list(master_df) if cell_name in a][0]

            # Determine which data to plot
            if COUNTS_COV=='COVERAGE':
                print "Computing coverage."
                master_df = reformat_df(master_df, cell_col_name, cell_name)
                print "DF coverage col changed from ", cell_col_name, " to ", cell_name
            elif COUNTS_COV=='READCOUNTS':
                print "Processing read counts."
                master_df = master_df.rename(index=str, columns={cell_col_name: cell_name})
                print "\nDF read counts col changed from ", cell_col_name, " to ", cell_name
            else:
                sys.exit("ERROR - must enter 'COVERAGE' or 'READCOUNTS' as 2nd argument.")                
            
            # Plot if argument is true
            #PLOT_Y_N =  raw_input("\nPlot read counts/coverage for that cell?: ('yes' or 'no') ")
            
            if PLOT_Y_N == 'no':
                print "Not plotting ..."
            elif PLOT_Y_N == 'yes':
                if PLOT_Y_N=='yes':
                    print "Plotting ",COUNTS_COV,"..."
                    if CHR == 'NA':
                        plot_rd(master_df, cell_name)
                    else:
                        plot_chr_df = master_df[master_df.CHR == CHR]
                        print "plot_chr_df", plot_chr_df.head(5)
                        plot_rd(plot_chr_df, cell_name)
                        #plot_rd_ch(plot_chr_df, cell_name, CHR) # plot read depth for specific chromosome
                elif PLOT_Y_N == 'no':
                    print "Not plotting for cell ", cell_name
                else: 
                    sys.exit("ERROR - must type 'yes; or 'no'.")
            else:
                sys.exit("ERROR - must type 'yes' or 'no' to plot.")
            # Keep just the read counts column
            master_df = master_df[[cell_name]]
            
        else: # Process rest of cells
                    
            print "\nFile", idx ,"/", len(onlyfiles),", cell ", i,". Processing ", DATA_DIR+i ,"\n"
            cell_name = raw_input("\nPlease follow order of files listed and enter desired cell name for dataframe and cluster plot: ")
             
            # Create df and get cell name
            df = pd.read_csv(DATA_DIR+"/"+i)
            df.reset_index()
            cell_col_name = list(df)[-1]#[a for a in list(df) if cell_name in a][0]
            #print "df col changed from ", cell_col_name, " to ", cell_name

            # Determine which data to plot
            if COUNTS_COV=='COVERAGE':
                print "Computing coverage."
                df = reformat_df(df, cell_col_name, cell_name)
                print "DF coverage col changed from ", cell_col_name, " to ", cell_name
            elif COUNTS_COV=='READCOUNTS':
                print "Processing read counts."
                df = df.rename(index=str, columns={cell_col_name: cell_name})
                print "DF read counts col changed from ", cell_col_name, " to ", cell_name
            else:
                sys.exit("ERROR - must enter 'COVERAGE' or 'READCOUNTS' as 2nd argument.")  

            # Plot if argument is true
            #PLOT_Y_N = 'no'#raw_input("\nPlot read counts/coverage for that cell?: ('yes' or 'no') ")
            if PLOT_Y_N == 'no':
                print "Not plotting..."
            elif PLOT_Y_N == 'yes':
                if PLOT_Y_N=='yes':
                    print "Plotting ",COUNTS_COV,"..."
                    if CHR == 'NA':
                        plot_rd(df, cell_name)
                    else:
                        plot_chr_df = df[df.CHR == CHR]
                        print "plot_chr_df", plot_chr_df.head(5)
                        plot_rd(plot_chr_df, cell_name)
                elif PLOT_Y_N == 'no':
                    print "Not plotting for cell ", cell_name
                else: 
                    sys.exit("ERROR - must type 'yes; or 'no'.")
            else:
                sys.exit("ERROR - must type 'yes' or 'no' to plot.")

            # Keep just the read counts column
            df = df[[cell_name]]
            #df = df.rename(index=str, columns={cell_col_name: cell_name})

            # Append to master_df
            master_df = master_df.join(df) 

    # Transpose master_df and turn into np.array
    print "master_df ", master_df
    names = list(master_df)
    master_df = master_df.transpose()
    #print master_df
    #master_df = master_df.reset_index().values
    
    # Turn master_df into np.array
    master_df = master_df.as_matrix()
    print "master_df as np.array: ", master_df
    
    # Create hierarchical clustering dendrogram
    TYPE_DIST_LIST = ['euclidean'] # 'cityblock', 'hamming', 'jaccard', 'chebyshev', 
    for type_dist in TYPE_DIST_LIST:
        print "type dist ", type_dist
        distance_measures(master_df, names, type_dist)

