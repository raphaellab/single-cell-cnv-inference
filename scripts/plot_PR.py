#!/usr/bin/python
#############################
#
# plot_PR.py 
# created by : Ashley Conard
# last updated: Oct. 26, 2017
#
#############################

from __future__ import division
import matplotlib
matplotlib.use('Agg')
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import glob 
from os import listdir
from matplotlib.colors import ListedColormap
import pandas as pd
import numpy as np
from os.path import isfile, join
import os
import warnings

# plotting example: https://stackoverflow.com/questions/42767489/add-legend-to-seaborn-point-plot
###########################

def label_point(x, y, val, ax):
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        ax.text(point['x'], point['y'], str(point['val']))

def change_color(row):
    #print row['CELL'].strip(" ")# , row['CELL']== 'SRR3082330_1.fastq.gz', type(row['CELL'])
    if "merged" in row['CELL_NAME'].strip(" "): #row['CELL'] == str(CHANGECOLOR):
        print "merged cell: ", row['CELL_NAME'].strip(" ")
    	return 'Blue'
    else:
        return 'Red'
    
def append_dataframes(toAdd_df, mast_df, cells_dont_include, list_cells):
    # Read in df
    df = pd.read_csv(toAdd_df, sep=',')

    # Keep only 3 columns
    df = df[['ID','CELL_NAME', 'NORM_X-SOMY_BINS']] 
    #df.CELL_NAME.astype(str).argsort()
    df = df.sort_values('CELL_NAME', ascending=True) 
    
    if df.shape[0] != len(list_cells): # make sure there are 35 rows - i.e. 35 cell values (34 cells + merged cell)
        print "List cells and sorted DF are not the same: ", len(list_cells), " vs. ", df.shape[0]
        #sys.exit("ERROR - there should be 35 cell values, 34 cells and 1 merged cell")

    if list_cells != df['CELL_NAME'].tolist():
        print "dif : ",  set(df['CELL_NAME'].tolist())-set(list_cells)
        #sys.exit("ERROR - current cell list does not match ADNACOPY cell list")

    # Add colors:
    #flatui = ["#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71", "#800080", "#FF00FF", "#000080",  "#0000FF", "#008080", "#00FFFF", "#008000", "#00FF00", "#808000", "#FFFF00", "#800000", "#FF0000", "SALMON", "INDIANRED", "#BD8932", "BD5432", "#5ABD32", "#13F39E", "#13F3DF", "#13A8F3", "#136FF3","#5A13F3","#B613F3","#F313E5","#F31386","#F3133C","#ECF313","#386E54","#399874"] #"#7B3998"] #sns.palplot(sns.color_palette("colorblind", 35))
    #df['COLOR_MAP']= flatui
    mast_df = mast_df.append(df)
    return mast_df

###########################
# USAGE
if len(sys.argv) != 4:
    sys.stderr.write("Usage: " + sys.argv[0] + " <CLONE (clone1 or clone2> <OUTDIR> <MEAN_MED_MODE (e.g. mean, median, mode>\n")
    sys.exit()

###########################
# ASSIGNMENTS
CLONE = str(sys.argv[1])
OUTDIR = str(sys.argv[2])
if not os.path.exists(OUTDIR):
    os.makedirs(OUTDIR)
MEAN_MED_MODE = str(sys.argv[3])

if __name__ == '__main__':
    #df_master = pd.DataFrame(columns = ['Cell'])
    
    files_ACP = [f for f in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/aneuFout/precision_recall_dict_"+CLONE+"_"+"mergedRefcloneN"+"_*"+"AneuCP/prec_recallVals*.csv")]
    
    files_AHMM = [f for f in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/aneuFout/precision_recall_dict_"+CLONE+"_"+"mergedRefcloneN"+"_*"+"AneuHMM/prec_recallVals*.csv")]
    
    files_G = [f for f in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/precision_recall_dict_"+CLONE+"_*Ginkgo/prec_recallVals*.csv")]
    
    # Create df_master_pn for TP, TN, FP, FN Precision, Recall, F1 for all method for all bin sizes
    # df_master_pn is cells by [TP, TN, FP, FP, Precision, Recall, F1, BINSIZE, METHOD] for all cells
    # Note that there are 665 rows because 35 cells 7 bin sizes for ACP and AHMM and 5 for Ginkgo
    
    iters = 0
    for acp in files_ACP:
        if iters == 0:
            bin_size = int(acp.split("_")[-4])
            df_master_pn = pd.read_csv(acp)
            df_master_pn['BINSIZE']=bin_size
            df_master_pn['METHOD'] = 'AneuCP'
            iters+=1 
        else:
            bin_size = int(acp.split("_")[-4])
            df_temp = pd.read_csv(acp)
            df_temp['BINSIZE']=bin_size
            df_temp['METHOD'] = 'AneuCP'
            df_master_pn = df_master_pn.append(df_temp,ignore_index=True)
    
   
    df_master = df_master_pn[['Cell','BINSIZE']] # to fill later for precision recall master_df
    df_master['Cell'] = df_master['Cell'].str.split('.').str[0]
    for ahm in files_AHMM:
        bin_size = int(ahm.split("_")[-4])
        df_temp = pd.read_csv(ahm)
        df_temp['BINSIZE']=bin_size
        df_temp['METHOD'] = 'AneuHMM'
        df_master_pn = df_master_pn.append(df_temp,ignore_index=True)
    
    for g in files_G:
        bin_size = int(g.split("_")[-4])
        df_temp = pd.read_csv(g)
        df_temp['BINSIZE']=bin_size
        df_temp['METHOD'] = 'Ginkgo'
        df_master_pn = df_master_pn.append(df_temp,ignore_index=True)
    
    df_master_pn['Precision'] = (df_master_pn.TP/(df_master_pn.TP+df_master_pn.FP)) # Precision
    df_master_pn['Recall'] = (df_master_pn.TP/(df_master_pn.TP+df_master_pn.FN)) # Recall
    df_master_pn['F1'] = ((2*df_master_pn.TP)/((2*df_master_pn.TP)+df_master_pn.FP+df_master_pn.FN)) # F1 score
    df_master_pn['Cell'] = df_master_pn['Cell'].str.split('.').str[0]
    df_master_pn.to_csv(OUTDIR+"/TPTNFPFN_PRF1_allcells_3methods_"+CLONE+"__plot_PR.csv")
    
    # Create df_master for Precision, Recall, F1 in different form for all method for all bin sizes
    # df_master is cells by [binsize, AneuCP_Precision, AneuCP_Recall, AneuCP_F1, etc. for HMM and Ginkgo]
    # Note that there are 245 rows because 35 cells 7 bin sizes for ACP and AHMM NOTE NA DENOTED FOR 40KB AND 30KB for Ginkgo
    
    # cellMedF1: cell with median F1 score for each bin size
    # cellMeaF1: cell with mean F1 score for each bin size
    # cellModF1: cell with mode F1 score for each bin size
    
    #### AneuCP ####
    df_ACP = df_master_pn[df_master_pn['METHOD'] == 'AneuCP'] 
    df_ACP_pr = df_ACP[df_ACP.Cell.str.contains('mer') & ~df_ACP.Cell.str.contains("down")] # adding merged cells for all bin sizes to precision recall df
    df_ACP_cell = df_ACP[df_ACP.Cell.str.contains('cell') & ~df_ACP.Cell.str.contains('merg')] # collect all 30 true cells
    
    list_binsizes = [250000,100000,50000,40000,30000,25000,10000]
    for bs in list_binsizes:
        #print "bin size: ",bs
        df_temp = df_ACP_cell[df_ACP_cell['BINSIZE']==bs]
        if MEAN_MED_MODE == 'Mean':
            m_val = df_temp['F1'].mean()
        elif MEAN_MED_MODE == 'Median':
            m_val = df_temp['F1'].median()
        elif MEAN_MED_MODE == 'Mode':
            m_val = df_temp['F1'].mode()
        else: 
            sys.exit("ERROR - must ender Mean Median or Mode as last argument")
        row_m = df_temp.iloc[(df_temp['F1']-m_val).abs().argsort()[:1]]
        df_ACP_pr = df_ACP_pr.append(row_m,ignore_index=True)
    
    # Splitting ACP dataframe to plot
    df_ACP_pr['Cell'] = df_ACP_pr.Cell.str.replace(r'(^.*matPat.*$)','cellMedF1_clone1')
    df_ACP_mer = df_ACP_pr[df_ACP_pr.Cell.str.contains('merg')]
    df_ACP_mer = df_ACP_mer.round(3)
    df_ACP_cellF1 = df_ACP_pr[~df_ACP_pr.Cell.str.contains('merg')]
    df_ACP_cellF1 = df_ACP_cellF1.round(3)  
    #SORT BY BINSIZE
    df_ACP_mer_sorted = df_ACP_mer.sort_values(['BINSIZE'], ascending=False)
    print df_ACP_mer_sorted
    df_ACP_cellF1_sorted = df_ACP_cellF1.sort_values(['BINSIZE'], ascending=False)
    print df_ACP_cellF1_sorted
    
    #### AneuHMM ####
    df_AHMM = df_master_pn[df_master_pn['METHOD'] == 'AneuHMM'] 
    df_AHMM_pr = df_AHMM[df_AHMM.Cell.str.contains('mer') & ~df_AHMM.Cell.str.contains("down")] # adding merged cells for all bin sizes to precision recall df
    df_AHMM_cell = df_AHMM[df_AHMM.Cell.str.contains('cell') & ~df_AHMM.Cell.str.contains('merg')] # collect all 30 true cells
    
    for bs in list_binsizes:
        #print "bin size: ",bs
        df_temp = df_AHMM_cell[df_AHMM_cell['BINSIZE']==bs]
        if MEAN_MED_MODE == 'Mean':
            m_val = df_temp['F1'].mean()
        elif MEAN_MED_MODE == 'Median':
            m_val = df_temp['F1'].median()
        elif MEAN_MED_MODE == 'Mode':
            m_val = df_temp['F1'].mode()
        else: 
            sys.exit("ERROR - must ender Mean Median or Mode as last argument")
        row_m = df_temp.iloc[(df_temp['F1']-m_val).abs().argsort()[:1]]
        df_AHMM_pr = df_AHMM_pr.append(row_m,ignore_index=True)
    
    # Splitting AHMM dataframe to plot
    df_AHMM_pr['Cell'] = df_AHMM_pr.Cell.str.replace(r'(^.*matPat.*$)','cellMedF1_clone1')
    df_AHMM_mer = df_AHMM_pr[df_AHMM_pr.Cell.str.contains('merg')]
    df_AHMM_mer.loc[-1] = ["merged_30cells_clone1",221,177,130,24,250000,"AneuHMM",0.625,0.861,0.742]
    df_AHMM_mer = df_AHMM_mer.round(3)
    df_AHMM_cellF1 = df_AHMM_pr[~df_AHMM_pr.Cell.str.contains('merg')]
    df_AHMM_cellF1 = df_AHMM_cellF1.round(3)
    #SORT BY BINSIZE
    df_AHMM_mer_sorted = df_AHMM_mer.sort_values(['BINSIZE'], ascending=False)
    print df_AHMM_mer_sorted
    df_AHMM_cellF1_sorted = df_AHMM_cellF1.sort_values(['BINSIZE'], ascending=False)
    print df_AHMM_cellF1_sorted
    
    #### Ginkgo ####
    df_G = df_master_pn[df_master_pn['METHOD'] == 'Ginkgo'] 
    df_G_pr = df_G[df_G.Cell.str.contains('mer') & ~df_G.Cell.str.contains("down")] # adding merged cells for all bin sizes to precision recall df
    df_G_cell = df_G[df_G.Cell.str.contains('cell') & ~df_G.Cell.str.contains('merg')] # collect all 30 true cells
    
    for bs in list_binsizes:
        #print "bin size: ",bs
        df_temp = df_G_cell[df_G_cell['BINSIZE']==bs]
        if MEAN_MED_MODE == 'Mean':
            m_val = df_temp['F1'].mean()
        elif MEAN_MED_MODE == 'Median':
            m_val = df_temp['F1'].median()
        elif MEAN_MED_MODE == 'Mode':
            m_val = df_temp['F1'].mode()
        else: 
            sys.exit("ERROR - must ender Mean Median or Mode as last argument")
        row_m = df_temp.iloc[(df_temp['F1']-m_val).abs().argsort()[:1]]
        df_G_pr = df_G_pr.append(row_m,ignore_index=True)
   
    # Splitting Ginkgo dataframe to plot
    df_G_pr['Cell'] = df_G_pr.Cell.str.replace(r'(^.*matPat.*$)','cellMedF1_clone1')
    df_G_mer = df_G_pr[df_G_pr.Cell.str.contains('merg')]
    df_G_mer = df_G_mer.round(3)
    df_G_cellF1 = df_G_pr[~df_G_pr.Cell.str.contains('merg')]
    df_G_cellF1 = df_G_cellF1.round(3)
    #SORT BY BINSIZE
    df_G_mer_sorted = df_G_mer.sort_values(['BINSIZE'], ascending=False)
    print df_G_mer_sorted
    df_G_cellF1_sorted = df_G_cellF1.sort_values(['BINSIZE'], ascending=False)
    print df_G_cellF1_sorted    
    
    #### Plotting ###
    #sns.set_style("darkgrid")
    f, ax = plt.subplots(1, 1)
    #f = sns.pointplot(x="Recall", y="Precision", data=df_ACP_mer, color="blue", join=True, markers='*', s=9) 
    #f = sns.pointplot(x="Recall", y="Precision", data=df_ACP_cellF1, color="blue",join=True, markers='.')
    plt.scatter(df_ACP_mer_sorted['Recall'], df_ACP_mer_sorted['Precision'], color='blue', marker="*", s=40)
    plt.plot(df_ACP_mer_sorted['Recall'], df_ACP_mer_sorted['Precision'], color='blue')
    plt.scatter(df_ACP_cellF1_sorted['Recall'], df_ACP_cellF1_sorted['Precision'],  color='#95d0fc', marker = ".")
    plt.plot(df_ACP_cellF1_sorted['Recall'], df_ACP_cellF1_sorted['Precision'], color='#95d0fc')
    
    plt.scatter(df_AHMM_mer_sorted['Recall'], df_AHMM_mer_sorted['Precision'], color='red', marker="*", s=40)
    plt.plot(df_AHMM_mer_sorted['Recall'], df_AHMM_mer_sorted['Precision'], color='red')
    plt.scatter(df_AHMM_cellF1_sorted['Recall'], df_AHMM_cellF1_sorted['Precision'],  color='#ffd1df', marker = ".")
    plt.plot(df_AHMM_cellF1_sorted['Recall'], df_AHMM_cellF1_sorted['Precision'], color='#ffd1df')
    
    plt.scatter(df_G_mer_sorted['Recall'], df_G_mer_sorted['Precision'], color='green', marker="*", s=40)
    plt.plot(df_G_mer_sorted['Recall'], df_G_mer_sorted['Precision'], color='green')
    plt.scatter(df_G_cellF1_sorted['Recall'], df_G_cellF1_sorted['Precision'],  color='#c7fdb5', marker = ".")
    plt.plot(df_G_cellF1_sorted['Recall'], df_G_cellF1_sorted['Precision'], color='#c7fdb5')
    
    ax.grid()
    #f = sns.pointplot(x="Recall", y="Precision", data=df_AHMM_mer, color="red", join=True, markers='*', s=9) 
    #f = sns.pointplot(x="Recall", y="Precision", data=df_AHMM_cellF1, color="red",join=True, markers='.')
    #f = sns.pointplot(x="Recall", y="Precision", data=df_G_mer, color="green", join=True, markers='*', s=9) 
    #f = sns.pointplot(x="Recall", y="Precision", data=df_G_cellF1, color="green",join=True, markers='.')
    
    #ax.legend(handles=ax.lines[::len(df1)+1], labels=["A","B","C"])
    #hue="Cell",markers='o', join=True)#, linewidth=1), edgecolor='black')
    #plt.plot(x="Recall", df_ACP_mer["Precision", data=df_ACP_mer)#, marker="v" ,s=7, linewidth=1), edgecolor='black')
    
    plt.title("Precision Recall of "+MEAN_MED_MODE+" F1 cell and Merged Cell")
    #plt.legend()
    ax.tick_params(axis='x', rotation=30)
    #axes = plt.gca()
    #ax.set_xlim([0, 1])
    #ax.set_ylim([0, 1])
    #axes.set_xlim([0,1])
    #axes.set_ylim([0,1])
    plt.ylim(ymin=0,ymax=1)
    #ax.set_xlim(xmin=0, xmax=1)
    plt.xlim(xmin=0,xmax=1)
    #ax.set(ylim=(-1, 1))
    #ax.set(xlim=(-1, 1))
    plt.ylabel("Precision")
    plt.xlabel("Recall")
    #plotted.legend(loc = 'right', ncol=1, prop={'size': 7}, bbox_to_anchor=(2, 0.5))
    #axes = plotted.axes
    #axes[0,0].set_ylim(0,)
    #axes[0,1].set_ylim(0,)
    #plotted.set(ylim=(0, 1))
    #plotted.set(xlim=(0, 1))
    
    
    plt.savefig(OUTDIR+"/"+CLONE+"_presence_absence_PRcurve_3methods_"+MEAN_MED_MODE+"__plot_PR.pdf") #df_ACP_mer
    
    sys.exit()
    
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_mer_d, jitter=True, color='blue', marker="v", s=7, linewidth=1)#, edgecolor='black')
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_mer, jitter=True, color='red', marker="s", s=7, linewidth=1)#, edgecolor='black')
    #plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MEDIAN, jitter=True, color='red', marker="*", s=7, linewidth=1) # 
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MODE, jitter=True, color='red', marker="*", s=7, linewidth=1) # 
    #plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MEAN, jitter=True, color='red', marker="v", s=7, linewidth=1) # 
    END = 'size_somy__plot_compareDifBinSizes.pdf'

    plt.title("%s" % BINSIZE_KB)
    plt.ylabel('F1')
    plt.xlabel(BIN_SIZE)
    plotted.legend(loc = 'right', ncol=1, prop={'size': 7}, bbox_to_anchor=(2, 0.5))
    plt.ylim(-0.1, 1.05)

    print "Saving .pdf plot here: ", OUTDIR+"/"+CLONE+"_"+BINSIZE_KB+"_"+"_plot__plot_F1.pdf"
    plt.savefig(OUTDIR+"/"+CLONE+"_"+BINSIZE_KB+"_"+"_plot__plot_F1.pdf")
    
    sys.exit()

    #(df_master_pn.TP/(df_master_pn.TP+df_master_pn.FP)) # Precision
    #df_master['AneuCP_R']=(df_master_pn.TP/(df_master_pn.TP+df_master_pn.FN)) # Recall
    #df_master['AneuCP_F1']=((2*df_master_pn.TP)/((2*df_master_pn.TP)+df_master_pn.FP+df_master_pn.FN)) # F1 score
    #df_master['BINSIZE']=bin_size

    #master_df = pd.DataFrame(columns=('ID','CELL_NAME', 'NORM_X-SOMY_BINS'))
    df_master = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])
    df_master_mer_d = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])
    df_master_mer = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])
    df_master_MODE = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])
    df_master_MEDIAN = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])
    df_master_MEAN = pd.DataFrame(columns = ['Cell','F1', 'METHOD'])

    # ACP
    df_ACP = pd.read_csv(DATA_ACP)
    df_ACP['F1']=((2*df_ACP.TP)/((2*df_ACP.TP)+df_ACP.FP+df_ACP.FN)) # F1 score
    df_ACP['METHOD']='Aneufinder_CP'

    df_ACP_cells = df_ACP[~df_ACP.Cell.str.contains('mer') & ~df_ACP.Cell.str.contains("M")].reset_index() # all 30 cells
    df_ACP_mer_d = df_ACP[df_ACP.Cell.str.contains('mer') & df_ACP.Cell.str.contains("down")] # merged downsampled
    df_ACP_mer = df_ACP[df_ACP.Cell.str.contains('mer') & ~df_ACP.Cell.str.contains("down")] 
    df_ACP_MEAN = df_ACP[ df_ACP.Cell.str.contains("MEAN")]
    df_ACP_MEDIAN = df_ACP[ df_ACP.Cell.str.contains("MEDIAN")]
    df_ACP_MODE = df_ACP[ df_ACP.Cell.str.contains("MODE")]

    # print"\ndf_ACP_cells", df_ACP_cells
    # print "\ndf_ACP_mer_d", df_ACP_mer_d
    # print "\ndf_ACP_MEAN", df_ACP_MEAN
    # print "\ndf_ACP_MEDIAN", df_ACP_MEDIAN
    # print "\ndf_ACP_MODE", df_ACP_MODE
    #(df_ACP.TP+df_ACP.TN) /(df_ACP.TP+df_ACP.TN+df_ACP.FP+df_ACP.FN))
    
    # AHMM
    df_AHMM = pd.read_csv(DATA_AHMM)
    df_AHMM['F1']=((2*df_AHMM.TP)/((2*df_AHMM.TP)+df_AHMM.FP+df_AHMM.FN)) # F1 score
    df_AHMM['METHOD']='Aneufinder_HMM'
    
    df_AHMM_cells = df_AHMM[~df_AHMM.Cell.str.contains('mer') & ~df_AHMM.Cell.str.contains("M")].reset_index() # all 30 cells
    df_AHMM_mer_d = df_AHMM[df_AHMM.Cell.str.contains('mer') & df_AHMM.Cell.str.contains("down")] # merged downsampled
    df_AHMM_mer = df_AHMM[df_AHMM.Cell.str.contains('mer') & ~df_AHMM.Cell.str.contains("down")]
    df_AHMM_MEAN = df_AHMM[ df_AHMM.Cell.str.contains("MEAN")]
    df_AHMM_MEDIAN = df_AHMM[ df_AHMM.Cell.str.contains("MEDIAN")]
    df_AHMM_MODE = df_AHMM[ df_AHMM.Cell.str.contains("MODE")]

    # print"\ndf_AH_cells", df_AHMM_cells
    # print "\ndf_AH_mer_d", df_AHMM_mer_d
    # print "\ndf_AH_MEAN", df_AHMM_MEAN
    # print "\ndf_AH_MEDIAN", df_AHMM_MEDIAN
    # print "\ndf_AH_MODE", df_AHMM_MODE
    
    # GINKGO
    if DATA_GINKGO !='NA':
        df_G = pd.read_csv(DATA_GINKGO)
        df_G['F1']=((2*df_G.TP)/((2*df_G.TP)+df_G.FP+df_G.FN)) # F1 score
        df_G['METHOD']='Ginkgo'
        df_G_cells = df_G[~df_G.Cell.str.contains('mer') & ~df_G.Cell.str.contains("M")].reset_index() # all 30 cells
        df_G_mer_d = df_G[df_G.Cell.str.contains('mer') & df_G.Cell.str.contains("down")] # merged downsampled
        df_G_mer = df_G[df_G.Cell.str.contains('mer') & ~df_G.Cell.str.contains("down")] # merged downsampled
        df_G_MEAN = df_G[ df_G.Cell.str.contains("MEAN")]
        df_G_MEDIAN = df_G[ df_G.Cell.str.contains("MEDIAN")]
        df_G_MODE = df_G[ df_G.Cell.str.contains("MODE")]

        # print"\ndf_G_cells", df_G_cells
        # print "\ndf_G_mer_d", df_G_mer_d
        # print "\ndf_G_MEAN", df_G_MEAN
        # print "\ndf_G_MEDIAN", df_G_MEDIAN
        # print "\ndf_G_MODE", df_G_MODE

    
    df_master = df_master.append(df_ACP_cells[['Cell','F1','METHOD']], ignore_index=True)
    df_master = df_master.append(df_AHMM_cells[['Cell','F1','METHOD']], ignore_index=True)
    
    df_master_mer_d = df_master_mer_d.append(df_ACP_mer_d[['Cell','F1','METHOD']], ignore_index=True)
    df_master_mer_d = df_master_mer_d.append(df_AHMM_mer_d[['Cell','F1','METHOD']], ignore_index=True)
    
    df_master_mer = df_master_mer.append(df_ACP_mer[['Cell','F1','METHOD']], ignore_index=True)
    df_master_mer = df_master_mer.append(df_AHMM_mer[['Cell','F1','METHOD']], ignore_index=True)
    
    df_master_MEAN = df_master_MEAN.append(df_ACP_MEAN[['Cell','F1','METHOD']], ignore_index=True)
    df_master_MEAN = df_master_MEAN.append(df_AHMM_MEAN[['Cell','F1','METHOD']], ignore_index=True)
    
    df_master_MEDIAN = df_master_MEDIAN.append(df_ACP_MEDIAN[['Cell','F1','METHOD']], ignore_index=True)
    df_master_MEDIAN = df_master_MEDIAN.append(df_AHMM_MEDIAN[['Cell','F1','METHOD']], ignore_index=True)
    
    df_master_MODE = df_master_MODE.append(df_ACP_MODE[['Cell','F1','METHOD']], ignore_index=True)
    df_master_MODE = df_master_MODE.append(df_AHMM_MODE[['Cell','F1','METHOD']], ignore_index=True)
    
    if DATA_GINKGO != 'NA':
        df_master = df_master.append(df_G_cells[['Cell','F1','METHOD']], ignore_index=True)
        df_master_mer_d = df_master_mer_d.append(df_G_mer_d[['Cell','F1','METHOD']], ignore_index=True)
        df_master_mer = df_master_mer.append(df_G_mer[['Cell','F1','METHOD']], ignore_index=True)
        df_master_MEAN = df_master_MEAN.append(df_G_MEAN[['Cell','F1','METHOD']], ignore_index=True)
        df_master_MEDIAN = df_master_MEDIAN.append(df_G_MEDIAN[['Cell','F1','METHOD']], ignore_index=True)
        df_master_MODE = df_master_MODE.append(df_G_MODE[['Cell','F1','METHOD']], ignore_index=True)
        
    # Save df_master combining all '3' methods
    #df_master.to_csv(OUTDIR+"/"+CLONE+"_"+BINSIZE_KB+"_"+"_dfmaster__plot_F1.csv")
    #df_merge_downsamp = df_master[df_master.Cell.str.contains("downsample")]
    #df_merge = df_master[df_master["Cell"].str.contains('merge') & ~df_master["Cell"].str.contains('downsample')]
    #df_master = df_master[~df_master["Cell"].str.contains('merge')]
    #print df_merge_downsamp
    print "df_master_mer_d", df_master_mer_d
    print "df_master_mer", df_master_mer
    print "df_master_MODE", df_master_MODE
    print "df_master_MEDIAN", df_master_MEDIAN
    print "df_master_MEAN", df_master_MEAN

    #master_df['LABEL'] = master_df.apply(change_color, axis=1)
    #master_df.to_csv(OUTPUT_NAME+"_"+BIN_SIZE+"size_4methods__plot_compareFreqXsomy.csv")
    #print "Saved master dataframe as: ", OUTPUT_NAME+"_"+BIN_SIZE+"size_4methods__plot_compareFreqXsomy.csv"

    # COLORFUL PLOT TO DISTINGUISH EACH CELL
    #master_df = master_df[(master_df['CELL_NAME'] != 'merged_cell_T5_normal_not2512_not2324_not2498_pt1')]
    #print "MASTER ", master_df
    sns.set_style("darkgrid")

    plotted = sns.boxplot(x="METHOD", y="F1", data=df_master)
    plotted = sns.stripplot(x="METHOD", y="F1", hue="Cell", data=df_master, jitter=True, c=df_master['Cell'], s=7, linewidth=1)#, edgecolor='black')
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_mer_d, jitter=True, color='blue', marker="v", s=7, linewidth=1)#, edgecolor='black')
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_mer, jitter=True, color='red', marker="s", s=7, linewidth=1)#, edgecolor='black')
    #plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MEDIAN, jitter=True, color='red', marker="*", s=7, linewidth=1) # 
    plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MODE, jitter=True, color='red', marker="*", s=7, linewidth=1) # 
    #plotted = sns.stripplot(x="METHOD", y="F1", data=df_master_MEAN, jitter=True, color='red', marker="v", s=7, linewidth=1) # 
    END = 'size_somy__plot_compareDifBinSizes.pdf'

    plt.title("%s" % BINSIZE_KB)
    plt.ylabel('F1')
    plt.xlabel(BIN_SIZE)
    plotted.legend(loc = 'right', ncol=1, prop={'size': 7}, bbox_to_anchor=(2, 0.5))
    plt.ylim(-0.1, 1.05)

    print "Saving .pdf plot here: ", OUTDIR+"/"+CLONE+"_"+BINSIZE_KB+"_"+"_plot__plot_F1.pdf"
    plt.savefig(OUTDIR+"/"+CLONE+"_"+BINSIZE_KB+"_"+"_plot__plot_F1.pdf")
    #sns.plt.show()
    
    """# AneuFinder CP
    # Adding precision
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuCP', ['Cell','BINSIZE','Precision']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Precision':'AneuCP_P'})

    # Adding recall
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuCP', ['Cell','BINSIZE','Recall']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Recall':'AneuCP_R'})

    # Adding F1
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuCP', ['Cell','BINSIZE','F1']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'F1':'AneuCP_F1'})

    #print df_master
    #print "\nNEXT\n"
    
    # AneuFinder HMM
    # Adding precision
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuHMM', ['Cell','BINSIZE','Precision']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Precision':'AneuHMM_P'})

    # Adding recall
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuHMM', ['Cell','BINSIZE','Recall']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Recall':'AneuHMM_R'})

    # Adding F1
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'AneuHMM', ['Cell','BINSIZE','F1']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'F1':'AneuHMM_F1'})
    
    # AneuFinder Ginkgo
    # Adding precision
    df_40kb_30kb_filled_Ginkgo = df_master_pn[df_master_pn['METHOD'] == 'Ginkgo']
    df_40kb_30kb_filled_Ginkgo['METHOD']
    print df_40kb_30kb_filled_Ginkgo
    sys.exit()
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'Ginkgo', ['Cell','BINSIZE','Precision']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Precision':'Ginkgo_P'})

    # Adding recall
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'Ginkgo', ['Cell','BINSIZE','Recall']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'Recall':'Ginkgo_R'})

    # Adding F1
    df_toadd = df_master_pn.loc[df_master_pn['METHOD'] == 'Ginkgo', ['Cell','BINSIZE','F1']]
    df_master = pd.merge(df_master, df_toadd, on=['BINSIZE', 'Cell']) 
    df_master = df_master.rename(columns = {'F1':'Ginkgo_F1'})
    print df_master
    """