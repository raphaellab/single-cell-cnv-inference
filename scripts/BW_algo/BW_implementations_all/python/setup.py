from distutils.core import setup, Extension

module1 = Extension('HMM_C', sources = ['HMM_C.c'])

setup (name = 'HMM_C',
                 version = '1.0',
                 description = 'HMM filtering / smoothing utilities',
                 ext_modules = [module1])
#print(setup_options)
#setup(**setup_options)

#export CFLAGS="-I /Library/Python/2.7/site-packages/numpy/core/include $CFLAGS"
