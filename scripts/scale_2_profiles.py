#!/usr/bin/python
#
# scale_2_profiles.py : Scale one copy-number profile into another. 
#	
# Ashley Conard
# Last Modified: June 7, 2017

from __future__ import division
import seaborn as sns
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os
from scipy.stats import mode


def plot_profiles_and_differences(dataframe, cond):

	fig,ax=plt.subplots(1,figsize=(12,3))
	for index,row in df.iterrows():
		#plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (row["GINKGO"],row["GINKGO"]), "|-", c="g", markersize=10, linewidth=.1)
	for index,row in df.iterrows():
		#plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (row["ANEUF"],row["ANEUF"]), "|-", c="b", markersize=10, linewidth=.1)
	
#	df.GINKGO.plot(style='g-', label='Ginkgo')
#	df.ANEUF.plot(style='b-', label='AneuFinder')
#	df.ANEUF.plot(style='_', c='b', label='AneuFinder', markersize=4)
#	df.GINKGO.plot(style='_', c='g', label='Ginkgo', markersize=4)
	plt.legend(loc='upper right')
	plt.ylim(-0.9, 10)
	plt.xlim(0)
	plt.title("%s %s Copy Number Profiles" % (CELL_NAME, cond))
	plt.xlabel('Bin')
	plt.ylabel('Integer Copy-Number')
	plt.savefig('%s_%s_copy-number-profiles_points.pdf' % (CELL_NAME, cond))
	plt.close(fig)

	
	fig,ax=plt.subplots(1,figsize=(12,3))
	for index,row in df.iterrows():
	#	plt.plot(x=index-0.3, y=row['GINKGO'], c='b', style='o')
		plt.plot((index-0.5, index+0.5), (abs(row["GINKGO"]-row["ANEUF"]),abs(row["GINKGO"]-row["ANEUF"])), "|-", c="r", markersize=10, linewidth=.1)
		#plt.plot((index-0.5, index+0.5), (abs(row["GINKGO"]-row["ANEUF"]),abs(row["GINKGO"]-row["ANEUF"])), "--", c="r", linewidth=6)
	# df.GINKGO.plot(style='g-', label='Ginkgo')
	# df.ANEUF.plot(style='b-', label='AneuFinder')
#	df.ANEUF.plot(style='_', c='b', label='AneuFinder', markersize=4)
#	df.GINKGO.plot(style='_', c='g', label='Ginkgo', markersize=4)
	plt.legend(loc='upper right')
	plt.ylim(-0.9, 10)
	plt.xlim(0)
	plt.title("%s %s Copy Number Profiles" % (CELL_NAME, cond))
	plt.xlabel('Bin')
	plt.ylabel('Integer Copy-Number')
	plt.savefig('%s_%s_overlap_copy-number-profiles_points.pdf' % (CELL_NAME, cond))
	plt.close(fig)


###########################
# USAGE
if (len(sys.argv) !=3):
    sys.stderr.write("Usage: " + sys.argv[0] +" PATIENT_ANEUFVSGINKGO_FILE (e.g.: T7_250kb_getting220kb_SRR3083650_1.fastq.gz.csv)"+ "OUTPUT_NAME (e.g. T7_220kb_SRR3083650)\n")
    sys.exit(1)

# ASSIGNMENTS
FILE = str(sys.argv[1])
CELL_NAME = str(sys.argv[2]) 
# if not os.path.exists(OUTDIR):
#    os.makedirs(OUTDIR)

if __name__ == '__main__':
	df = pd.read_csv(FILE,sep=',')
	
	# Plot the copy-number profiles and the differences before scaling by the mode of the more amplified copy-number profile 
	CONDITION = "Not_Scaled"
	plot_profiles_and_differences(df, CONDITION)
	
	# Find the mode of both inferred copy-number profiles (from both methods)
	mode_ginkgo = df['GINKGO'].value_counts().idxmax()
	mode_aneufinder = df['ANEUF'].value_counts().idxmax()

	print "modes: g ",mode_ginkgo, "a ", mode_aneufinder
	print "dataframe size", df.shape[0]

	# Find the mode and scale and plot accordingly
	if mode_ginkgo > mode_aneufinder:
		print "Ginkgo has more amplifications"
		df["ANEUF"]+= abs(mode_ginkgo - mode_aneufinder)
		CONDITION = "Scaled_AneuFinder"
		plot_profiles_and_differences(df, CONDITION) # Aneufinder result was scaled up to Ginkgo
		df[2:].to_csv("%s_%s.csv" % (CELL_NAME, CONDITION))

	elif mode_ginkgo < mode_aneufinder:
		print "AneuFinder has more amplifications"
		df["GINKGO"] += abs(mode_ginkgo - mode_aneufinder)
		CONDITION = "Scaled_Ginkgo"
		plot_profiles_and_differences(df, CONDITION) # Ginkgo result was scaled up to Aneufinder
		df[2:].to_csv("%s_%s.csv" % (CELL_NAME, CONDITION))
	else:
		print "Same size"
		sys.exit()



