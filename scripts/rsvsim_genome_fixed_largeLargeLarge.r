# Ashley Conard
# rsvsim_genome - simulates structural variation events in the genome (hg19 reference)
# Last update : Sept 4, 2017
# Fantastic Tutorial for RSVSim : https://rdrr.io/bioc/RSVSim/man/simulateSV.html#heading-8

# To install
#source("https://bioconductor.org/biocLite.R")
#biocLite("RSVSim")
#biocLite("BSgenome.Hsapiens.UCSC.hg19") # Default genome is hg19

# Libraries
library(RSVSim)
library("BSgenome.Hsapiens.UCSC.hg19")
library("BSgenome.Hsapiens.UCSC.hg19.masked")

# size of events (drawn from beta with estimateSVSizes)
#size_del = estimateSVSizes(n=60, minSize=1000, maxSize=1000000, hist=TRUE, default="deletions")
#size_dup = estimateSVSizes(n=40, minSize=10000, maxSize=1000000, hist=TRUE, default='insertions')
#size_ins = estimateSVSizes(n=50, minSize=10000, maxSize=1000000, hist=TRUE, default="insertions")

# deletions (drawn from beta), insertions (drawn from beta), duplication (drawn from beta)
#sim = simulateSV(output=".", dels=60, ins=50, dups=40, sizeDels=size_del, sizeIns=size_ins, sizeDups=size_ins)# percCopiedIns=0.5)

knownDeletion = c(GRanges(IRanges(5000000,10000000), seqnames="chr11"), GRanges(IRanges(500000,1000000), seqnames="chr2"), GRanges(IRanges(80000,880000), seqnames="chr5"))
names(knownDeletion) = c("knownD1","knownD2","knownD3")


# knownInsertions
knownInsertion = c(GRanges(IRanges(2000000,3000000), seqnames='chr1', chrB='chr4', startB = 1000000), GRanges(IRanges(1000000,11000000), seqnames='chr16', chrB='chr18', startB = 200000))
#knownInsertion = GRanges(IRanges(50,1000050), seqnames='chr1', chrB='chr4', startB = 1000)
#names(knownInsertion) = "knownI1"
names(knownInsertion) =  c("knownI1","knownI2")

#knownDuplications
#knownDuplication = c(GRanges(IRanges(1000000,1020000), seqnames='chr6', maxDups=3), GRanges(IRanges(2000000,2040000), seqnames='chr7', maxDups=2))
#names(knownDuplication) = c("knownDu1", "knownDu2")
knownDuplication = c(GRanges(IRanges(2000000,12000000), seqnames='chr10', maxDups=9))
names(knownDuplication) = c("knownDu1")

sim = simulateSV(output=".", regionsIns=knownInsertion, regionsDels=knownDeletion, regionsDups=knownDuplication, seed=3, random=FALSE, verbose=FALSE)
metadata(sim)

## Setting the weights for SV formation mechanism and repeat biases demands a given data.frame structure
## The following weights are the default settings
## Please make sure your data.frames have the same row and column names, when setting your own weights
data(weightsMechanisms, package="RSVSim")
weightsMechanisms
data(weightsRepeats, package="RSVSim")