#!/usr/bin/python

# Forward Backward Algorithm 
# Ashley Conard
# 

#
# fb_algo.py
# To address: 
#   changed - hard coded bound on allowed repeated copy number for a segment. Allowed range now: 0-20 with 30 max different copy number changes
#        ok - NB regression and overdispersion param. 
#           - when CN is 0 <- still add noise...
#           - use the nbCNV simulated or 12 patient Gao.
#           - determine chrom. percent based on haplo or diplo?
#           - bin coverage
#           - None, dizzy, cold <- BKW no normal?

# To Do:     
#           - not using genome length (total number of copy-number states)
#           - BW -> https://www.math.univ-toulouse.fr/~agarivie/Telecom/code/index.php
#           - R BW -> https://cran.r-project.org/web/packages/HMM/HMM.pdf
#           -> compare!!! -> FB to R FB~
#           - normalization: http://www.analytictech.com/ba762/handouts/normalization.htm
#           - http://www.math.uah.edu/stat/apps/SpecialCalculator.html
#           - explanation of dnbinom: http://stats.stackexchange.com/questions/125477/negative-binomial-distribution-r
#           - helpful for negative binomial simulation! http://www.math.uah.edu/stat/bernoulli/NegativeBinomial.html

# Notes:
#           diplo_len = 3095693981 # normal diploid genome length
#                       (3407692*50)/1547846990.5 = 0.1 <- coverage = (read_len * num_reads) / haplo_geno
#           chr_num_reads = diplo_len*(chr_chosen_perc_reads/100) # genome length
#                       3407692 average num reads from WGS dataset (Gao et al. 2016)
#           Get num bins at 50KB bins (as in simulations for nbCNV, CNVnator, and control-FREEC)
#           n_bins = int(float("%.2f" % round(chr_num_reads/220000,0)))  #or 50 000 in nbCNV
#           read_matrix_norm = [] # matrix RD normalized (reads/())
#           read_matrix_coverage = [] # matrix RD coverage (reads/bin_len) * read_len

# Cluster/Dendo:
#           - blog used : https://joernhees.de/blog/2015/08/26/scipy-hierarchical-clustering-and-dendrogram-tutorial/#comment-6215
#           - fcluster work : http://stackoverflow.com/questions/7724635/clustering-with-scipy-clusters-via-distance-matrix-how-to-get-back-the-origin?rq=1
#           - used for explanation : http://mathoverflow.net/questions/38855/nodes-clusters-with-a-distance-matrix
#           - potentially the answer: using distance matrix with linkage: http://stackoverflow.com/questions/18952587/use-distance-matrix-in-scipy-cluster-hierarchy-linkage
#           - annotation with distance between cells: http://stackoverflow.com/questions/11917779/how-to-plot-and-annotate-hierarchical-clustering-dendrograms-in-scipy-matplotlib

# Commands
# python fb_algo.py 3 3 5 21

from __future__ import division
from datadiff import diff
import seaborn as sns
import matplotlib.pyplot as plt
import math as m
import sys
from scipy.special import gamma#ln
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import subprocess
from scipy.cluster.hierarchy import fcluster
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

def create_clone_transition_mat(n_b, transition_probability):
    c = [2] # create a new clone

    for i in range(0, n_b-1):
        # draw from uniform distribution
        draw = np.random.uniform(0,1)
        # create cumulative list of probabilities
        cum_l = [] # cumulative list of probability values (roulette wheel)
        for idx,e in enumerate(transition_probability[c[-1]].values()):
            if idx == 0:
                cum_l.append((0,e))
            elif idx!=6: # do not consider the 'E' state
                cum_l.append((cum_l[-1][1],cum_l[-1][1]+e))
        #print "cum_l for CN",c[-1],"is ", cum_l
        # FLOATING POINT ISSUES: ENSURE THAT LAST ENTRY IS 1.
        cum_l[-1] = (cum_l[-1][0], 1.0)
        for ide,ii in enumerate(cum_l):
            minim, maxim = ii
            if minim <= draw <= maxim:
                #print "draw: ", draw
                #print "next CN: ", idx
                c.append(ide)
    return c

def create_clones_assign_cells(m_cells,n_bins,k_clones,h_cn_bound, transition_probability): # Create the true clones (ground truth copy number profiles for each clone)
    clones = []

    for i in range(0,k_clones-1):
        new_clone = create_clone_transition_mat(n_bins, transition_probability)
        clones.append(new_clone)

    c_dup = [2]*n_bins # diploid case
    clones.append(c_dup)

    # Assign cells to clones
    assign_cells = np.random.random_integers(0, k_clones-1, m_cells) # indices are the cells
    return clones, assign_cells

def random_sample_NB(sampl_reads, alp):
    command = 'Rscript'
    path2script = '/Users/ashleymaeconard/Desktop/FB_algo/r_nb_readDepth.r'
    args = [sampl_reads, alp]
    cmd = [command, path2script] + args
    nb_reads = int(subprocess.check_output(cmd, universal_newlines=True))
    return nb_reads

def neg_binom_function(obs, mu, alpha):
    mu_calc = (mu/(mu+alpha))**obs
    size_calc = (alpha/(mu+alpha))**alpha
    return ((gamma(obs+alpha)/(m.factorial(obs)*gamma(alpha))) * mu_calc * size_calc)

def run_FB_one_cell(observations, transition_probability, mean_read_counts):
    h_bound=6 # 0-5
    start_probability = {}
    
    # Hidden states : Z = {0, ..., h} 
    states = list(range(h_bound)) # states
    
    # a_0 : all equal
    for i in list(range(h_bound)):
        start_probability[i] = (1/h_bound)
    print "\nInitial Probability: ", start_probability

    # end_st
    end_state = 'E'

    print "RD Observations: ", observations
    print "Mean Read Counts:"
    pprint.pprint(mean_read_counts)

    print "Transition Probability: "
    pprint.pprint(transition_probability)

    print "\nBeginning FB Algorithm"

    return fwd_bkw(observations,
                       states,
                       start_probability,
                       transition_probability,
                       mean_read_counts,
                       end_state)

LOOK_UP_EMISSION = {}
def get_emission_prob(states, state, x_is, e_m, alpha):
    commands = 'Rscript'
    scriptpaths = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'

    # Only do this when mu_i not in LOOK_UP_EMISSION
    mu = e_m[state]
    #print "(x_is, mu, alpha): ", x_is, mu, alpha
    if (x_is, mu, alpha) not in LOOK_UP_EMISSION:
        total_sum = 0.0
        for st in states:
            mu_i = e_m[st]
            #print "mu_i", mu_i
            if mu_i==0.0: # in the case of mean of 0.0 reads for CN 0
                argss = [str(x_is),str(1),ALPHA] # observed RD_i, mean RD_i with 1 read for computation, alpha
            else:
                argss = [str(x_is),str(mu_i),ALPHA] # observed RD_i, mean RD_i, alpha
            cmds = [commands, scriptpaths] + argss
            LOOK_UP_EMISSION[(x_is, mu_i, alpha)] = float(subprocess.check_output(cmds, universal_newlines=True))
            #print "LOOK_UP_EMISSION[(x_is, mu_i, alpha)]=", LOOK_UP_EMISSION[(x_is, mu_i, alpha)]
            total_sum += LOOK_UP_EMISSION[(x_is, mu_i, alpha)]
            #print "total_sum", total_sum
        assert total_sum != 0

        # Normalizing with respect to all CN state probabilities for that observation
        for st in states:
            mu_i = e_m[st]
            #print "before normalization:LOOK_UP_EMISSION[(x_is, mu_i, alpha)] = ", LOOK_UP_EMISSION[(x_is, mu_i, alpha)]

            LOOK_UP_EMISSION[(x_is, mu_i, alpha)] = LOOK_UP_EMISSION[(x_is, mu_i, alpha)] / total_sum
            #print "after LOOK_UP_EMISSION[(x_is, mu_i, alpha)=", LOOK_UP_EMISSION[(x_is, mu_i, alpha)]
    #print "LOOK UP EMISSION: ", LOOK_UP_EMISSION
    return LOOK_UP_EMISSION[(x_is, mu, alpha)]

def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
    fwd = []
    f_prev = {}

    # Forward part of algorithm
    print "\nFORWARDS"
    for i, x_i in enumerate(x): # for every observation
        print "\nbin i, observation x_i: ", i, x_i
        f_curr = {}
        for st in states: # for every CN state
            print "CN state", st
            if i == 0:
                print "base (prev_f_sum): ", a_0[st]
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                for k in states:
                    print "k: ", "sum", f_prev[k], "*", a[k][st]
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
            
            # calculate NB emission probability
            prob_emis = get_emission_prob(states, st, x_i, e, ALPHA)
            print "NB(x,mu,alpha)", x_i, e[st], ALPHA, "= norm prob_emis", prob_emis

            f_curr[st] = prob_emis * prev_f_sum
            print "f_curr[st] = prob_emis * prev_f_sum: ",f_curr[st], "=",prob_emis, "*", prev_f_sum 

        # Normalize f_curr
        #sum_f_cur = sum(f_curr.values())
        #for k,v in f_curr.items():
        #    f_curr[k] = (v/sum_f_cur)
        
        fwd.append(f_curr)
        f_prev = f_curr

        print "current fwd: ", fwd
        print "\nf_prev = f_curr: ", f_prev, "=", f_curr

    # Termination Step Forward prob
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)

    print "\np_fwd = sum(f_curr[k]*a[k][end_st] for k in states)"
    for k in states:
        print "k: ", k
        print "summing this: f_curr[k]*a[k][end_st]", f_curr[k], "*", a[k][end_st]
        print f_curr[k]*a[k][end_st]
    print "p_fwd: ", p_fwd


    print "\nBACKWARDS"
    bkw = []
    b_prev = {}

    # Backward part of algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+['E'])):
        print "\ni, x_i_plus: ", i, x_i_plus
        b_curr = {}

        for st in states:
            print "state: ", st
            if i == 0: # base case for backward part
                print "base case: ", a[st][end_st]
                b_curr[st] = a[st][end_st]
            else:
                b_curr[st]=0.0 # default setting probability
                for l in states:                    
                    print "\ne [x mean ALPHA]", x_i_plus, e[l], ALPHA
                    
                    # calculate NB emission probability given observation and state
                    prob_emis_b = get_emission_prob(states, l, x_i_plus, e, ALPHA)
                    print "prob_emis", prob_emis_b
                    b_curr[st]+= (a[st][l]*prob_emis_b*b_prev[l])
                    print " b_curr[st]", b_curr, " sum(", a[st][l], "*", prob_emis_b ,"*", b_prev[l],")"

        # Normalize f_curr
        #sum_b_cur = sum(b_curr.values())
        #for key,val in b_curr.items():
        #    b_curr[key] = (val/sum_b_cur)
        
        bkw.insert(0,b_curr)
        b_prev = b_curr

        print "bkw: ", bkw
        print "b_prev = b_curr", b_prev, "=", b_curr

    # Termination Step Backward prob
    print "\np_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)"
    p_bkw=0.0 # default setting first observation probability
    for ll in states:
        print "ll: ", ll

        # calculate NB emission probability for first
        final_b_emis = get_emission_prob(states, ll, x[0], e, ALPHA)
        print "final_backward_emis_prob", final_b_emis
        print "a_0[l]", a_0[l], "e", final_b_emis, "b_curr[ll]", b_curr[ll]
        p_bkw+=(a_0[ll] * final_b_emis * b_curr[ll])
        print "p_bkw=",p_bkw #, "summing this: ", a_0[l], "*", e[l][x[0]], "*", b_curr[l]
    
    print "first observation p_bkw", p_bkw

    # Posterior (F*B / p_bkw) or (F*B / p_fwd)
    print "\nPOSTERIOR"
    posterior = []
    for i in range(L):
        print "i: ", i
        for st in states:
            print "fwd*bkw", fwd[i][st],"*",bkw[i][st],"/",p_fwd
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})
        print "posterior", posterior

    print "posterior forward and backward: p_fwd, p_bkw", round(p_fwd,20), round(p_bkw,20)
    if round(p_fwd,10)!=round(p_bkw,10):
        print "not equal"
    assert round(p_fwd,10)==round(p_bkw,10) # make sure that the forward and backward termination probabilities are equal
    
    #print "observations", x
    #print "fwdd: ", fwd
    #print "bkwdd: ", bkw
    #print "posterior ", posterior

    return fwd, bkw, posterior

def create_mu_emission(total_num_reads_cells, genome_len_cells, all_assign_cells, m_cells, n_bins):
    # e 
    mu_emis_matrix = {}
    for i in range(m_cells):
        #print "\ni: ", i
        mu_emis_matrix[i]={}
        for j in range(n_bins):
            #print "j ", genome_len_cells[all_assign_cells[i]]
            mu_emis_matrix[i][j]=(total_num_reads_cells[i]/genome_len_cells[all_assign_cells[i]])*j
            if mu_emis_matrix[i][j] == 0.0:
                mu_emis_matrix[i][j]=1.0

    #print "create_mu_emission"
    #for k,v in mu_emis_matrix.items():
    #    print k
    #    print v
    return mu_emis_matrix
    #.append(pos_expected_num_reads)

def pnorm(A,B,L_norm):
    dist = 0.0

    # Check that the sizes of f and n are the same for all S (soft copy number profiles)
    assert len(A)==len(B)
    assert len(A[0])==len(B[0])

    d_sum_f = d_sum_n = 0.0
    for i in range(0,len(A)): # num bins n
        for f in range(0,len(A[0])): # num hidden CN states f
            d = abs(A[i][f] - B[i][f])**L_norm
            #print "i", i, "f", f, "A[i][f]",A[i][f], "B[i][f]",B[i][f]
            #print "d = ", d
            d_sum_f+=d
            #print "d_sum_f", d_sum_f
        d_sum_n+=d_sum_f
        #print "d_sum_n", d_sum_n
        d_sum_f=0.0 # reset
    #print "d_sum_n: ", d_sum_n**(1/L_norm)
    return d_sum_n**(1/L_norm)

def create_dist_matrix(S_mat,L_norm):
    dist_mat = {}

    for cell_A in range(len(S_mat)) :
        dist_mat[cell_A] = {}
        for cell_B in range(len(S_mat)):
            if cell_A == cell_B:
                #print "same cell: ", cell_A
                dist_mat[cell_A][cell_B] = 0.0
            else:
                #print "A"
                print S_mat[cell_A]
                #print "B"
                print S_mat[cell_B]
                dist_mat[cell_A][cell_B] = pnorm(S_mat[cell_A], S_mat[cell_B], L_norm)
    
    # Create list from dictionary  
    d_l = []
    for i in dist_mat.values():
        l = []
        for j in i.values():
            l.append(j)
        d_l.append(l)
    return d_l 

def create_dendrogram(dist_mat):
    #mat = np.array([[0.0, 2.0, 3.8459253727671276e-16], [2.0, 0.0, 2.0], [3.8459253727671276e-16, 2.0, 0.0]])
    #dist_mat = np.array([[0.0, 2.0, 0.1], [2.0, 0.0, 2.0], [0.1, 2.0, 0.0]])
    dists = squareform(dist_mat)
    linkage_matrix = linkage(dists, "single")
    #dendrogram(linkage_matrix, color_threshold=1,show_leaf_counts=False)
    print "link:", linkage_matrix
    show_leaf_counts = False
    ddata = fancy_dendrogram(linkage_matrix,
                   color_threshold=1,
                   p=len(dist_mat),
                   truncate_mode='lastp',
                   show_leaf_counts=show_leaf_counts #,
                   #max_d = T_THRESHOLD
                   )
    plt.title("Dendrogram of %d Cells Similarity at %d Alpha" % (len(dist_mat),float(ALPHA)))
    plt.xlabel('Cell Number')
    plt.ylim(ymin=-0.1)
    plt.ylabel('Distance: p-norm, p = %s' % P_NORM_VAL)
    plt.savefig('plot_%dcells_%dbins_%dclones_%dalpha.eps' % (M_CELLS,N_BINS,K_CLONES,float(ALPHA)))
    
    k = NUM_CLUST
    if DISTANCE_MEASURE == 'distance':
        choice = 'distance'
    elif DISTANCE_MEASURE == 'maxclust':
        choice = 'maxclust'
    else:
        print "ERROR: 'maxclust' (max number of clusters) or 'distance' (maximum distance (p-norm)) "
    
    k_clus = fcluster(linkage_matrix, k, criterion=choice) # maxclust, distance
    print "k_clus", k_clus
    return k_clus

def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

def assign_hard_CNP(k_cl,S_cells):
    n_clust = []
    print "k_clusters", k_cl
    for a in range(min(k_cl),max(k_cl)+1):
        print "cluster ", a
        n_clust.append([i for i, j in enumerate(k_cl) if j == a])
        print " cells ",n_clust
   
    hard_CNP = {}
    for clu in n_clust:
        cluster_CNP=[]
        #print "current to cluster: ", clu
        for i in range(0,N_BINS):
            #print "BIN: ", i
            old_prob = 0.0 # initial probability for f* (argmax of hard CN for each bin for each cell for each cluster)
            new_prob_l = [] # list of numbers to multiply
            for j in range(0,H_CN_BOUND+1):
                #print " \nCN: ", j
                for cc in clu:
                    #print "cell:", cc
                    #print S_cells[cc][i][j]
                    new_prob_l.append(S_cells[cc][i][j])
                new_prob = reduce(lambda x, y: x*y, new_prob_l)
                if new_prob>=old_prob:
                    #print "yes - higher probability: ", new_prob
                    old_prob = new_prob
                    max_CN = j 
                    #print "coordinates: ", i,j
                new_prob_l = []
            if old_prob == 0.0:
                cluster_CNP.append(8)
            else:
                 cluster_CNP.append(max_CN)   
        for ccc in clu:
            hard_CNP[ccc] = cluster_CNP
    return hard_CNP, n_clust

def compare_to_truth(infer_cells, clone_mat, cell_assign):
    truth = {}
    print "\ncell_ass:", cell_assign
    #print "clone_m", clone_mat
    print "infer: ", infer_cells
    for idx,i in enumerate(cell_assign):
        truth[idx]=clone_mat[i]
    print "truth:", truth
    print "\ndifference: ", dict_diff(truth, infer_cells)
    return truth

def dict_diff(truth, inferred):
    diff = dict()
    diff['truth'] = set(truth) - set(inferred)
    diff['inferred'] = set(inferred) - set(truth)
    diff['difference'] = {k for k in set(truth) & set(inferred) if truth[k]!=inferred[k]}
    return diff

def manhattan_distance(x,y):
    return sum(abs(a-b) for a,b in zip(x,y))

def create_heatmaps(inf,tru):
    assert len(inf)==len(tru)# make sure same number of cells
    assert len(inf[0])==len(tru[0])# make sure same number of bins
    full_manh = 0
    for i in range(0,len(inf)):
        full_manh+=manhattan_distance(inf[i],tru[i])
    L1_final_dist = full_manh/(len(inf)*len(inf[0])) # manhattan distance / (m * n)
    return L1_final_dist
    #ax = sns.heatmap(uniform_data)
    #sns.plt.show()


###########################
# USAGE
if len(sys.argv) != 11:
    sys.stderr.write("Usage: " + sys.argv[0] + " <M_CELLS> <N_BINS> <K_CLONES> <CHR_NUM> <P_NORM_DIST> <MARG_POSTERIOR_THRESHOLD> <NUM_CLUST> <'maxclust' or 'distance'> <ALPHA> <'random' or 'fixed'>\n")
    sys.exit(1)

# ASSIGNMENT
M_CELLS = int(sys.argv[1])
if M_CELLS <= 2:
    sys.stderr.write("Enter num. cells greater than 2\n")
    sys.exit(1)
N_BINS = int(sys.argv[2])
if N_BINS <= 5:
    sys.stderr.write("Enter at least 6 bins\n")
    sys.exit(1)
K_CLONES = int(sys.argv[3])
H_CN_BOUND = 5
CHR = int(sys.argv[4])
P_NORM_VAL = int(sys.argv[5])
MARG_POSTERIOR_THRESHOLD = float(sys.argv[6]) # minimum marginal posterior prob allowed
NUM_CLUST = float(sys.argv[7])
DISTANCE_MEASURE = str(sys.argv[8])
ALPHA = str(sys.argv[9])
EXAMPLE = str(sys.argv[10])

###########################
# MAIN
if __name__ == '__main__':
    answer = neg_binom_function(120,100,11)
    print answer
    sys.exit()

    # Initialize RD matrix and genome length
    read_matrix = [] # matrix of read depth. 
    geno_length = [] # num of copies of each cell's genome (chromosome in this case) for normalization

    # Get genome length and chromosome (note ind 23 = X and 24 = Y)
    chrom_ID_percentage = [8,7.9,6.5,6.2,5.9,5.5,5.2,4.7,4.6,4.4,4.4,4.3,3.7,3.5,3.3,2.9,2.6,2.5,2.1,2,1.5,1.6,5,1.9]
    chr_chosen_perc_reads = chrom_ID_percentage[CHR-1]
    
    # Transition Matrix - used to create instances and in FB algorithm 
    a_trans_matrix = { 
      0 : {0: 0.39, 1: 0.2, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      1 : {0: 0.2, 1: 0.39, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      2 : {0: 0.1, 1: 0.2, 2: 0.39, 3: 0.2, 4: 0.075, 5: 0.025, 'E': 0.01},
      3 : {0: 0.025, 1: 0.075, 2: 0.2, 3: 0.39, 4: 0.2, 5: 0.1, 'E': 0.01},
      4 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.39, 5: 0.2, 'E': 0.01},
      5 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.2, 5: 0.39, 'E': 0.01},
      }

    if EXAMPLE == 'random':
        infer = {0: [2, 2, 2, 2, 3, 4, 5, 4, 4, 3], 1: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 2: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 3: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 4: [2, 2, 2, 2, 3, 4, 5, 4, 4, 3], 5: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 6: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 7: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 8: [2, 2, 1, 1, 4, 4, 4, 2, 4, 3]}
        truth = {0: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 1: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 2: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 3: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 4: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 5: [2, 2, 1, 1, 1, 0, 4, 5, 2, 3], 6: [2, 2, 2, 2, 4, 4, 5, 4, 4, 3], 7: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 8: [2, 2, 1, 1, 4, 3, 4, 2, 4, 2]}
        norm_manhat_dist = create_heatmaps(infer, truth)
        print "normalized_man_dist: ",norm_manhat_dist
        
        sys.exit()
        # hard coded:
        chr_num_reads = 28.6585 * 50000 * N_BINS #100
        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21

        clone_matrix, assign_cells = create_clones_assign_cells(M_CELLS, N_BINS, K_CLONES, H_CN_BOUND, a_trans_matrix) #H_CN_BOUND
        print "Clones: "
        for i in clone_matrix:
            print i
        [geno_length.append(sum(i)) for i in clone_matrix]
        print "geno_length: ", geno_length
        print "Assign: ", assign_cells

        # m draws from the Poisson, one for each cell, to determine number of reads for that chromosome
        cell_chr_tot_num_reads = np.random.poisson(chr_num_reads, M_CELLS)
        #print "chr ", CHR ," num reads for each cell: ", cell_chr_tot_num_reads, "\n"
            # Mean Values for Emission Probabilities 
        mu_e_emis_matrix = create_mu_emission(cell_chr_tot_num_reads, geno_length, assign_cells, M_CELLS, N_BINS)
        
        # Proportion of reads in each bin for each cell
        for idx,i in enumerate(assign_cells):
            #print "\ncell : ", idx, " clone: ", i, "d",cell_chr_tot_num_reads[idx],"/","pi", geno_length[i]
            e = cell_chr_tot_num_reads[idx]/geno_length[i]
            #print "if CN is 1 (e): ",e
            to_add = []

            for j in clone_matrix[i]: # copy number 
                #print "      j: ", j
                #print "prop_reads ->", "e*j", e*j
                prop_reads = e*j

                # Add NB noise
                if prop_reads == 0.0:
                    reads = random_sample_NB('1', ALPHA)
                    #print "CN 0 nb distr: reads ", reads
                    to_add.append(reads)
                else:
                    reads = random_sample_NB(str(prop_reads), ALPHA)
                    #print "CN >0 nb distr: reads ", reads
                    to_add.append(reads) # sample from negative binomial with p = .5
            read_matrix.append(to_add) # add proportion of reads in each bin for every cell
        

        print "read matrix: ", read_matrix
        print "expected num reads ", mu_e_emis_matrix
    
    elif EXAMPLE == 'fixed1':
        # hard coded:
        M_CELLS = 3
        N_BINS = 10
        chr_num_reads = 28.6585 * 50000 * N_BINS #100
        K_CLONES = 3
        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21

        clone_matrix = [[2,0,4,3,4,5,3,2,3,4],[2,3,4,4,3,3,4,3,4,2],[2,2,2,2,2,2,2,2,2,2]]
        geno_length = [30,32,20]
        assign_cells = np.array([0,0,1,1])
        cell_chr_tot_num_reads = [14323781, 14323213, 14329968, 14332274]
        read_matrix = [[1148622, 2, 2048342, 1218059, 1769834, 2529336, 1499882, 465919, 1078257, 2505195], [1534441, 1, 1339401, 1220316, 1736459, 1480539, 1822409, 905747, 1425143, 2135003], [713060, 1888618, 3314687, 1756060, 999052, 1740894, 2152623, 2110787, 1120029, 959460], [902470, 1319056, 2872951, 1948942, 1593613, 1440500, 960473, 1935242, 1290958, 826063]]
        mu_e_emis_matrix = {0: {0: 1.0, 1: 477459.36666666664, 2: 954918.7333333333, 3: 1432378.0999999999, 4: 1909837.4666666666, 5: 2387296.833333333, 6: 2864756.1999999997, 7: 3342215.5666666664, 8: 3819674.933333333, 9: 4297134.3}, 1: {0: 1.0, 1: 477440.43333333335, 2: 954880.8666666667, 3: 1432321.3, 4: 1909761.7333333334, 5: 2387202.166666667, 6: 2864642.6, 7: 3342083.033333333, 8: 3819523.466666667, 9: 4296963.9}, 2: {0: 1.0, 1: 447811.5, 2: 895623.0, 3: 1343434.5, 4: 1791246.0, 5: 2239057.5, 6: 2686869.0, 7: 3134680.5, 8: 3582492.0, 9: 4030303.5}, 3: {0: 1.0, 1: 447883.5625, 2: 895767.125, 3: 1343650.6875, 4: 1791534.25, 5: 2239417.8125, 6: 2687301.375, 7: 3135184.9375, 8: 3583068.5, 9: 4030952.0625}}
        print "fixed Clones: "
        for i in clone_matrix:
             print i
        print "fixed geno_length: ", geno_length
        print "fixed assign cells: ", assign_cells
        print "fixed sampling cell size: ", cell_chr_tot_num_reads
        print "fixed NB noise added reads: ", read_matrix
        print  "fixed mean RD matrix: ", mu_e_emis_matrix
    
    elif EXAMPLE == 'fixed2': #fill
        M_CELLS = 4
        N_BINS = 6
        chr_num_reads = 28.6585 * 50000 * N_BINS #100
        K_CLONES = 4
        clone_matrix = [[2,0,4,5,3,3],[2,3,4,4,4,4],[0,0,2,2,4,4],[2,2,2,2,2,2]]
        geno_length = [17,21,12,12]
        assign_cells = np.array([0,2,1,1])
        cell_chr_tot_num_reads = []
        read_matrix = []
        mu_e_emis_matrix = []

        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21
        print "fixed Clones: "
        for i in clone_matrix:
             print i
        print "fixed geno_length: ", geno_length
        print "fixed assign cells: ", assign_cells
        print "fixed sampling cell size: ", cell_chr_tot_num_reads
        print "fixed NB noise added reads: ", read_matrix
        print  "fixed mean RD matrix: ", mu_e_emis_matrix
        
    
    elif EXAMPLE == 'fixed3': # good example
        M_CELLS = 4
        N_BINS = 6
        chr_num_reads = 28.6585 * 50000 * N_BINS #100
        K_CLONES = 3
        clone_matrix = [[2,2,2,2,3,3],[2,2,1,5,1,2],[2,2,2,2,2,2]]
        geno_length = [14, 13, 12]
        assign_cells = np.array([2,2,1,1])
        cell_chr_tot_num_reads = [8597001,8590046,8600702,8594915] 
        read_matrix = [[821564, 2011939, 1260429, 1616534, 721619, 1479476], [1528088, 1425835, 1135200, 1628785, 914245, 1285801], [1109627, 976230, 834756, 2880669, 497200, 1077729], [997257, 1452545, 552608, 2975452, 633777, 550460]] 
        mu_e_emis_matrix = {0: {0: 1.0, 1: 716416.75, 2: 1432833.5, 3: 2149250.25, 4: 2865667.0, 5: 3582083.75}, 1: {0: 1.0, 1: 715837.16666666663, 2: 1431674.3333333333, 3: 2147511.5, 4: 2863348.6666666665, 5: 3579185.833333333}, 2: {0: 1.0, 1: 661592.4615384615, 2: 1323184.923076923, 3: 1984777.3846153845, 4: 2646369.846153846, 5: 3307962.3076923075}, 3: {0: 1.0, 1: 661147.30769230775, 2: 1322294.6153846155, 3: 1983441.9230769232, 4: 2644589.230769231, 5: 3305736.538461539}}
        
        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21
        print "fixed Clones: "
        for i in clone_matrix:
             print i
        print "fixed geno_length: ", geno_length
        print "fixed assign cells: ", assign_cells
        print "fixed sampling cell size: ", cell_chr_tot_num_reads
        print "fixed NB noise added reads: ", read_matrix
        print "fixed mean RD matrix: ", mu_e_emis_matrix

    elif EXAMPLE == 'fixed4': # python fb_algo.py 4 6 3 21 2 0.4 3 maxclust 110 fixed4
        ALPHA = '110'
        M_CELLS = 4
        N_BINS = 6
        chr_num_reads = 28.6585 * 50000 * N_BINS #100
        K_CLONES = 3
        clone_matrix = [[2,4,1,0,0,4],[2,0,0,1,1,1],[2,2,2,2,2,2]]
        geno_length = [11, 5, 12]
        assign_cells = np.array([2,2,0,1])
        cell_chr_tot_num_reads = [8597001,8590046,8600702,8594915] 
        read_matrix = [[1610530, 1543346, 1467165, 1561100, 1423552, 1919547], [1605638, 1522849, 1385600, 1415511, 1277482, 1192145], [1761769, 3085824, 852920, 2, 1, 3781404], [3328448, 0, 0, 1717981, 1664634, 1745324]]
        mu_e_emis_matrix = {0: {0: 1.0, 1: 716442.08333333337, 2: 1432884.1666666667, 3: 2149326.25, 4: 2865768.3333333335, 5: 3582210.416666667}, 1: {0: 1.0, 1: 716618.08333333337, 2: 1433236.1666666667, 3: 2149854.25, 4: 2866472.3333333335, 5: 3583090.416666667}, 2: {0: 1.0, 1: 781361.0, 2: 1562722.0, 3: 2344083.0, 4: 3125444.0, 5: 3906805.0}, 3: {0: 1.0, 1: 1719107.6000000001, 2: 3438215.2000000002, 3: 5157322.8000000007, 4: 6876430.4000000004, 5: 8595538.0}}

        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21
        print "fixed Clones: "
        for i in clone_matrix:
             print i
        print "fixed geno_length: ", geno_length
        print "fixed assign cells: ", assign_cells
        print "fixed sampling cell size: ", cell_chr_tot_num_reads
        print "fixed NB noise added reads: ", read_matrix
        print "fixed mean RD matrix: ", mu_e_emis_matrix

    elif EXAMPLE == 'fixed5': # python fb_algo.py 19 10 5 21 2 0.4 5 maxclust 110 fixed5
        ALPHA = '11'
        M_CELLS = 19
        N_BINS = 10
        chr_num_reads = 28.6585 * 50000 * N_BINS
        K_CLONES = 5
        clone_matrix = [2, 3, 3, 3, 2, 3, 3, 2, 2, 2],[2, 1, 2, 3, 3, 4, 4, 1, 4, 3],[2, 1, 2, 2, 2, 3, 3, 3, 5, 2],[2, 2, 3, 3, 3, 4, 4, 4, 5, 4],[2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        geno_length = [25, 27, 25, 34, 20]
        assign_cells = np.array([4,1,2,0,1,4,4,2,4,1,2,0,1,3,3,0,2,4,1])
        cell_chr_tot_num_reads = [8597001,8590046,8600702,8594915] 
        read_matrix = [[1506551, 1431881, 1515334, 1539629, 1381226, 1300373, 1594536, 1451624, 1102154, 1513867], [1047871, 497849, 865497, 1484393, 1721447, 1587784, 2264733, 657847, 1963688, 1627798], [1010371, 576287, 1247537, 1321798, 1031625, 1494643, 1768397, 1724813, 2922166, 818198], [1226044, 1698244, 1768328, 1606876, 947420, 1654832, 1838433, 1337241, 1097645, 1382089], [1359556, 506442, 1039576, 1426516, 1598313, 2378861, 2333575, 652275, 1849200, 1764709], [1518834, 1310771, 1413639, 1410826, 1545080, 1344320, 1323682, 1630114, 1404569, 1516344], [1614497, 1545425, 1562930, 1526819, 1610133, 1315920, 1379060, 1493775, 1427659, 1471574], [1159655, 585347, 1197778, 1088884, 1079889, 1613437, 1580085, 2156450, 3176746, 1047142], [1456289, 1447914, 1310420, 1542520, 1323221, 1358253, 1370981, 1560009, 1522963, 1431105], [1003667, 494051, 1222914, 1588357, 1641176, 1970445, 1939968, 463745, 2345760, 1570437], [1224927, 555579, 981756, 1048977, 1220805, 1779282, 1571940, 1731912, 2493578, 1042959], [1100510, 1428012, 1681616, 1719503, 1169868, 1660242, 1613662, 1045870, 1137018, 953281], [1151751, 509632, 999576, 1520783, 1520999, 2225786, 2310392, 446076, 1967158, 1590067], [848652, 849287, 1390230, 1224451, 1030102, 1676091, 1596166, 1675236, 2196128, 1668762], [936530, 895845, 1173808, 1342857, 1379293, 1593293, 1878997, 1871174, 2146611, 1707011], [1098294, 1917564, 1451699, 1656092, 979740, 1720831, 1728654, 1119722, 1118574, 1159773], [873715, 559642, 1338112, 1134514, 1323484, 1463615, 1726253, 1636963, 2688256, 1139781], [1624739, 1361222, 1295677, 1460058, 1707334, 1517495, 1566202, 1670039, 1535179, 1581959], [1195488, 432995, 1144396, 1410989, 1366434, 2098406, 1978129, 593630, 2107664, 1689792]]
        mu_e_emis_matrix = {0: {0: 1.0, 1: 716456.0, 2: 1432912.0, 3: 2149368.0, 4: 2865824.0, 5: 3582280.0, 6: 4298736.0, 7: 5015192.0, 8: 5731648.0, 9: 6448104.0}, 1: {0: 1.0, 1: 530803.88888888888, 2: 1061607.7777777778, 3: 1592411.6666666665, 4: 2123215.5555555555, 5: 2654019.4444444445, 6: 3184823.333333333, 7: 3715627.222222222, 8: 4246431.111111111, 9: 4777235.0}, 2: {0: 1.0, 1: 573073.47999999998, 2: 1146146.96, 3: 1719220.4399999999, 4: 2292293.9199999999, 5: 2865367.3999999999, 6: 3438440.8799999999, 7: 4011514.3599999999, 8: 4584587.8399999999, 9: 5157661.3200000003}, 3: {0: 1.0, 1: 573466.40000000002, 2: 1146932.8, 3: 1720399.2000000002, 4: 2293865.6000000001, 5: 2867332.0, 6: 3440798.4000000004, 7: 4014264.8000000003, 8: 4587731.2000000002, 9: 5161197.6000000006}, 4: {0: 1.0, 1: 530719.70370370371, 2: 1061439.4074074074, 3: 1592159.111111111, 4: 2122878.8148148148, 5: 2653598.5185185187, 6: 3184318.222222222, 7: 3715037.9259259258, 8: 4245757.6296296297, 9: 4776477.333333333}, 5: {0: 1.0, 1: 716289.44999999995, 2: 1432578.8999999999, 3: 2148868.3499999996, 4: 2865157.7999999998, 5: 3581447.25, 6: 4297736.6999999993, 7: 5014026.1499999994, 8: 5730315.5999999996, 9: 6446605.0499999998}, 6: {0: 1.0, 1: 716775.44999999995, 2: 1433550.8999999999, 3: 2150326.3499999996, 4: 2867101.7999999998, 5: 3583877.25, 6: 4300652.6999999993, 7: 5017428.1499999994, 8: 5734203.5999999996, 9: 6450979.0499999998}, 7: {0: 1.0, 1: 573314.43999999994, 2: 1146628.8799999999, 3: 1719943.3199999998, 4: 2293257.7599999998, 5: 2866572.1999999997, 6: 3439886.6399999997, 7: 4013201.0799999996, 8: 4586515.5199999996, 9: 5159829.959999999}, 8: {0: 1.0, 1: 716324.30000000005, 2: 1432648.6000000001, 3: 2148972.9000000004, 4: 2865297.2000000002, 5: 3581621.5, 6: 4297945.8000000007, 7: 5014270.1000000006, 8: 5730594.4000000004, 9: 6446918.7000000002}, 9: {0: 1.0, 1: 530601.18518518517, 2: 1061202.3703703703, 3: 1591803.5555555555, 4: 2122404.7407407407, 5: 2653005.9259259258, 6: 3183607.111111111, 7: 3714208.2962962962, 8: 4244809.4814814813, 9: 4775410.666666666}, 10: {0: 1.0, 1: 573294.35999999999, 2: 1146588.72, 3: 1719883.0800000001, 4: 2293177.4399999999, 5: 2866471.7999999998, 6: 3439766.1600000001, 7: 4013060.52, 8: 4586354.8799999999, 9: 5159649.2400000002}, 11: {0: 1.0, 1: 573091.64000000001, 2: 1146183.28, 3: 1719274.9199999999, 4: 2292366.5600000001, 5: 2865458.2000000002, 6: 3438549.8399999999, 7: 4011641.48, 8: 4584733.1200000001, 9: 5157824.7599999998}, 12: {0: 1.0, 1: 530600.92592592596, 2: 1061201.8518518519, 3: 1591802.777777778, 4: 2122403.7037037038, 5: 2653004.6296296297, 6: 3183605.555555556, 7: 3714206.4814814818, 8: 4244807.4074074076, 9: 4775408.333333334}, 13: {0: 1.0, 1: 421579.14705882355, 2: 843158.29411764711, 3: 1264737.4411764706, 4: 1686316.5882352942, 5: 2107895.7352941176, 6: 2529474.8823529412, 7: 2951054.0294117648, 8: 3372633.1764705884, 9: 3794212.323529412}, 14: {0: 1.0, 1: 421530.67647058825, 2: 843061.3529411765, 3: 1264592.0294117648, 4: 1686122.705882353, 5: 2107653.3823529412, 6: 2529184.0588235296, 7: 2950714.7352941176, 8: 3372245.411764706, 9: 3793776.0882352944}, 15: {0: 1.0, 1: 573227.04000000004, 2: 1146454.0800000001, 3: 1719681.1200000001, 4: 2292908.1600000001, 5: 2866135.2000000002, 6: 3439362.2400000002, 7: 4012589.2800000003, 8: 4585816.3200000003, 9: 5159043.3600000003}, 16: {0: 1.0, 1: 573251.88, 2: 1146503.76, 3: 1719755.6400000001, 4: 2293007.52, 5: 2866259.3999999999, 6: 3439511.2800000003, 7: 4012763.1600000001, 8: 4586015.04, 9: 5159266.9199999999}, 17: {0: 1.0, 1: 716646.84999999998, 2: 1433293.7, 3: 2149940.5499999998, 4: 2866587.3999999999, 5: 3583234.25, 6: 4299881.0999999996, 7: 5016527.9500000002, 8: 5733174.7999999998, 9: 6449821.6499999994}, 18: {0: 1.0, 1: 530515.96296296292, 2: 1061031.9259259258, 3: 1591547.8888888888, 4: 2122063.8518518517, 5: 2652579.8148148144, 6: 3183095.7777777775, 7: 3713611.7407407407, 8: 4244127.7037037034, 9: 4774643.666666666}}

        print "Cells: ", M_CELLS
        print "Bins: ", N_BINS
        print "Clones (one is diplod): ", K_CLONES
        print "CN Bound:", H_CN_BOUND
        print "Indexing starts at 0 for cell assignment."
        print "Chromosome chosen: ", CHR
        print "Avg num reads", chr_num_reads,'\n' #eg: 51115.38 for chrom 21
        print "fixed Clones: "
        for i in clone_matrix:
             print i
        print "fixed geno_length: ", geno_length
        print "fixed assign cells: ", assign_cells
        print "fixed sampling cell size: ", cell_chr_tot_num_reads
        print "fixed NB noise added reads: ", read_matrix
        print "fixed mean RD matrix: ", mu_e_emis_matrix

    else:
        sys.stderr.write("ERROR, enter 'random' or 'fixed' to create instance\n")
        sys.exit(1)

    # Run forward backward algorithm for each cell
    S = {}
    for idx, chr_rd in enumerate(read_matrix):
        print "Run forward, backward: cell and observation", idx, chr_rd
        fwd_mat, bkw_mat, posterior_mat = run_FB_one_cell(chr_rd, a_trans_matrix, mu_e_emis_matrix[idx]) #min_max[idx]
        print "cell idx, chr_rd", idx, chr_rd
        S[idx]= posterior_mat
    for c,p in S.items():
        for v in p:
            if round(sum(v.values()),2)!=1.0:
                sys.stderr.write("Cell "+str(c)+' does not sum to one')
                sys.exit(1)

    distance_matrix = create_dist_matrix(S, P_NORM_VAL)
    k_clusters = create_dendrogram(distance_matrix)
    hard_CNPs, cell_groups = assign_hard_CNP(k_clusters,S)
    final = compare_to_truth(hard_CNPs, clone_matrix, assign_cells)
    create_heatmaps(hard_CNPs, )

    print "\nS"
    for k,v in S.items():
        print k
        for val in v:
            print val

    print "\nCells: ", M_CELLS
    print "Bins: ", N_BINS
    print "Clones (one is diploid): ", K_CLONES
    print "CN Bound:", H_CN_BOUND
    print "Indexing starts at 0 for cell assignment."
    print "Chromosome chosen: ", CHR
    print "Avg num reads",chr_num_reads,'\n' #eg: 51115.38 for chrom 21
    print "clusters: ", cell_groups
    print "Clones: "
    for i in clone_matrix:
        print i
    print "geno_length: ", geno_length
    print "Assign: ", assign_cells
    print "Distance Matrix", distance_matrix
    print "Hard CNPS: ", hard_CNPs
    print "Cell Groups", cell_groups
    print "Number of cell groups: ", len(cell_groups)

###############################
    #answer = neg_binom_function(2865009,2865009,11)
    #print answer



