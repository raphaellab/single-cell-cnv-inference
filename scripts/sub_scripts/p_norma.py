from __future__ import division
from __future__ import division
from datadiff import diff
import matplotlib.pyplot as plt
import sys
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import subprocess
from scipy.cluster.hierarchy import fcluster
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

def pnorm(A,B,L_norm):
    dist = 0.0

    # Check that thee sizes of f and n are the same for all S (soft copy number profiles)
    assert len(A)==len(B)
    assert len(A[0])==len(B[0])

    d_sum_f = d_sum_n = 0.0
    for i in range(0,len(A)): # num bins n
        for f in range(0,len(A[0])): # num hidden CN states f
            d = abs(A[i][f] - B[i][f])**L_norm
            print "i", i, "f", f, "A[i][f]",A[i][f], "B[i][f]",B[i][f]
            print "d = ", d
            d_sum_f+=d
            print "d_sum_f", d_sum_f
        d_sum_n+=d_sum_f
        print "d_sum_n", d_sum_n
        d_sum_f=0.0 # reset
    print "d_sum_n: ", d_sum_n**(1/L_norm)
    return d_sum_n**(1/L_norm)

def create_dendrogram(dist_mat):
    #mat = np.array([[0.0, 2.0, 3.8459253727671276e-16], [2.0, 0.0, 2.0], [3.8459253727671276e-16, 2.0, 0.0]])
    #dist_mat = np.array([[0.0, 2.0, 0.1], [2.0, 0.0, 2.0], [0.1, 2.0, 0.0]])
    dists = squareform(dist_mat)
    linkage_matrix = linkage(dists, "single")
    #dendrogram(linkage_matrix, color_threshold=1,show_leaf_counts=False)
    print "link:", linkage_matrix
    show_leaf_counts = False
    #ddata = fancy_dendrogram(linkage_matrix,
    #               color_threshold=1,
    #               p=len(dist_mat),
    #               truncate_mode='lastp',
    #               show_leaf_counts=show_leaf_counts,
    #               max_d = T_THRESHOLD
    #               )
    #plt.title("Dendrogram of %d Cells Similarity at %d Alpha" % (len(dist_mat),float(ALPHA)))
    #plt.xlabel('Cell Number')
    #plt.ylim(ymin=-0.1)
    #plt.ylabel('Distance: p-norm, p = %s' % P_NORM_VAL)
    #plt.show()
    #plt.savefig('tester_plot_%dcells_%dbins_%dclones_%dalpha.eps' % (M_CELLS,N_BINS,K_CLONES,float(ALPHA)))
    #k = NUM_K # 3
    k = NUM_DIST
    if DISTANCE_MEASURE == 'distance':
        choice = 'distance'
    elif DISTANCE_MEASURE == 'maxclust':
        choice = 'maxclust'
    else:
        print "ERROR: 'maxclust' (max number of clusters) or 'distance' (maximum distance (p-norm)) "

    k_clus = fcluster(linkage_matrix, k, criterion=choice) # distance
    print "k_clus", k_clus
    return k_clus

S = {0: [{0: 2, 1: 3, 2: 2}, {0: 3, 1: 0.0, 2: 6}], \
 1: [{0: 5, 1: 1, 2: 3}, {0: 0.0, 1: 0.0, 2: 6}], \
 2: [{0: 0.0, 1: 0.0, 2: 7.0}, {0: 0.0, 1: 0.0, 2: 4}]}

#S = {0: [{0: 0.4, 1: 0.44, 2: 1.0}, {0: 3.0, 1: 0.0, 2: 1.02}], \
# 1: [{0: 0.53, 1: 0.6, 2: 0.998}, {0: 0.0, 1: 0.0, 2: 0.998}], \
# 2: [{0: 0.0, 1: 0.0, 2: 7.0}, {0: 0.0, 1: 0.0, 2: 4}]}

p = []
y = []
for i in range(2,15):
    print "i", i
    answer = pnorm(S[0], S[1],i)
    print "answer", answer
    p.append(i)
    y.append(answer)

plt.plot(p, y, '-o')
plt.title("Increase of p for P-norm and Distance Between Matrices")
plt.xlabel('P')
plt.ylabel('P-norm Distance')
#plt.ylim(ymin=-0.1)
#plt.ylabel('Distance: p-norm, p = %s' % P_NORM_VAL)
#plt.savefig('plot_%dcells_%dbins_%dclones_%dalpha.eps' % (M_CELLS,N_BINS,K_CLONES,float(ALPHA)))
plt.show()

#NUM_DIST = 1
#DISTANCE_MEASURE = 'distance'
#S = {0:[{0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0000000000000004, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0000000000000002, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 1.0000000000000002}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 1.0000000000000002}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}], \
#    1: [{0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 1.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}], \
#    2: [{0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999998, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999998, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999998, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}]}
#clone_matrix= [[2, 2, 3, 5, 5, 3], [2, 2, 0, 0, 1, 2], [2, 2, 2, 2, 2, 2]]
#cn_tot = [20, 7, 12]
#d_m = [[0.0, 2.8284271247461903, 2.8284271247461903], [2.8284271247461903, 0.0, 2.449489742783178], [2.8284271247461903, 2.449489742783178, 0.0]]
#create_dendrogram(d_m)


