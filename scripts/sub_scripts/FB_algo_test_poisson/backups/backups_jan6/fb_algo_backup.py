# 
# Forward Backward Algorithm 
# Ashley Conard
# 

import warnings
warnings.filterwarnings("ignore")
import numpy as np

def example():

    states = ('Healthy', 'Fever') # states
     
    observations = ('normal', 'cold', 'dizzy') # x
     
    start_probability = {'Healthy': 0.6, 'Fever': 0.4} # a_0
    end_state = 'E' # end_st
     
    transition_probability = { # a 
       'Healthy' : {'Healthy': 0.69, 'Fever': 0.3, 'E': 0.01},
       'Fever' : {'Healthy': 0.4, 'Fever': 0.59, 'E': 0.01},
       }
     
    emission_probability = { # e
       'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
       'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6},
       }
    return fwd_bkw(observations,
                       states,
                       start_probability,
                       transition_probability,
                       emission_probability,
                       end_state)

def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
    print "x: ", x
 
    fwd = []
    f_prev = {}

    # forward part of the algorithm
    for i, x_i in enumerate(x): # for every observation
        print "\ni, x_i: ", i, x_i
        f_curr = {}
        for st in states: # for every state
            print "state", st
            if i == 0:
                print "base: ", a_0[st]
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                for k in states:
                    print "k: ", "sum", f_prev[k], "*", a[k][st]
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
 
            f_curr[st] = e[st][x_i] * prev_f_sum
            print "f_curr[st] = e[st][x_i] * prev_f_sum: ",f_curr[st], "=",e[st][x_i], "*", prev_f_sum 

        fwd.append(f_curr)
        print "fwd: ", fwd
        f_prev = f_curr
        print "f_prev = f_curr: ", f_prev, "=", f_curr
    
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)
    print "\np_fwd = sum(f_curr[k]*a[k][end_st] for k in states)"
    for k in states:
        print "k: ", k
        print "summing this: f_curr[k]*a[k][end_st]", f_curr[k], "*", a[k][end_st]
        print f_curr[k]*a[k][end_st]
    print "p_fwd: ", p_fwd

    print "\nENTERING BACKWARDS"
    bkw = []
    b_prev = {}
    # backward part of the algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+(None,))):
        print "\ni, x_i_plus: ", i, x_i_plus
        b_curr = {}
        for st in states:
            print "state: ", st
            if i == 0:
                print "base: ", a[st][end_st]
                # base case for backward part
                b_curr[st] = a[st][end_st]
            else:
                for l in states:
                    print "back l: ","sum", a[st][l], "*", e[l][x_i_plus], "*", b_prev[l]
                b_curr[st] = sum(a[st][l]*e[l][x_i_plus]*b_prev[l] for l in states)
 
        bkw.insert(0,b_curr)
        print "bkw: ", bkw
        b_prev = b_curr
        print "b_prev = b_curr", b_prev, "=", b_curr
 
    p_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)
    print "\np_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)"
    for l in states:
        print "l: ", l
        print "summing this: ", a_0[l], "*", e[l][x[0]], "*", b_curr[l]
    print "p_bkw", p_bkw

    # merging the two parts
    print "\nPOSTERIOR"
    posterior = []
    for i in range(L): # for all observations
        print "i: ", i
        for st in states:
            print "fwd*bkw", fwd[i][st],"*",bkw[i][st],"/",p_fwd
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})
        print "posterior", posterior

    assert p_fwd == p_bkw # make sure that the forward and backward probabilities are the same
    return fwd, bkw, posterior

if __name__ == '__main__':
    for line in example():
        print line
