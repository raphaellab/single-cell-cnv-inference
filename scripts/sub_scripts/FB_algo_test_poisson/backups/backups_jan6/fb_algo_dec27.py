# 
# Forward Backward Algorithm 
# Ashley Conard
# 

#
# fb_algo.py
# To address: 
#           - hard coded bound on allowed repeated copy number for a segment. Allowed range now: 0-20 with 30 max different copy number changes
#        ok - NB regression and overdispersion param. 
#           - when CN is 0 <- still add noise...
#           - use the nbCNV simulated or 12 patient Gao.
#           - coverage
#           - determine chrom. percent based on haplo or diplo?
#           - bin coverage

# Note:     - not using genome length (total number of copy-number states)
#           - BW -> https://www.math.univ-toulouse.fr/~agarivie/Telecom/code/index.php
#           - R BW -> https://cran.r-project.org/web/packages/HMM/HMM.pdf
#           -> compare!!! -> FB to R FB
#           - normalization: http://www.analytictech.com/ba762/handouts/normalization.htm
#           - http://www.math.uah.edu/stat/apps/SpecialCalculator.html
#           - explanation of dnbinom: http://stats.stackexchange.com/questions/125477/negative-binomial-distribution-r

# Commands
# python fb_algo.py 3 3 5 21


from __future__ import division
import sys
import subprocess
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

def create_clone_transition_mat(n_b):
    #pi = {0: 0.1, 1: 0.2, 2: 0.6, 3: 0.2, 4: 0.075, 5: 0.025}
    draw = np.random.uniform(0,1)

    c = [2] # create a new clone

    # a 
    transition_probability = { 
      0 : {0: 0.4, 1: 0.2, 2: 0.2, 3: 0.1, 4: 1.0, 5: 0.0},
      1 : {0: 0.2, 1: 0.4, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0},
      2 : {0: 0.1, 1: 0.2, 2: 0.4, 3: 0.2, 4: 0.075, 5: 0.025},
      3 : {0: 0.025, 1: 0.075, 2: 0.2, 3: 0.4, 4: 0.2, 5: 1.0},
      4 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.4, 5: 0.2},
      5 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.2, 5: 4.0},
      }

    for i in range(0, n_b-1):
        #print "\nlast CN: ", c[-1]
        # draw from uniform distribution
        draw = np.random.uniform(0,1)

        # create cumulative list of probabilities
        cum_l = [] # cumulative list of probability values (roulette wheel)
        for idx,i in enumerate(transition_probability[c[-1]].values()):
            if idx == 0:
                cum_l.append((0,i))
            else:
                cum_l.append((cum_l[-1][1],cum_l[-1][1]+i))

        #print "cum_l for CN",c[-1],"is ", cum_l
        for idx,i in enumerate(cum_l):
           minim, maxim = i
           if minim <= draw <= maxim:
                #print "draw: ", draw
                #print "next CN: ", idx
                c.append(idx)
    #print "c: ", c
    return c

def create_clones_assign_cells(m_cells,n_bins,k_clones,h_cn_bound): # Create the true clones (ground truth copy number profiles for each clone)
    clones = []

    for i in range(0,k_clones-1):
        new_clone = create_clone_transition_mat(n_bins)
        clones.append(new_clone)
        #print "new clone: ", clones
    # for m in range(0,k_clones-1):
    #     c = [] # create new clone
    #     num_cn_changes = np.random.randint(3,n_bins) # number cn changes (num segments) for that clone
    #     print "number cn changes ", num_cn_changes
    #     while len(c)<=n_bins:
    #         for i in range(0,num_cn_changes):
    #             #print "i",i
    #             single_copy_num = np.random.randint(0,h_cn_bound) # random CN
    #             #print single_copy_num
    #             repeated = np.random.randint(2,(n_bins/2)) # random times
    #             #print "repeated: ", repeated
    #             to_add = [single_copy_num]*repeated
    #             #print "to_add: ", to_add
    #             c+=to_add

    #     #print "clone: ", c
    #     c = c[:n_bins] # 
    #     c[-1] = c[-2] # convert the truncated number into the next
    #    # print "clone again: ", c
    #     clones.append(c) # add the 'mth' clone
    
    c_dup = [2]*n_bins # diploid case
    clones.append(c_dup)
    #print "final clones", clones

    # Assign cells to clones
    assign_cells = np.random.random_integers(0, k_clones-1, m_cells) # indices are the cells

    #num_consecutive = constrained_sum_sample_pos(num_cn_changes-1,n_bins) # list consecutive copy numbers to form segments
    #print "number: ",num_consecutive
    #single_copy_num = np.random.random_integers(0,h_cn_bound,num_cn_changes) # single copy numbers
    #print "single copy num ",single_copy_num, type(single_copy_num)
    #for idx,i in enumerate(num_consecutive):
    #    print "i num consec: ", idx, i
        #clone.append([single_copy_num[]*])
    return clones, assign_cells

def run_FB_one_cell(rd_chr, expected_rd_chr):
    #h_bound+=1 # adding the zero value to the bound

    #start_probability = {}
    #transition_probability = {}
    #emission_probability = {}

    print "\nRD for chrom + cell: ", rd_chr
    # Hidden states : Z = {0, ..., h} 
    states = list(range(h_bound)) # states
    
    # Observations : r
    # SRR052047 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
    #obs_SRR052047 = [0.77806776,0.8706802,0.889233228,0.863939233,1.021125463,0.836110713,0.814030414,0.992722601,1.148053361,0.965225271,1.046192541,1.04444888,0.816800372,1.05751418,1.097178986,1.1234807,1.18536586,1.526331293,0.96987376,1.031842185,0.953924534,1.042056403,0.891504673,1.156397777,0.976200602,0.978680122,1.016798644,1.279794669,1.1535604,1.008492531,1.13780037,0.855484448,0.927554311,0.818982972,0.697686053,1.032557659,1.145447073,1.13780434,1.230917429,0.840985288,0.961531159,0.901335599,0.898183629,0.842617657,1.07894533,1.031574466,1.008341419,0.96048324,1.238420669,1.127501235,1.02595526,1.221936907,0.938263832,0.864880586,0.783964844,1.074301753,1.285414511,0.846687417,0.962066415,0.793602344,0.806410521,0.910206359,1.007754639,0.764786591]
    # SRR089403 = [2,2,2,2,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
    #obs_SRR089403 = [0.673231229,0.705328928,0.526511068,1.001868753,1.381871082,1.196407922,1.307027524,1.072112842,1.499989388,1.665460624,1.941298383,1.71116958,1.828588457,2.237613811,3.157302145,2.734676081,3.241616238,2.725533009,2.653532494,2.044520189,1.805089791,1.978969459,2.332543619,2.002017487,2.078320064,1.829412508,1.941661737,2.181900328,1.535244857,1.336817619,1.627407702,1.446380991,1.243583647,1.513406732,1.048893156,1.308918322,1.428707549,1.142987518,1.209283956,1.047960162,1.152209084,1.050022377,1.260733585,1.437738921,1.026307143,1.246137498,0.94394513,0.970178723,1.094897395,1.286519419,1.435459747,0.999064941,1.233428976,1.177229307,1.05809945,1.108716196,0.835326426,1.412023688,1.385012221,1.106995555,1.03211341,1.157230606,1.22776956,1.01770534]
    # SRR054571 = [1,1,1,1,2,2,2,2,2,2,3,3,3,3,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    #obs_SRR054571 = [0.406827092,0.637891131,0.524799309,0.879838649,1.433752879,1.52638348,1.106342567,1.096617134,1.25730174,1.598737755,1.787049522,2.167200795,1.544775911,2.182987729,2.712485448,2.717199703,3.015678799,2.518941443,2.694599331,2.13227597,2.015920443,1.995730509,2.268257942,2.038048154,1.973345354,1.910157271,1.874390808,2.003574785,1.681933316,1.159297011,1.192309239,1.587723674,1.071899364,0.979535447,1.162445509,1.192612025,1.488727164,1.133140986,1.227483949,1.191403949,1.110865697,0.925173148,1.406718867,1.539989135,1.137138466,1.356556692,1.149752157,0.995325748,0.858490029,0.588539377,0.672203872,0.635792762,0.678811521,0.620071416,0.484618954,0.456072081,0.653739759,0.722217183,0.765301926,0.480302162,0.504739698,0.650556091,0.520300182,0.605722102]
    ## SRR054604 = [5,5,5,5,5,5,5,5,5,5,5,5,5,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
    ##obs_SRR054604 = [0.690128799,0.862630136,0.878295966,1.024519313,1.177601246,1.070293828,0.976969878,0.998091723,1.132110272,1.305591437,1.261828056,1.377081017,1.335606228,1.569137988,1.922251878,2.017183709,2.205533909,2.01875768,2.13642358,2.157302036,1.699443032,1.702219932,2.273160186,1.939208387,2.091640998,2.052299053,2.141762928,2.80726171,2.389526821,2.274888939,2.527860565,2.269719658,2.091012156,2.084461306,1.945071682,2.182927368,2.517020228,1.907741412,2.144930486,1.669629054,1.839893789,1.915908323,2.304194841,2.207132785,2.063202873,2.013349316,1.800905038,1.269861013,1.095235798,1.352667535,1.040121114,1.388926399,1.135663535,1.19761284,1.022203784,1.161069645,1.595866326,1.130251432,1.181366054,1.186038631,1.336033178,1.146201508,1.14382639,0.892010437]

    # a_0 : all equal
    for i in list(range(h_bound)):
        start_probability[i] = (1/h_bound)
    print "ps: ", start_probability

    #start_probability = {0: 0.125, 1: 0.125, 2: 0.125, 3: 0.125, 4: 0.125, 5: 0.125, 6: 0.125, 7: 0.125}

    # end_st
    end_state = 'E'
     
    # a 
    # for i in list(range(h_bound)):
    #     transition_probability[i]={}
    #     for j in list(range(h_bound)):
    #         transition_probability[i][j] = (1/h_bound)

    # print "\nt: ", transition_probability

    # a 
    transition_probability = { 
      0 : {0: 0.4, 1: 0.2, 2: 0.2, 3: 0.1, 4: 1.0, 5: 0.0},
      1 : {0: 0.2, 1: 0.4, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0},
      2 : {0: 0.1, 1: 0.2, 2: 0.4, 3: 0.2, 4: 0.075, 5: 0.025},
      3 : {0: 0.025, 1: 0.075, 2: 0.2, 3: 0.4, 4: 0.2, 5: 1.0},
      4 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.4, 5: 0.2},
      5 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.2, 5: 4.0},
      }

    command = 'Rscript'
    path2script = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'
    #args = [,,'11']
    cmd = [command, path2script] + args
    reads = int(subprocess.check_output(cmd, universal_newlines=True))

    # print "min max: ", mi_ma[0], mi_ma[1]
    # # e
    # for i in list(range(h_bound)):
    #     emission_probability[i]=OrderedDict()
    #     for j in np.linspace(mi_ma[0],mi_ma[1],4):
    #         print j
    #         emission_probability[i][j] = OrderedDict()
    #         emission_probability[i][j] = (1/h_bound)

    # print "\ne: ", emission_probability
    #pprint.pprint(emission_probability)

    #emission_probability = {
    #   0 : {0: 0.4, 1: 0.2, 2: 0.15, 3: 0.15, 4: 0.1}, #.........
    #   1 : {0: 0.2, 1: 0.4, 2: 0.2, 3: 0.1, 4: 0.1},
    #   2 : {0: 0.1, 1: 0.2, 2: 0.4, 3: 0.2, 4: 0.1},
    #   3 : {0: 0.1, 1: 0.1, 2: 0.2, 3: 0.4, 4: 0.2},
    #   4 : {0: 0.1, 1: 0.15, 2: 0.15, 3: 0.2, 4: 0.4},
    #   }
    sys.exit()
    return fwd_bkw(observations,
                       states,
                       start_probability,
                       transition_probability,
                       emission_probability,
                       end_state)

def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
    print "x: ", x
 
    fwd = []
    f_prev = {}

    # forward part of the algorithm
    for i, x_i in enumerate(x): # for every observation
        print "\ni, x_i: ", i, x_i
        f_curr = {}
        for st in states: # for every state
            print "state", st
            if i == 0:
                print "base: ", a_0[st]
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                for k in states:
                    print "k: ", "sum", f_prev[k], "*", a[k][st]
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
 
            f_curr[st] = e[st][x_i] * prev_f_sum
            print "f_curr[st] = e[st][x_i] * prev_f_sum: ",f_curr[st], "=",e[st][x_i], "*", prev_f_sum 

        fwd.append(f_curr)
        print "fwd: ", fwd
        f_prev = f_curr
        print "f_prev = f_curr: ", f_prev, "=", f_curr
    
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)
    print "\np_fwd = sum(f_curr[k]*a[k][end_st] for k in states)"
    for k in states:
        print "k: ", k
        print "summing this: f_curr[k]*a[k][end_st]", f_curr[k], "*", a[k][end_st]
        print f_curr[k]*a[k][end_st]
    print "p_fwd: ", p_fwd

    print "\nENTERING BACKWARDS"
    bkw = []
    b_prev = {}
    # backward part of the algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+(None,))):
        print "\ni, x_i_plus: ", i, x_i_plus
        b_curr = {}
        for st in states:
            print "state: ", st
            if i == 0:
                print "base: ", a[st][end_st]
                # base case for backward part
                b_curr[st] = a[st][end_st]
            else:
                for l in states:
                    print "l: ","sum", a[st][l], "*", e[l][x_i_plus], "*", b_prev[l]
                b_curr[st] = sum(a[st][l]*e[l][x_i_plus]*b_prev[l] for l in states)
 
        bkw.insert(0,b_curr)
        print "bkw: ", bkw
        b_prev = b_curr
        print "b_prev = b_curr", b_prev, "=", b_curr
 
    p_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)
    print "\np_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)"
    for l in states:
        print "l: ", l
        print "summing this: ", a_0[l], "*", e[l][x[0]], "*", b_curr[l]
    print "p_bkw", p_bkw

    # merging the two parts
    print "\nPOSTERIOR"
    posterior = []
    for i in range(L):
        print "i: ", i
        for st in states:
            print "fwd*bkw", fwd[i][st],"*",bkw[i][st],"/",p_fwd
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})
        print "posterior", posterior

    assert p_fwd == p_bkw # make sure that the forward and backward probabilities are the same
    return fwd, bkw, posterior

# USAGE
if len(sys.argv) != 5:
    sys.stderr.write("Usage: " + sys.argv[0] + " <M_CELLS> <N_BINS> <K_CLONES> <CHR_NUM>\n")
    sys.exit(1)

# ASSIGNMENT
M_CELLS = int(sys.argv[1])
if M_CELLS <= 2:
    sys.stderr.write("Enter num. cells greater than 2\n")
    sys.exit(1)
N_BINS = int(sys.argv[2])
K_CLONES = int(sys.argv[3])
#H_CN_BOUND = int(sys.argv[3])
CHR = int(sys.argv[4])

# MAIN
if __name__ == '__main__':
    read_matrix = [] # matrix of read depth. 
    read_matrix_norm = [] # matrix RD normalized (reads/())
    read_matrix_coverage = [] # matrix RD coverage (reads/bin_len) * read_len
    cn_tot = [] # num of copies of each cell's genome (chromosome in this case) for normalization
    min_max = [] # find minimum and maximum RD to create emission matrix intervals
    expected_num_reads = [] # expected number of reads for each bin in each cell

    # Get genome length and chromosome (note ind 23 = X and 24 = Y)
    chrom_ID_percentage = [8,7.9,6.5,6.2,5.9,5.5,5.2,4.7,4.6,4.4,4.4,4.3,3.7,3.5,3.3,2.9,2.6,2.5,2.1,2,1.5,1.6,5,1.9]
    chr_chosen_perc_reads = chrom_ID_percentage[CHR-1]
##diplo_len = 3095693981 # normal diploid genome length
    # (3407692*50)/1547846990.5 = 0.1 <- coverage = (read_len * num_reads) / haplo_geno

##chr_num_reads = diplo_len*(chr_chosen_perc_reads/100) # genome length
    # 3407692 average num reads from WGS dataset (Gao et al. 2016)
    # Get num bins at 50KB bins (as in simulations for nbCNV, CNVnator, and control-FREEC)
    
##n_bins = int(float("%.2f" % round(chr_num_reads/220000,0)))  #or 50 000 in nbCNV
    # hard coded:
    chr_num_reads = 28.6585 * 50000 * N_BINS #100
    #n_bins = 4
    print "Cells: ", M_CELLS
    print "Bins: ", N_BINS
    print "Clones (one is diplod): ", K_CLONES
    print "CN Bound:", 5 #H_CN_BOUND
    print "Indexing starts at 0 for cell assignment."
    print "Chromosome chosen: ", CHR
    print "Avg num reads",chr_num_reads,'\n' #eg: 51115.38 for chrom 21

    clone_matrix, assign_cells = create_clones_assign_cells(M_CELLS, N_BINS, K_CLONES, 5) #H_CN_BOUND
    print "Clones: "
    for i in clone_matrix:
        print i
    [cn_tot.append(sum(i)) for i in clone_matrix]
    print "cn_tot: ", cn_tot
    print "Assign: ", assign_cells

    # m draws from the Poisson, one for each cell, to determine number of reads for that chromosome
    cell_chr_tot_num_reads = np.random.poisson(chr_num_reads, M_CELLS)
    print "chr ", CHR ," num reads for each cell: ", cell_chr_tot_num_reads, "\n"

    # Proportion of reads in each bin for each cell
    for idx,i in enumerate(assign_cells):
        print "\ncell : ", idx, " clone: ", i, "d",cell_chr_tot_num_reads[idx],"/","pi", cn_tot[idx]
        e = cell_chr_tot_num_reads[idx]/cn_tot[idx]
        print "e",e
        to_add = []
        to_add_coverage = []
        to_add_norm = []
        pos_expected_num_reads = [] # store expected num reads

        for j in clone_matrix[i]: # copy number 
            print "      j: ", j
            print "prop_reads: ", cell_chr_tot_num_reads[idx],"*(", j,"/", 10,")"
            prop_reads = int(round(cell_chr_tot_num_reads[idx]*(j/10),0)) # gather proportion of reads for cell in question
            print "= prop_reads->", prop_reads
            pos_expected_num_reads.append(prop_reads)

            if prop_reads == 0.0:
                command = 'Rscript'
                path2script = '/Users/ashleymaeconard/Desktop/FB_algo/r_nb_readDepth.r'
                args = ['1', '11']
                cmd = [command, path2script] + args
                reads = int(subprocess.check_output(cmd, universal_newlines=True))
                #reads = int(os.popen("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom 1 .5").read())
                print "nb distr: reads ", reads
                to_add.append(reads)
                to_add_coverage.append((reads/220000)*50)
                print "reads*e", reads, e, reads*e
                to_add_norm.append(reads*e)
                #to_add_norm.append(reads/(cn_tot[i]*cell_chr_tot_num_reads[idx]))
                #to_add.append(0.0)
                #to_add_coverage.append(0.0)
                #to_add_norm.append(0.0)
            else:
                command = 'Rscript'
                path2script = '/Users/ashleymaeconard/Desktop/FB_algo/r_nb_readDepth.r'
                args = [str(prop_reads), '11']
                cmd = [command, path2script] + args
                reads = int(subprocess.check_output(cmd, universal_newlines=True))
                print "nb distr: reads ", reads
                #reads = int(os.popen("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom "+ str(prop_reads) +" .5").read())
               # reads = os.system("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom "+ str(prop_reads) +" .6")
                #print "nb distr: reads ", reads
                #reads = np.random.negative_binomial(prop_reads,.5)
                to_add.append(reads) # sample from negative binomial with p = .5
                to_add_coverage.append((reads/220000)*50)
                #print "reads: ", reads , "/"
                #print "cn_tot[idx]", i, cn_tot[i]
                #print "cell_chr_tot_num_reads[i]: ", idx, cell_chr_tot_num_reads[idx]
                print "reads*e", reads, e, reads*e
                to_add_norm.append(reads*e)
                #to_add_norm.append(reads/(cn_tot[i]*cell_chr_tot_num_reads[idx]))
                #print "reads to add norm: ", to_add_norm
                #print "reads to add norm ", to_add_norm
        read_matrix.append(to_add) # add proportion of reads in each bin for every cell
        read_matrix_coverage.append(to_add_coverage)
        read_matrix_norm.append(to_add_norm)
        expected_num_reads.append(pos_expected_num_reads)
     
    #print "read matrix: "
    #for i in read_matrix:
    #    print i

    print "expected num reads "
    for i in expected_num_reads:
        print i
    print "read matrix"
    for x in read_matrix_norm:
        print x

    # Find min and max of RD matrix across all cells
    # for a in read_matrix_norm:
    #     min_max.append((min(a),max(a)))
#    print "min max ", [(min(a), max(a)) for a in zip(*read_mat)]
    # print "min, max: ", min_max

    # run forward backward algorithm for each cell
    for idx, chr_rd in enumerate(read_matrix_norm):
        print "Run forward, backward: ", idx, chr_rd
        fb_mat = run_FB_one_cell(chr_rd, min_max[idx], 5)# H_CN_BOUND
        sys.exit()

#    for line in run_FB_one_cell(read_matrix_norm, H_CN_BOUND):
#       print line


# class negBin(object):
#     def __init__(self, p = 0.1, r = 10):
#         nbin_mpmath = lambda k, p, r: mpmath.gamma(k + r)/(mpmath.gamma(k+1)*mpmath.gamma(r))*np.power(1-p, r)*np.power(p, k)
#         self.nbin = np.frompyfunc(nbin_mpmath, 3, 1)
#         self.p = p
#         self.r = r

#     def mleFun(self, par, data, sm):
#         '''
#         Objective function for MLE estimate according to
#         https://en.wikipedia.org/wiki/Negative_binomial_distribution#Maximum_likelihood_estimation

#         Keywords:
#         data -- the points to be fit
#         sm -- \sum data / len(data)
#         '''
#         p = par[0]
#         r = par[1]
#         n = len(data)
#         f0 = sm/(r+sm)-p
#         f1 = np.sum(special.psi(data+r)) - n*special.psi(r) + n*np.log(r/(r+sm))
#         return np.array([f0, f1])

#     def fit(self, data, p = None, r = None):
#         if p is None or r is None:
#             av = np.average(data)
#             va = np.var(data)
#             r = (av*av)/(va-av)
#             p = (va-av)/(va)
#         sm = np.sum(data)/len(data)
#         x = optimize.fsolve(self.mleFun, np.array([p, r]), args=(data, sm))
#         self.p = x[0]
#         self.r = x[1]

#     def pdf(self, k):
#         return self.nbin(k, self.p, self.r).astype('float64')


# def constrained_sum_sample_pos(num_cn_changes, n_bins): #Returns a randomly chosen list of n positive integers summing to total.
# # ref: http://stackoverflow.com/questions/1534939/how-to-trim-a-list-in-python
#     #print "num_cn_changes",num_cn_changes," n_bins ", n_bins
#     dividers = sorted(random.sample(xrange(1, n_bins), num_cn_changes-1))
#     #print "dividers", dividers
#     #print "dividers + [n_bins]", dividers + [n_bins]
#     #print "[0] + dividers", [0] + dividers
#     return [a - b for a, b in zip(dividers + [n_bins], [0] + dividers)]
