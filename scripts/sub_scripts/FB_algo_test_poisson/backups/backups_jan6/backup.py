# 
# Forward Backward Algorithm 
# Ashley Conard
# 
import warnings
warnings.filterwarnings("ignore")
import numpy as np


# generating inputs
def generate_inputs():
    
    # the transition matrix
    A = np.array([[.6, .4], [.2, .8]])

    # the emission matrix
    E = np.array([[.5, .5], [.15, .85]])
    
    # sample heads or tails, 0 is heads, 1 is tails
    num_obs = 4
    observations1 = np.random.randn( num_obs ) # s = np.random.negative_binomial(1, 0.1, 100000)
    print "obser: ", observations1
    observations1[observations1>0] = 1
    observations1[observations1<=0] = 0
    return A, E

# forward backward algorithm 
def fb_alg(A_mat, O_mat, observ):
    print "A_mat", A_mat
    print "O_mat", O_mat
    print "OBS", observ
    
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) ) # to include initial state
    bw = np.zeros( (n,k+1) ) # to include initial state
    print "prob_mat: ", prob_mat 
    print "fw: ",fw
    print "bw: ", bw, "\n"
    
    # forward part
    fw[:, 0] = 1.0/n # initial probability of being in state CF (fair coin) or CN (not fair coin)
    print "FORWARD : initial probabilities", fw
    for obs_ind in xrange(k):
        print "\ntime t = : ", obs_ind
        f_row_vec = np.matrix(fw[:,obs_ind])
       # print "f_row_vec", f_row_vec
       # print "np.matrix", np.matrix(A_mat)
       # print "\nO_mat: ", O_mat
       # print "observ[obs_ind]", observ[obs_ind]
       # print "O_mat[:,observ[obs_ind]]", O_mat[:,observ[obs_ind]] # observation at time t
       # print "np.matrix(np.diag(O_mat[:,observ[obs_ind]]))",  np.matrix(np.diag(O_mat[:,observ[obs_ind]]))

        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
        print "fw[:, obs_ind+1]: ", f_row_vec, "*", np.matrix(A_mat), "*", np.matrix(np.diag(O_mat[:,observ[obs_ind]])) , "=", "\nequals fw:",fw
    
    
    # backward part
    bw[:,-1] = 1.0
    print "\nBACKWARD : initial probabilities", bw
    for obs_ind in xrange(k, 0, -1):
        print "\ntime t = : ", obs_ind
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
        print "O_mat[:,observ[obs_ind-1]]", O_mat[:,observ[obs_ind-1]]
        print "bw[:,obs_ind-1]: ", (np.matrix(A_mat)), "*", np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) , "*",b_col_vec.transpose(), "=", "\nequals bw:",bw

    # combine it
    print "np.array(fw)",np.array(fw)
    print "np.array(bw)",np.array(bw)
    prob_mat = np.array(fw)*np.array(bw)
    print "PROB - MAT: ", prob_mat
    print "summm", np.sum(prob_mat, 0)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    print "PROB NORM MAT: ", prob_mat
    
    # get out
    return prob_mat, fw, bw
 
# main
if __name__ == '__main__':
    # get transition and emission matrices 
    A_mat, O_mat = generate_inputs()

    print "\nEntering forward backward algorithm: ", observations1
    p,f,b = fb_alg(A_mat, O_mat, observations1)
    print "FINISHED FB ALGO"
    print "p ", p
    #print "f ", f
    #print "b ", b

    # change observations to reflect messed up ratio
    #observations2 = np.random.random(num_obs)
    #observations2[observations2>.85] = 0
    #observations2[observations2<=.85] = 1
    # majority of the time its tails, now what?
    #p,f,b = fb_alg(A_mat, O_mat, observations1)
    #print p
    #p,f,b = fb_alg(A_mat, O_mat, np.hstack( (observations1, observations2) ))
    #print p