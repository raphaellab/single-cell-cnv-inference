# 
# Forward Backward Algorithm 
# Ashley Conard
# 

#
# fb_algo.py
# To address: 
#           - hard coded bound on allowed repeated copy number for a segment. Allowed range now: 0-20 with 30 max different copy number changes
#        ok - NB regression and overdispersion param. 
#           - when CN is 0 <- still add noise...
#           - use the nbCNV simulated or 12 patient Gao.
#           - coverage
#           - determine chrom. percent based on haplo or diplo?
#           - bin coverage
#           - None, dizzy, cold <- BKW no normal?

# Note:     
#           - not using genome length (total number of copy-number states)
#           - BW -> https://www.math.univ-toulouse.fr/~agarivie/Telecom/code/index.php
#           - R BW -> https://cran.r-project.org/web/packages/HMM/HMM.pdf
#           -> compare!!! -> FB to R FB
#           - normalization: http://www.analytictech.com/ba762/handouts/normalization.htm
#           - http://www.math.uah.edu/stat/apps/SpecialCalculator.html
#           - explanation of dnbinom: http://stats.stackexchange.com/questions/125477/negative-binomial-distribution-r
#           - helpful for negative binomial simulation! http://www.math.uah.edu/stat/bernoulli/NegativeBinomial.html

# Cluster/Dendo:
#           - https://joernhees.de/blog/2015/08/26/scipy-hierarchical-clustering-and-dendrogram-tutorial/#comment-6215
#           - http://stackoverflow.com/questions/7724635/clustering-with-scipy-clusters-via-distance-matrix-how-to-get-back-the-origin?rq=1
#           - http://mathoverflow.net/questions/38855/nodes-clusters-with-a-distance-matrix
#           - potentially the answer: http://stackoverflow.com/questions/18952587/use-distance-matrix-in-scipy-cluster-hierarchy-linkage

# Commands
# python fb_algo.py 3 3 5 21

from __future__ import division
from datadiff import diff
from scipy.cluster.hierarchy import dendrogram
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage
import sys
import subprocess
from scipy.cluster.hierarchy import fcluster
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

def create_clone_transition_mat(n_b, transition_probability):
    #pi = {0: 0.1, 1: 0.2, 2: 0.6, 3: 0.2, 4: 0.075, 5: 0.025}
    draw = np.random.uniform(0,1)

    c = [2] # create a new clone

    for i in range(0, n_b-1):
        #print "\nlast CN: ", c[-1]
        # draw from uniform distribution
        draw = np.random.uniform(0,1)

        # create cumulative list of probabilities
        cum_l = [] # cumulative list of probability values (roulette wheel)
        for idx,i in enumerate(transition_probability[c[-1]].values()):
            if idx == 0:
                cum_l.append((0,i))
            else:
                cum_l.append((cum_l[-1][1],cum_l[-1][1]+i))

        #print "cum_l for CN",c[-1],"is ", cum_l
        for idx,i in enumerate(cum_l):
           minim, maxim = i
           if minim <= draw <= maxim:
                #print "draw: ", draw
                #print "next CN: ", idx
                c.append(idx)
    #print "c: ", c
    return c

def create_clones_assign_cells(m_cells,n_bins,k_clones,h_cn_bound, transition_probability): # Create the true clones (ground truth copy number profiles for each clone)
    clones = []

    for i in range(0,k_clones-1):
        new_clone = create_clone_transition_mat(n_bins, transition_probability)
        clones.append(new_clone)
        #print "new clone: ", clones

    c_dup = [2]*n_bins # diploid case
    clones.append(c_dup)
    #print "final clones", clones

    # Assign cells to clones
    assign_cells = np.random.random_integers(0, k_clones-1, m_cells) # indices are the cells

    return clones, assign_cells

def run_FB_one_cell(observations, transition_probability, emission_probability):

    h_bound=6 # 0-5

    start_probability = {}
    #transition_probability = {}
    #emission_probability = {}
    
    # Hidden states : Z = {0, ..., h} 
    states = list(range(h_bound)) # states
    
    # a_0 : all equal
    for i in list(range(h_bound)):
        start_probability[i] = (1/h_bound)
    print "\nstart_probability: ", start_probability

    # end_st
    end_state = 'E'

    print "observations: ", observations
    print "emission_probability:"
    pprint.pprint(emission_probability)

    print "transition_probability: "
    pprint.pprint(transition_probability)

    print "\nFB_algo"

    return fwd_bkw(observations,
                       states,
                       start_probability,
                       transition_probability,
                       emission_probability,
                       end_state)

def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
    print "x: ", x
 
    fwd = []
    f_prev = {}

    #args = ['READS','MEAN','11']
    #prob_emis = int(subprocess.check_output(cmd, universal_newlines=True))

    # forward part of the algorithm
    for i, x_i in enumerate(x): # for every observation
        print "\ni, x_i: ", i, x_i
        f_curr = {}
        for st in states: # for every CN state
            print "CN state", st
            if i == 0:
                print "base: ", a_0[st]
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                for k in states:
                    print "k: ", "sum", f_prev[k], "*", a[k][st]
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
            
            print "emission prob: NB(x,mu,alpha)", x_i, e[st], 11
            
            # calculate NB emission probability
            commands = 'Rscript'
            scriptpaths = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'
            if e[st]==0.0: # in the case of mean of 0.0 reads for CN 0
                argss = [str(x_i),str(e[st]+1),'11'] # observed RD_i, mean RD_i with 1 read for computation, alpha
            else:
                argss = [str(x_i),str(e[st]),'11'] # observed RD_i, mean RD_i, alpha
            cmds = [commands, scriptpaths] + argss
            prob_emis = float(subprocess.check_output(cmds, universal_newlines=True))
            print "prob_emis", prob_emis

            f_curr[st] = prob_emis * prev_f_sum
            print "f_curr[st] = prob_emis * prev_f_sum: ",f_curr[st], "=",prob_emis, "*", prev_f_sum 

        fwd.append(f_curr)
        print "fwd: ", fwd
        f_prev = f_curr
        print "\nf_prev = f_curr: ", f_prev, "=", f_curr
    
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)

    print "\np_fwd = sum(f_curr[k]*a[k][end_st] for k in states)"
    for k in states:
        print "k: ", k
        print "summing this: f_curr[k]*a[k][end_st]", f_curr[k], "*", a[k][end_st]
        print f_curr[k]*a[k][end_st]
    print "p_fwd: ", p_fwd

    print "\nENTERING BACKWARDS"
    bkw = []
    b_prev = {}
    # backward part of the algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+['E'])):
        print "\ni, x_i_plus: ", i, x_i_plus
        b_curr = {}
        #b_curra = {}

        for st in states:
            print "state: ", st
            if i == 0:
                print "base: ", a[st][end_st]
                # base case for backward part
                b_curr[st] = a[st][end_st]
            else:
                b_curr[st]=0.0 # default setting probability
                for l in states:                    
                    print "\ne [x mean 11]", x_i_plus, e[l], 11
                    # calculate NB emission probability given observation and state
                    commandb = 'Rscript'
                    scriptpathb = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'
                    if e[l]==0.0: # in the case of mean of 0.0 reads for CN 0
                        argb = [str(x_i_plus),str(e[l]+1),'11'] # observed RD_i, mean RD_i with 1 read for computation, alpha
                    else:
                        argb = [str(x_i_plus),str(e[l]),'11'] # observed RD_i, mean RD_i, alpha
                    cmdb = [commandb, scriptpathb] + argb
                    b_prob_emis = float(subprocess.check_output(cmdb, universal_newlines=True))
                    print "e[x mean 11]", b_prob_emis
                    b_curr[st]+= (a[st][l]*b_prob_emis*b_prev[l])
                    print " b_curr[st]", b_curr, " sum(", a[st][l], "*", b_prob_emis ,"*", b_prev[l],")"
                #b_curra[st] = sum(a[st][l]*b_prob_emis*b_prev[l] for l in states)
                #print "b_curra shrunk: ", b_curra #,b_curr[l], "=",b_prob_emis, "*..." 

        bkw.insert(0,b_curr)
        print "bkw: ", bkw
        b_prev = b_curr
        print "b_prev = b_curr", b_prev, "=", b_curr

    # calculate NB emission probability for first
   #  commandbfaa = 'Rscript'
   #  scriptpathbfaa = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'
   #  if e[l]==0.0: # in the case of mean of 0.0 reads for CN 0
   #      argbfaa = [str(x[0]),str(e[l]+1),'11'] # observed RD_i, mean RD_i with 1 read for computation, alpha
   #  else:
   #      argbfaa = [str(x[0]),str(e[l]),'11'] # observed RD_i, mean RD_i, alpha
   #  cmdbfaa = [commandbfaa, scriptpathbfaa] + argbfaa
   #  f_b_prob_emisaa = float(subprocess.check_output(cmdsf, universal_newlines=True))
   #  print "f_b_prob_emisaa", f_b_prob_emisaa
   # ########check how to express l ########
   #  p_bkwaa = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)
   ################

    # print "before: ",p_fwd
    # p_fwad=0.0
    # for k in states:
    #     to_addd = f_curr[k]*a[k][end_st]
    #     print "to_addd:", to_addd
    #     p_fwad+=to_addd
    # print "after: ", p_fwad

    print "\np_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)"
    for l in states:
        p_bkw=0.0 # default setting first observation probability
        print "l: ", l

        # calculate NB emission probability for first
        commandbf = 'Rscript'
        scriptpathbf = '/Users/ashleymaeconard/Desktop/FB_algo/dnbinom_emissions.r'
        if e[l]==0.0: # in the case of mean of 0.0 reads for CN 0
            argbf = [str(x[0]),str(e[l]+1),'11'] # observed RD_i, mean RD_i with 1 read for computation, alpha
        else:
            argbf = [str(x[0]),str(e[l]),'11'] # observed RD_i, mean RD_i, alpha
        cmdbf = [commandbf, scriptpathbf] + argbf
        f_b_prob_emis = float(subprocess.check_output(cmdbf, universal_newlines=True))
        print "f_b_prob_emis", f_b_prob_emis
        p_bkw+=(a_0[l] * f_b_prob_emis * b_curr[l])
        print "p_bkw=",p_bkw #, "summing this: ", a_0[l], "*", e[l][x[0]], "*", b_curr[l]
    print "first observation p_bkw", p_bkw

    # merging the two parts
    print "\nPOSTERIOR"
    posterior = []
    for i in range(L):
        print "i: ", i
        for st in states:
            print "fwd*bkw", fwd[i][st],"*",bkw[i][st],"/",p_fwd
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})
        print "posterior", posterior

    #assert p_fwd == p_bkw # make sure that the forward and backward probabilities are the same
    print "observations", x
    print "fwdd: ", fwd
    print "bkwdd: ", bkw
    print "posterior: ", posterior

    return fwd, bkw, posterior

def create_mu_emission(total_num_reads_cells, genome_len_cells, all_assign_cells, m_cells, n_bins):
    # e 
    mu_emis_matrix = {}
    for i in range(m_cells):
        #print "\ni: ", i
        mu_emis_matrix[i]={}
        for j in range(n_bins+1):
            #print "j ", genome_len_cells[all_assign_cells[i]]
            mu_emis_matrix[i][j]=(total_num_reads_cells[i]/genome_len_cells[all_assign_cells[i]])*j
    return mu_emis_matrix
    #print "emission mu ", mu_e_emis_matrix
    #.append(pos_expected_num_reads)

def pnorm(A,B,L_norm):
    dist = 0.0

    # Check that thee sizes of f and n are the same for all S (soft copy number profiles)
    assert len(A)==len(B)
    assert len(A[0])==len(B[0])

    d_sum_f = d_sum_n = 0.0
    for i in range(1,len(A)): # num bins n
        for f in range(0,len(A[0])): # num hidden CN states f
            d = abs(A[i][f] - B[i][f])**L_norm
            print "A[i][f] B[i][f]", i, f, A[i][f], B[i][f]
            print "d = ", d
            d_sum_f+=d
        d_sum_n+=d_sum_f
        d_sum_f=0.0 # reset
    print "d_sum_n: ", d_sum_n**(1/L_norm)
    return d_sum_n**(1/L_norm)

def create_dist_matrix(S_mat,L_norm):
    dist_mat = {}

    for cell_A in range(len(S_mat)) :
        dist_mat[cell_A] = {}
        for cell_B in range(len(S_mat)):
            if cell_A == cell_B:
                #print "same cell: ", cell_A
                dist_mat[cell_A][cell_B] = 0.0
            else:
                dist_mat[cell_A][cell_B] = pnorm(S_mat[cell_A], S_mat[cell_B], L_norm)

    # Create list from dictionary
    d_l = []
    for i in dist_mat.values():
        l = []
        for j in i.values():
            l.append(j)
        d_l.append(l)
    return d_l 

def create_dendrogram(dist_mat):
    #mat = np.array([[0.0, 2.0, 3.8459253727671276e-16], [2.0, 0.0, 2.0], [3.8459253727671276e-16, 2.0, 0.0]])
    linkage_matrix = linkage(dist_mat, "single")
    #dendrogram(linkage_matrix, color_threshold=1,show_leaf_counts=False)
    print "link:", linkage_matrix
    show_leaf_counts = False
    ddata = fancy_dendrogram(linkage_matrix,
                   color_threshold=1,
                   p=len(dist_mat),
                   truncate_mode='lastp',
                   show_leaf_counts=show_leaf_counts,
                   max_d = T_THRESHOLD
                   )
    plt.title("Dendrogram of %s Cells Similarity" % len(dist_mat))
    plt.xlabel('Cell Number')
    plt.ylabel('Distance: p-norm, p = %s' % P_NORM_VAL)
    plt.show()
    k = NUM_K
    k_clus = fcluster(linkage_matrix, k, criterion='maxclust')
    return k_clus

def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

def assign_hard_CNP(k_cl,S_cells):
    n_clust = []
    print "k_clusters", k_cl
    for a in range(min(k_cl),max(k_cl)+1):
        print "cluster ", a
        n_clust.append([i for i, j in enumerate(k_cl) if j == a])
        print " cells ",n_clust
   
    hard_CNP = {}
    for clu in n_clust:
        cluster_CNP=[]
        print "cluster: ", clu
        for i in range(0,N_BINS):
            print "BIN: ", i
            old_prob = 0.0 # initial probability for f* (argmax of hard CN for each bin for each cell for each cluster)
            new_prob_l = [] # list of numbers to multiply
            for j in range(0,H_CN_BOUND+1):
                print " \nCN: ", j
                for cc in clu:
                    print "cell:", cc
                    print S_cells[cc][i][j]
                    new_prob_l.append(S_cells[cc][i][j])
                new_prob = reduce(lambda x, y: x*y, new_prob_l)
                if new_prob>=old_prob:
                    print "yes - higher probability: ", new_prob
                    old_prob = new_prob
                    max_CN = j 
                    print "coordinates: ", i,j
                new_prob_l = []
            cluster_CNP.append(max_CN)
            print "cluster CNP", cluster_CNP
        for ccc in clu:
            hard_CNP[ccc] = cluster_CNP
    return hard_CNP, n_clust

def compare_to_truth(infer_cells, clone_mat, cell_assign):
    truth = {}
    print "\ncell_Ass:", cell_assign
    print "clone_m", clone_mat
    print "infer: ", infer_cells
    for idx,i in enumerate(cell_assign):
        truth[idx]=clone_mat[i]
    print "truthhh:", truth
    print "\ndifference: ", dict_diff(truth, infer_cells)

def dict_diff(truth, inferred):
    diff = dict()
    diff['truth'] = set(truth) - set(inferred)
    diff['inferred'] = set(inferred) - set(truth)
    diff['difference'] = {k for k in set(truth) & set(inferred) if truth[k]!=inferred[k]}
    return diff

        
###########################
# USAGE
if len(sys.argv) != 8:
    sys.stderr.write("Usage: " + sys.argv[0] + " <M_CELLS> <N_BINS> <K_CLONES> <CHR_NUM> <P_NORM_DIST> <T_THRESHOLD> <NUM_K>\n")
    sys.exit(1)

# ASSIGNMENT
M_CELLS = int(sys.argv[1])
if M_CELLS <= 2:
    sys.stderr.write("Enter num. cells greater than 2\n")
    sys.exit(1)
N_BINS = int(sys.argv[2])
K_CLONES = int(sys.argv[3])
H_CN_BOUND = 5
CHR = int(sys.argv[4])
P_NORM_VAL = int(sys.argv[5])
T_THRESHOLD = float(sys.argv[6])
NUM_K = int(sys.argv[7])

# MAIN
if __name__ == '__main__':

    #S = {0: [{0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}], 1: [{0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0000000000000002, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0000000000000002, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}], 2: [{0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}]}
    #S = {0: [{0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}], 1: [{0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 1.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0}], 2: [{0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}]}
    S = {0: [{0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999997, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999997, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.9999999999999998, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.9999999999999998, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999999, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}], 1: [{0: 0.0, 1: 0.0, 2: 1.0000000000000004, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0000000000000002, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 1.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999998, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999998, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}], 2: [{0: 0.0, 1: 0.0, 2: 0.9999999999999998, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999998, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.9999999999999998, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999998, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999998, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}], 3: [{0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 1.0, 3: 0.0, 4: 0.0, 5: 0.0}], 4: [{0: 0.0, 1: 0.0, 2: 0.9999999999999997, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999997, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.9999999999999994, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999994, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999997, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.9999999999999997, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.9999999999999999, 3: 0.0, 4: 0.0, 5: 0.0}, {0: 0.0, 1: 0.0, 2: 0.0, 3: 1.0, 4: 0.0, 5: 0.0}]}

    distance_matrix = create_dist_matrix(S, P_NORM_VAL)
    print "distance matrix", distance_matrix
    k_clusters = create_dendrogram(distance_matrix)
    hard_CNPs, cell_groups = assign_hard_CNP(k_clusters,S)
    print "hard_CNPS: ", hard_CNPs
    print "cell_groups", cell_groups
    print "num cell groups: ", cell_groups
    assign_cells=[0,1,1,2,1]
    clone_matrix=[[2, 3, 3, 4, 4, 3, 3, 3],[2, 3, 4, 3, 3, 3, 2, 3],[2, 2, 2, 2, 2, 2, 2, 2]]
    final = compare_to_truth(hard_CNPs, clone_matrix, assign_cells)

    sys.exit()

    read_matrix = [] # matrix of read depth. 
    read_matrix_norm = [] # matrix RD normalized (reads/())
    read_matrix_coverage = [] # matrix RD coverage (reads/bin_len) * read_len
    cn_tot = [] # num of copies of each cell's genome (chromosome in this case) for normalization
   # min_max = [] # find minimum and maximum RD to create emission matrix intervals
   # e_emis_matrix = [] # expected number of reads for each bin in each cell

    # Get genome length and chromosome (note ind 23 = X and 24 = Y)
    chrom_ID_percentage = [8,7.9,6.5,6.2,5.9,5.5,5.2,4.7,4.6,4.4,4.4,4.3,3.7,3.5,3.3,2.9,2.6,2.5,2.1,2,1.5,1.6,5,1.9]
    chr_chosen_perc_reads = chrom_ID_percentage[CHR-1]
##diplo_len = 3095693981 # normal diploid genome length
    # (3407692*50)/1547846990.5 = 0.1 <- coverage = (read_len * num_reads) / haplo_geno

##chr_num_reads = diplo_len*(chr_chosen_perc_reads/100) # genome length
    # 3407692 average num reads from WGS dataset (Gao et al. 2016)
    # Get num bins at 50KB bins (as in simulations for nbCNV, CNVnator, and control-FREEC)
    
##n_bins = int(float("%.2f" % round(chr_num_reads/220000,0)))  #or 50 000 in nbCNV
    # a 
    a_trans_matrix = { 
      0 : {0: 0.39, 1: 0.2, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      1 : {0: 0.2, 1: 0.39, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      2 : {0: 0.1, 1: 0.2, 2: 0.39, 3: 0.2, 4: 0.075, 5: 0.025, 'E': 0.01},
      3 : {0: 0.025, 1: 0.075, 2: 0.2, 3: 0.39, 4: 0.2, 5: 0.1, 'E': 0.01},
      4 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.39, 5: 0.2, 'E': 0.01},
      5 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.2, 5: 0.39, 'E': 0.01},
      }

    # hard coded:
    chr_num_reads = 28.6585 * 50000 * N_BINS #100
    #n_bins = 4
    print "Cells: ", M_CELLS
    print "Bins: ", N_BINS
    print "Clones (one is diplod): ", K_CLONES
    print "CN Bound:", H_CN_BOUND
    print "Indexing starts at 0 for cell assignment."
    print "Chromosome chosen: ", CHR
    print "Avg num reads",chr_num_reads,'\n' #eg: 51115.38 for chrom 21

    clone_matrix, assign_cells = create_clones_assign_cells(M_CELLS, N_BINS, K_CLONES, H_CN_BOUND, a_trans_matrix) #H_CN_BOUND
    print "Clones: "
    for i in clone_matrix:
        print i
    [cn_tot.append(sum(i)) for i in clone_matrix]
    print "cn_tot: ", cn_tot
    print "Assign: ", assign_cells

    # m draws from the Poisson, one for each cell, to determine number of reads for that chromosome
    cell_chr_tot_num_reads = np.random.poisson(chr_num_reads, M_CELLS)
    print "chr ", CHR ," num reads for each cell: ", cell_chr_tot_num_reads, "\n"

    # mean values for emission probabilities 
    mu_e_emis_matrix = create_mu_emission(cell_chr_tot_num_reads, cn_tot, assign_cells, M_CELLS, N_BINS)
    #print "mu_e_emis_matrix", mu_e_emis_matrix
    # Proportion of reads in each bin for each cell
    for idx,i in enumerate(assign_cells):
        print "\ncell : ", idx, " clone: ", i, "d",cell_chr_tot_num_reads[idx],"/","pi", cn_tot[i]
        e = cell_chr_tot_num_reads[idx]/cn_tot[i]
        print "if CN is 1 (e): ",e
        to_add = []
        to_add_coverage = []
        to_add_norm = []
        #pos_expected_num_reads = [] # store expected num reads

        for j in clone_matrix[i]: # copy number 
            print "      j: ", j
            #print "prop_reads: ", cell_chr_tot_num_reads[idx]/cn_tot[idx]),"*(", j,"/", 10,")"
            #prop_reads = int(round(cell_chr_tot_num_reads[idx]*(j/10),0)) # gather proportion of reads for cell in question
            #print "= prop_reads->", prop_reads
            print "prop_reads ->", "e*j", e*j
            prop_reads = e*j
            #pos_expected_num_reads.append(prop_reads)

            if prop_reads == 0.0:
                command = 'Rscript'
                path2script = '/Users/ashleymaeconard/Desktop/FB_algo/r_nb_readDepth.r'
                args = ['1', '11']
                cmd = [command, path2script] + args
                reads = int(subprocess.check_output(cmd, universal_newlines=True))
                #reads = int(os.popen("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom 1 .5").read())
                print "nb distr: reads ", reads
                to_add.append(reads)
                to_add_coverage.append((reads/50000)*50)
                #print "normalized reads/e", reads, e, reads/e
                to_add_norm.append(reads/e)
                #to_add_norm.append(reads/(cn_tot[i]*cell_chr_tot_num_reads[idx]))
                #to_add.append(0.0)
                #to_add_coverage.append(0.0)
                #to_add_norm.append(0.0)
            else:
                command = 'Rscript'
                path2script = '/Users/ashleymaeconard/Desktop/FB_algo/r_nb_readDepth.r'
                args = [str(prop_reads), '11']
                cmd = [command, path2script] + args
                reads = int(subprocess.check_output(cmd, universal_newlines=True))
                print "nb distr: reads ", reads
                #reads = int(os.popen("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom "+ str(prop_reads) +" .5").read())
               # reads = os.system("/Users/ashleymaeconard/Desktop/misc/misc/build/Debug/negbinom "+ str(prop_reads) +" .6")
                #reads = np.random.negative_binomial(prop_reads,.5)
                to_add.append(reads) # sample from negative binomial with p = .5
                to_add_coverage.append((reads/50000)*50)
                #print "reads: ", reads , "/"
                #print "cn_tot[idx]", i, cn_tot[i]
                #print "cell_chr_tot_num_reads[i]: ", idx, cell_chr_tot_num_reads[idx]
                #print "reads/e", reads, e, reads/e
                to_add_norm.append(reads/e)
                #to_add_norm.append(reads/(cn_tot[i]*cell_chr_tot_num_reads[idx]))
                #print "reads to add norm ", to_add_norm
        read_matrix.append(to_add) # add proportion of reads in each bin for every cell
        read_matrix_coverage.append(to_add_coverage)
        read_matrix_norm.append(to_add_norm)
     
    # print "read matrix: "
    # for i in read_matrix:
    #    print i

    # print "expected num reads "
    # for i in mu_e_emis_matrix.items():
    #     print i

    # print "read matrix"
    # for x in read_matrix_norm:
    #     print x
    # Find min and max of RD matrix across all cells
    # for a in read_matrix_norm:
    #     min_max.append((min(a),max(a)))
#    print "min max ", [(min(a), max(a)) for a in zip(*read_mat)]
    # print "min, max: ", min_max

    # run forward backward algorithm for each cell
    S = {}
    for idx, chr_rd in enumerate(read_matrix):
        print "Run forward, backward: ", idx, chr_rd
        fwd_mat, bkw_mat, posterior_mat = run_FB_one_cell(chr_rd, a_trans_matrix, mu_e_emis_matrix[idx]) #min_max[idx]
        print "cell idx, chr_rd", idx, chr_rd
        S[idx]= posterior_mat
    print "S: ", S
     
#    for line in run_FB_one_cell(read_matrix_norm, H_CN_BOUND):
#       print line


# def augmented_dendrogram(*args, **kwargs):
#     ddata = dendrogram(*args, **kwargs)
#     if not kwargs.get('no_plot', False):
#         for i, d in zip(ddata['icoord'], ddata['dcoord']):
#             x = 0.5 * sum(i[1:3])
#             y = d[1]
#             plt.plot(x, y, 'ro')
#             plt.annotate("%.3g" % y, (x, y), xytext=(0, -8),
#                          textcoords='offset points',
#                          va='top', ha='center')
#     return ddata
