#!/usr/bin/python
#
# create_scatter.py : Create scatter plot of the distance between truth and joint, and truth and indep. 
#	
# Ashley Conard

from __future__ import division
from datadiff import diff
import seaborn as sns
import ast
from collections import Counter
import matplotlib.pyplot as plt
import math as m
import sys
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import subprocess
from scipy.cluster.hierarchy import fcluster
import numpy as np
import random
import os
import scipy.special as special
from collections import OrderedDict


def plot_cnp(clos):
	alpha_cl = clos.keys()[0][0]
	cells_cl = clos.keys()[0][1]
	clones_cl = clos.keys()[0][2] 
	N_BINS = len(clos.values()[0][0])

	ind = range(1,N_BINS+1)

	for idx, i in enumerate(clos.values()[0]):
		cl = i
		plt.scatter(ind,cl)
		plt.plot(ind,cl)
		plt.title("Clone %d CNP: %d Cells, %d Clones, %d Bins" % (idx,cells_cl,clones_cl,N_BINS))
		plt.xlabel('Bin')
		plt.ylabel('Integer Copy-Number')
		plt.savefig(OUTDIR+'/cnp_clone%d_of%dclones_%dcells_%dbins.pdf' % (idx,clones_cl,cells_cl,N_BINS))
		#plt.show()
		plt.clf()
	return alpha_cl, cells_cl, clones_cl, N_BINS

def plot_joint_cnp(joi):
	alpha_cl = joi.keys()[0][0]
	cells_cl = joi.keys()[0][1]
	clones_cl = joi.keys()[0][2] 
	N_BINS = len(joi.values()[0][0])
	#print "a, c, clo: N_BINS, ", alpha_cl, cells_cl, clones_cl, N_BINS
	#print "j: ", joi.values()[0]
	ind = range(1,N_BINS+1)

	for k,v in joi.values()[0].items():
		cel_cnp = v
		plt.scatter(ind,cel_cnp)
		plt.plot(ind,cel_cnp)
		plt.title("Jointly Inferred CNP Cell %d of %d Cells %d Bins" % (k,cells_cl,N_BINS))
		plt.xlabel('Bin')
		plt.ylabel('Integer Copy-Number')
		plt.savefig(OUTDIR+'/joint_cell_cnp%d_of%dclones_%dcells_%dbins.pdf' % (k,clones_cl,cells_cl,N_BINS))
		#plt.show()
		plt.clf()
	#return alpha_cl, cells_cl, clones_cl, N_BINS

def manhattan_distance(x,y):
    #print "dist truth ", x
    #print "dist other ", y
    return sum(abs(a-b) for a,b in zip(x,y))

def get_max(S_matrix):
	max_dict={}
	for cell,bins in S_matrix.items():
		#print "cell ", cell
		max_dict[cell]={}
		l = []
		for b in bins:
			#print "\nb", b
			b_max_val= max(b,key=b.get)
			#print "b_max_val", b_max_val
			l.append(b_max_val)
		max_dict[cell] = l
	return max_dict

def normalized_manhattan_distance(inf,tru):
    assert len(inf)==len(tru)# make sure same number of cells
    assert len(inf[0])==len(tru[0])# make sure same number of bins
    full_manh = 0
    #print "m*n", len(inf), '*', len(inf[0])
    for i in range(0,len(inf)):
        full_manh+=manhattan_distance(inf[i],tru[i])
    L1_final_dist = full_manh/(len(inf)*len(inf[0])) # manhattan distance / (m * n)
    return L1_final_dist

def find_num_cn_changes(cl):
    list_clone_changes = []

    for idx,i in enumerate(cl):
        #print "\nNew cell", idx
        counter = 0
        first = True
        for j in i:
            #print "next: ", j
            if first:
                cur = j
                #print "first: ", cur
                first = False
            else:
                if cur!=j:
                    counter+=1
                    cur=j
                    #print "not eq: ", j
        list_clone_changes.append(counter) 
        #print "clone ", idx, "cn changes",counter
    #print list_clone_changes.index(min(list_clone_changes))
    return list_clone_changes

def read_file(input_file):
    truth_dict = {}
    joint_dict = {} 
    indep_dict = {}
    clones_dict = {}

    alpha = 0
    with open(input_file,'r') as f:
		for line in f:
			if line.startswith("a "):
				x = line
				x = [i for i in x.split()]
				#print "x", x
				#if alpha==0:
				alpha = int(x[1])
				ms = int(x[3])
				ks = int(x[5])

				# elif int(x[1])!=alpha:
				# 	#print "new alpha ", int(x[1])
				# 	#dict_max_posteriors[alpha] = alpha_dict
				# 	#print dict_max_posteriors
				# 	#alpha_dict = {}#OrderedDict()
				# 	alpha = int(x[1])
				# 	ms = int(x[3])
				# 	ks = int(x[5])
				# else:
				# 	alpha = int(x[1])
				# 	ms = int(x[3])
				# 	ks = int(x[5])

				#print "\nalph", alpha, "m ", ms, "k ", ks
			elif "Clones: " in line:
				clo = ast.literal_eval(line.split('Clones: ', 1)[-1].strip(" ")) # get()
				clo.pop(-1)
				clones_dict[(alpha,ms,ks)] = clo

			elif "Indep_Hard_CNPs" in line: # GET INDEPENDENT CELL CNPS
				hard_indep = ast.literal_eval(line.split('Indep_Hard_CNPs:', 1)[-1].strip(" ")) # get()
				#print "indep_hard_cnp", hard_indep
				#print "alpha, m, k", alpha, ms, ks
				indep_dict[(alpha,ms,ks)] = hard_indep

			elif "Joint_Hard_CNPs" in line: # GET JOINT CELL CNPS
				hard_joint = ast.literal_eval(line.split('Joint_Hard_CNPs:', 1)[-1].strip(" "))
				#print "joint_hard_cnp", hard_joint 
				#print "alpha, m, k", alpha, ms, ks
				joint_dict[(alpha,ms,ks)] = hard_joint
			
			elif "True_CNPs" in line: # GET TRUE CELL CNPS
				true_cnps = ast.literal_eval(line.split('True_CNPs:', 1)[-1].strip(" "))
				#print "true_cnps", true_cnps
				#print "alpha, m, k", alpha, ms, ks
				truth_dict[(alpha,ms,ks)] = true_cnps
				#alpha_dict[(ms,ks)] = normalized_manhattan_distance(s_max_prof,true_cnps)
				#print "alpha dict: ", alpha_dict
			# elif line.startswith("Alpha Heat Map "):
			# 	joint_infered = ast.literal_eval(line.split('Alpha Heat Map Dictionary ',1)[-1].strip(" "))
				#print "joint_CNPs", joint_infered
		#dict_max_posteriors[alpha] = alpha_dict
		#print dict_max_posteriors
    #joint_infered = OrderedDict(sorted(joint_infered.items()))#[OrderedDict(v) for k,v in joint_infered.items()]
    return truth_dict, joint_dict, indep_dict, clones_dict

###########################
# USAGE
if (len(sys.argv) !=3):
    sys.stderr.write("Usage: " + sys.argv[0] +" #alphas_#ms_#ks_output.txt "+ "OUT_DIR/NEW_DIR_NAME (if needed) \n")
    sys.exit(1)

# ASSIGNMENTS
FILE = str(sys.argv[1])
OUTDIR = str(sys.argv[2]) 
if not os.path.exists(OUTDIR):
    os.makedirs(OUTDIR)

if __name__ == '__main__':
   	label = []
   	j_i_to_scatter = []

   	tru,join,indep,clones = read_file(FILE)
   	#print "join: ", join
   	#for k,v in join.items():
   		#for key,val in v.items():
   			#print val

   	#in print "CN Changes Truth: ", num_cn_changes_clones
   	#print "CN Changes Joint Inf.: ", num_cn_cells_jointly
   	#print "\ntrue: ", tru
   	#print "\njoint: ", join
   	#print "\nindep: ", indep
   	#print "\nclones: ", clones
 #   	plot_joint_cnp(join) # plot CNPs for all jointly inferred cells

 #   	ALPHA, M_CELLS, K_CLONES, N_BINS = plot_cnp(clones)

	# t_cnps = [val for k,v in tru.items() for key,val in v.items()] # get all CNPs from truth
 #   	j_cnps = [val for k,v in join.items() for key,val in v.items()] # get all CNPs from joint

 #   	num_cn_changes_truth = find_num_cn_changes(t_cnps)
 #   	num_cn_cells_jointly = find_num_cn_changes(j_cnps)

 #   	file_out = "CN_changes_{}m_{}k_{}n.txt".format(M_CELLS, K_CLONES, N_BINS)
 #   	count_cell = -1
 #    	with open(OUTDIR+"/"+file_out, "w") as out:
 #        	out.write(("Cell\tTruth_Changes\tJoint_Infer_Changes\n"))
 #        	for t,j in zip(num_cn_changes_truth, num_cn_cells_jointly):
 #        		count_cell+=1
	#         	out.write((str(count_cell)+"\t"+str(t)+"\t"+str(j)+"\n"))


   	for (t_k,t_v),(j_k,j_v),(i_k,i_v) in zip(tru.items(),join.items(),indep.items()):
   		assert t_k==j_k==i_k # assert that the same alpha, num cells, and k are used
   		if t_k[0]==11:
	   		#print "joint - t_k,j_k,i_k", t_k,j_k,i_k
	   		#print "len first category cells t", len(t_v)
	   		#print "len first category cells j", len(j_v)
	   		#print "len first category cells i", len(i_v)
	   		# joint is 1 * #cells
	   		# indep is 2 * #cells
	   		
	   		# compare truth to joint
	   		for (t_cell,t_prof), (j_cell, j_prof) in zip(t_v.items(), j_v.items()):
	   			#print "joint - t_k,j_k,i_k", t_k,j_k,i_k
	   			assert t_cell==j_cell # assert that the same cells are being considered
	   			assert len(t_prof)==len(j_prof)# assert that profiles are same length
	 
	   			dist = manhattan_distance(t_prof, j_prof)
	   			#print "dist ", dist
	   			j_i_to_scatter.append(dist) # for each cell, get the distance from truth
	   			label.append('Inferred Jointly')
	   		#print "distance for joint ", j_i_to_scatter
	   		#print "label ", label
	   		
	   		# compare truth to independent
	   		for (t_cell,t_prof), (i_cell, i_prof) in zip(t_v.items(), i_v.items()):
	   			#print "indep - t_k,j_k,i_k", t_k,j_k,i_k
	   			assert t_cell==i_cell # assert that the same cells are being considered
	   			assert len(t_prof)==len(i_prof)# assert that profiles are same length

	   			dist = manhattan_distance(t_prof, i_prof)
	   			#print "dist", dist
	   			j_i_to_scatter.append(dist)
	   			label.append('Inferred Independently')
	   		#print "distance for indep ", j_i_to_scatter

	   		#print "distance for indep ", j_i_to_scatter
	   		#print "label ", label
	   		assert label.count('joint')==label.count('indep') # check that the distance vectors are the same for both sets of cells

	   		plt.title("Manhattan Distance from True CNPs for "+str(t_k[1])+" Cells "+str(t_k[2])+" Clones")
	   		plt.ylabel('Manhattan Distance from Truth')
	   		plt.xlabel('Hard Copy Number Profiles')
	   		plt.ylim(-10, 110)
	   		sns.stripplot(label,j_i_to_scatter, jitter=True);   	
	   		sns.plt.savefig(OUTDIR+'/scatter_%dcells_%dclones_%dbins.pdf' % (t_k[1],t_k[2],len(t_prof)))	

	   		sns.plt.show()
        	sns.plt.clf()
        	label = []
        	j_i_to_scatter = []


   	#print distance_s_indep

# for (k,v),(k2,v2) in zip(j.items(),i.items()):
# 	dist = manhattan_distance(v,v2)
# 	print "k,k2",k,k2
# 	j_sc.append(dist)
# 	print dist
# 	lab.append('Inferred Jointly')

