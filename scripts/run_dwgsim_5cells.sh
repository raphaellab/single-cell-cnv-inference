#!/bin/bash

# Usage
if [ ${#@} == 0 ]; then
#if [ $# -ne 1 ]; then
    echo $0: Usage: ./run_dwgsim_5cells.sh GENOME_DIR/genome_rearranged.fasta CELL_TYPE_NAME NUM_READS
    exit 1
fi

/n/fs/ragr-code/bio/DWGSIM_2/dwgsim -N $3 -n 101 -e 0.01 -d 1 -r 0 -1 101 -2 0 -z 0 $1 cell0$2.out &
/n/fs/ragr-code/bio/DWGSIM_2/dwgsim -N $3 -n 101 -e 0.01 -d 1 -r 0 -1 101 -2 0 -z 1 $1 cell1$2.out &
/n/fs/ragr-code/bio/DWGSIM_2/dwgsim -N $3 -n 101 -e 0.01 -d 1 -r 0 -1 101 -2 0 -z 2 $1 cell2$2.out &
/n/fs/ragr-code/bio/DWGSIM_2/dwgsim -N $3 -n 101 -e 0.01 -d 1 -r 0 -1 101 -2 0 -z 3 $1 cell3$2.out &
/n/fs/ragr-code/bio/DWGSIM_2/dwgsim -N $3 -n 101 -e 0.01 -d 1 -r 0 -1 101 -2 0 -z 4 $1 cell4$2.out &
wait
