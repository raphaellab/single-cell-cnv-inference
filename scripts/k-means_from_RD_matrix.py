#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
from matplotlib import pyplot
matplotlib.rcParams['backend'] = "Qt4Agg"
import matplotlib.pyplot as plt 
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import csv
import pysam as pys
import pandas as pd
from sklearn import cluster
from sklearn.cluster import KMeans
import numpy as np

# resource: http://fromdatawithlove.thegovans.us/2013/05/clustering-using-scikit-learn.html

# Ashley Mae
# Created July 20, 2017
#####
#
# k-means_from_RD_matrix.py
# e.g.: python singleCell/single-cell-cnv-inference/scripts/k-means_from_RD_matrix.py results_gather/test_all_mast_df_indices.csv 4 T5_normal
# 
#####

# USAGE
if len(sys.argv) != 4:
    sys.stderr.write("Usage: python " + sys.argv[0] + " <MATRIX_RD (bin x cell)> <NUM_CLUSTERS> <NAME_PATIENT_AND_CELLS(tumor or normal)>\n") # <LOCI_IN_FILE (FROM DEPTH COMMAND)> 
    sys.exit(1)

# ASSIGNMENT
FILE = sys.argv[1] 
K = int(sys.argv[2])
OUTPUT_NAME = sys.argv[3]

if __name__ == '__main__':
	df = pd.read_csv(FILE).ix[:,4:] # get only cells
	list_cells = list(df)
	print "headers: ", list_cells
	df = df.transpose()
	
	X = df.as_matrix(columns=df.columns[:])
	#print X#[0][0], X[1], X[2][0]
	#centroids,labels,inertia = cluster.k_means(X,n_clusters=k)

	 #np.array([[1, 2], [1, 4], [1, 0],[4, 2], [4, 4], [4, 0]])
	kmeans = KMeans(n_clusters=K, random_state=0).fit(X)
	labels = kmeans.labels_
	centroids = kmeans.cluster_centers_
	
	print "There are: ", len(kmeans.labels_), "kmeans.labels_"#, kmeans.labels_
	#print "kmeans.centroids", kmeans.cluster_centers_
	
	# Create tuples of labels and cells:
	cluster_assignments = zip(list_cells, labels)
	#print cluster_assignments

	with open(OUTPUT_NAME+'_'+str(K)+'_clusters__k-means_from_RD_matrix.csv','wb') as out:
	    csv_out=csv.writer(out)
	    csv_out.writerow(['Cell','Cluster'])
	    for row in cluster_assignments:
	        csv_out.writerow(row)


	for i in range(K):
	    # select only data observations with cluster label == i
	    ds = X[np.where(labels==i)]
	    # plot the data observations
	    pyplot.plot(ds[:,0],ds[:,1],'o')
	    # plot the centroids
	    lines = pyplot.plot(centroids[i,0],centroids[i,1],'kx')
	    # make the centroid x's bigger
	    pyplot.setp(lines,ms=8.0)
	    pyplot.setp(lines,mew=1.0)
	pyplot.show()
