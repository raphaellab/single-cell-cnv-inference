#!/bin/sh
# Gathers data from each .bam file (SORTED) generated and passes it to an R script to make a file "chrom, position, num_reads"
# Ashley Conard
# Sept 27, 2016

#ARGUMENTS
# 1: List Chromosome , example: chr2, chr3 ...
# 2: Directory of data, example: ../data/read_depth_data/
# 3: .bam File, example SRR1506318_1.fastq.sorted.bam
# 4: Desired output directory
# 5: Bin (skip) size desired
# 6: Read length 76 (Gao), which is single-end
# 7: Path to read_depth_plot.py, example: ~/Desktop/singleCell/single-cell-cnv-inference/scripts/scripts_plot_readDepth_normal/read_depth_plot.py

# Example: ./gather_read_depth_data.sh chr17 ../data/read_depth_data/ SRR3082145_1.fastq.gz.SORTED.bam ../results/ 200000
# Example2: ./gather_read_depth_data.sh chr17 ~/Desktop/singleCell/data/read_depth_data/ SRR3082145_1.fastq.gz.SORTED.bam ~/Desktop/results_gather/ 250000 76 ~/Desktop/singleCell/single-cell-cnv-inference/scripts/scripts_plot_readDepth_normal/ 

#METHOD CALLS
echo "Calling Samtools for $2"
echo "Stats: "
samtools flagstat $2$3
echo "For single-end, I want # mapped alignments, not those marked as secondary or supplementary. so number of mapped alignments: " 
samtools view -F 0x904 -c $2$3
# Getting average and stdev for read depth of each cell
echo "Cell average read depth and standard deviation:"
samtools depth $2$3 |  awk '{sum+=$3; sumsq+=$3*$3} END { print "Average = ",sum/NR; print "Stdev = ",sqrt(sumsq/NR - (sum/NR)**2)}'

# Getting genome length of each cell
genomeLen=$(samtools view -H $2$3 | grep '^@SQ' | cut -f 3 -d ':' | awk '{sum+=$1} END {print sum}')
echo "Cell Genome Length : $genomeLen"

# Get each locus depth
samtools depth $2$3 -r $1 > $3_$1_depth_loci.txt
mv $3_$1_depth_loci.txt $2/

# Calling Read Depth 
echo "Calling python on $2$3"
python "$7"read_depth_plot.py "$1" "$2$3" "$2$3_$1_depth_loci.txt" "$3" "$4" "$5" "$6"


##### Alternative way using R and plotting by locus:
# samtools depth "$2$3" > "$3_sorted.coverage"
# filename="$3_$1_SORTED.coverage"
# awk'$1=="$1"{print $0}' "$3_SORTED.coverage" > "$3_$1_sorted.coverage"
# Rscript plot_read_depth_byLocus_R.r

# Note: Not sure why it is wrong 
	# Getting Start and End - JUST READS. NOT ACCURATE
	#start=$(samtools view $2$3 | awk /$1/ | head -1 | cut -f 4 )
	#end=$(samtools view $2$3 | awk /$1/ | tail -1 | cut -f 4 )
	#echo start of chrom $1 : $start
	#echo end of chrom $1: $end

	# Getting approx length of chromosome given by first and last read covering it
	#endStart=$(expr $end - $start)
	#echo end - start $endStart