#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import collections as c
import numpy as np
import pysam as pys

# Ashley Mae
# Created Sept 27, 2016
#####
#
# read_depth_plot.py: plots the read depth
# 
#####

# ASSIGNMENTS FROM MERGED_MAF2MAF_MCG
CHROM = 0
POS = 1
NUM_READS = 2

def read_file(file, bin_size):
    genome_dict = c.defaultdict(list)

    with open(file) as f:
        #header = next(f)
        bin_size_iterator = 0
        bin_num_reads = 0
        num_bins = 0

        for line in f:
            s = line.rstrip("\r\n").split('\t')
            e = [a.replace(' ','') for a in s]

            if bin_size_iterator < bin_size:
                bin_size_iterator += 1 # counting the loci 
                bin_num_reads += int(e[NUM_READS]) # adding up the number of reads for each locus
            
            elif bin_size_iterator <= bin_size:
                bin_size_iterator = 0 # reset bin_size_iterator
                num_bins+=1
                genome_dict[num_bins] = bin_num_reads
                bin_num_reads = 0 # resent bin_size_iterator

    print genome_dict[2]
    return genome_dict

def grouper(n, iterable):
    it = iter(iterable)
    while True:
       chunk = tuple(itertools.islice(it, n))
       if not chunk:
           return
       yield chunk

def get_start_end_chrom(l_file): # bam file is l_file
    with open(l_file, "rb") as f:
        first = int(f.readline().rstrip("\r\n").split('\t')[POS])      # Read the first line.
        f.seek(-2, 2)             # Jump to the second last byte.
        while f.read(1) != b"\n": # Until EOL is found...
            f.seek(-2, 1)         # ...jump back the read byte plus one more.
        last = int(f.readline().rstrip("\r\n").split('\t')[POS]) # Read last line.
    return first, last

def fill_in_gaps(gap_start,gap_end,x_add,y_add):
    #print gap_start, gap_end
    for i in range(gap_start, gap_end):
        #print "gap", i
        x_add.append(i)
        y_add.append(0)
    return x_add, y_add

def loci_file(l_file): # with DEPTH command
    x = []
    y = []
    starting_position = 0 # get the first position
    with open (l_file) as f:
        for line in f:
            s = line.rstrip("\r\n").split('\t')
            e = [a.replace(' ','') for a in s]
            
            if starting_position==0:
                starting_position=int(e[POS])
                counter=int(e[POS])-1 # only replace the starting_position once
            counter+=1

            #print e[CHROM], e[POS], e[NUM_READS], starting_position
            if int(e[POS])!=counter:
                x,y = fill_in_gaps(int(counter), int(e[POS]), x, y)
            else:
                #print starting_position,counter
                x.append(int(e[POS]))
                y.append(int(e[NUM_READS]))

            # Print high read depth loci
            if int(e[NUM_READS]) > 7:
                print e[NUM_READS], e[POS]

    # Print length of chromosome in question
    print "Length of chromosome: ", int(x[-1]) - int(x[0]), "start: ", x[0], "end: ", x[-1]
    start_chr=int(x[0])
    end_chr=int(x[-1])

    # Plot read depth
    f, ax = plt.subplots()
    ax.plot(x,y)
    ax.set_title('Locus RD %s %s %s-%s Bin %s'%(NAME_SAMPLE,CHR,str(x[0]),str(x[-1]),BIN_SKIP))
    #plt.show()

    # Save read depth plot
    text_l = '%s/locus_RD_%s_%s_%s_%s.png'%(OUTPUT_DIR,NAME_SAMPLE,CHR,str(x[0]),str(x[-1]))
    f.savefig(text_l,dpi=300, format='png',bbox_inches='tight')
    return start_chr, end_chr

def plot_readDepth(chrm,start,end,file): # with FETCH command
    reads = pys.AlignmentFile(file,"rb")

    # Example of how to print the lines where each read begins
    # iter = reads.fetch('chr1', 0, 84478)
    # for i in iter:
    #     print (str(i))

    skip = BIN_SKIP # bins of 1000 bp 
    x = [] # x axis
    y = [] # y axis

    print "Starting to count read depth for skip ", skip

    text_w = '%s/data_binned_RD_%s_%s_%s_%s_%s_fetch.csv'%(OUTPUT_DIR,NAME_SAMPLE,chrm,str(start),str(end),BIN_SKIP)
    with open(text_w, "a") as fp:
        for i in range(start,end,skip):
            raw_counts = len(list(reads.fetch(chrm,i,i+skip)))
            depth = raw_counts/float(skip)*READ_LEN # calc read depth
            fp.write('{} {} {} {}\n'.format(chrm, i, i+skip, depth))
            #myfile.write((str(chrm),str(i),str(i+skip),str(depth)))
            #print i,i+skip,depth
            #print list(reads.fetch(chrm,i,i+skip))[0],depth
            x.append(i)
            y.append(depth)
    
    print "End of count read depth"
    f, ax = plt.subplots()
    ax.plot(x,y)
    plt.xticks(x, x)
    ax.set_title('Read Depth %s %s %s-%s'%(NAME_SAMPLE,chrm,str(start),str(end)))
    text_b = '%s/binned_RD_%s_%s_%s_%s_%s_fetch.png'%(OUTPUT_DIR,NAME_SAMPLE,chrm,str(start),str(end),BIN_SKIP)
    f.savefig(text_b,dpi=300, format='png',bbox_inches='tight')

# USAGE
if len(sys.argv) != 8:
    sys.stderr.write("Usage: " + sys.argv[0] + " <CHROMOSOME (e.g. chr2)> <SORTED_BAM_FILE> <LOCI_IN_FILE (FROM DEPTH COMMAND)> <CELL_NAME> <OUTPUT_FILE> <BIN_SIZE> <READ_LEN>\n")
    sys.exit(1)

# ASSIGNMENT
CHR = sys.argv[1]
FILE = sys.argv[2]
LOCI_IN_FILE = sys.argv[3]
NAME_SAMPLE = sys.argv[4]
OUTPUT_DIR = sys.argv[5]
BIN_SKIP = int(sys.argv[6])
READ_LEN = int(sys.argv[7])
#DATA_FILE = sys.argv[1] # CHROM\tPOS\tNUM_READS
#BINSIZE = int(sys.argv[2]) # BIN SIZE DESIRED
#STARTING_READ = int(sys.argv[2])
#ENDING_READ = int(sys.argv[3])

# METHOD CALLING
#dic = read_file(DATA_FILE, BINSIZE)

#start_chrom, end_chrom = loci_file(LOCI_IN_FILE)

start_chrom, end_chrom = get_start_end_chrom(LOCI_IN_FILE)
result = plot_readDepth(CHR, start_chrom, end_chrom, FILE)

sys.exit()
# PRINTING
plt.hist(dic, bins=len(dic))
plt.title("Read Depth Histogram")
plt.xlabel("Bin")
plt.ylabel("Frequency")
plt.show()

fig = plt.gcf()
plot_url = py.plot_mpl(fig, filename='genome_read_depth')

# out_file = "merged-MAF2MAF-MCG-PANCAN.tsv".format(CAN_TYPE)
# with open(out_file, "w") as outfile:
#     output = [ k+"\t"+'\t'.join(v) for k, v in merged.items() ]
#     outfile.write( "\n".join(output))

