#!/usr/bin/python
from __future__ import division
import math, csv
from collections import defaultdict as dd
import sys,os,itertools, argparse, scipy
from plotly.tools import FigureFactory as FF
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt 
from os import listdir
from scipy.cluster.hierarchy import dendrogram, linkage
from os.path import isfile, join
import pysam as pys
import pandas as pd
import plotly.plotly as py
import collections as c
import ast
import numpy as np
import pysam as pys
import re

# Ashley Mae
# Created Oct 20, 2016
#####

def roundup(x): # rounding the starting position up to the nearest ten.
    return int(math.ceil(x/10.0))*10 

def in_blacklist(poten_start, chromo, size_ev, blacklist_dict):
    for i in blacklist[chromo]: # for each blacklist interval
        print "interval to check: ", i 
        if ((poten_start <= int(i[1])) and (int(i[0]) <= (poten_start+size_ev))): # check if ranges overlap (blacklist intervals and start and end of event)
            print "yes"
            return True # return true
    print "adding: ", poten_start, (poten_start+size_ev)
    blacklist_dict[chromo].append((poten_start,(poten_start+size_ev)))
    return False

def create_event(chromo, event_size, end_chroms, blackl):
    #start_potential = roundup(np.random.randint(0,high=20))
    start_potential = roundup(np.random.randint(1000000, high=end_chroms[chromo]-11000000)) # subtract 11MB from end of each chrom (10MB for bin size and 1MB for telomere)
    print "start_potential", start_potential, "end potential: ", start_potential+event_size

    # rerun if any chosen start-end val is in blacklist

    while in_blacklist(start_potential, chromo, event_size, blackl):
        print "its in the blacklist so restart"
        #create_event(chromo, event_size)
        #start_potential = roundup(np.random.randint(0,high=20))
        start_potential = roundup(np.random.randint(1000000, high=end_chroms[chromo]-11000000))

    print "final start_pos: ", start_potential
    return start_potential

######################################
# USAGE
if len(sys.argv) != 3:
    sys.stderr.write("Usage: python" + sys.argv[0] + " <OUTPUT_NAME> <OUTPUT_DIR> <genome (e.g. g1 or g2>\n Note: Should be called by generate_CNV_events_plusNoise_PROD1CELL.py\n")
    sys.exit(1)

######################################
# ARGUMENTS
COMMON_EVENTS_FILE = sys.argv[1]
COMMON_N_RANDOM_EVENTS_FILE = sys.argv[2]

# knownDeletion = c(GRanges(IRanges(5000000,10000000), seqnames="chr11"), GRanges(IRanges(500000,1000000), seqnames="chr2"), GRanges(IRanges(80000,880000), seqnames="chr5"))
######################################
# MAIN
if __name__ == '__main__':
    with open('blacklist.csv', 'rb') as csv_file:
        reader = csv.reader(csv_file)
        blacklist = dict(reader)
    blacklist = dd(list, blacklist)

    # DETERMINE IF NOISE IS DEL OR AMP.
    noise_amp_del = []
    for i in range(0,20):
        i = np.random.randint(0,high=2)
        noise_amp_del.append(i)

    chromosome_ends = {'chr1':248956422, 
    'chr2': 242193529, 
    'chr3': 198295559, 
    'chr4': 190214555,
    'chr5': 181538259, 
    'chr6': 170805979, 
    'chr7': 159345973, 
    'chr8': 145138636,
    'chr9': 138394717, 
    'chr10': 133797422, 
    'chr11': 135086622, 
    'chr12': 133275309, 
    'chr13': 114364328, 
    'chr14': 107043718, 
    'chr15': 101991189,
    'chr16': 90338345, 
    'chr17': 83257441, 
    'chr18': 80373285,
    'chr19': 58617616, 
    'chr20': 64444167, 
    'chr21': 46709983, 
    'chr22': 50818468}
    #s = [str(i) for i in range(1,23)]
    #list_chrom = map(( lambda x: 'chr' + x), s)
    list_sizes = [10000,20000,30000,40000,50000]
    
    amplicon_sizes = []
    for i in range(0,20):
        i = np.random.randint(0,high=5)
        amplicon_sizes.append(list_sizes[i])
    
    with open(COMMON_EVENTS_FILE, 'a') as f:
        # DETERMINE CNV NOISE EVENT
        #for i in range(0,20): # going through the 20 amplicon errors to insert into cell
        for idx, size_event in enumerate(list_sizes):
            
            # deletion
            if noise_amp_del[idx] == 0: 
                chromosome = 'chr'+str(np.random.randint(1,high=23))# chromosome
                max_CN = np.random.randint(0,high=2) # CN
                print "\nSize DEL event, chromo, max CN: ", size_event, chromosome, max_CN 
                start = create_event(chromosome, size_event, chromosome_ends, blacklist)
                end = start + size_event # size
                print "Chrom: ", chromosome, " Start and end are: ", start, end
                f.write(chromosome+","+str(start)+","+str(size_event)+","+str(max_CN)+"\n")
            
            # duplication
            elif noise_amp_del[idx] == 1: 
                chromosome = 'chr'+str(np.random.randint(1,high=23))# chromosome
                max_CN = np.random.randint(3,high=5) # CN
                print "\nSize AMP event, chromo, max CN: ", size_event, chromosome, max_CN 
                start = create_event(chromosome, size_event, chromosome_ends, blacklist)
                print "starta", start
                end = start + size_event # size
                print "Chrom: ", chromosome, " Start and end are: ", start, end
                f.write(chromosome+","+str(start)+","+str(size_event)+","+str(max_CN)+"\n")
        
