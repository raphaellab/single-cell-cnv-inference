
#!/usr/bin/python

from __future__ import division
#import seaborn as sns
from collections import Counter
import subprocess
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import re
import csv
import subprocess
from multiprocessing import Pool, Lock
import pandas as pd
import copy
from timeit import default_timer as timer
import pybedtools as pyb
from pybedtools import BedTool #https://daler.github.io/pybedtools/autodocs/pybedtools.bedtool.BedTool.html
import sys
import os
from os import listdir
from cStringIO import StringIO
import itertools
from os.path import isfile, join
import matplotlib
#%matplotlib inline
from scipy.stats import mode
#from IPython.display import HTML
import glob
#from pylab import rcParams
import scipy as sp

###########################
# USAGE
if len(sys.argv) != 6:
    sys.stderr.write("Usage: " + sys.argv[0] + " <CLONE (clone1 or clone2)> <MIN_BIN_SIZE (e.g. 250000)> <METHOD (AneuCP, AneuHMM, Ginkgo)> <KB_BIN_SIZE (250kb)> <# PROCESSORS (10 recommended)>\n")
    sys.exit()
###########################
# ASSIGNMENTS
CLONE = str(sys.argv[1])
MIN_BIN_SIZE = str(sys.argv[2])
METHOD = str(sys.argv[3]) 
KB_BIN_SIZE = str(sys.argv[4])
PROCESSORS = int(sys.argv[5]) # 10 recommended

if METHOD == 'AneuCP':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/"+
               "aneuFout/"+MIN_BIN_SIZE+"_"+"mergedRefcloneN"+"_"+CLONE+"/BROWSERFILES/method-dnacopy/*")][0]
    print "Processing ", naam, " for AneuFinder Change Point"
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'AneuHMM':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/"+
               "aneuFout/"+MIN_BIN_SIZE+"_"+"mergedRefcloneN"+"_"+CLONE+"/BROWSERFILES/method-HMM/*")][0]
    print "Processing ", naam, " for AneuFinder HMM"  
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'Ginkgo':
    DATA = pd.read_csv("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/"+KB_BIN_SIZE+"_"+CLONE+".tsv", sep='\t') 

#### No need to change below ####
OUTPUT_NAME = "__precision_recall.csv"

# Events to check
if CLONE == 'clone1':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone1_seed14.csv")
elif CLONE == 'clone2':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone2_seed23.csv")

# Output Directory
if METHOD == "Ginkgo":
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/precision_recall_dict_"+CLONE+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
else:
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/aneuFout/precision_recall_dict_"+CLONE+"_"+"mergedRefcloneN"+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
if not os.path.exists(OUTPUTDIR):
    os.makedirs(OUTPUTDIR)

###########################

def create_indiv_bed_files():
    list_cell_names = []
    list_cell_bed_files = []
    
    # get indices for how to split .bed file into multiple
    df_names = DATA[DATA['chrom'].str.contains("track")] # lines with name
    list_lines = df_names['chrom'].values.tolist() # list of all lines with name
    list_indices_cells = df_names.index.tolist()
    list_indices_cells.append(DATA.shape[0]) # get of the file
    
    # get names of cells
    for line in list_lines: 
        line_s = line.split(" ")
        list_cell_names.append([ i for i in line_s if "_" in i][0].strip('"')) # extracted names of all cells in order of bed file
     
    # split each cell into its own bed file
    for idx, i in  enumerate(list_indices_cells[:-1]):
        curr_cell_bed = DATA[list_indices_cells[idx]: list_indices_cells[idx+1]]
        curr_cell_bed = curr_cell_bed.drop(curr_cell_bed.index[[0]]) # drop weird title line
        curr_cell_bed = curr_cell_bed.drop(["score",'strand','thickStart','thickEnd','itemRgb'], axis=1) # drop unneeded cols
        curr_cell_bed.reset_index()
        curr_cell_bed['name'] = curr_cell_bed['name'].map(lambda x: x.rstrip('-somy'))# remove -somy
        curr_cell_bed['start'] = curr_cell_bed['start'].astype(int)
        curr_cell_bed['end'] = curr_cell_bed['end'].astype(int)
        curr_cell_bed.ix[curr_cell_bed.name == "zero-inflation", 'name'] = 0

        # saving .bed file
        print idx, ": Saving sep"+str(list_cell_names[idx])+"_"+METHOD+".bed"
        list_cell_bed_files.append("sep"+str(list_cell_names[idx])+"_"+METHOD+".bed")
        curr_cell_bed.to_csv(OUTPUTDIR+"/"+"sep"+str(list_cell_names[idx])+"_"+METHOD+".bed",sep="\t", index=False, header=False)
    
    if len(list_cell_names)!=32:
        print "ERROR - there are only", len(list_cell_names), "but continuing..."
        #sys.exit("ERROR - there are only"), len(list_cell_names)
    return list_cell_names, list_cell_bed_files

def extract_chr_cell_bed(n_bed_cell, chromo): # ANEUFINDER EXTRACTION
    sep_cell_bed = pd.read_csv(OUTPUTDIR+"/"+n_bed_cell, sep="\t", header=None)
    sep_cell_bed.columns =['CHR', 'START','END','CN']
    sep_cell_bed["END"] = sep_cell_bed["END"].apply(lambda x: x - 1)
    cur_cell = sep_cell_bed[sep_cell_bed['CHR']==chromo]
    return cur_cell

def extract_chr_cell_tsv(n_cell): # GINKGO EXTRACTION
    cur_cell = DATA[['CHR','START','END',n_cell]][DATA[['CHR','START','END',n_cell]]['CHR']==chro]
    return cur_cell

def dfToDict(df):
    return [{column : row[column] for column in list(df)} for index, row in df.iterrows()]

# Integrate each cell into master df in parallel (each cell and each chrom)
def parallel_cell_chrom(args):
    METHOD=args[0]; chromo=args[1]; n_cur_cell=args[2]; n_cur_master=args[3]; cur_cell=args[4]; cur_master=args[5]
    print "Chrom: ", chromo, " cell: ", n_cur_cell, "=>mast df name: ", n_cur_master 
    
    if (METHOD == 'AneuHMM') or (METHOD == 'AneuCP'):
        for idx, row in enumerate(cur_master): # go through df_master intervals for that chrom
            num_matches=0 # master interval should only match once!
            for idx_cr_cell, row_cr_cell in enumerate(cur_cell): # go through each cell bed for that chrom
                if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                    num_matches+=1
                    row['CN'] = int(row_cr_cell['CN'])
                    # if master interval is covered more than once in cur_cell-> flag to take a look
                    if num_matches>1:
                        sys.exit("ERROR", num_matches)
        [(sys.exit()) for i in cur_master if len(i)!=4] # make sure each dictionary was filled
        return (n_cur_master, chromo, cur_master)

    # Processing Ginkgo
    else:
        for idx, row in enumerate(cur_master): # go through df_master intervals for that chrom
            num_matches=0 # master interval should only match once!
            for idx_cr_cell, row_cr_cell in enumerate(cur_cell): # go through each cell bed for that chrom
                if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                    num_matches+=1
                    row['CN'] = int(row_cr_cell[n_cur_cell])
                    # if master interval is covered more than once in cur_cell-> flag to take a look
                    if num_matches>1:
                        sys.exit("ERROR", num_matches)
                        
        [(sys.exit()) for i in cur_master if len(i)!=4] # make sure each dictionary was filled
        return (n_cur_master, chromo, cur_master)

def check_each_chrom(l_di_cell_chrom, l_di_clone_event_chrom, TP,TN,FP,FN):
    cell_name = [i for i in l_di_cell_chrom[0].keys() if '_clone' in i][0]
    
    if bool(l_di_clone_event_chrom)== False:
        for row_no_evt in l_di_cell_chrom:
            if row_no_evt[cell_name]==2:
                TN+=1
            elif row_no_evt[cell_name]!=2:
                FP+=1
    else:
        for na in l_di_cell_chrom:
            na['ACCNTD'] = 'n'
        for nan in l_di_clone_event_chrom:
            nan['ACCNTD'] = 'n'

        # for all inserted events
        for row_evt in l_di_clone_event_chrom:
            num_matches=0 # see how many segments match with the inserted events (so to choose the largest one).
            
            # for all called events by method
            for idx_di_cell_chrom, row in enumerate(l_di_cell_chrom): 

                # if overlap 
                if ((row['START'] <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <= row['END'])):
                    num_matches+=1
                    row_evt['ACCNTD']= 0
                    # initiate overlap_df

                    if num_matches == 1: 
                        overlap_dict = [row] # make it a list of one item

                        # if the next does not exist or does not overlap
                        if (len(l_di_cell_chrom) == idx_di_cell_chrom): # at end of list of intervals
                            #print "next1 is not overlap - find_event()"
                            TP,TN,FP,FN = find_event(overlap_dict, row_evt,TP,TN,FP,FN, l_di_cell_chrom, cell_name)
                            break
                        elif (((l_di_cell_chrom[idx_di_cell_chrom+1]['START'] <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <= l_di_cell_chrom[idx_di_cell_chrom+1]['END'])) == False):
                            TP,TN,FP,FN = find_event(overlap_dict, row_evt,TP,TN,FP,FN, l_di_cell_chrom, cell_name)
                            break
                    
                    elif (len(l_di_cell_chrom) == idx_di_cell_chrom):
                        overlap_dict.append(row) 
                        TP,TN,FP,FN = find_event(overlap_dict, row_evt,TP,TN,FP,FN, l_di_cell_chrom, cell_name)
                        break
                    elif (((l_di_cell_chrom[idx_di_cell_chrom+1]['START'] <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <= l_di_cell_chrom[idx_di_cell_chrom+1]['END'])) == False):
                        overlap_dict.append(row)
                        TP,TN,FP,FN = find_event(overlap_dict, row_evt,TP,TN,FP,FN, l_di_cell_chrom, cell_name)
                        break

                    else: # keep adding to dataframe
                        overlap_dict.append(row)# ignore_index=True) 

        nan_dict_cell_chrom = [row for row in l_di_cell_chrom if row['ACCNTD'] == 'n'] 
        if nan_dict_cell_chrom: # for rest of values
            for row in nan_dict_cell_chrom:
                if row[cell_name]==2:
                    TN+=1
                    row['ACCNTD'] = 0
                elif row[cell_name]!=2:
                    FP+=1
                    row['ACCNTD'] = 0
    
    return TP,TN,FP,FN

def less_1(l_cell,cell_n): # getting any rows less than 1
    row_less_1 = []
    for l in l_cell:
        if l[cell_n]<=1:
            row_less_1.append(l)
    return row_less_1

def greater_2(l_cell,cell_n): # getting any rows greater than 3
    row_greater_2 = []
    for l in l_cell:
        if l[cell_n]>=3:
            row_greater_2.append(l)
    return row_greater_2

def eq_2(l_cell,cell_n):
    row_eq_2 = []
    for l in l_cell:
        if l[cell_n]==2:
            row_eq_2.append(l)
    return row_eq_2

def find_event(l_cell_dict_inevent, row_evt, TP,TN,FP,FN, l_cell_dict_chrom, cell_name):

    # If only one overlapping segment
    if len(l_cell_dict_inevent) == 1:
        for row in l_cell_dict_inevent:  # will only iterate once
            #t_TP=TP;t_TN=TN;t_FP=FP;t_FN=FN
            if (row_evt['MAX_CN'] <= 1) and (row[cell_name] <= 1): # TP del
                #print "TP-"
                TP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
            elif (row_evt['MAX_CN'] >= 3) and (row[cell_name] >= 3): # TP amp
                #print "TP+"
                TP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
            elif (row_evt['MAX_CN'] == 2) and (row[cell_name] == 2): # TN diploid
                print "SHOULD NOT HAPPEN - TN"
                sys.exit()
            elif (row_evt['MAX_CN'] <= 1) and (row[cell_name] == 2):
                #print "FN-2"
                FN+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
            elif (row_evt['MAX_CN'] >= 3) and (row[cell_name] == 2):
                #print "FN+2"
                FN+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
            elif (row_evt['MAX_CN'] <= 1) and (row[cell_name] >= 3):
                #print "FP-+"
                FP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
            
            elif (row_evt['MAX_CN'] >= 3) and (row[cell_name] <= 1):
                #print "FP+-"
                FP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row['START']) & (row_c_d_chr['END']==row['END']) & (row_c_d_chr[cell_name]==row[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0

            else:
                print("ERROR- another case...")
                sys.exit()
        
    # Multiple overlaping segements with one inserted event. Get all called amp or del events (depending on truth) for that region. 
    # All else will be considered later FP (if was amp but called del & vice versa) or FN (if called 2)
    elif len(l_cell_dict_inevent) > 1:
        
        if (row_evt['MAX_CN'] <=1) and len(less_1(l_cell_dict_inevent,cell_name)) >= 1: # true dels
            selected = less_1(l_cell_dict_inevent,cell_name)
            for row_s in selected:
                #print "TP-"
                TP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and len(greater_2(l_cell_dict_inevent,cell_name)) >=1: # true amps
            selected = greater_2(l_cell_dict_inevent,cell_name)#cell_df_inevent[cell_df_inevent['CN'] >= 3]
            for row_s in selected:
                #print "TP+"
                TP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
        elif (row_evt['MAX_CN'] <= 1) and len(greater_2(l_cell_dict_inevent,cell_name)) >=1:
            selected = greater_2(l_cell_dict_inevent,cell_name) #cell_df_inevent[cell_df_inevent['CN'] >= 3]
            for row_s in selected:
                #print "FP-+"
                FP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and len(less_1(l_cell_dict_inevent,cell_name)) >= 1:
            selected = less_1(l_cell_dict_inevent,cell_name)
            for row_s in selected:
                #print "FP+-"
                FP+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
                #print "cell_dict_chrom", l_cell_dict_chrom
                #cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and len(eq_2(l_cell_dict_inevent,cell_name)) >= 1: # this will be hit if the others are not - so ALL are 2 
            #if count_once==1:
            selected = eq_2(l_cell_dict_inevent,cell_name)
            for row_s in selected:
                #print "FN+2"
                FN+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
                #print "cell_dict_chrom", l_cell_dict_chrom
                #cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0                
        elif (row_evt['MAX_CN'] <= 1) and len(eq_2(l_cell_dict_inevent,cell_name)) >= 1: # this will be hit if the others are not - so ALL are 2
            #if count_once==1:
            selected = eq_2(l_cell_dict_inevent,cell_name)
            for row_s in selected: 
                FN+=1
                for row_c_d_chr in l_cell_dict_chrom:
                    if ((row_c_d_chr['START']==row_s['START']) & (row_c_d_chr['END']==row_s['END']) & (row_c_d_chr[cell_name]==row_s[cell_name])):
                        row_c_d_chr['ACCNTD'] = 0
        else:
            print "another case..."
            sys.exit()     
    
    return TP, TN, FP, FN

def get_vals_prec_recall(args): # curr_cell_df, cell_name
    curr_cell = args[0]; cell_name=args[1]; clone_ev_dict=args[2]
    print "Currently counting TP,TN,FP,FN for: ", cell_name
    TP=TN=FN=FP=0
    for chro in chromosome:
        cur_cell_chrom = [row for row in curr_cell if row['CHR'] == chro] 
        cur_clone_dict = [ro for ro in clone_ev_dict if ro['CHR'] == chro]

        TP,TN,FP,FN = check_each_chrom(cur_cell_chrom,cur_clone_dict,TP,TN,FP,FN)
    return (cell_name,TP,TN,FP,FN)

def extract_cur_cell(ldm,cur_cell): # returns the entire cell's CNP dict for each chrom and interval
    cur_cell_dict_list = []
    extract = lambda keys, dict: reduce(lambda x, y: x.update({y[0]:y[1]}) or x, map(None, keys, map(dict.get, keys)), {})
    for row in ldm:
        cur_cell_dict_list.append(extract(['CHR','START','END',cur_cell],row))
    return cur_cell_dict_list


#### MAIN ####
if __name__ == '__main__':
    start = timer()
    
    # CREATE MASTER INTERVAL DATAFRAME: DF_MASTER
    chromosome = ["chr" + str(i) for i in range(1,23)]# chromosome
    chromosome.append('chrX'); chromosome.append("chrY")
    nans = lambda df: df[df.isnull().any(axis=1)]

    # Ginkgo processing df_master
    if (METHOD == 'Ginkgo'):
        list_names_cells = list(DATA)[3:]
        if len(list_names_cells)!=32:
            sys.exit("ERROR - there are only"), len(list_names_cells)

        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                #for ind, row in DATA:
                cur_chrom_DATA = DATA[DATA['CHR']=='chr1']
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                cur_chrom_DATA = DATA[DATA['CHR']==chro]
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        #df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        df_master = df_master[abs(df_master.START-df_master.END)>1]
        print "size df_master: ",df_master.shape 

    # AneuFinder processing df_master
    else:
        list_names_cells, list_bed_files_cells = create_indiv_bed_files()

        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file chromosome 1
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                #df_res.append(res)
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file all other chromosomes
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        print "size df_master: ",df_master.shape 

    # import copy
    cy = copy.deepcopy

    # Calling parallel_cell_chrom
    dict_dfs_chro_cell= {} # dict of (cells, chrom): df

    if (METHOD == 'AneuHMM') or (METHOD == 'AneuCP'):
        #POOLED
        print "AneuFinder, ",METHOD, ". There are ", len(list_bed_files_cells), " cells to process for 24 chromosomes.\n"
        pool = Pool(processes = PROCESSORS)
        df_parsed = pool.map(parallel_cell_chrom, [(cy(METHOD), cy(chro), cy(cell_bed), cy(list_names_cells[idex]), cy(dfToDict(extract_chr_cell_bed(cell_bed, chro))), cy(dfToDict(df_master[df_master['CHR']==chro]))) for idex, cell_bed in enumerate(list_bed_files_cells) for chro in chromosome])
        for i in df_parsed:
            dict_dfs_chro_cell[( i[0], i[1] )] = i[2] # cell, chrom, dictionary for that chrom and cell

    else:

        #POOLED
        print "Ginkgo. There are ", len(list(DATA)[3:]), " cells to process for 24 chromosomes."
        pool = Pool(processes = PROCESSORS)
        df_parsed = pool.map(parallel_cell_chrom, [(cy(METHOD), cy(chro), cy(cell), cy(cell), dfToDict(extract_chr_cell_tsv(cell)), dfToDict(df_master[df_master['CHR']==chro])) for cell in list(DATA)[3:] for chro in chromosome])
        for i in df_parsed:
            dict_dfs_chro_cell[( i[0], i[1] )] = i[2] # cell, chrom, dictionary for that chrom and cell

    
    list_dict_mast = dfToDict(df_master)
    print "Filling list_dict_mast (df_master)"
    CHR = 0;END = 1;START = 2
    for chro in chromosome:# for every chromosome
        list_chrom_mast = [row for row in list_dict_mast if row['CHR'] == chro] #extract only dictionary for that chromosome
        for idx, row_dict_chr_mast in enumerate(list_chrom_mast):# for each row (CHR, START, END) - list of dictionaries of values
            print "On chrom: ",chro  ,"interval: ",idx+1, " of ", len(list_chrom_mast)
            for cell in list_names_cells: #k,v in dict_dfs_chro_cell.iteritems():   
                for i in dict_dfs_chro_cell[cell, chro]:
                    if row_dict_chr_mast['START']== i['START']:
                        row_dict_chr_mast[cell]= i['CN']


    # Adding mean, median, mode
    blacklisted_set = ['CHR', 'START', 'END']
    for dict_interv in list_dict_mast:
        to_sum = [value for key, value in dict_interv.items() if key not in blacklisted_set]
        dict_interv['MEDIAN_'+CLONE] = round(np.median(to_sum))
        dict_interv['MEAN_'+CLONE] = round(np.mean(to_sum))
        dict_interv['MODE_'+CLONE] = round(stats.mode(to_sum)[0][0])

    list_names_cells.append('MEAN_'+CLONE)
    list_names_cells.append('MEDIAN_'+CLONE)
    list_names_cells.append('MODE_'+CLONE)

    dict_cell_pr={}
    # import copy
    cy = copy.deepcopy
    clone_events_dict = dfToDict(clone_events)

    # Calculate Precision and Recall in parallel for each cell
    pool = Pool(processes = PROCESSORS)
    tp_tn_fp_fn = pool.map(get_vals_prec_recall, [(cy(extract_cur_cell(list_dict_mast,cell_cur)), cy(cell_cur), cy(clone_events_dict)) for cell_cur in list_names_cells]) # list(DATA)[3:5] 
    for i in tp_tn_fp_fn:
        dict_cell_pr[i[0]] = ((i[1],i[2],i[3],i[4]))
    
    #for cell_cur in list_names_cells:
    #    tp_tn_fp_fn = get_vals_prec_recall((cy(extract_cur_cell(list_dict_mast,cell_cur)), cy(cell_cur), cy(clone_events_dict)))
    #dict_cell_pr[tp_tn_fp_fn[0]] = ((tp_tn_fp_fn[1],tp_tn_fp_fn[2],tp_tn_fp_fn[3],tp_tn_fp_fn[4]))
    
    
    # Check that all segments have been accounted for across all cells
    for k,v in dict_cell_pr.iteritems():
        if sum(v)!=len(df_master):
            sys.exit("ERROR - TP, TN, FP, FN values do not sum to total number of segments(",len(df_master),"): ", k,v)

    # TP, TN, FP, FN into csv for all cells
    with open(OUTPUTDIR+"/"+"prec_recallVals_"+CLONE+"_"+METHOD+"_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, 'w') as f:
        f.write("Cell,TP,TN,FP,FN\n")
        for k,v in dict_cell_pr.iteritems():
            f.write(k+","+str(v[0])+","+str(v[1])+","+str(v[2])+","+str(v[3])+"\n")
    
    # CNP data (list_dict_mast) into csv for all cells
    with open(OUTPUTDIR+"/"+CLONE+"_"+METHOD+"_integrated_beds_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, 'w') as f:
        w = csv.DictWriter(f, row.keys())
        w.writeheader()
        for row in list_dict_mast:
            w = csv.DictWriter(f, row.keys())
            w.writerow(row)
        
    elapsed_time = timer() - start
    print "ELAPSED TIME: ", elapsed_time
