#!/usr/bin/python
from __future__ import division
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from sklearn import decomposition
from sklearn import datasets
import matplotlib
import seaborn as sns
import sys,os,itertools, argparse
import matplotlib.pyplot as plt 
import plotly.plotly as py
import collections as c
import pandas as pd
import numpy as np
import re

# Ashley Mae
# Created Oct 10, 2016
#####
#
# decomposition_PCA_all_cells.py: creates X matrix from individual cells and performs PCA
# reference: http://scikit-learn.org/stable/auto_examples/decomposition/plot_pca_iris.html
# label reference: http://stackoverflow.com/questions/10374930/matplotlib-annotating-a-3d-scatter-plot
# dimentionality issue PCA explanation: http://stats.stackexchange.com/questions/8261/is-the-matrix-dimension-important-for-performing-a-valid-pca 
#										http://stats.stackexchange.com/questions/28909/pca-when-the-dimensionality-is-greater-than-the-number-of-samples
# distance measures explanation: https://www.youtube.com/watch?v=_EEcjn0Uirw
#####

# ASSIGNMENTS
CHR = 0
START_BIN = 1
END_BIN = 2
DEPTH = 3

def add_to_X_matrix(x_l, file):
	bin_limit = 414 # limit to number of bins (n)
	cell_list = [] # create list of cells to be added
	iterator = 1

	with open(file) as f:
		for line in f:
			if iterator <= bin_limit:
				s = line.rstrip("\r\n").split(' ')
				e = [a.replace(' ','') for a in s]
				cell_list.append(float(e[DEPTH]))
				iterator+=1

	x_l.append(cell_list)
	return x_l

def plot_3D(xMatrix, nCells):
	print xMatrix
	fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111, projection='3d')
	plt.rcParams['legend.fontsize'] = 10   
	ax.plot(xMatrix[0,:], xMatrix[1,:], xMatrix[2,:], 'o', markersize=8, color='blue', alpha=0.5)
	plt.title('Samples from '+str(nCells)+' cells')
	#ax.legend(loc='upper right')

	#plt.show()
def heatmap(xMatrix):
	xMatrix_sns = pd.DataFrame(xMatrix)
	#sns.heatmap(xMatrix_sns)

	#yticks = xMatrix_sns.index
	#keptticks = yticks[::int(len(yticks)/10)]
	#yticks = ['' for y in yticks]
	#yticks[::int(len(yticks)/10)] = keptticks

	#xticks = xMatrix_sns.columns
	#keptticks = xticks[::int(len(xticks)/10)]
	#xticks = ['' for y in xticks]
	#xticks[::int(len(xticks)/10)] = keptticks

	#fig = matplotlib.pyplot.figure()

	cm = sns.clustermap(xMatrix_sns)# replace clustermap with heatmap #,linewidth=0,yticklabels=yticks,xticklabels=xticks)
	hm = cm.ax_heatmap.get_position()
	plt.setp(cm.ax_heatmap.yaxis.get_majorticklabels(), fontsize=5)
	plt.setp(cm.ax_heatmap.xaxis.get_majorticklabels(), fontsize=3, rotation=90)

	cm.ax_heatmap.set_position([hm.x0, hm.y0, hm.width, hm.height])
	col = cm.ax_col_dendrogram.get_position()
	cm.ax_col_dendrogram.set_position([col.x0, col.y0, col.width, col.height])

	#plt.setp(cg.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
	# This sets the yticks "upright" with 0, as opposed to sideways with 90.
	#plt.yticks(rotation=0) 
	#plt.yticks(np.arange(1, 128, 1))
	#ax.axis([xmin, xmax, ymin, ymax])
	plt.show()
	sys.exit()

def pca_plot(xMatrix, nCells,allNames): 
	fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111, projection='3d')
	plt.cla()
	pca = decomposition.PCA(n_components=3)
	pca.fit(xMatrix)
	xMatrix = pca.transform(xMatrix)
	row,col = xMatrix.shape
	colors = ['red','blue'] # normal - red, tumor - blue

	for i in range(row): # for all cells
		if '_N' in allNames[i]:
			ax.scatter(xMatrix[i, 0], xMatrix[i, 1], xMatrix[i, 2], 'o', c='red')
			ax.text(xMatrix[i,0], xMatrix[i,1], xMatrix[i, 2],'%s' % (allNames[i]), size=6, zorder=1, color='k')
		elif '_T' in allNames[i]:
			ax.scatter(xMatrix[i, 0], xMatrix[i, 1], xMatrix[i, 2], 'o', c='blue')
			ax.text(xMatrix[i,0], xMatrix[i,1], xMatrix[i, 2],'%s' % (allNames[i]), size=6, zorder=1, color='k')
		else:
			print "ERROR - must be _N or _T for labeling to function for marker coloring", sys.exit()
	ax.w_xaxis.set_ticklabels(["x-axis"])
	ax.w_yaxis.set_ticklabels(["y-axis"])
	ax.w_zaxis.set_ticklabels(["z-axis"])
	plt.title('Samples from '+str(nCells)+' cells')
	#ax.set_title('Samples from %s Cells'%(nCells))

	plt.show()

######################################
# ASSIGNMENTS
NUM_FILES = int(sys.argv[1])
x_list = []
num_cells = 0
name = str
all_names = []

######################################
# USAGE
if len(sys.argv) != NUM_FILES+2:
    sys.stderr.write("Usage: " + sys.argv[0] + " <NUM_FILES> <data_binned_RD_SRR308173... file>\n")
    sys.exit(1)

######################################
# METHODS

# Iterate through cell files
for i in range(2,int(NUM_FILES+2)): # starting with 2nd argument
    # Counting the number of cells (for plot titles)
    num_cells+=1
    
    # Preparing text for the names of each cell
    text = sys.argv[i]

    # Printing cell file to keep track
    print "cell data file: ", sys.argv[i]
    
    # If normal cell - add name with "N"
    if 'normal' in text:
    	m = re.search('_RD_(.+?)_1', text)
    	if m:
    		found = m.group(1)
    		name = str(found+'_N')
   
    # If tumor cell - add name with "T"
    elif 'tumor' in text:
    	m = re.search('_RD_(.+?)_1', text)
    	if m:
    		found = m.group(1)
    		name = str(found+'_T')
    	#print name
    else:
    	print "ERROR - must be tumor or normal: ", text
    	sys.exit()
    
    # Create cell list
    all_names.append(name)

    # Create array of cells
    x_list = add_to_X_matrix(x_list, sys.argv[i])

# Created starting matrix X
x_matrix = np.array((x_list))
print "full matrix cells X bins created \n"

# Plot 3D
#plot_3D(x_matrix,num_cells) 

# Heatmap
heatmap(x_matrix)

# Plot PCA of all cells
pca_plot(x_matrix, num_cells, all_names)
