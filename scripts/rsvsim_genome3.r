# Ashley Conard
# rsvsim_genome - simulates structural variation events in the genome (hg19 reference)
# Last update : June 2nd
# Fantastic Tutorial for RSVSim : https://rdrr.io/bioc/RSVSim/man/simulateSV.html#heading-8

# To install
#source("https://bioconductor.org/biocLite.R")
#biocLite("BSgenome.Hsapiens.UCSC.hg19.masked")
#biocLite("RSVSim")
#biocLite("BSgenome.Hsapiens.UCSC.hg19") # Default genome is hg19

# Libraries
library(RSVSim)
#library("BSgenome.Hsapiens.UCSC.hg19")
library("BSgenome.Hsapiens.UCSC.hg19.masked")

# size of events (drawn from beta with estimateSVSizes)
size_dup = estimateSVSizes(n=5, minSize=10000, maxSize=100000, hist=TRUE, default='insertions')
size_ins = estimateSVSizes(n=5, minSize=50000, maxSize=4000000, hist=TRUE, default="insertions")
size_del = estimateSVSizes(n=5, minSize=100000, maxSize=500000, hist=TRUE, default="deletions")

# deletions (drawn from beta), insertions (drawn from beta), duplication (drawn from beta)
sim = simulateSV(output=".", dels=5, ins=5, dups=5, sizeDels=size_del, sizeIns=size_ins, sizeDups=size_ins)# percCopiedIns=0.5)
#sim
metadata(sim)

## Setting the weights for SV formation mechanism and repeat biases demands a given data.frame structure
## The following weights are the default settings
## Please make sure your data.frames have the same row and column names, when setting your own weights
data(weightsMechanisms, package="RSVSim")
weightsMechanisms
data(weightsRepeats, package="RSVSim")
