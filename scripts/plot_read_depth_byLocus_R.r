#####
# Ashley Mae
# Oct 2, 2016
# plot_read_depth_byLocus_R.r : R Script to plot read depth by locus
#####

options(echo=FALSE) # Change to TRUE if you want to see commands in the output file
args <- commandArgs(trailingOnly = TRUE) # trailingOnly=TRUE means that only your arguments are returned, check:

chrom_table <- read.table("chr2_SRR3082145_1.coverage", header=FALSE, sep="\t", na.strings="NA", dec=".", strip.white=TRUE)

library(reshape2)
library(plyr)

chrom_table <- rename(chrom_table, c(V1="Chr", V2="locus", V3="depth"))

jpeg('chr2_SRR3082145.jpeg')

plot(chrom_table$locus, chrom_table$depth)

library(lattice, pos=10)

xyplot(depth~locus, type="p", pch=16, auto.key=list(border=TRUE),par.settings=simpleTheme(pch=16),scales=list(x=list(relation='same'), y=list(relation='same')), data=chr2_SRR3082145, main="Depth by locus - Chr2 SRR3082145)")

dev.off()


