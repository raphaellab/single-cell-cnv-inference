#!/usr/bin/python
#############################
#
# plot_variance_difCells.py: bins .bed or .tsv files
# created by : Ashley Conard
# last updated: Aug. 31, 2017
#
#############################
from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import warnings
import pandas as pd
warnings.filterwarnings("ignore")
import numpy as np
from os import listdir
from os.path import isfile, join
import os

def reformat_df(data):
    df = pd.read_csv(data)
    print "List column names: ", list(df)
    cell_n = raw_input("Please enter the desired cell name from list of columns: ")
    cell_col_name = [a for a in list(df) if cell_n in a][0]
    df.reset_index()
    len_rows = df.shape[0]
    df['CELL_NAME'] = [cell_n] * len_rows
    df.rename(columns={cell_col_name:'READ_COUNTS'}, inplace=True)
    df['COVERAGE'] = df.apply(calc_coverage, axis=1) # add column for coverage
    return df
    
def calc_coverage(row):
    return (row['READ_COUNTS']/BINSIZE*76)

def get_sd(df):
    df[np.abs(df.Data-df.Data.mean())<=(3*df.Data.std())] #keep only the ones that are within +3 to -3 standard deviations in the column 'Data'.
    df[~(np.abs(df.Data-df.Data.mean())>(3*df.Data.std()))] #or if you prefer the other way around

def plots(m_c_pd, RC_COV, LIM, VIOLIN_BOX): #LIM = NUMBER OR NA
    m_c_pd = m_c_pd.reset_index()
    list_max_vals = []
    
    # REPORT STATS WITH ALL DATA AND WITHOUT MAX VALUE
    if RC_COV == 'COVERAGE':
        print "Plotting Coverage"
        SNS_PLOT_TITLE = ("Cell Coverage Distribution Across Genome for %s, at Bin Size %d" % (TITLE, BINSIZE))
        OUTPUT_NAME = '_covVar__plot_variance_difCells.pdf'
        stats_cov = m_c_pd.groupby("CELL_NAME").COVERAGE.describe().unstack()
        print "Coverage Stats: \n", stats_cov
        stats_cov.to_csv(OUT_NAME+"stats_cov__plot_variance_difCells.csv")
    
        # Get all the max coverage values for each bin size (to be removed in plotting)
        list_max_per_numcel = m_c_pd.groupby(['CELL_NAME'])['COVERAGE'].max()
        for index in list_max_per_numcel.index:
            list_max_vals.append(list_max_per_numcel[index]) #print index, list_max_per_binsize[index]
        
        #print list_max_vals
        print "\nAll data in df is", m_c_pd.shape
        temp_m_c_pd = m_c_pd[~m_c_pd['COVERAGE'].isin(list_max_vals)]# != list_max_vals)]     
        print "Without max values, df is now: ", temp_m_c_pd.shape
        print "\nCoverage Stats without max values for each cell: \n", temp_m_c_pd.groupby("CELL_NAME").COVERAGE.describe().unstack()
    
    elif RC_COV == 'READ_COUNTS':
        print "Plotting Read Counts"
        SNS_PLOT_TITLE = ("Cell Read Count Distribution Across Genome for %s, at Bin Size %d" % (TITLE, BINSIZE))
        OUTPUT_NAME ='_RCvar__plot_variance_difCells.pdf'
        print "Read Count Stats: \n", m_c_pd.groupby("CELL_NAME").READ_COUNTS.describe().unstack()
        
        # Get all the max values for each bin size.
        list_max_per_numcel = m_c_pd.groupby(['CELL_NAME'])['READ_COUNTS'].max()
        for index in list_max_per_numcel.index:
            list_max_vals.append(list_max_per_numcel[index]) #print index, list_max_per_binsize[index]
        
        #print list_max_vals
        print "\nAll data in df is", m_c_pd.shape
        temp_m_c_pd = m_c_pd[~m_c_pd['READ_COUNTS'].isin(list_max_vals)]# != list_max_vals)]     
        print "Without max values, df is now: ", temp_m_c_pd.shape
        print "\nCoverage Stats without max values for each num cell size: \n", temp_m_c_pd.groupby("CELL_NAME").READ_COUNTS.describe().unstack()

    # REMOVE OUTLIERS TO PLOT
    if REMOVE_OUTLIERS == 'True': # OPTIONAL: Change from all to <22000 to <5000 for filtering.
        print "\nWill remove outliers (>65000 for RC and >15 for Coverage. This is max val: ", m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].COVERAGE, 'for bin size', m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].CELL_NAME
        if RC_COV == 'READ_COUNTS':
            m_c_pd = m_c_pd[(m_c_pd['READ_COUNTS'] < 65000)] #& (m_c_pd['BINSIZE']!=1000000) & (m_c_pd['BINSIZE']!=500000) & (m_c_pd['BINSIZE']!=250000)]
        elif RC_COV == 'COVERAGE':
            m_c_pd = m_c_pd[(m_c_pd['COVERAGE'] < 4)]#m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].COVERAGE)]
    
    sns.set_style("whitegrid")
    fig, ax = plt.subplots()
    fig.set_size_inches(50, 15)# the size of A4 paper
    sns.plt.title(SNS_PLOT_TITLE, size = 20)
    plt.xticks(size = 20)
    plt.yticks(size = 20)
    
    if RC_COV == 'COVERAGE':
        sns.plt.ylabel('Coverage', size = 20)
    elif RC_COV == 'READ_COUNTS':
        sns.plt.ylabel('Read Counts', size = 20)  
    sns.plt.xlabel('Num. Cells', size = 20)
    
    # ZOOM into the plot.  
    if type(LIM) == int:
        plt.ylim(ymin=-100,ymax=6000) #OPTIONAL: Zoom into a specific part of plot
        
    # Violin or Boxplot
    if VIOLIN_BOX == 'boxplot':
        ax = sns.boxplot(x=m_c_pd['CELL_NAME'],y=m_c_pd[RC_COV]) #ax = sns.boxplot(x=m_c_pd['BINSIZE'],y=m_c_pd['READ_COUNTS'])#, width=1, bw=.1)
    elif VIOLIN_BOX == 'violinplot':
        ax = sns.violinplot(x=m_c_pd['CELL_NAME'],y=m_c_pd[RC_COV])
    
    # Save figure
    #cell_n = cell_name.replace("binsXcells__read_depth_plot.csv","")
    plt.savefig(OUTPUT_DIR+"/"+OUT_NAME+'_'+VIOLIN_BOX+'_'+OUTPUT_NAME)
    #plt.show()

###########################
# USAGE
if len(sys.argv) != 10:
  sys.stderr.write("Usage: " + sys.argv[0] + " <DATA_DIR (dir of read_count_plot.py output (1 file per cell))> <OUTPUT_NAME> <OUTPUT_DIR> <BIN_SIZE> <RC_COV (COVERAGE or READ_COUNTS)> <LIM (NA or int> <VIOLIN_BOX (boxplot or violinplot)> <REMOVE_OUTLIERS (True or False)> <TITLE OF CELL TYPE (e.g. 'SRR_1566 Neuronal Cell') or 'Various Cells'>\n")
  sys.exit(1)

###########################

# ARGUMENTS
DATA_DIR = str(sys.argv[1]) # DIRECTORY OF ALL BIN_SIZE FILES
#DATA_DIR = "/Users/ashleymaeconard/Desktop/singleCell/results_reproducible/T5_normal_2328cell_read_count_vectors_1MB-50KB/data/"
OUT_NAME = str(sys.argv[2])# "2328cell_T5_normal_1MB-1kb"
OUTPUT_DIR = str(sys.argv[3])#"/Users/ashleymaeconard/Desktop/singleCell/results_reproducible/T5_normal_2328cell_read_count_vectors_1MB-50KB/"
BINSIZE = int(sys.argv[4])
# Options to alter
RC_COV = str(sys.argv[5])#'COVERAGE' # READ_COUNTS, COVERAGE
if (RC_COV != 'READ_COUNTS') and (RC_COV != 'COVERAGE'):
    sys.exit("ERROR: Neither READ_COUNTS nor COVERAGE specified")
LIM = sys.argv[6] # 'NA' OR int(INTEGER)
VIOLIN_BOX = str(sys.argv[7]) #'boxplot'# violinplot or boxplot
if (VIOLIN_BOX != 'boxplot') and (VIOLIN_BOX != 'violinplot'):
    sys.exit("ERROR: Neither boxplot nor violinplot specified")
REMOVE_OUTLIERS = str(sys.argv[8]) # True
if REMOVE_OUTLIERS != 'True' and REMOVE_OUTLIERS != 'False':
    sys.exit("ERROR: Neither True nor False specified")
TITLE = str(sys.argv[9])

if __name__ == '__main__':
    # MAIN
    counter = 0 # keeping track of the number of cells which have been processed.

    # Create list of all .bam files to transform into vectors of normalized read counts

    filter_sym = raw_input("Please enter the desired ending for files to consider: ")
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and filter_sym in f]
    print "Data to process: ", onlyfiles, "\n"

    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store")

    # DF to fill with vectors of normalized read counts across all cells in the input directory
    master_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'CELL_NAME', 'READ_COUNTS', 'COVERAGE'))

    for cell_file in onlyfiles:
        counter+=1
        cell_file_split = cell_file.split("_")
        print "File # ", counter ," of ", len(onlyfiles),". Num Cells ", len(onlyfiles), " in DIR+name: ", DATA_DIR+cell_file, "\n" # file and filepath
        # Reformat df
        df_reformatted = reformat_df(DATA_DIR+cell_file)
        # Append to master_df
        master_df = master_df.append(df_reformatted)
    
    # CREATING PLOTS AND SAVING
    #master_df.CELL_NAME = master_df.CELL_NAME.astype(float)
    #master_df = master_df.sort_values('CELL_NAME', ascending=True)
    master_df = master_df[['CHR', 'START', 'END', 'CELL_NAME', 'READ_COUNTS', 'COVERAGE']]
    master_df.to_csv(OUTPUT_DIR+"/"+OUT_NAME+"_"+RC_COV+"__plot_difCells.csv")
    plots(master_df, RC_COV, LIM, VIOLIN_BOX)