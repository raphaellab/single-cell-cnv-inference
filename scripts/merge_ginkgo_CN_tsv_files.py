#!/usr/bin/python
#############################
#
# merge_ginkgo_CN_tsv_files.py: compares aneuFinder and Ginkgo copy-number results and outputs
# created by : Ashley Conard
# last updated: May 15, 2017
#
# e.g. python singleCell/single-cell-cnv-inference/scripts/merge_ginkgo_CN_tsv_files.py T5NORMALhg19250kb76readlength.tsv merged_t5_normal_and_2normal_varied_bins.tsv 250000 test
#
#############################
from __future__ import division
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import re
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
import os


def get_cn_in_window(to_add ,df, start, end):
  print "\nInput start end interval: ", start, end
  #print df.loc[:,['START','END']]
  #print "df[(df.START >= start) & (df.END >= end)]\n", df[(start >= df.START)& (end <= df.END)][to_add].values[0][0]

  # If there are no CHRSTART&END within the required start and end of the input window from user. 
  if df[(start >= df.START)& (end <= df.END)][to_add].empty == True: # get merged data is within interval of window (from set fixed interval from input)
    print "\ndataframe start and end not in interval"
    # If | START start(input) END end(input) | and only one window in dataframe
    if df[(start >= df.START) & (start < df.END)][to_add].empty == False:
    #if (df[(df.START <= start) & (df.END <= end) & (df.END >= start)].empty == False) & (len(df[(df.START <= start) & (df.END <= end) & (df.END >= start)])<2):
      print "1ST - START start(input) END end(input)"
      print df[(start >= df.START) & (start < df.END)][to_add].values[0][0]
      #print df[(df.START <= start) & (df.END <= end) & (df.END >= start)][to_add], df[(df.START <= start) & (df.END <= end) & (df.END >= start)][to_add].values[0][0]
      #return df[(df.START <= start) & (df.END <= end) & (df.END >= start)][to_add].values[0][0]
      return df[(start >= df.START) & (start < df.END)][to_add].values[0][0]

    # If | start(input) START end(input) END | and only one window in dataframe
    elif df[(df.START>= start) & (df.START <= end) & (df.END >= end)].empty == False: 
      print "2ND - start(input) START end(input) END"
      print df[(df.START>= start) & (df.START <= end) & (df.END >= end)][to_add], df[(df.START>= start) & (df.START <= end) & (df.END >= end)][to_add].values[0][0]
      return df[(df.START>= start) & (df.START <= end) & (df.END >= end)][to_add].values[0][0]

    # Check to see what else could be happening
    else:
      sys.stderr.write("QUITTING : START "+str(start)+" END "+str(end)+'\n')
      sys.exit()
  
  # If the CHRSTART&END is within the required start and end of the input window from user.
  elif df[(start >= df.START)& (end <= df.END)][to_add].empty == False: 
    print "in interval:)", df[(start >= df.START)& (end <= df.END)][to_add].values[0][0]
    return df[(start >= df.START)& (end <= df.END)][to_add].values[0][0]
  
    # If there is more than 1 copy-number given that input interval (should not happen unless input interval is very large)
  elif len(df[(start >= df.START) & (end <= df.END)][to_add])>1: 
    print "something else"
    sys.exit("More than one copy-number for that interval in the merged_T5_normal_cells df")

  else:
    sys.exit("ERROR last resort")


def merge_given_kb_bins(g1, g2, master_df, to_add, kb_bins, chromo):
  #to_merge_df = pd.DataFrame(columns=('CHR', 'CN')) # initializing a dataframe for cell to merge
  to_merge_list = []
  idx = -1
  # Make sure list of chromosomes are equal
  #print "set of chromosomes g1 ", set(list(g1['CHR']))
  #print "set of chromosomes g2 ", set(list(g2['CHR']))

  if set(list(g1['CHR']))!= set(list(g2['CHR'])):
    sys.stderr.write("Both datasets do not have the same number of chromosomes.")
    sys.exit()
  
  # Iterate through all chromosomes
  for ch in chromo:
    print "chrom", ch
    g1_chr_max = g1.loc[g1['CHR'] == ch]['END'].max() #g2_chr_max = g2.loc[g2['CHR'] == ch]['END'].max()
    g1_chr = g1[g1.CHR==ch] # reduced dataframe based on chrom.
    print "g1_chr", g1_chr
    #print "Max end of g1 chrom", ch, " is ", g1_chr_max
    
    for it in range(0,len(g1_chr)): # go through each bin for that chromosome
      idx+=1 # index of large df
      to_merge_list.append(get_cn_in_window(to_add, g2[(g2.CHR == ch)], g1_chr['START'][idx], g1_chr['END'][idx]))
      #to_merge_df.loc[it] = [ch, get_cn_in_window(to_add, g2, g1_chr['START'][it], g1_chr['END'][it])] #get_aneu(aneu) ,get_ginkgo()] # grab the value in that range

  return to_merge_list

def union(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))

###########################
# USAGE
if len(sys.argv) != 5:
  sys.stderr.write("Usage: " + sys.argv[0] + " GINKGO_FILE_1  GINKGO_COL_FILE_TO_MERGE_2  KB  (e.g. 50000) OUTPUT_FILE_NAME\n")
  sys.exit(1)
###########################

# ARGUMENTS
GINK1 = sys.argv[1]
GINK2 = sys.argv[2]
KB = int(sys.argv[3])
OUTPUT_FILE = str(sys.argv[4])

if __name__ == '__main__':
  chromosomes = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY']

  # Read ginkgo .tsv file into df
  gin1_df = pd.read_csv(GINK1, sep='\t') #print list(g_df)[3], g_df[g_df.columns[3]] # print all after chrom, start, end
  gin2_df = pd.read_csv(GINK2, sep='\t')

  # Create the merged dataframe
  cells = union(list(gin1_df), list(gin2_df))
  merged_df = pd.DataFrame(columns=cells) 

  # Get name of column to add. 
  # Currently the column to add is hard-coded to be the merged cell, which can be altered to include more if needed.
  toadd = [word for word in cells if word.startswith('merg')] # only add the merged cell!
  #print "Cell to add: ", toadd
  
  if len(toadd) !=1:
    sys.exit("Column to add is larger than 1 cell.")
  
  # Gather column names and move chr, start, end to the beginning
  cols = list(merged_df)
  cols.insert(0, cols.pop(cols.index('CHR')))
  cols.insert(1, cols.pop(cols.index('START')))
  cols.insert(2, cols.pop(cols.index('END')))
  merged_df = merged_df.ix[:, cols]

  # Gather the copy-numbers in the cell to merge within the input interval
  tomergedf = pd.DataFrame({toadd[0]: merge_given_kb_bins(gin1_df, gin2_df, merged_df, toadd, KB, chromosomes)})
  #print tomergedf
  # Merge the two dataframes now with same START and END for each CHR
  merged_dataframe = pd.merge(gin1_df, tomergedf, left_index=True, right_index=True, how='outer')
  #merged_dataframe = pd.concat([gin1_df, tomergedf])
  #print merged_dataframe
  print "to file"
  merged_dataframe.to_csv(OUTPUT_FILE+"_merged_dataframes.csv", sep='\t')


