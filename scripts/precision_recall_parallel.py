#!/usr/bin/python

from __future__ import division
#import seaborn as sns
from collections import Counter
import subprocess
import matplotlib.pyplot as plt
import numpy as np
import re
import subprocess
from multiprocessing import Pool, Lock
import pandas as pd
from timeit import default_timer as timer
import pybedtools as pyb
from pybedtools import BedTool #https://daler.github.io/pybedtools/autodocs/pybedtools.bedtool.BedTool.html
import sys
import os
from os import listdir
from cStringIO import StringIO
import itertools
from os.path import isfile, join
import matplotlib
from scipy.stats import mode
from IPython.display import HTML
import glob
from pylab import rcParams
import scipy as sp

###########################
# USAGE
if len(sys.argv) != 6:
    sys.stderr.write("Usage: " + sys.argv[0] + " <CLONE (clone1 or clone2)> <MIN_BIN_SIZE (e.g. 250000)> <METHOD (AneuCP, AneuHMM, Ginkgo)> <KB_BIN_SIZE (250kb)> <# PROCESSORS (10 recommended)>\n")
    sys.exit()

###########################
# ASSIGNMENTS
CLONE = str(sys.argv[1])
MIN_BIN_SIZE = str(sys.argv[2])
METHOD = str(sys.argv[3]) 
KB_BIN_SIZE = str(sys.argv[4])
PROCESSORS = int(sys.argv[5]) # 10 recommended

if METHOD == 'AneuCP':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/"+
               "aneuFout/"+KB_BIN_SIZE+"_"+CLONE+"/BROWSERFILES/method-dnacopy/*")][0]
    print "Processing ", naam, " for AneuFinder Change Point"   
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'AneuHMM':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/"+
               "aneuFout/"+KB_BIN_SIZE+"_"+CLONE+"/BROWSERFILES/method-HMM/*")][0]
    print "Processing ", naam, " for AneuFinder HMM"  
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'Ginkgo':
    DATA = pd.read_csv("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/"+KB_BIN_SIZE+"_"+CLONE+".tsv", sep='\t') 

#### No need to change below ####
OUTPUT_NAME = "__precision_recall.csv"

# Events to check
if CLONE == 'clone1':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone1_seed14.csv")
elif CLONE == 'clone2':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone2_seed23.csv")

# Output Directory
if METHOD == "Ginkgo":
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/precision_recall_"+CLONE+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
else:
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/aneuFout/precision_recall_"+CLONE+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
if not os.path.exists(OUTPUTDIR):
    os.makedirs(OUTPUTDIR)

###########################

def create_indiv_bed_files():
    list_cell_names = []
    list_cell_bed_files = []
    
    # get indices for how to split .bed file into multiple
    df_names = DATA[DATA['chrom'].str.contains("track")] # lines with name
    list_lines = df_names['chrom'].values.tolist() # list of all lines with name
    list_indices_cells = df_names.index.tolist()
    list_indices_cells.append(DATA.shape[0]) # get of the file
    
    # get names of cells
    for line in list_lines: 
        line_s = line.split(" ")
        list_cell_names.append([ i for i in line_s if "_" in i][0].strip('"')) # extracted names of all cells in order of bed file
     
    # split each cell into its own bed file
    for idx, i in  enumerate(list_indices_cells[:-1]):
        curr_cell_bed = DATA[list_indices_cells[idx]: list_indices_cells[idx+1]]
        curr_cell_bed = curr_cell_bed.drop(curr_cell_bed.index[[0]]) # drop weird title line
        curr_cell_bed = curr_cell_bed.drop(["score",'strand','thickStart','thickEnd','itemRgb'], axis=1) # drop unneeded cols
        curr_cell_bed.reset_index()
        curr_cell_bed['name'] = curr_cell_bed['name'].map(lambda x: x.rstrip('-somy'))# remove -somy
        curr_cell_bed['start'] = curr_cell_bed['start'].astype(int)
        curr_cell_bed['end'] = curr_cell_bed['end'].astype(int)
                
        # saving .bed file
        print idx, ": Saving sep"+str(list_cell_names[idx])+"_"+METHOD+".bed"
        list_cell_bed_files.append("sep"+str(list_cell_names[idx])+"_"+METHOD+".bed")
        curr_cell_bed.to_csv(OUTPUTDIR+"/"+"sep"+str(list_cell_names[idx])+"_"+METHOD+".bed",sep="\t", index=False, header=False)
        
    return list_cell_names, list_cell_bed_files

def extract_chr_cell_bed(n_bed_cell, chromo): # ANEUFINDER EXTRACTION
    sep_cell_bed = pd.read_csv(OUTPUTDIR+"/"+n_bed_cell, sep="\t", header=None)
    sep_cell_bed.columns =['CHR', 'START','END','CN']
    sep_cell_bed["END"] = sep_cell_bed["END"].apply(lambda x: x - 1)
    cur_cell = sep_cell_bed[sep_cell_bed['CHR']==chromo]
    return cur_cell

def extract_chr_cell_tsv(n_cell): # GINKGO EXTRACTION
    cur_cell = DATA[['CHR','START','END',cell]][DATA[['CHR','START','END',cell]]['CHR']==chro]
    #cur_cell = cur_g_cell[cur_g_cell['CHR']==chro]
    return cur_cell

# Integrate each cell into master df in parallel (each cell and each chrom)
def parallel_cell_chrom(args):
    METHOD=args[0]; chromo=args[1]; n_cur_cell=args[2]; n_cur_master=args[3]; cur_cell=args[4]; cur_master=args[5]
    
    print "Chrom: ", chromo, " cell: ", n_cur_cell, " => master df col: ", n_cur_master 
    if (METHOD == 'AneuHMM') or (METHOD == 'AneuCP'):
       #print "cur_cell", cur_cell,"\n\n"
        #print "cur_master", cur_master,"\n\n"
        for idx, row in cur_master.iterrows(): # go through df_master intervals for that chrom
            num_matches=0 # master interval should only match once!
            for idx_cr_cell, row_cr_cell in cur_cell.iterrows(): # go through each cell bed for that chrom
                
                if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                    num_matches+=1
                    if METHOD == 'AneuHMM':
                        if "zero" in row_cr_cell['CN']:
                        #if type(row_cr_cell['CN']) == 'str':
                            cur_master.set_value(idx, n_cur_master, 0)
                            row_cr_cell['CN'] = 0
                    elif METHOD == 'AneuCP':
                        if type(row_cr_cell['CN']) == 'str':
                            cur_master.set_value(idx, n_cur_master, 0)
                            row_cr_cell['CN'] = 0
                    cur_master.loc[idx, n_cur_master] = int(row_cr_cell['CN'])
                    # if master interval is covered more than once in cur_cell-> flag to take a look
                    if num_matches>1:
                        sys.exit("ERROR", num_matches)
        
        # RETURNING NAN ROWS <- INDICATING THAT A MASTER INTERVAL WAS NOT FILLED (SHOULD NOT OCCUR) 
        # if master interval is NOT coverged - NAN -> check upon running
        nan_df = nans(cur_master)
        #print "NAN DF_MASTER SHAPE: \n",  nan_df.shape
        if nan_df.empty:
            return (n_cur_master, chromo, cur_master)
        else:
            print "NOT EMPTY: ", nan_df
            sys.exit("Must fix this")

    # Processing Ginkgo
    else:
        sys.exit("change if needed")
        for idx, row in cur_master.iterrows(): # go through df_master intervals for that chrom
            num_matches=0 # master interval should only match once!
            for idx_cr_cell, row_cr_cell in cur_cell.iterrows(): # go through each cell bed for that chrom
                if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                    num_matches+=1
                    cur_master.loc[idx, cell] = row_cr_cell[cell]
                    # if master interval is covered more than once in cur_cell-> flag to take a look
                    if num_matches>1:
                        sys.exit("ERROR", num_matches)

        # RETURNING NAN ROWS <- INDICATING THAT A MASTER INTERVAL WAS NOT FILLED (SHOULD NOT OCCUR) 
        # if master interval is NOT coverged - NAN -> check upon running
        
        nan_df = nans(cur_master)
        if nan_df.empty:
            return (chromo, n_cur_master, cur_master) 
        else:
            print nan_df
            sys.exit("Must fix this")

def check_each_chrom(df_cell_chrom, df_clone_event_chrom,TP,TN,FP,FN):
    if df_clone_event_chrom.empty:
        print "chrom does not have any inserted events:", df_clone_event_chrom
        print df_cell_chrom
        for idx, row in df_cell_chrom.iterrows():
            if row['CN']==2:
                TN+=1
            elif row['CN']!=2:
                FP+=1
    else:
        df_cell_chrom['ACCNTD'] =  np.nan
        df_clone_event_chrom['ACCNTD'] =  np.nan
        df_cell_chrom = df_cell_chrom.reset_index(); df_clone_event_chrom = df_clone_event_chrom.reset_index()
        print "events\n",df_clone_event_chrom
        print "cell\n",df_cell_chrom

        # for all inserted events
        for idx_evt, row_evt in df_clone_event_chrom.iterrows():
            num_matches=0 # see how many segments match with the inserted events (so to choose the largest one).
            print "index_event", idx_evt
            # for all called events by method
            for idx, row in df_cell_chrom.iterrows(): 
                print "\n TO ASSESS:", row['START'],"<=",row_evt['START']+row_evt['EVENT_SIZE']-1, "&", row_evt['START'],"<=", row['END'],"\n"

                # if overlap 
                # if (row.start <= (row_evt.START+row_evt.EVENT_SIZE+MIN_BIN_SIZE)) & (row_evt.START-MIN_BIN_SIZE <= row.end): # x1 <= y2 && y1 <= x2
                if ((row['START'] <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <= row['END'])):
                    num_matches+=1
                    df_clone_event_chrom = df_clone_event_chrom.set_value(idx_evt,'ACCNTD',0)
                    #print df_clone_event_chrom
                    #print "\n in interval"
                    # initiate overlap_df

                    if num_matches == 1: 
                        #print "num matches == 1"
                        overlap_df = pd.DataFrame(columns=['CHR','START','END','CN','ACCNTD'])
                        overlap_df = overlap_df.append(row, ignore_index=True)
                        #print "overlap\n", overlap_df

                        # if the next does not exist or does not overlap
                        if (df_cell_chrom.shape[0]-1 == idx):
                            #print "next1 is not overlap - find_event()"
                            TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                            break
                        elif (((df_cell_chrom.iloc[idx+1].START <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <=df_cell_chrom.iloc[idx+1].END)) == False):
                            #print "next2 is not overlap - find_event()"
                            TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                            break
                        else:
                            print "next is another overlap"
                    elif (df_cell_chrom.shape[0]-1 == idx):
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        #print "overlap added to", overlap_df
                        #print "at end of df, next is not overlap - find_event()"
                        TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                        break
                    elif (((df_cell_chrom.iloc[idx+1].START <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <=df_cell_chrom.iloc[idx+1].END)) == False) or (df_cell_chrom.shape[0] == idx): # if next row does not overlap - interval is complete
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        #print "overlap added to", overlap_df
                        #print "next3 is not overlap - find_event()"
                        TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                        break

                    else: # keep adding to dataframe
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        #print "overlap added to\n", overlap_df
                else:
                    print "\n not in interval"

        #print "transformed events\n",df_clone_event_chrom
        #print "transformed cell\n",df_cell_chrom

        nan_df_cell_chrom = nans(df_cell_chrom)
        if not nan_df_cell_chrom.empty:
            for idx,row in nan_df_cell_chrom.iterrows():
                if row['CN']==2:
                    TN+=1
                elif row['CN']!=2:
                    FP+=1
    #print "TPTN...", TP, TN, FP, FN       
    return TP,TN,FP,FN

def find_event(cell_df_inevent, row_evt, TP,TN,FP,FN, cell_df_chrom):
    #print "\n\nCELL_DF\n",cell_df_inevent
    #print "row_event\n",row_evt
    #count_once = 0
    
    # If only one overlapping segment
    if cell_df_inevent.shape[0] == 1:
        for idx, row in cell_df_inevent.iterrows():  # will only iterate once
            #t_TP=TP;t_TN=TN;t_FP=FP;t_FN=FN
            if (row_evt['MAX_CN'] <= 1) and (row['CN'] <= 1): # TP del
                #print "TP-"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                TP+=1
            elif (row_evt['MAX_CN'] >= 3) and (row['CN'] >= 3): # TP amp
                #print "TP+"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                TP+=1
            elif (row_evt['MAX_CN'] == 2) and (row['CN'] == 2): # TN diploid
                print "SHOULD NOT HAPPEN - TN"
                sys.exit()
            elif (row_evt['MAX_CN'] <= 1) and (row['CN'] == 2):
                #print "FN-2"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FN+=1
                #print "cell_df_chrom", cell_df_chrom
            elif (row_evt['MAX_CN'] >= 3) and (row['CN'] == 2):
                #print "FN+2"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FN+=1
            elif (row_evt['MAX_CN'] <= 1) and (row['CN'] >= 3):
                #print "FP-+"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FP+=1
            elif (row_evt['MAX_CN'] >= 3) and (row['CN'] <= 1):
                #print "FP+-"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FP+=1
            else:
                print("ERROR- another case...")
                sys.exit()
        #print "before: ", t_TP,t_TN,t_FP,t_FN
        #print "cont...", TP, TN, FP, FN
    
    # Multiple overlaping segements with one inserted event. Get all called amp or del events (depending on truth) for that region. 
    # All else will be considered later FP (if was amp but called del & vice versa) or FN (if called 2)
    elif cell_df_inevent.shape[0] > 1:
        #count_once+=1
        print "MULTIPLE SEGMENTS OVERLAP WITH ONE EVENT"
        print "cell_df_inevent", cell_df_inevent
        if (row_evt['MAX_CN'] <=1) and any(cell_df_inevent['CN'] <=1): # true dels
            selected = cell_df_inevent[cell_df_inevent['CN'] <= 1]
            for it, row_s in selected.iterrows():
                #print "TP-"
                TP+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and any(cell_df_inevent['CN'] >=3): # true amps
            selected = cell_df_inevent[cell_df_inevent['CN'] >= 3]
            for it, row_s in selected.iterrows():
                #print "TP+"
                TP+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
        elif (row_evt['MAX_CN'] <= 1) and any(cell_df_inevent['CN'] >= 3):
            selected = cell_df_inevent[cell_df_inevent['CN'] >= 3]
            for it, row_s in selected.iterrows():
                #print "FP-+"
                FP+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and any(cell_df_inevent['CN'] <= 1):
            selected = cell_df_inevent[cell_df_inevent['CN'] <=1]
            for it, row_s in selected.iterrows():
                #print "FP+-"
                FP+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
        elif (row_evt['MAX_CN'] >= 3) and any(cell_df_inevent['CN'] == 2): # this will be hit if the others are not - so ALL are 2 
            #if count_once==1:
            selected = cell_df_inevent[cell_df_inevent['CN'] == 2]
            for it, row_s in selected.iterrows():
                #print "FN+2"
                FN+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0                
        elif (row_evt['MAX_CN'] <= 1) and any(cell_df_inevent['CN'] == 2): # this will be hit if the others are not - so ALL are 2
            #if count_once==1:
            selected = cell_df_inevent[cell_df_inevent['CN'] == 2]
            for it, row_s in selected.iterrows():
                #print "FN-2"
                FN+=1
                cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0                    
        else:
            print "another case..."
            sys.exit()     
        #print "multi cont...", TP, TN, FP, FN
    
    return TP, TN, FP, FN


def get_vals_prec_recall(args): # curr_cell_df, cell_name
    curr_cell = args[0]; cell_name=args[1]
    #print "Currently counting TP,TN,FP,FN for: ", cell_name
    #curr_cell = curr_cell.rename(columns={i:'CN'})
    if METHOD != 'Ginkgo':            
        if curr_cell['CN'].dtype != np.float64 and curr_cell['CN'].dtype != np.int64:
            #print "Converting zero-inflation from df_master (should have been handled)"
            curr_cell['CN'] = curr_cell.CN.replace({"zero-inflation":0})
    curr_cell['CN'] = curr_cell['CN'].astype(int)
    TP=TN=FN=FP=0
    for chro in chromosome:
        curr_cell_chrom = curr_cell[curr_cell['CHR']==chro]
        TP,TN,FP,FN = check_each_chrom(curr_cell_chrom,clone_events[clone_events.CHR==chro],TP,TN,FP,FN)
    return (cell_name,TP,TN,FP,FN)


#### MAIN ####
if __name__ == '__main__':
    start = timer()
    
    # CREATE MASTER INTERVAL DATAFRAME: DF_MASTER
    chromosome = ["chr" + str(i) for i in range(1,23)]# chromosome
    chromosome.append('chrX'); chromosome.append("chrY")
    nans = lambda df: df[df.isnull().any(axis=1)]

    # Ginkgo processing df_master
    if (METHOD == 'Ginkgo'):
        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                #for ind, row in DATA:
                cur_chrom_DATA = DATA[DATA['CHR']=='chr1']
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                cur_chrom_DATA = DATA[DATA['CHR']==chro]
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        #df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        df_master = df_master[abs(df_master.START-df_master.END)>1]
        print "size df_master: ",df_master.shape 
        list_names_cells = list(df_master)[3:]
        print "list names cells:", list_names_cells

    # AneuFinder processing df_master
    else:
        list_names_cells, list_bed_files_cells = create_indiv_bed_files()

        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file chromosome 1
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                #df_res.append(res)
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file all other chromosomes
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        print "size df_master: ",df_master.shape 
    # Calling parallel_cell_chrom
    dict_dfs_chro_cell= {} # dict of (cells, chrom): df

    if (METHOD == 'AneuHMM') or (METHOD == 'AneuCP'):
        print "AneuFinder, ",METHOD, ". There are ", len(list_bed_files_cells), " cells to process for 24 chromosomes.\n"
        pool = Pool(processes = 10)
        df_parsed = pool.map(parallel_cell_chrom, [(METHOD, chro, cell_bed, list_names_cells[idex], extract_chr_cell_bed(cell_bed, chro), df_master[df_master['CHR']==chro]) for idex, cell_bed in enumerate(list_bed_files_cells) for chro in chromosome]) 
        for i in df_parsed:
            dict_dfs_chro_cell[( i[0], i[1] )] = i[2]

    else:
        print "Ginkgo. There are ", len(list(DATA)[3:]), " cells to process for 24 chromosomes."
        pool = Pool(processes = 10)
        p_chro, p_cell, p_df_mast = pool.map(parallel_cell_chrom, [(METHOD, chro, cell, cell, extract_chr_cell_tsv(cell), df_master[df_master['CHR']==chro]) for cell in list(DATA)[3:] for chro in chromosome])
        dict_dfs_chro_cell[( p_chro, p_cell )] = p_df_mast

    for name_cell in list_names_cells:
        print "Merging all chroms for cell: ", name_cell
        df_temp = pd.DataFrame(columns=['CHR','START','END',name_cell])
        for ch in chromosome:
            df_temp = df_temp.append(dict_dfs_chro_cell[(name_cell, ch)], ignore_index=True)
        df_master = df_master.join(df_temp[name_cell],how='outer')

    # Check to make sure all rows have been filled
    nan_df = nans(df_master)
    if nan_df.empty:
        df_master.to_csv(OUTPUTDIR+"/"+CLONE+"_"+METHOD+"_integrated_beds_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, index=False) 
    else:
        print nan_df
        sys.exit("Must fix this")

    # Calculate Precision and Recall in parallel for each cell
    dict_cell_pr={}
    pool = Pool(processes = PROCESSORS)

    tp_tn_fp_fn = pool.map(get_vals_prec_recall, [(df_master[['CHR','START','END',i]].rename(columns={i:'CN'}), i) for idx, i in enumerate(list_names_cells)]) 
    
    for i in tp_tn_fp_fn:
        dict_cell_pr[i[0]] = ((i[1],i[2],i[3],i[4]))


    # Check that all segments have been accounted for across all cells
    for k,v in dict_cell_pr.iteritems():
        if sum(v)!=len(df_master):
            sys.exit("ERROR - TP, TN, FP, FN values do not sum to total number of segments(",len(df_master),"): ", k,v)

    # Save TP,TN,FP,FN         
    with open(OUTPUTDIR+"/"+"accuracyVals_"+CLONE+"_"+METHOD+"_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, 'w') as f:
        f.write("Cell,TP,TN,FP,FN\n")
        for k,v in dict_cell_pr.iteritems():
            f.write(k+","+str(v[0])+","+str(v[1])+","+str(v[2])+","+str(v[3])+"\n")
    
    elapsed_time = timer() - start
    print "PRECISION_RECALL_PARALLEL.PY TIME ELAPSED: ", elapsed_time