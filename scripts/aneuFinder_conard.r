# Created By: Ashley Conard
# Last Modified : July 24, 2017
# AneuFinder commands - T5

#source("https://bioconductor.org/biocLite.R")
#biocLite("BSgenome.Hsapiens.UCSC.hg38")
#biocLite("AneuFinder")

#Installing through Github: http://r-pkgs.had.co.nz/git.html
# https://csg.sph.umich.edu/docs/R/localpackages.html
# http://kbroman.org/pkg_primer/pages/build.html # how to get R libraries

library(BSgenome.Hsapiens.UCSC.hg38)
library(AneuFinder)

#bins <- binReads('/Library/Frameworks/R.framework/Versions/3.3/Resources/library/AneuFinderData/t5_data_Gao/normal/SRR3082328_1.fastq.gz.bam.sorted.bam', binsizes=c(220000,320000), assembly='hg38') # variable-width bins are constructed in such a way that each bin will contain the same (mean) number of reference-reads corresponding to the specified binsize

# Local Folders
var.width.ref <- "/Users/ashleymaeconard/Desktop/singleCell/references/SRR3082324_1.fastq.gz.bam.sorted.bam"#system.file("t5_data_Gao/normal","SRR3082328_1.fastq.gz.bam.sorted.bam",package="AneuFinderData")
#var.width.ref <-("/Users/ashleymaeconard/Desktop/singleCell/data/merged_T5_cell/merged_T5_normal_cells.bed.gz")
#var.width.ref <- "/Users/ashleymaeconard/Desktop/singleCell/data/merged_T5_cell/SRR_all_cells_merged.bam"
datafolder <- "/Users/ashleymaeconard/Desktop/singleCell/data/T5_normal_bed_files/test/"#system.file("t5_data_Gao", "tumor/two/", package = "AneuFinderData")
outputfolder <- "/Users/ashleymaeconard/Desktop/AnF_test_ref_2423/" #outputs/50kb_t5/"

# Server Folders - Normal cells T5
#var.width.ref <- "/nbu/compbio/aconard/gao_2016_SCS/newBowtie_run_normal_T5/bam_files_normal_t5/merged_cell_bam/SRR_all_cells_merged.bam"
#datafolder <- "/nbu/compbio/aconard/gao_2016_SCS/newBowtie_run_normal_T5/bam_files_normal_t5/merged_cell_bam"
#outputfolder <- "/data/compbio/aconard/SCS/aneuFinder_output_pooledCells_as_ref_T5_normal"
# princeton servers - 10 CPUs

# Server Folders <- Tumor cells T5
#var.width.ref <- "/home/aconard/Projects/singleCell/aneuFinder/normal_ref_SRR3082485_1_aneuFinder/SRR3082328_1.fastq.gz.bam.sorted.bam"
#datafolder <- "/nbu/compbio/aconard/gao_2016_SCS/BAM_SAM_BED_Gao_TNBC_tumor_T5/sorted_bam_aneuFinder_INPUT"
#outputfolder <- "/home/aconard/aneuFinder/output_t5_aneuFinder/" 

# Initialize copy-number states and call AneuFinder.
cnv.states <- c('zero-inflation', paste0(1:10, '-somy'))
Aneufinder(inputfolder = datafolder, outputfolder = outputfolder, numCPU = 1, binsizes = c(220000), variable.width.reference = var.width.ref, chromosomes = paste0('chr', c(1:22,'X')), states = cnv.states, correction.method = 'GC', GC.BSgenome = BSgenome.Hsapiens.UCSC.hg38,num.trials = 1, assembly = 'hg38')

# Output the copy-numbers for each bin count. OR JUST GRAB THE .BED FILE IN BROWSERFILES
#files <- list.files('/Users/ashleymaeconard/Desktop/results_normal_38_T5_220kb_AneuFinder/MODELS/method-dnacopy/', full.names=TRUE, pattern='binsize_5e\\+04')
#models <- loadFromFiles(files)
#cn <- sapply(models, function(model) { AneuFinder:::initializeStates(levels(model$bins$state))$multiplicity[as.character(model$bins$state)] })
#dimnames(cn) <- list(bin=NULL, file=basename(files))
#write.csv(cn, 'cn_t5_normal.csv')
