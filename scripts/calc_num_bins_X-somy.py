#!/usr/bin/python
#############################
#
# calc_num_bins_X-somy.py: calc num bins of a particular state (diploid, triploid, ect.)
# created by : Ashley Conard
# last updated: July 26, 2017
#
#############################
from __future__ import division
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import re
import numpy as np
import collections as c
from collections import Counter
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from os import listdir
from os.path import isfile, join
from collections import OrderedDict

# FILE POSITIONS - ANEUFINDER
CHR_A = 0
START = 1
END = 2
CN_A = 4

def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2
    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1])/2.0

###########################
# USAGE
if len(sys.argv) != 5:
  sys.stderr.write("Usage: python " + sys.argv[0] + " DIRECTORY_INPUT_FILES(.csv files from compare_methods.py - e.g.T5_normal_refAllCells_38cells_aneuDNACOPY_SRR3082145_1.fastq.gz.csv)  STATE(2,3,etc.)  OUTPUT_FILE_DIR  OUTPUT_FILE_NAME\n")
  sys.exit(1)
###########################

# ARGUMENTS
FILEPATH = sys.argv[1]
STATE = str(sys.argv[2])
if ".0" not in STATE:
  STATE = STATE+".0"
OUTPUT_DIR = str(sys.argv[3])
OUTPUT_FILE = str(sys.argv[4])

if __name__ == '__main__':
  list_all_seq = [] # set of all unique cell sequences
  mast_df = pd.DataFrame(columns=('CELL', 'STATE_COUNT', 'TOTAL', 'FRAC_TOT')) 
  temp = 0

  onlyfiles = [f for f in listdir(FILEPATH) if (isfile(join(FILEPATH, f)) and ".csv" in f)]
  print "There are ", len(onlyfiles), " files to process."
  idx=-1
  list_all_state_counts=[]
  
  for f in onlyfiles:

    idx+=1
    print "Reading .csv for cell: ", FILEPATH+f # file and filepath
    df = pd.read_csv(FILEPATH+f, header=None)    
    
    df.columns = ["Index", "CHR", "START", "END", "ANEUF", "GINKGO"] 
    df = df.drop(df.index[0])
    
    state_count = df[df["ANEUF"] == STATE].count().ANEUF 
    total_bins = df.shape[0]
    #print "State count: ", state_count, " Total: ", total_bins

    list_all_seq.append(df["ANEUF"].tolist())

    if "merged" in f:
      print "merged in name - not adding to averaging list"
    else:
      list_all_state_counts.append(state_count)

    mast_df.loc[idx] = [f, state_count, total_bins, (state_count/total_bins)]
  
  mast_df.to_csv(OUTPUT_FILE+"_"+STATE+"-somy__calc_num_bins_X-somy.csv")
  print "Saved .csv file here: ", OUTPUT_FILE+"_"+STATE+"-somy__calc_num_bins_X-somy.csv"
  
  print "\nThere are ", len(set(tuple(i) for i in list_all_seq)), " unique sequences. Which translates to this many clones."
  
  c_list_all_state_counts = Counter(list_all_state_counts)
  #print "list :", list_all_state_counts
  print "\nCounts of number of bins with ",STATE,"-somy ", c_list_all_state_counts
  print "Total number of bins: ", total_bins
  print "\nMean of ",STATE, "-somy across all single cells: ", (sum(list_all_state_counts) / float(len(list_all_state_counts)))
  print "Median of ", STATE, "-somy across all single cells: ", median(list_all_state_counts)
  print "Mode of ", STATE, "-somy across all single cells: ", c_list_all_state_counts.most_common(1)[0][0]

#    aneuFinder_processing(df)