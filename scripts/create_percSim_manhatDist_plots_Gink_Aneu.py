#!/usr/bin/python
#
# create_percSim_manhatDist_plots_Gink_Aneu.py 
#	
# Ashley Conard

from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import pandas as pd
import os
import warnings

###########################
# USAGE
if len(sys.argv) != 3:
  sys.stderr.write("Usage: " + sys.argv[0] + " PROCESSED_FILE.tsv(from comparing_methods.py output) PDF_FILE_OUTPUTNAME\n")
  sys.exit(1)

###########################
# ASSIGNMENTS
FILE = str(sys.argv[1])
OUTPUT_NAME = str(sys.argv[2]) 

###########################
# merge the tsv files as output from comparing_methods.py (with CELL, PERCENT_SIM, MANH_DIST, NUM_BINS, PATIENT)

def label_point(x, y, val, ax):
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        ax.text(point['x'], point['y'], str(point['val']))

if __name__ == '__main__':
	data_merged = [['T5N', 0.9440298507]] # 94.40298507/100
	df_merged = pd.DataFrame(data_merged,columns=['PATIENT','VAL'])

	df = pd.read_csv(FILE, sep=',')
	df.PERCENT_SIM = df.PERCENT_SIM/100
	# Create percent similarity fig.
	perce_si = sns.stripplot(x="PATIENT", y="VAL", data=df_merged, color="red")
	perce_sim = sns.boxplot(x="PATIENT", y="PERCENT_SIM", data=df)
	perce_sim = sns.stripplot(x="PATIENT", y="PERCENT_SIM", data=df, jitter=True, edgecolor=sns.color_palette("hls", 8), facecolors="none", split=False, alpha=0.7, color="0") #hue="CELL"
	# Count fraction of cells below 40% 
	
	#T5
	df_T5_bel30 = df[(df['PATIENT']=='T5') & (df['PERCENT_SIM'] <= 30)]
	df_T5 = df[df['PATIENT'] == 'T5']
	print "Below sim 30%, T5: ", len(df_T5_bel30)/len(df_T5)*100
	
	#T4
	df_T4_bel30 = df[(df['PATIENT']=='T4') & (df['PERCENT_SIM'] <= 30)]
	df_T4 = df[df['PATIENT'] == 'T4']
	print "Below sim 30%, T4: ", len(df_T4_bel30)/len(df_T4)*100

	#T7
	df_T7_bel30 = df[(df['PATIENT']=='T7') & (df['PERCENT_SIM'] <= 30)]
	df_T7 = df[df['PATIENT'] == 'T7']
	print "Below sim 30%, T7: ", len(df_T7_bel30)/len(df_T7)*100

	plt.title("Fraction of Concordant Bins Between Ginkgo and AneuFinder")
	plt.ylabel('Frac. Concordant Bins')
	plt.xlabel('Patients')
	#perce_sim.legend_.remove()
	#plt.legend(loc=1,prop={'size':5},ncol = 2)
	plt.ylim(0, 1)
	#label_point(df.PATIENT, df.PERCENT_SIM, df.CELL, perce_sim)

	sns.plt.savefig(OUTPUT_NAME+'_perc_sim.pdf')

	sns.plt.show()	
	sns.plt.clf()

	# Create manhattan distance fig.
	man_dist = sns.stripplot(x="PATIENT", y="MANH_DIST", hue="CELL", data=df, jitter=True, edgecolor=sns.color_palette("hls", 4), facecolors="none", split=False, alpha=0.7)
	
	plt.title("Manhattan Distance Between Profiles")
	plt.ylabel('Manhattan Distance')
	plt.xlabel('Patients')
	man_dist.legend_.remove()
	plt.ylim(0)
	sns.plt.savefig(OUTPUT_NAME+'_manh_dist.pdf')

	sns.plt.show()
