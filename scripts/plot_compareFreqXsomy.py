#!/usr/bin/python
#############################
#
# plot_compareFreqXsomy.py 
# created by : Ashley Conard
# last updated: Aug. 7, 2017
#
#############################

from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
from os import listdir
from matplotlib.colors import ListedColormap
import pandas as pd
import numpy as np
from os.path import isfile, join
import os
import warnings

###########################
# merge the tsv files as output from comparing_methods.py (with CELL, PERCENT_SIM, MANH_DIST, NUM_BINS, PATIENT)

def label_point(x, y, val, ax):
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        ax.text(point['x'], point['y'], str(point['val']))

def change_color(row):
    #print row['CELL'].strip(" ")# , row['CELL']== 'SRR3082330_1.fastq.gz', type(row['CELL'])
    if "merged" in row['CELL_NAME'].strip(" "): #row['CELL'] == str(CHANGECOLOR):
        print "merged cell: ", row['CELL_NAME'].strip(" ")
    	return 'Blue'
    else:
        return 'Red'

def create_list_cells(dataf):
    df = pd.read_csv(dataf, sep=',')
    # Remove unwanted cells in df
    df['CELL_NAME'] = df.CELL_NAME.str.replace('.bam.sorted.bam?' , '')
    df = df[df.CELL_NAME != 'merged_cell_T5_normal_not2512_not2324_not2498.bam.sorted.bam'] 
    df = df[df.CELL_NAME != 'SRR3082512_1.fastq.gz']
    df = df[df.CELL_NAME != 'SRR3082324_1.fastq.gz']
    df = df[df.CELL_NAME != 'SRR3082498_1.fastq.gz']
    df = df.sort_values('CELL_NAME', ascending=True) 

    list_c = df['CELL_NAME'].tolist()
    return list_c
    
def append_dataframes(toAdd_df, mast_df, cells_dont_include, list_cells):
    
    # Read in df
    df = pd.read_csv(toAdd_df, sep=',')
    
    # Remove unwanted cells in df
    df['CELL_NAME'] = df.CELL_NAME.str.replace('.bam.sorted.bam?' , '')
    df['CELL_NAME'] = df.CELL_NAME.str.replace('.bam.sorted', '')
    df = df[df.CELL_NAME != 'SRR3082512_1.fastq.gz']
    df = df[df.CELL_NAME != 'SRR3082324_1.fastq.gz']
    df = df[df.CELL_NAME != 'SRR3082498_1.fastq.gz']

    # Keep only 3 columns
    df = df[['ID','CELL_NAME', 'NORM_X-SOMY_BINS']] 
    #df.CELL_NAME.astype(str).argsort()
    df = df.sort_values('CELL_NAME', ascending=True) 
    
    if df.shape[0] != len(list_cells): # make sure there are 35 rows - i.e. 35 cell values (34 cells + merged cell)
        print "List cells and sorted DF are not the same: ", len(list_cells), " vs. ", df.shape[0]
        #sys.exit("ERROR - there should be 35 cell values, 34 cells and 1 merged cell")

    if list_cells != df['CELL_NAME'].tolist():
        print "dif : ",  set(df['CELL_NAME'].tolist())-set(list_cells)
        #sys.exit("ERROR - current cell list does not match ADNACOPY cell list")

    # Add colors:
    #flatui = ["#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71", "#800080", "#FF00FF", "#000080",  "#0000FF", "#008080", "#00FFFF", "#008000", "#00FF00", "#808000", "#FFFF00", "#800000", "#FF0000", "SALMON", "INDIANRED", "#BD8932", "BD5432", "#5ABD32", "#13F39E", "#13F3DF", "#13A8F3", "#136FF3","#5A13F3","#B613F3","#F313E5","#F31386","#F3133C","#ECF313","#386E54","#399874"] #"#7B3998"] #sns.palplot(sns.color_palette("colorblind", 35))
    #df['COLOR_MAP']= flatui
    mast_df = mast_df.append(df)
    return mast_df

###########################
# USAGE
if len(sys.argv) != 7:
  sys.stderr.write("Usage: " + sys.argv[0] + " <DATA_DIR (dir of freq files from bin_bed_or_tsv.py output "+
                                            "(e.g. folder of 4 files, T5_normal_ID_ADNACOPY_1000000size_frequency_2-somy_bins__bin_bed_or_tsv.tsv "+
                                            "other 3 for AHMM,GFIXCP,GVARCP))>  <BIN_SIZE> <CELL_LIST_NOT_TO_INCLUDE> <PDF_FILE_OUTPUTNAME> <BG (bluegreen) or COLOR>"+
                                            "<BIN SIZE FOR PLOT TITLE>\n")
  sys.exit(1)

###########################
# ASSIGNMENTS
DATA_DIR = str(sys.argv[1])
BIN_SIZE = str(sys.argv[2])
DONOT_INCLUDE = str(sys.argv[3])
OUTPUT_NAME = str(sys.argv[4]) 
PLOT_COLOR = str(sys.argv[5])
BINSIZE_KB = str(sys.argv[6])
if ((PLOT_COLOR!='COLOR') and (PLOT_COLOR!='BG')):
    sys.exit("ERROR - PLOT_COLOR (last argument) must be 'BG' or 'COLOR'")
PLOT_COLOR = str(sys.argv[5])
if ((PLOT_COLOR!='COLOR') and (PLOT_COLOR!='BG')):
    sys.exit("ERROR - PLOT_COLOR (last argument) must be 'BG' or 'COLOR'")

if __name__ == '__main__':
    master_df = pd.DataFrame(columns=('ID','CELL_NAME', 'NORM_X-SOMY_BINS'))
    
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and not "merged_T5" in f]
    print "\nAll Files: ", onlyfiles

    downsampled = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and "T5_normal_downsample" in f]
    print "\nDownsampled Files: ", downsampled
    downsampled_df = pd.read_csv(DATA_DIR+"/"+downsampled[0])
    downsampled.remove(downsampled[0]) # remove the first cell
    
    for i in downsampled:
        downsampled_2 = pd.read_csv(DATA_DIR+"/"+i)
        downsampled_df = downsampled_df.append(downsampled_2)

    print "downsampled_df", downsampled_df
    
    print "\nThose cells not to include: ", DONOT_INCLUDE, type(DONOT_INCLUDE)
    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store")
        #panel = panel1.append(panel2)
    for indx,i in enumerate(onlyfiles):
        print "\nfile: ", i
        if indx == 0:
            list_cells = create_list_cells(DATA_DIR+"/"+i) # create the list of cells to compare all dfs to
        master_df = append_dataframes(DATA_DIR+"/"+i, master_df, DONOT_INCLUDE, list_cells)

    # Count fraction of cells below 40% 
	#ADNACOPY
    if master_df['ID'].str.contains("ADNACOPY").any():
        df_adna_pt3 = master_df[(master_df['ID']=='ADNACOPY') & (master_df['NORM_X-SOMY_BINS'] <= .3)]
        df_adna = master_df[master_df['ID'] == 'ADNACOPY']
        print "Below sim 30%, ADNACOPY: ", len(df_adna_pt3)/len(df_adna)*100

    #AHMM
    if master_df['ID'].str.contains("AHMM").any():
        df_ahmm_pt3 = master_df[(master_df['ID']=='AHMM') & (master_df['NORM_X-SOMY_BINS'] <= .3)]
        df_ahmm = master_df[master_df['ID'] == 'AHMM']
        print "Below sim 30%, AHMM: ", len(df_ahmm_pt3)/len(df_ahmm)*100

    #GFIXCP
    if master_df['ID'].str.contains("GFIXCP").any():
        df_fixed_pt3 = master_df[(master_df['ID']=='GFIXCP') & (master_df['NORM_X-SOMY_BINS'] <= .3)]
        df_fixed = master_df[master_df['ID'] == 'GFIXCP']
        print "Below sim 30%, GFIXCP: ", len(df_fixed_pt3)/len(df_fixed)*100

    #GVARCP
    if master_df['ID'].str.contains("GVARCP").any():
        df_gvar_pt3 = master_df[(master_df['ID']=='GVARCP') & (master_df['NORM_X-SOMY_BINS'] <= .3)]
        df_var = master_df[master_df['ID'] == 'GVARCP']
        print "Below sim 30%, GVARCP: ", len(df_gvar_pt3)/len(df_var)*100
    
    master_df['LABEL'] = master_df.apply(change_color, axis=1)
    master_df.to_csv(OUTPUT_NAME+"_"+BIN_SIZE+"size_4methods__plot_compareFreqXsomy.csv")
    merged_master_df = master_df[(master_df['CELL_NAME'] == 'merged_cell_T5_normal_not2512_not2324_not2498')]
    print "Saved master dataframe as: ", OUTPUT_NAME+"_"+BIN_SIZE+"size_4methods__plot_compareFreqXsomy.csv"

	# Create percent similarity fig.
    if PLOT_COLOR == 'COLOR':
    # COLORFUL PLOT TO DISTINGUISH EACH CELL
        master_df = master_df[master_df.CELL_NAME.str.contains("merged")== False]
        #master_df = master_df[(master_df['CELL_NAME'] != 'merged_cell_T5_normal_not2512_not2324_not2498_pt1')]
        #print "MASTER ", master_df
        
        plotted = sns.boxplot(x="ID", y="NORM_X-SOMY_BINS", data=master_df)
        plotted = sns.stripplot(x="ID", y="NORM_X-SOMY_BINS", hue="CELL_NAME", data=master_df, jitter=True, c=master_df['CELL_NAME'], s=7, linewidth=1)#, edgecolor='black')
        #plotted = sns.stripplot(x="ID", y="NORM_X-SOMY_BINS", data=merged_master_df, jitter=True, color='red', s=7, linewidth=1)#, edgecolor='black')
        plotted = sns.stripplot(x="ID", y="NORM_X-SOMY_BINS", data=downsampled_df, jitter=True, color='red', marker="*", s=15, linewidth=1)
        END = 'size_somy__plot_compareDifBinSizes.pdf'
    
    elif PLOT_COLOR == 'BG':
    # BLUE-GREEN PLOTS
        plotted = sns.stripplot(x="ID", y="NORM_X-SOMY_BINS", hue="LABEL", data=master_df, jitter=True, c=master_df['LABEL'], s=7, palette="muted", linewidth=1, color=".3" )#, edgecolor='black')
        END = 'size_somy_BLUEGREEN__plot_compareDifBinSizes.pdf'

    plt.title("%s" % BINSIZE_KB)
    plt.ylabel('Frac. Diploid Bins')
    plt.xlabel(BIN_SIZE)
    plotted.legend(loc = 'right', ncol=1, prop={'size': 7}, bbox_to_anchor=(2, 0.5))
    plt.ylim(-0.1, 1.05)

    sns.plt.savefig(OUTPUT_NAME+'_freq_bins_'+BIN_SIZE+END)
    sns.plt.show()