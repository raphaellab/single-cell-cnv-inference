#!/usr/bin/python
from __future__ import division
import math, os, csv
from collections import defaultdict as dd
import sys,os,itertools, argparse, scipy
from plotly.tools import FigureFactory as FF
import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt 
from os import listdir
from scipy.cluster.hierarchy import dendrogram, linkage
from os.path import isfile, join
import pysam as pys
import pandas as pd
import subprocess
import plotly.plotly as py
import collections as c
import numpy as np
import pysam as pys
import re

# Ashley Mae
# Created Oct 20, 2016
#####

def roundup(x): # rounding the starting position up to the nearest ten.
    return int(math.ceil(x/10.0))*10 

def in_blacklist(poten_start, chromo, size_ev, blacklist_dict):
    for i in blacklist[chromo]: # for each blacklist interval
        print "interval to check: ", i 
        if ((poten_start <= int(i[1])) and (int(i[0]) <= (poten_start+size_ev))): # check if ranges overlap (blacklist intervals and start and end of event)
            print "yes"
            return True # return true
    print "adding: ", poten_start, (poten_start+size_ev)
    blacklist_dict[chromo].append((poten_start,(poten_start+size_ev)))
    print blacklist_dict
    return False

def create_event(chromo, event_size, end_chroms, blackl):
    #start_potential = roundup(np.random.randint(0,high=20))
    start_potential = roundup(np.random.randint(1000000, high=end_chroms[chromo]-11000000)) # subtract 11MB from end of each chrom (10MB for bin size and 1MB for telomere)
    print "start_potential", start_potential, "end potential: ", start_potential+event_size

    # rerun if any chosen start-end val is in blacklist

    while in_blacklist(start_potential, chromo, event_size, blackl):
        print "its in the blacklist so restart"
        #create_event(chromo, event_size)
        #start_potential = roundup(np.random.randint(0,high=20))
        start_potential = roundup(np.random.randint(1000000, high=end_chroms[chromo]-11000000))

    print "final start_pos: ", start_potential
    return start_potential
        
######################################
# USAGE
if len(sys.argv) != 6:
    sys.stderr.write("Usage: python" + sys.argv[0] + " <OUTPUT_NAME> <OUTPUT_DIR> <genome (e.g. g1 or g2> <seed> <num_cells> THEN CALLS generate_CNV_events_addedNoise_PROD1CELL.py\n")
    sys.exit(1)

######################################
# ARGUMENTS
OUTPUT_NAME = sys.argv[1]
OUTPUT_DIR = sys.argv[2]
GENOME = str(sys.argv[3])
seed_to_use = np.random.seed(int(sys.argv[4]))
num_cells = int(sys.argv[5])
# knownDeletion = c(GRanges(IRanges(5000000,10000000), seqnames="chr11"), GRanges(IRanges(500000,1000000), seqnames="chr2"), GRanges(IRanges(80000,880000), seqnames="chr5"))
######################################
# MAIN
if __name__ == '__main__':
    if GENOME == 'g1':
        g_del_dup = [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 
       0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 
       1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 
       1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 
       1, 1, 0, 1, 0, 1, 1, 1, 1, 0]

    elif GENOME == 'g2':
        sys.exit("To add later.")
        g_del_dup = [0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0,
       1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1,
       1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1,
       1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0,
       1, 0, 0, 0, 1, 0, 0, 1]
    else:
        sys.exit("ERROR: Enter g1 or g2.")
        
    #d = defaultdict(list, (('chr1', [(1,5)]), ('chr2', [(6,19)])))
    blacklist = dd(list, (('chr1',[(124500000, 125500000)]), 
    ('chr2', [(92800000.0, 93800000.0)]), 
    ('chr3', [(90500000, 91500000)]), 
    ('chr4', [(49900000.0, 50900000.0)]),
    ('chr5', [(47900000.0, 48900000.0)]), 
    ('chr6', [(60500000, 61500000)]), 
    ('chr7', [(59400000.0, 60400000.0)]), 
    ('chr8', [(45100000.0, 46100000.0)]),
    ('chr9', [(48500000, 49500000)]), 
    ('chr10', [(39700000.0, 40700000.0)]), 
    ('chr11', [(53200000.0, 54200000.0)]), 
    ('chr12', [(35300000.0, 36300000.0)]), 
    ('chr13', [(17400000.0, 18400000.0)]), 
    ('chr14', [(17100000.0, 18100000.0)]), 
    ('chr15', [(18500000, 19500000)]), 
    ('chr16', [(36100000.0, 37100000.0)]), 
    ('chr17', [(23500000, 24500000)]), 
    ('chr18', [(16700000.0, 17700000.0)]),
    ('chr19', [(26000000.0, 27000000.0)]), 
    ('chr20', [(27000000.0, 28000000.0)]), 
    ('chr21', [(12700000.0, 13700000.0)]), 
    ('chr22', [(14200000.0, 15200000.0)])))
    #blacklist = dd(list, (('chr1', [(1,5)]), ('chr2', [(6,19)])))
    chromosome_ends = {'chr1':248956422, 
    'chr2': 242193529, 
    'chr3': 198295559, 
    'chr4': 190214555,
    'chr5': 181538259, 
    'chr6': 170805979, 
    'chr7': 159345973, 
    'chr8': 145138636,
    'chr9': 138394717, 
    'chr10': 133797422, 
    'chr11': 135086622, 
    'chr12': 133275309, 
    'chr13': 114364328, 
    'chr14': 107043718, 
    'chr15': 101991189,
    'chr16': 90338345, 
    'chr17': 83257441, 
    'chr18': 80373285,
    'chr19': 58617616, 
    'chr20': 64444167, 
    'chr21': 46709983, 
    'chr22': 50818468}
    #s = [str(i) for i in range(1,23)]
    #list_chrom = map(( lambda x: 'chr' + x), s)
    list_sizes = [10000000,5000000,1000000,500000,100000,50000,40000,30000,20000,10000]

    idx = -1
    with open(OUTPUT_DIR+"/"+OUTPUT_NAME+".csv", 'w') as f:
        f.write("CHR,START,EVENT_SIZE,MAX_CN\n")
        # TRUE EVENTS
        for i in range(0,5): # going through the sizes of sizes 10 times (to insert 100 events)
            for size_event in list_sizes:
                idx+=1
                # deletion
                if g_del_dup[idx] == 0: 
                    chromosome = 'chr'+str(np.random.randint(1,high=23))# chromosome
                    max_CN = np.random.randint(0,high=2) # CN
                    print "\nSize DEL event, chromo, max CN: ", size_event, chromosome, max_CN 
                    start = create_event(chromosome, size_event, chromosome_ends, blacklist)
                    end = start + size_event # size
                    print "Chrom: ", chromosome, " Start and end are: ", start, end
                    f.write(chromosome+","+str(start)+","+str(size_event)+","+str(max_CN)+"\n")
                
                # duplication
                elif g_del_dup[idx] == 1: 
                    chromosome = 'chr'+str(np.random.randint(1,high=23))# chromosome
                    max_CN = np.random.randint(3,high=9) # CN
                    print "\nSize AMP event, chromo, max CN: ", size_event, chromosome, max_CN 
                    start = create_event(chromosome, size_event, chromosome_ends, blacklist)
                    print "starta", start
                    end = start + size_event # size
                    print "Chrom: ", chromosome, " Start and end are: ", start, end
                    f.write(chromosome+","+str(start)+","+str(size_event)+","+str(max_CN)+"\n")

    with open('blacklist.csv', 'wb') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in blacklist.items():
            writer.writerow([key, value])

    #python" + sys.argv[0] + " <OUTPUT_NAME_per_cell> <OUTPUT_DIR> \n")
    for i in range(0,num_cells):
        cmd = r"python ~/Desktop/singleCell/single-cell-cnv-inference/scripts/generate_CNV_events_addedNoise_PROD1CELL.py "+OUTPUT_DIR+OUTPUT_NAME+".csv " + OUTPUT_DIR+OUTPUT_NAME + "n_randNoise_cell" + str(i)+ ".csv"
        os.system(cmd)  