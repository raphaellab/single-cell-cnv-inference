#!/usr/bin/python2.7

# Forward Backward Algorithm 
# Ashley Conard
# 

#
# fb_algo.py
# To address: 
#   changed - hard coded bound on allowed repeated copy number for a segment. Allowed range now: 0-20 with 30 max different copy number changes
#        ok - NB regression and overdispersion param. 
#           - when CN is 0 <- still add noise...
#           - use the nbCNV simulated or 12 patient Gao.
#           - determine chrom. percent based on haplo or diplo?
#           - bin coverage
#           - None, dizzy, cold <- BKW no normal?

# To Do:     
#           - not using genome length (total number of copy-number states)
#           - BW -> https://www.math.univ-toulouse.fr/~agarivie/Telecom/code/index.php
#           - R BW -> https://cran.r-project.org/web/packages/HMM/HMM.pdf
#           -> compare!!! -> FB to R FB~
#           - normalization: http://www.analytictech.com/ba762/handouts/normalization.htm
#           - http://www.math.uah.edu/stat/apps/SpecialCalculator.html
#           - explanation of dnbinom: http://stats.stackexchange.com/questions/125477/negative-binomial-distribution-r
#           - helpful for negative binomial simulation! http://www.math.uah.edu/stat/bernoulli/NegativeBinomial.html

# Notes:
#           diplo_len = 3095693981 # normal diploid genome length
#                       (3407692*50)/1547846990.5 = 0.1 <- coverage = (read_len * num_reads) / haplo_geno
#           chr_num_reads = diplo_len*(chr_chosen_perc_reads/100) # genome length
#                       3407692 average num reads from WGS dataset (Gao et al. 2016)
#           Get num bins at 50KB bins (as in simulations for nbCNV, CNVnator, and control-FREEC)
#           n_bins = int(float("%.2f" % round(chr_num_reads/220000,0)))  #or 50 000 in nbCNV
#           read_matrix_norm = [] # matrix RD normalized (reads/())
#           read_matrix_coverage = [] # matrix RD coverage (reads/bin_len) * read_len

# Cluster/Dendo:
#           - blog used : https://joernhees.de/blog/2015/08/26/scipy-hierarchical-clustering-and-dendrogram-tutorial/#comment-6215
#           - fcluster work : http://stackoverflow.com/questions/7724635/clustering-with-scipy-clusters-via-distance-matrix-how-to-get-back-the-origin?rq=1
#           - used for explanation : http://mathoverflow.net/questions/38855/nodes-clusters-with-a-distance-matrix
#           - potentially the answer: using distance matrix with linkage: http://stackoverflow.com/questions/18952587/use-distance-matrix-in-scipy-cluster-hierarchy-linkage
#           - annotation with distance between cells: http://stackoverflow.com/questions/11917779/how-to-plot-and-annotate-hierarchical-clustering-dendrograms-in-scipy-matplotlib

# Commands
# python fb_algo.py 3 3 5 21

from __future__ import division
from datadiff import diff
import seaborn as sns
import ast
import matplotlib
from scipy.stats import poisson as pois
#matplotlib.style.use('ggplot')
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
#plt.style.use('ggplot')
import math as m
import sys
from scipy.special import gamma#ln
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import subprocess
from scipy.cluster.hierarchy import fcluster
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import random
import os
import math
import scipy.special as special
import scipy.optimize as optimize
import pprint
from collections import OrderedDict

# Read from input file
def read_file(input_file):
    with open(input_file,'r') as f:
#   if "ALPHA" in line:
        ALPHA = str(f.readline().split('=', 1)[-1].strip(" "))
#   elif "M_CELLS" in line:
        M_CELLS = int(f.readline().split('=', 1)[-1].strip(" "))
#   elif "N_BINS" in line:
        N_BINS = int(f.readline().split('=', 1)[-1].strip(" "))
#   elif "K_CLONES" in line:
        K_CLONES = int(f.readline().split('=', 1)[-1].strip(" "))
#   elif "NUM_CLUST" in line:
        NUM_CLUST = int(f.readline().split('=', 1)[-1].strip(" "))
#   elif "clone_matrix" in line:
        stri = f.readline().split('=', 1)[-1].strip(" ")
        strs = stri.replace('[','').split('],')
        clone_matrix = [map(int, s.replace(']','').split(',')) for s in strs]
#   elif "geno_length" in line:
        stri = f.readline().split('=', 1)[-1].strip(" ")
        strs = stri.replace('[','').split('],')
        geno_length = [map(int, s.replace(']','').split(',')) for s in strs][0]
#   elif "assign_cells" in line:
        stri = f.readline().split('=', 1)[-1].strip(" ")
        strs = stri.replace('[','').split('],')
        str_list = [map(int, s.replace(']','').split(',')) for s in strs][0]
        assign_cells = np.array(str_list)
#   elif "cell_chr_tot_num_reads" in line:
        stri = f.readline().split('=', 1)[-1].strip(" ")
        strs = stri.replace('[','').split('],')
        str_list = [map(int, s.replace(']','').split(',')) for s in strs][0]
        cell_chr_tot_num_reads = np.array(str_list)
#   elif "read_matrix" in line:
        stri = f.readline().split('=', 1)[-1].strip(" ")
        strs = stri.replace('[','').split('],')
        read_matrix = [map(int, s.replace(']','').split(',')) for s in strs]
#   elif "mu_e_emis_matrix" in line:
        mu_e_emis_matrix = ast.literal_eval(f.readline().split('=', 1)[-1].strip(" "))

    chr_num_reads = (28.6585 * 50000 * N_BINS)
    return ALPHA, M_CELLS, N_BINS, K_CLONES, NUM_CLUST, clone_matrix, geno_length, cell_chr_tot_num_reads, read_matrix, mu_e_emis_matrix, assign_cells, chr_num_reads

# Create clone transition matrix
def create_clones(n_b, transition_probability):
    c = [2] # create a new clone
    for i in range(0, n_b-1):
        draw = np.random.uniform(0,0.99) # draw from uniform distribution
        #print "draw: ", draw
        cum_l = [] # cumulative list of probability values (roulette wheel)
        for idx,e in enumerate(transition_probability[c[-1]].values()):
            if idx == 0:
                cum_l.append((0,e))
            elif idx!=6: # do not consider the 'E' state
                cum_l.append((cum_l[-1][1],cum_l[-1][1]+e)) 
        cum_l[-1] = (cum_l[-1][0], 1.0) # DO NOT CONSIDER THE END STATE, THUS ENSURE THAT LAST ENTRY IS 1.
        #print "cum_l for CN",c[-1],"is ", cum_l
        for ide,ii in enumerate(cum_l):
            minim, maxim = ii
            if minim <= draw <= maxim:
                #prob_pt33 = np.random.random_integers(0,2) # with probability 0.5 you will switch to another copy-number state, determined by the transition matrix
                #if prob_pt33 == 0 or prob_pt33 == 1:
                #    c.append(c[-1])
                #else:
                c.append(ide) # next copy number is idx
                #print 'prb: ', prob_pt33
                #print "c: ", c
    return c

# Create the true clones (ground truth copy number profiles for each clone)
def create_clones_assign_cells(m_cells, n_bins, k_clones, transition_probability): 
    clones = []
    for i in range(0,k_clones-1): # create all-1 clone CNPs
        new_clone = create_clones(n_bins, transition_probability)
        clones.append(new_clone)

    c_dup = [2]*n_bins # diploid case
    clones.append(c_dup)

    # Assign cells to clones
    assign_cells = np.random.random_integers(0, k_clones-1, m_cells) # indices are the cells
    return clones, assign_cells

# Get the emission probabilities
LOOK_UP_EMISSION = {}
def get_emission_prob(states, state, x_is, e_m):
    # Only do this when mu_i and x_is(observation) are not in LOOK_UP_EMISSION
    mu = e_m[state]
    #print "(x_is, mu): ", x_is (observation), mu
    if (x_is, mu) not in LOOK_UP_EMISSION:
        total_sum = 0.0
        for st in states:
            mu_i = e_m[st]
            #print "mu_i", mu_i
            if mu_i==0.0: # in the case of mean of 0.0 reads for CN 0
                argss = [x_is,1] # observed RD_i, mean RD_i with 1 read for computation
            else:
                argss = [x_is,mu_i] # observed RD_i, mean RD_i,
            LOOK_UP_EMISSION[(x_is, mu_i)] = float(pois.pmf(argss[0],argss[1]))
            #print "LOOK_UP_EMISSION[(x_is, mu_i)]=", LOOK_UP_EMISSION[(x_is, mu_i)]
            total_sum += LOOK_UP_EMISSION[(x_is, mu_i)]
            #print "total_sum", total_sum
        assert total_sum != 0

        # Normalizing with respect to all CN state probabilities for that observation
        for st in states:
            mu_i = e_m[st]
            #print "before normalization:LOOK_UP_EMISSION[(x_is, mu_i)] = ", LOOK_UP_EMISSION[(x_is, mu_i)]
            LOOK_UP_EMISSION[(x_is, mu_i)] = LOOK_UP_EMISSION[(x_is, mu_i)] / total_sum
            #print "after LOOK_UP_EMISSION[(x_is, mu_i)=", LOOK_UP_EMISSION[(x_is, mu_i)]
    #print "LOOK UP EMISSION: ", LOOK_UP_EMISSION
    return LOOK_UP_EMISSION[(x_is, mu)]


def run_FB_each_cell(read_mat, a_trans, mu_e_emis):
    S_tensor = {}
    for idx, chr_rd in enumerate(read_mat):
        sys.stderr.write("Run forward, backward: cell and observation "+ str(idx)+"\n")#+str(chr_rd)+"\n")
        fwd_mat, bkw_mat, posterior_mat = run_FB_one_cell(chr_rd, a_trans, mu_e_emis[idx]) #min_max[idx]
        #print "cell idx, chr_rd", idx, chr_rd
        S_tensor[idx]= posterior_mat
    for c,p in S_tensor.items():
        for v in p:
            if round(sum(v.values()),2)!=1.0:
                sys.stderr.write("Cell "+str(c)+' does not sum to one')
                sys.exit(1)
    return S_tensor

def run_FB_one_cell(observations, transition_probability, mean_read_counts):
    h_bound=6 # 0-5
    start_probability = {}
    
    # Hidden states : Z = {0, ..., h} 
    states = list(range(h_bound)) 
    
    # Initial probability a_0 : all equal
    for i in list(range(h_bound)):
        start_probability[i] = (1/h_bound)
    #print "\nInitial Probability: ", start_probability

    # End_st
    end_state = 'E'

    #print "RD Observations: ", observations
    #print "Mean Read Counts:"
    #pprint.pprint(mean_read_counts)

    #print "Transition Probability: "
    #pprint.pprint(transition_probability)

    #print "\nBeginning FB Algorithm"

    return fwd_bkw(observations,
                       states,
                       start_probability,
                       transition_probability,
                       mean_read_counts,
                       end_state)

# Forward-Backward Algorithm
def fwd_bkw(x, states, a_0, a, e, end_st):
    L = len(x)
    fwd = []
    f_prev = {}

    # Forward part of algorithm
    #print "\nFORWARDS"
    for i, x_i in enumerate(x): # for every observation
        #print "\nbin i, observation x_i: ", i, x_i
        f_curr = {}
        for st in states: # for every CN state
            #print "CN state", st
            if i == 0:
                #print "base (prev_f_sum): ", a_0[st]
                # base case for the forward part
                prev_f_sum = a_0[st]
            else:
                #for k in states:
                    #print "k: ", "sum", f_prev[k], "*", a[k][st]
                prev_f_sum = sum(f_prev[k]*a[k][st] for k in states)
            
            # calculate NB emission probability
            prob_emis = get_emission_prob(states, st, x_i, e)
            #print "NB(x,mu)", x_i, e[st], ALPHA, "= norm prob_emis", prob_emis

            f_curr[st] = prob_emis * prev_f_sum
            #print "f_curr[st] = prob_emis * prev_f_sum: ",f_curr[st], "=",prob_emis, "*", prev_f_sum 

        # Normalize f_curr
        #sum_f_cur = sum(f_curr.values())
        #for k,v in f_curr.items():
        #    f_curr[k] = (v/sum_f_cur)
        
        fwd.append(f_curr)
        f_prev = f_curr

       # print "current fwd: ", fwd
       # print "\nf_prev = f_curr: ", f_prev, "=", f_curr

    # Termination Step Forward prob
    p_fwd = sum(f_curr[k]*a[k][end_st] for k in states)

   # print "\np_fwd = sum(f_curr[k]*a[k][end_st] for k in states)"
    #for k in states:
        #print "k: ", k
        #print "summing this: f_curr[k]*a[k][end_st]", f_curr[k], "*", a[k][end_st]
        #print f_curr[k]*a[k][end_st]
    #print "p_fwd: ", p_fwd


    #print "\nBACKWARDS"
    bkw = []
    b_prev = {}

    # Backward part of algorithm
    for i, x_i_plus in enumerate(reversed(x[1:]+['E'])):
       # print "\ni, x_i_plus: ", i, x_i_plus
        b_curr = {}

        for st in states:
           # print "state: ", st
            if i == 0: # base case for backward part
               # print "base case: ", a[st][end_st]
                b_curr[st] = a[st][end_st]
            else:
                b_curr[st]=0.0 # default setting probability
                for l in states:                    
                   # print "\ne [x mean ALPHA]", x_i_plus, e[l], ALPHA
                    
                    # calculate NB emission probability given observation and state
                    prob_emis_b = get_emission_prob(states, l, x_i_plus, e)
                    #print "prob_emis", prob_emis_b
                    b_curr[st]+= (a[st][l]*prob_emis_b*b_prev[l])
                    #print " b_curr[st]", b_curr, " sum(", a[st][l], "*", prob_emis_b ,"*", b_prev[l],")"

        # Normalize f_curr
        #sum_b_cur = sum(b_curr.values())
        #for key,val in b_curr.items():
        #    b_curr[key] = (val/sum_b_cur)
        
        bkw.insert(0,b_curr)
        b_prev = b_curr

        #print "bkw: ", bkw
        #print "b_prev = b_curr", b_prev, "=", b_curr

    # Termination Step Backward prob
    #print "\np_bkw = sum(a_0[l] * e[l][x[0]] * b_curr[l] for l in states)"
    p_bkw=0.0 # default setting first observation probability
    for ll in states:
       #print "ll: ", ll

        # calculate NB emission probability for first
        final_b_emis = get_emission_prob(states, ll, x[0], e)
        #print "final_backward_emis_prob", final_b_emis
        #print "a_0[l]", a_0[l], "e", final_b_emis, "b_curr[ll]", b_curr[ll]
        p_bkw+=(a_0[ll] * final_b_emis * b_curr[ll])
        #print "p_bkw=",p_bkw #, "summing this: ", a_0[l], "*", e[l][x[0]], "*", b_curr[l]
    
    #print "first observation p_bkw", p_bkw

    # Posterior (F*B / p_bkw) or (F*B / p_fwd)
    #print "\nPOSTERIOR"
    posterior = []
    for i in range(L):
        #print "i: ", i
        #for st in states:
            #print "fwd*bkw", fwd[i][st],"*",bkw[i][st],"/",p_fwd
        posterior.append({st: fwd[i][st]*bkw[i][st]/p_fwd for st in states})
        #print "posterior", posterior

    #print "posterior forward and backward: p_fwd, p_bkw", round(p_fwd,20), round(p_bkw,20)
    if round(p_fwd,10)!=round(p_bkw,10):
        sys.stderr.write("not equal "+str(round(p_fwd,10))+" "+str(round(p_bkw,10)))
    assert round(p_fwd,10)==round(p_bkw,10) # make sure that the forward and backward termination probabilities are equal
    
    #print "observations", x
    #print "fwdd: ", fwd
    #print "bkwdd: ", bkw
    #print "posterior ", posterior

    return fwd, bkw, posterior

# Create mean emission matrix for read counts
def create_mu_emission(total_num_reads_cells, genome_len_cells, all_assign_cells, m_cells, n_bins, h_cn_bound):
    # e 
    mu_emis_matrix = {}
    for i in range(m_cells):
        #print "\ni: ", i
        mu_emis_matrix[i]={}
        for j in range(0,h_cn_bound): # 0 - 5
            #print "j ", genome_len_cells[all_assign_cells[i]]
            mu_emis_matrix[i][j]=(total_num_reads_cells[i]/genome_len_cells[all_assign_cells[i]])*j
            if mu_emis_matrix[i][j] == 0.0:
                mu_emis_matrix[i][j]=1.0

    #print "create_mu_emission"
    #for k,v in mu_emis_matrix.items():
    #    print k
    #    print v
    return mu_emis_matrix
    #.append(pos_expected_num_reads)

# p-norm calculation
def pnorm(A,B,L_norm):
    dist = 0.0

    # Check that the sizes of f and n are the same for all S (soft copy number profiles)
    assert len(A)==len(B)
    assert len(A[0])==len(B[0])

    d_sum_f = d_sum_n = 0.0
    for i in range(0,len(A)): # num bins n
        for f in range(0,len(A[0])): # num hidden CN states f
            d = abs(A[i][f] - B[i][f])**L_norm
            #print "i", i, "f", f, "A[i][f]",A[i][f], "B[i][f]",B[i][f]
            #print "d = ", d
            d_sum_f+=d
            #print "d_sum_f", d_sum_f
        d_sum_n+=d_sum_f
        #print "d_sum_n", d_sum_n
        d_sum_f=0.0 # reset
    #print "d_sum_n: ", d_sum_n**(1/L_norm)
    return d_sum_n**(1/L_norm)

# Create the distance matrix for clustering
def create_dist_matrix(S_mat,L_norm):
    dist_mat = {}

    for cell_A in range(len(S_mat)) :
        dist_mat[cell_A] = {}
        for cell_B in range(len(S_mat)):
            if cell_A == cell_B:
                #print "same cell: ", cell_A
                dist_mat[cell_A][cell_B] = 0.0
            else:
                #print "A"
                #print S_mat[cell_A]
                #print "B"
                #print S_mat[cell_B]
                dist_mat[cell_A][cell_B] = pnorm(S_mat[cell_A], S_mat[cell_B], L_norm)
    
    # Create list from dictionary  
    d_l = []
    for i in dist_mat.values():
        l = []
        for j in i.values():
            l.append(j)
        d_l.append(l)
    return d_l 

# Create the dendrogram if PLOT is on
def create_dendrogram(dist_mat):
    #mat = np.array([[0.0, 2.0, 3.8459253727671276e-16], [2.0, 0.0, 2.0], [3.8459253727671276e-16, 2.0, 0.0]])
    #dist_mat = np.array([[0.0, 2.0, 0.1], [2.0, 0.0, 2.0], [0.1, 2.0, 0.0]])
    dists = squareform(dist_mat)
    linkage_matrix = linkage(dists, "single")
    #dendrogram(linkage_matrix, color_threshold=1,show_leaf_counts=False)
   # print "link:", linkage_matrix
    if PLOT == 'yes':
        show_leaf_counts = False
        ddata = fancy_dendrogram(linkage_matrix,
                    color_threshold=1,
                    p=len(dist_mat),
                    truncate_mode='lastp',
                    show_leaf_counts=show_leaf_counts #,
                    #max_d = T_THRESHOLD
                    )
        plt.title("Dendrogram of %d Cells Similarity, %d Alpha, Clones %d" % (len(dist_mat),K_CLONES))
        plt.xlabel('Cell Number')
        plt.ylim(ymin=-0.1)
        plt.ylabel('Distance: p-norm, p = %s' % P_NORM_VAL)
        plt.savefig('plot_%dcells_%dbins_%dclones_pois.eps' % (M_CELLS,N_BINS,K_CLONES))
        plt.clf()


    k = NUM_CLUST
    if DISTANCE_MEASURE == 'distance':
        choice = 'distance'
    elif DISTANCE_MEASURE == 'maxclust':
        choice = 'maxclust'
    else:
        print "ERROR: 'maxclust' (max number of clusters) or 'distance' (maximum distance (p-norm)) "
    
    k_clus = fcluster(linkage_matrix, k, criterion=choice) # maxclust, distance
    #print "k_clus", k_clus
    return k_clus

# Create a 'fancy' dendrogram
def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)
    ddata = dendrogram(*args, **kwargs)
    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

# Assign the hard CNP given k (using cells jointly)
def assign_hard_CNP(k_cl,S_cells):
    n_clust = []
   # print "k_clusters", k_cl
    for a in range(min(k_cl),max(k_cl)+1):
        #print "cluster ", a
        n_clust.append([i for i, j in enumerate(k_cl) if j == a])
        #print " cells ",n_clust
   
    hard_CNP = {}
    for clu in n_clust:
        cluster_CNP=[]
        #print "current to cluster: ", clu
        for i in range(0,N_BINS):
            #print "BIN: ", i
            old_prob = 0.0 # initial probability for f* (argmax of hard CN for each bin for each cell for each cluster)
            new_prob_l = [] # list of numbers to multiply
            for j in range(0,H_CN_BOUND+1):
                #print " \nCN: ", j
                for cc in clu:
                    #print "cell:", cc
                    #print S_cells[cc][i][j]
                    new_prob_l.append(S_cells[cc][i][j])
                new_prob = reduce(lambda x, y: x*y, new_prob_l)
                if new_prob>=old_prob:
                    #print "yes - higher probability: ", new_prob
                    old_prob = new_prob
                    max_CN = j 
                    #print "coordinates: ", i,j
                new_prob_l = []
            if old_prob == 0.0:
                cluster_CNP.append(8)
            else:
                 cluster_CNP.append(max_CN)   
        for ccc in clu:
            hard_CNP[ccc] = cluster_CNP
    return hard_CNP, n_clust

# Return the number of cells where the profiles are different
def compare_to_truth(infer_cells, clone_mat, cell_assign):
    truth = {}
    for idx,i in enumerate(cell_assign):
        truth[idx]=clone_mat[i]

    #print "\ncell_ass:", cell_assign
    #print "clone_m", clone_mat
    #print "infer: ", infer_cells
    #print "truth:", truth

    # Comparing CNPs
    #diffs = dict_diff(truth, infer_cells)
    #print diffs
    #sys.stderr.write("\nDifference: "+str(diffs)+"\n")
    
    return truth

# Helper: Return the number of cells where the profiles are different
def dict_diff(truth, inferred):
    diff = dict()
    diff['truth'] = set(truth) - set(inferred)
    diff['inferred'] = set(inferred) - set(truth)
    diff['difference'] = {k for k in set(truth) & set(inferred) if truth[k]!=inferred[k]}
    return diff

# Calculate the distance between two CNPs
def manhattan_distance(x,y):
    return sum(abs(a-b) for a,b in zip(x,y))

# Calculate normalized manhattan distance measure between all inferred CNPs and the true CNPs
def create_heatmaps(inf,tru):
    assert len(inf)==len(tru)# make sure same number of cells
    assert len(inf[0])==len(tru[0])# make sure same number of bins
    full_manh = 0
    for i in range(0,len(inf)):
        full_manh+=manhattan_distance(inf[i],tru[i])
    L1_final_dist = full_manh/(len(inf)*len(inf[0])) # manhattan distance / (m * n)
    return L1_final_dist

def get_max(S_matrix):
    max_dict={}
    for cell,bins in S_matrix.items():
        #print "cell ", cell
        max_dict[cell]={}
        l = []
        for b in bins:
            #print "\nb", b
            b_max_val= max(b,key=b.get)
            #print "b_max_val", b_max_val
            l.append(b_max_val)
        max_dict[cell] = l
    return max_dict

def common_print_statements(M_CELLS, N_BINS, K_CLONES, NUM_CLUST, H_CN_BOUND, CHR, chr_num_reads):
    sys.stderr.write("Cells "+str(M_CELLS)+"\n")
    sys.stderr.write("Bins "+str(N_BINS)+"\n")
    sys.stderr.write("Clones (one is diplod): "+str(K_CLONES)+"\n")
    sys.stderr.write("NUM_CLUST: "+str(NUM_CLUST)+"\n")
    sys.stderr.write("CN Bound: "+str(H_CN_BOUND)+"\n")
    sys.stderr.write("Chromosome chosen: "+str(CHR)+"\n")
    sys.stderr.write("Indexing starts at 0 for cell assignment.\n")
    sys.stderr.write("Avg num reads "+ str(chr_num_reads)+"\n") #eg: 51115.38 for chrom 21

def print_clone_cell_info(clone_matri, geno_lengt, assign_cell, CHR, cell_chr_tot_num_read):    
    sys.stderr.write("Clones: \n")
    for i in clone_matri:
        sys.stderr.write(str(i) +"\n")
    sys.stderr.write("geno_length: "+str(geno_lengt)+"\n")
    sys.stderr.write("Assign: "+str(assign_cell)+"\n")
    sys.stderr.write("chr "+str(CHR)+" num reads for each cell: "+ str(cell_chr_tot_num_reads)+"\n")

def fixed_print_statements(clone_matrix, geno_length, assign_cells, cell_chr_tot_num_reads, read_matrix, mu_e_emis_matrix):
    sys.stderr.write("fixed clone_matrix "+str(type(clone_matrix))+"\n")
    for i in clone_matrix:
        sys.stderr.write(str(i)+"\n")
    sys.stderr.write("fixed geno_length: "+str(geno_length)+"\n")
    sys.stderr.write("fixed assign cells: "+str(assign_cells)+"\n")
    sys.stderr.write("fixed sampling cell size: "+str(cell_chr_tot_num_reads)+"\n")
    sys.stderr.write("fixed NB noise added reads:\n")
    for i in read_matrix:
        sys.stderr.write(str(i)+"\n")
    sys.stderr.write("fixed mean RD matrix:\n")
    for k,v in mu_e_emis_matrix.items():
        sys.stderr.write(str(k)+" "+str(v)+"\n")

def plot_cnp(clos):
    ind = range(1,N_BINS+1)

    for idx, i in enumerate(clos):
        cl = i
        plt.scatter(ind,cl)
        plt.plot(ind,cl)
        #plt.title("Clone %d CNP: %d Cells, %d Clones, %d Bins" % (idx,cells_cl,clones_cl,N_BINS))
        plt.xlabel('Bin')
        plt.ylabel('Integer Copy-Number')
        #plt.savefig(OUTDIR+'/cnp_clone%d_of%dclones_%dcells_%dbins.pdf' % (idx,clones_cl,cells_cl,N_BINS))
        plt.show()
        plt.clf()
    #return alpha_cl, cells_cl, clones_cl, N_BINS

def final_print_statements(M_CELLS, K_CLONES, S, cell_groups, clone_matrix, distance_matrix, indep_hard_CNP, joint_hard_CNPs, true_hard_CNPs, geno_length, assign_cells,cur_heat_map):
    # PRINT TO STANDARD ERROR
    #sys.stderr.write("Soft Copy Number Profiles\n")
    #for k,v in S.items():
    #    sys.stderr.write(str(k)+"\n")
    #    for val in v:
    #        sys.stderr.write(str(val)+"\n")
    sys.stderr.write("Clusters: \n"+str(cell_groups)+"\n")
    #sys.stderr.write("Clones: \n")
    #for i in clone_matrix:
    #    sys.stderr.write(str(i))
    #sys.stderr.write("Distance Matrix:\n"+str(distance_matrix)+"\n")
    sys.stderr.write("Cell Groups "+str(cell_groups)+"\n")
    sys.stderr.write("Number of cell groups: "+str(len(cell_groups))+"\n")

    # PRINT TO STANDARD OUT
    file_out = "{}m-{}k-{}n-pois.txt".format(M_CELLS, K_CLONES, N_BINS)
    with open(file_out, "w") as out:
        out.write(("m "+str(M_CELLS)+" k "+str(K_CLONES)+"\n"))
        out.write(("Clones: "+str(clone_matrix)+"\n"))
        out.write(("geno_length "+str(geno_length)+"\n"))
        out.write(("assign "+str(assign_cells)+"\n"))
        out.write(("Indep_Hard_CNPs:"+str(indep_hard_CNP)+"\n"))
        out.write(("Joint_Hard_CNPs:"+str(joint_hard_CNPs)+"\n"))
        out.write(("True_CNPs:"+str(true_hard_CNPs)+"\n"))
        out.write(("Soft_CNPs:"+str(S)+"\n"))
        out.write(("Current Heat Map"+str(cur_heat_map)+"\n"))

   # print "Indep_Hard_CNPs:", indep_hard_CNP
   # print "Joint_Hard_CNPs:", joint_hard_CNPs
   # print "True_CNPs:", true_hard_CNPs
   # print "Soft_CNPs:", S

def pois_straight(k,mu):
    return math.exp(-mu)*(mu**k)/(math.factorial(k))

###########################
# USAGE
if (len(sys.argv) < 2):
    sys.stderr.write("Usage: " + sys.argv[0] + " One of three: \n'iterate' -> <'iterate'> GO INTO SCRIPT AND CHANGE \n'random'-> <'random'> <M_CELLS> <N_BINS> <K_CLONES> <NUM_CLUST> \n'file' -> 'file' with each line-> <M_CELLS> <N_BINS> <K_CLONES> <NUM_CLUST> <clone_matrix> <geno_length> <assign_cells> <cell_chr_tot_num_reads> <read_matrix> <mu_e_emis_matrix>\n")
    sys.exit(1)
elif sys.argv[1] == 'iterate':
    sys.stderr.write('Iterate - arguments should be specified in code\n')
    sys.exit(1)
elif (len(sys.argv) < 5): # should have at least three arguments
    sys.stderr.write("Usage: " + sys.argv[0] + " One of three: \n'iterate' -> <'iterate'> GO INTO SCRIPT AND CHANGE \n'random'-> <'random'> <M_CELLS> <N_BINS> <K_CLONES> <NUM_CLUST> \n'file' -> 'file' with each line-> <M_CELLS> <N_BINS> <K_CLONES> <NUM_CLUST> <clone_matrix> <geno_length> <assign_cells> <cell_chr_tot_num_reads> <read_matrix> <mu_e_emis_matrix>\n")
    sys.exit(1)

# ASSIGNMENTS
EXAMPLE = str(sys.argv[1]) # either 'random' or 'file'
PLOT = 'no'
H_CN_BOUND = 5
CHR = 21 #int(sys.argv[4])
P_NORM_VAL = 2 #int(sys.argv[5])
DISTANCE_MEASURE = 'maxclust' #str(sys.argv[8])
#MARG_POSTERIOR_THRESHOLD = float(sys.argv[6]) # minimum marginal posterior prob allowed

###########################
# MAIN
if __name__ == '__main__':
    #Poisson testing
    #p_straight = pois_straight(100,2.6)
    #p_func = pois.pmf(100,2.6)
    #print "p_straight ", p_straight
    #print "python function ", p_func
    # sys.exit()

    # Initialize RD matrix and genome length
    read_matrix = [] # matrix of read depth. 
    geno_length = [] # num of copies of each cell's genome (chromosome in this case) for normalization

    # Get genome length and chromosome (note ind 23 = X and 24 = Y)
    chrom_ID_percentage = [8,7.9,6.5,6.2,5.9,5.5,5.2,4.7,4.6,4.4,4.4,4.3,3.7,3.5,3.3,2.9,2.6,2.5,2.1,2,1.5,1.6,5,1.9]
    chr_chosen_perc_reads = chrom_ID_percentage[CHR-1]
    
    # Transition Matrix - used to create instances and in FB algorithm 

    # Permitting jump to second or third from current CN state
    # a_trans_matrix = { 
    #   0 : {0: 0.9, 1: 0.08, 2: 0.001, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   1 : {0: 0.045, 1: 0.9, 2: 0.045, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   2 : {0: 0.005, 1: 0.04, 2: 0.9, 3: 0.04, 4: 0.005, 5: 0.0, 'E': 0.01},
    #   3 : {0: 0.0, 1: 0.005, 2: 0.04, 3: 0.9, 4: 0.04, 5: 0.005, 'E': 0.01},
    #   4 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.045, 4: 0.9, 5: 0.045, 'E': 0.01},
    #   5 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.001, 4: 0.08, 5: 0.9, 'E': 0.01},
    #   }

    # Most drastic
    # a_trans_matrix = { 
    #   0 : {0: 0.89, 1: 0.1, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   1 : {0: 0.05, 1: 0.89, 2: 0.05, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   2 : {0: 0.0, 1: 0.05, 2: 0.89, 3: 0.05, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   3 : {0: 0.0, 1: 0.0, 2: 0.05, 3: 0.89, 4: 0.05, 5: 0.0, 'E': 0.01},
    #   4 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.05, 4: 0.89, 5: 0.05, 'E': 0.01},
    #   5 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.1, 5: 0.89, 'E': 0.01},
    #   }

    # Second Most Drastic
    # a_trans_matrix = { 
    #   0 : {0: 0.79, 1: 0.1, 2: 0.1, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   1 : {0: 0.1, 1: 0.79, 2: 0.1, 3: 0.0, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   2 : {0: 0.0, 1: 0.1, 2: 0.79, 3: 0.1, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   3 : {0: 0.0, 1: 0.0, 2: 0.1, 3: 0.79, 4: 0.1, 5: 0.0, 'E': 0.01},
    #   4 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.1, 4: 0.79, 5: 0.1, 'E': 0.01},
    #   5 : {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.1, 4: 0.1, 5: 0.79, 'E': 0.01},
    #   }
    
    # a_trans_matrix = { 
    #   0 : {0: 0.69, 1: 0.15, 2: 0.1, 3: 0.05, 4: 0.0, 5: 0.0, 'E': 0.01},
    #   1 : {0: 0.1, 1: 0.69, 2: 0.1, 3: 0.05, 4: 0.05, 5: 0.0, 'E': 0.01},
    #   2 : {0: 0.05, 1: 0.1, 2: 0.69, 3: 0.1, 4: 0.05, 5: 0.0, 'E': 0.01},
    #   3 : {0: 0.0, 1: 0.05, 2: 0.1, 3: 0.69, 4: 0.1, 5: 0.05, 'E': 0.01},
    #   4 : {0: 0.0, 1: 0.05, 2: 0.05, 3: 0.1, 4: 0.69, 5: 0.1, 'E': 0.01},
    #   5 : {0: 0.0, 1: 0.0, 2: 0.05, 3: 0.1, 4: 0.15, 5: 0.69, 'E': 0.01},
    #   }

    # Least drastic
    a_trans_matrix = { 
      0 : {0: 0.39, 1: 0.2, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      1 : {0: 0.2, 1: 0.39, 2: 0.2, 3: 0.1, 4: 0.1, 5: 0.0, 'E': 0.01},
      2 : {0: 0.1, 1: 0.2, 2: 0.39, 3: 0.2, 4: 0.075, 5: 0.025, 'E': 0.01},
      3 : {0: 0.025, 1: 0.075, 2: 0.2, 3: 0.39, 4: 0.2, 5: 0.1, 'E': 0.01},
      4 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.39, 5: 0.2, 'E': 0.01},
      5 : {0: 0.0, 1: 0.1, 2: 0.1, 3: 0.2, 4: 0.2, 5: 0.39, 'E': 0.01},
      }

################################################
# SPECIFY INPUTS    

    if EXAMPLE == 'random':
        #alpha_heatmap_dict = {}

        # Main parameters
        M_CELLS = int(sys.argv[3])
        N_BINS = int(sys.argv[4])
        K_CLONES = int(sys.argv[5])
        NUM_CLUST = float(sys.argv[6])
        
        # Check number of cells and number of bins
        if M_CELLS <= 2:
            sys.stderr.write("Enter num. cells greater than 2\n")
            sys.exit(1)

        if N_BINS <= 5:
            sys.stderr.write("Enter at least 6 bins\n")
            sys.exit(1)
        
        #sys.stderr.write("a "+str(ALPHA)+"\n")
        #sys.stderr.write("m "+str(M_CELLS)+"\n")
        #sys.stderr.write("k "+str(K_CLONES)+"\n")        

#        for a in num_alphas:#range(80,110,20):
        heat_map_dict = {}
        # Total number of reads
        chr_num_reads = 0.1 * 50000 * N_BINS 
        #chr_num_reads = 28.6585 * 50000 * N_BINS #100
        # Create clones and assign cells
        clone_matrix, assign_cells = create_clones_assign_cells(M_CELLS, N_BINS, K_CLONES, a_trans_matrix)
        sys.stderr.write("Finished clone matrix and assigning cells: M_Cells, N_Bins K_Clones "+str(M_CELLS)+" "+str(N_BINS)+" "+str(K_CLONES)+"\n")
        #plot_cnp(clone_matrix)
        
        # Get the length of the genomes
        [geno_length.append(sum(i)) for i in clone_matrix]
        # Take m draws from the Poisson, one for each cell, to determine number of reads for that chromosome
        cell_chr_tot_num_reads = np.random.poisson(chr_num_reads, M_CELLS)
        # Mean Values for Emission Probabilities 
        mu_e_emis_matrix = create_mu_emission(cell_chr_tot_num_reads, geno_length, assign_cells, M_CELLS, N_BINS, H_CN_BOUND+1)
        sys.stderr.write("mu emission matrix finished M_Cells, N_Bins K_Clones "+ str(M_CELLS)+" "+str(N_BINS)+" "+str(K_CLONES)+"\n")

        # Get proportion of reads in each bin for each cell
        for idx,i in enumerate(assign_cells):
            #print "\ncell : ", idx, " clone: ", i, "d",cell_chr_tot_num_reads[idx],"/","pi", geno_length[i]
            e = cell_chr_tot_num_reads[idx]/geno_length[i]
            #print "if CN is 1 (e): ",e
            to_add = []
            for j in clone_matrix[i]: # copy number 
                #print "      j: ", j
                #print "prop_reads ->", "e*j", e*j
                prop_reads = e*j
                # Add NB noise
                if prop_reads == 0.0:
                    reads = np.random.poisson(0)
                    #random_sample_NB('1', ALPHA)
                    #print "CN 0 nb distr: reads ", reads
                    to_add.append(reads)
                else:
                    reads = np.random.poisson(prop_reads)
                    #reads = random_sample_NB(str(prop_reads), ALPHA)
                    #print "CN >0 nb distr: reads ", reads
                    to_add.append(reads)
            read_matrix.append(to_add) # add proportion of reads in each bin for every cell
        sys.stderr.write("Finished read matrix M_Cells, N_Bins K_Clones "+str(M_CELLS)+" "+str(N_BINS)+" "+str(K_CLONES)+"\n")
        # Print Statements
        # Common Print Statements
        common_print_statements(M_CELLS, N_BINS, K_CLONES, NUM_CLUST, H_CN_BOUND, CHR, chr_num_reads)
        #  Processed Clone and Cell Information Print Statements
        print_clone_cell_info(clone_matrix, geno_length, assign_cells, CHR, cell_chr_tot_num_reads)
        # 'Random' Print Statements
        sys.stderr.write("read matrix: "+str(read_matrix)+"\n")
        sys.stderr.write("expected num reads "+str(mu_e_emis_matrix)+"\n")

        # Run forward backward algorithm for each cell
        S = run_FB_each_cell(read_matrix, a_trans_matrix, mu_e_emis_matrix)

        # Get Hard CNP from considering cells independently
        indep_hard_CNPs = get_max(S)

        # Compute Distance Matrix, Cluster, and Produce Truth Matrix for all cells
        distance_matrix = create_dist_matrix(S, P_NORM_VAL)
        k_clusters = create_dendrogram(distance_matrix)
        joint_hard_CNPs, cell_groups = assign_hard_CNP(k_clusters,S)
        true_hard_CNPs = compare_to_truth(joint_hard_CNPs, clone_matrix, assign_cells)
        norm_manhat_dist = create_heatmaps(joint_hard_CNPs,true_hard_CNPs)
        heat_map_dict[(M_CELLS,K_CLONES)]= norm_manhat_dist
        #alpha_heatmap_dict[ALPHA]= heat_map_dict

        # PRINT TO STANDARD ERROR 
        final_print_statements(M_CELLS, K_CLONES, S, cell_groups, clone_matrix, distance_matrix, indep_hard_CNPs, joint_hard_CNPs, true_hard_CNPs, geno_length, assign_cells,heat_map_dict)
        #print "So far alpha Heat Map Dictionary", alpha_heatmap_dict

#################################################################
    elif EXAMPLE == 'file':

        # Get all information from file
        IN_FILE = sys.argv[2]
        M_CELLS, N_BINS, K_CLONES, NUM_CLUST, clone_matrix, geno_length, cell_chr_tot_num_reads, read_matrix, mu_e_emis_matrix, assign_cells, chr_num_reads = read_file(IN_FILE)
        
        # Check number of cells and number of bins
        if M_CELLS <= 2:
            sys.stderr.write("Enter num. cells greater than 2\n")
            sys.exit(1)
        
        if N_BINS <= 5:
            sys.stderr.write("Enter at least 6 bins\n")
            sys.exit(1)

        # Common Print Statements
        common_print_statements(M_CELLS, N_BINS, K_CLONES, NUM_CLUST, H_CN_BOUND, CHR, chr_num_reads)
        # 'File' Print Statements
        fixed_print_statements(clone_matrix, geno_length, assign_cells, cell_chr_tot_num_reads, read_matrix, mu_e_emis_matrix)

        # Run forward backward algorithm for each cell
        S = run_FB_each_cell(read_matrix, a_trans_matrix, mu_e_emis_matrix)

        # Get Hard CNP from considering cells independently
        indep_hard_CNPs = get_max(S)

        # Compute Distance Matrix, Cluster, and Produce Truth Matrix for all cells
        distance_matrix = create_dist_matrix(S, P_NORM_VAL)
        k_clusters = create_dendrogram(distance_matrix)
        joint_hard_CNPs, cell_groups = assign_hard_CNP(k_clusters,S)
        true_hard_CNPs = compare_to_truth(joint_hard_CNPs, clone_matrix, assign_cells)

        # PRINT TO STANDARD ERROR
        final_print_statements(S, cell_groups, clone_matrix, distance_matrix, indep_hard_CNPs, joint_hard_CNPs, true_hard_CNPs)

#################################################################
    elif EXAMPLE == 'iterate':
        #alpha_heatmap_dict = {}

        iterator = 0
        # num_alphas = [4, 6, 11, 30, 50, 110] #range(4,110,7)#range(6,120,20)
        # num_cells = range(10,41,5)
        # num_clones = range(3,11)
        a = 0
        num_cells = range(3,5)
        num_clones = range(3,5)
        N_BINS = int(10) #100
        chr_num_reads = 28.6585 * 50000 * N_BINS #100

        heat_map_dict = {}
        for m in num_cells: #range(3,41)
            for k in num_clones: # range(3,8)
                read_matrix = [] # matrix of read depth. 
                geno_length = [] # num of copies of each cell's genome (chromosome in this case) for normalization
                
                iterator+=1
                print "a ", a, "m ", m, "k ", k
                sys.stderr.write("m "+str(m)+"\n")
                sys.stderr.write("k "+str(k)+"\n")
                ALPHA = str(a) # SET TO 0 HERE FOR POISSON
                M_CELLS = int(m)
                K_CLONES = int(k)
                NUM_CLUST = float(k)

                # Create clone matrix
                clone_matrix, assign_cells = create_clones_assign_cells(M_CELLS, N_BINS, K_CLONES, a_trans_matrix)
                # Get Genome Lengths
                [geno_length.append(sum(i)) for i in clone_matrix]
                # m draws from the Poisson, one for each cell, to determine number of reads for that chromosome
                cell_chr_tot_num_reads = np.random.poisson(chr_num_reads, M_CELLS)
                # Mean Values for Emission Probabilities 
                mu_e_emis_matrix = create_mu_emission(cell_chr_tot_num_reads, geno_length, assign_cells, M_CELLS, N_BINS, H_CN_BOUND+1)
                
                # Print Statements
                # 'Iterate' Print Statements
                sys.stderr.write("\nROUND "+str(iterator)+" of "+str(len(num_clones)*len(num_cells))+"\n")
                # Common Print Statements
                common_print_statements(M_CELLS, N_BINS, K_CLONES, NUM_CLUST, H_CN_BOUND, CHR, chr_num_reads)
                # Processed Clone and Cell Related Information Print Statements
                print_clone_cell_info(clone_matrix, geno_length, assign_cells, CHR, cell_chr_tot_num_reads)

                # Proportion of reads in each bin for each cell
                for idx,i in enumerate(assign_cells):
                    #print "\ncell : ", idx, " clone: ", i, "d",cell_chr_tot_num_reads[idx],"/","pi", geno_length[i]
                    e = cell_chr_tot_num_reads[idx]/geno_length[i]
                    #print "if CN is 1 (e): ",e
                    to_add = []

                    for j in clone_matrix[i]: # copy number 
                        #print "      j: ", j
                        #print "prop_reads ->", "e*j", e*j
                        prop_reads = e*j
                        # Add NB noise
                        if prop_reads == 0.0:
                            reads = np.random.poisson(1)
                            #print "CN 0 nb distr: reads ", reads
                            to_add.append(reads)
                        else:
                            reads = np.random.poisson(prop_reads)
                            #print "CN >0 nb distr: reads ", reads
                            to_add.append(reads) # sample from negative binomial with p = .5
                    read_matrix.append(to_add) # add proportion of reads in each bin for every cell

                sys.stderr.write("read matrix: "+str(read_matrix)+"\n")
                sys.stderr.write("expected num reads "+str(mu_e_emis_matrix)+"\n")
                
                # Run forward backward algorithm for each cell
                S = run_FB_each_cell(read_matrix, a_trans_matrix, mu_e_emis_matrix)

                # Get Hard CNP from considering cells independently
                indep_hard_CNPs = get_max(S)

                # Compute Distance Matrix, Cluster, and Produce Truth Matrix for all cells
                distance_matrix = create_dist_matrix(S, P_NORM_VAL)
                k_clusters = create_dendrogram(distance_matrix)
                joint_hard_CNPs, cell_groups = assign_hard_CNP(k_clusters,S)
                true_hard_CNPs = compare_to_truth(joint_hard_CNPs, clone_matrix, assign_cells)

                # Create normalized heatmap dictionary
                norm_manhat_dist = create_heatmaps(joint_hard_CNPs,true_hard_CNPs)
                heat_map_dict[(m,k)]= norm_manhat_dist

                final_print_statements(S, cell_groups, clone_matrix, distance_matrix, indep_hard_CNPs, joint_hard_CNPs, true_hard_CNPs, heat_map_dict)
                # 'Iterate' Print Statement
                print "Current Heat Map Dict", heat_map_dict
        sys.exit()

    else:
        sys.stderr.write("ERROR, enter 'random' or 'fixed' or 'iterate' to create instance\n")
        sys.exit(1)
