#!/bin/bash

# Usage
if [ ${#@} == 0 ]; then
#if [ $# -ne 1 ]; then
    echo $0: Usage: ./run_all_read_depth_plot_scripts.sh DATA_DIR OUTPUT_DIR BIN_SIZE READ_LEN CELL_BAM_NAME
    exit 1
fi

#Usage: /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py <SORTED_BAM_FILE_DIR (with .bai index files)> <CHR_START_END_FILE_NAME> <OUTPUT_DIR> <BIN_SIZE> <READ_LEN> <CELL_BAM_NAME> <LIST_CELLS_NOT_TO_CLUSTER (e.g. cell1,cell3 or NA> <IF_YOU_WANT_PLOT_RD_FOR_EACH_CELL(0 or 1)> <COUNTS_OR_DEPTH ('rawcounts' OR 'rawcountsnormalized' OR 'depth' OR 'depthnormalized')>

python /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py $1/cell0/ /n/fs/ragr-data/projects/scDNA/scripts/SRR3081691_1.fastq.gz.bam.sorted.bam_use_for_chrom_len.txt $2 $3 $4 cell0$5 NA 1 rawcounts &
python /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py $1/cell1/ /n/fs/ragr-data/projects/scDNA/scripts/SRR3081691_1.fastq.gz.bam.sorted.bam_use_for_chrom_len.txt $2 $3 $4 cell1$5 NA 1 rawcounts &
python /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py $1/cell2/ /n/fs/ragr-data/projects/scDNA/scripts/SRR3081691_1.fastq.gz.bam.sorted.bam_use_for_chrom_len.txt $2 $3 $4 cell2$5 NA 1 rawcounts &
python /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py $1/cell3/ /n/fs/ragr-data/projects/scDNA/scripts/SRR3081691_1.fastq.gz.bam.sorted.bam_use_for_chrom_len.txt $2 $3 $4 cell3$5 NA 1 rawcounts &
python /n/fs/ragr-data/projects/scDNA/scripts/read_depth_plot.py $1/cell4/ /n/fs/ragr-data/projects/scDNA/scripts/SRR3081691_1.fastq.gz.bam.sorted.bam_use_for_chrom_len.txt $2 $3 $4 cell4$5 NA 1 rawcounts &
wait

