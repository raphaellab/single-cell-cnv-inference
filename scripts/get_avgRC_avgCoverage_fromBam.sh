#!/bin/bash
# Generate certain number of cells at .1X using DownsampleSam
# Last Modified: Sept 14, 2017
# Ashley Conard

if [ ${#@} == 0 ]; then
        echo $0: Usage: ./get_avgRC_avgCoverage_fromBam.sh FULL_INPUT_DIR_ONLY_BAM_N_BAI ">" OUTPUT.csv
        exit 1
fi

echo NAME,AVG_COV,STD,NUM_READS
for i in $1/*.bam
do	
	#num_reads=$"(python get_num_mapped_reads.py $i)"
	num_reads=`python /n/fs/ragr-data/projects/scDNA/scripts/get_num_mapped_reads.py $i`	
	avgCov="$(samtools depth $i | awk '{sum+=$3; sumsq+=$3*$3} END { print sum/NR }')"
	std="$(samtools depth $i | awk '{sum+=$3; sumsq+=$3*$3} END { print sqrt(sumsq/NR - (sum/NR)**2)}')"
	base=$(basename "$i")
	echo $base,$avgCov,$std,$num_reads
done
