#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import csv
import pysam as pys
from subprocess import Popen, PIPE
import pandas as pd
from time import sleep
from multiprocessing import Pool
from multiprocessing import Process
from random import randint

# Ashley Mae
# Last Modified Sept. 6, 2017
#####
#
# downsample_from_sam.py
#
#####
def get_sam_input(file):
    indx = 0
    dict_sam = {}
    with open(file,'r') as f:
	for line in f:
		if line.startswith("@"):
                    sys.stderr("header detected")
                elif not line.startswith("@"):# skip header 
                    indx+=1
                    dict_sam[indx] = line.replace("\n",'')
    return dict_sam
               
# USAGE
if len(sys.argv) != 5:
    sys.stderr.write("Usage: " + sys.argv[0] + " <BAM_or_SAM_FILE (sorted or not)> <NUM_UNIF_RANDOM_SAMPLED_DATASETS> <FRAC_TO_SAMPLE (e.g. .1, i.e. what fraction of file to sample> <OUTPUT_DIR> \n")
    sys.exit(1)

# ASSIGNMENT
SAM_BAM_FILE = sys.argv[1] 
NUM_RAND_SAMP = int(sys.argv[2])
FRAC_SAMPLE = float(sys.argv[3])
OUTDIR = sys.argv[4]

if __name__ == '__main__':
    # Check to see what kind of file it is (bam or sam)
    bam_sam_file = SAM_BAM_FILE.split("_")
    if 'bam' in bam_sam_file[-1]:
        print "Processing bam file"
        bam_sam = 1
    elif 'sam' in bam_sam_file[-1]:
        print "Processing sam file"
        bam_sam = 0
    else:
        sys.exit("ERROR - must input .sam or .bam file")

    # Process the bam or sam file
    if bam_sam: # processing bam file
        print "TO DO"

    else: # processing sam file
        sam_dict = get_sam_input(SAM_BAM_FILE)

    # Create a set of sample datasets. For each dataset, uniformly at random (w/out replacement) choose a fraction of the lines. 
    for i in range(NUM_RAND_SAMP):
        
        # Choose set of random numbers
        num_lines_sampling = len(sam_dict)*FRAC_SAMPLE
        print i,"Sample: getting ", num_lines_sampling, " from ", len(sam_dict), " total lines."
        list_rand_indices = np.random.choice(range(1,len(sam_dict)), int(num_lines_sampling), replace=False) # without replacement
        print "list_rand_indices: ", list_rand_indices

        # Print all values to file
        orig_stdout = sys.stdout
        OUT_FILE_NAME = "sample"+str(i)+"_"+str(FRAC_SAMPLE)+"frac__downsample_from_sam.sam"

        f = open(OUT_FILE_NAME, 'w')
        sys.stdout = f
        for v in list_rand_indices:
            print sam_dict[v]
        sys.stdout = orig_stdout
        f.close()