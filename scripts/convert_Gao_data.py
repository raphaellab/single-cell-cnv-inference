#!/usr/bin/python
#
# convert_Gao_data.py : takes as input the .txt files for each patient, produces a pandas dataframe, and produces two CSV
#	
# Ashley Conard

from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import pandas as pd
import os
import numpy as np
from pandas import DataFrame
import warnings

###########################
# USAGE
if len(sys.argv) != 3:
  sys.stderr.write("Usage: " + sys.argv[0] + " GAO_DATA.txt(files from Dr.Gao) PDF_FILE_OUTPUTNAME\n")
  sys.exit(1)

###########################
# ASSIGNMENTS
FILE = str(sys.argv[1])
CELL_NAME = str(sys.argv[2])

###########################

def copy_num_known_ratio(x):
	return 2**(x+1)
def copy_num_ratio_unknown(x):
	print 2**x
	return 2**x

if __name__ == '__main__':
	df = pd.read_csv(FILE,sep='\t').astype(float)
	df[:3].applymap(copy_num_ratio_unknown)
#	df.GINKGO.plot(style='g-', label='Ginkgo')
#	df.ANEUF.plot(style='b-', label='AneuFinder')

	#df.ANEUF.plot(style=['o', 'blue'], label='AneuFinder', markersize=5)
	#df.GINKGO.plot(style=['o','green'], label='Ginkgo', markersize=5)
	plt.legend(loc='upper right')
	plt.ylim(0, 10)
	plt.title("%s Copy Number Profiles" % (CELL_NAME))
	plt.xlabel('Bin')
	plt.ylabel('Integer Copy-Number')
	plt.savefig('%s_copy-number-profiles_points.pdf' % (CELL_NAME))
	plt.show()