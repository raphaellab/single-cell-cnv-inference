#!/usr/bin/python
from __future__ import division
import sys,os,itertools, argparse
import matplotlib
import matplotlib.pyplot as plt 
from os import listdir
from os.path import isfile, join
import collections as c
import numpy as np
import pysam as pys
import seaborn as sns
import pandas as pd

# Ashley Mae
# Last Modified Sept 6, 2017
#####
#
# plot_cov-rc-std_by_fracDipBins.py
#
####

# USAGE
if len(sys.argv)!=2:
   sys.exit("Usage: python "+ sys.argv[0] + " DATA_DIR/ (with .csv files with NUM_READS and NORM_X-SOMY_BINS information)")

# ARGUMENTS
DATA_DIR = sys.argv[1]
idx=-1
if __name__ == '__main__':
    co=['red', 'orange', 'green', 'blue','purple']
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and '__bin_bed_or_tsvALL.csv' in f]
    #print "files: ", onlyfiles
    for i in onlyfiles:
        idx+=1
        kb=i.split("_")[0] # get name of the file by bin size
        df = pd.read_csv(DATA_DIR+'/'+i)
        sns.set_style("darkgrid")
        sns.regplot(x=df['NORM_X-SOMY_BINS'], y=df['NUM_READS'], data=df, color=co[idx]) # marker=['o','v','s','x','b'],
        print "Processing ", i
        plt.xticks(rotation=70)
        plt.xlim(0,1)
        plt.ylim(min(df['NUM_READS']), max(df['NUM_READS'])+10)
        plt.title("Frac. Diploid Bins by Avg Cov for 35 Cells and 2 Merged Cells in Patient T5: %s kb"%(kb))
        #plt.savefig("%skb__plot_cov-rc-std_by_fracDipBins.pdf"%(kb))
        #plt.show()
        if kb=='40':
            plt.savefig("all40kb-10kb_AHMM__plot_cov-rc-std_by_fracDipBins.pdf")
            plt.show()