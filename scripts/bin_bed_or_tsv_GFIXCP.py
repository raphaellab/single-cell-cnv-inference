#!/usr/bin/python
#############################
#
# bin_bed_or_tsv.py: bins .bed or .tsv files
# created by : Ashley Conard
# last updated: Aug. 7, 2017
#
#############################
from __future__ import division
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from pylab import imshow, show
import sys
import gzip
import string
import re
import numpy as np
from matplotlib import colors
import collections as c
import matplotlib.pyplot
from matplotlib.colors import LinearSegmentedColormap
import os
from collections import OrderedDict

def get_cn_gink(chrom, ist, ien):
    #print "chrom: ", chrom
    ist_int = chrom[(chrom.START <= ist) & (ist < chrom.END)].index[0]
    ien_int = chrom[(chrom.START <= ien) & (ien < chrom.END)].index[0]
    #print "ist_int: ", ist_int
    #print "ien_int: ", ien_int

    if ist_int != ien_int:
        ien_int+=1
        interval_chrom = chrom.iloc[ist_int:ien_int,:]
        #print "interval_chrom: ", interval_chrom
        interval_chrom['diff'] = interval_chrom.END - interval_chrom.START # make the difference between the start of the ist and its chrom.END and end of ien and its chrom.START  
        #print "\nmultiple", ist, ien, "\n", interval_chrom 
        #cn = interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]
        #print "\nCN: ", cn
        return string.replace(str(interval_chrom.loc[interval_chrom['diff']== interval_chrom['diff'].max(), 'CN'].iloc[0]), ".0", "")
        
    elif ist_int == ien_int:
        if len(chrom[(chrom.START <= ist) & (ien <= chrom.END)])==1:
            n = chrom[(chrom.START <= ist) & (ien <= chrom.END)]
            #print "in", ist, ien, "\n", n
            return string.replace(str(n.iloc[0].CN),".0","")#n.iloc[0].CN

        else:
            sys.exit("IST_INT == IEN_INT but not in interval...")
    
    else:
        sys.exit("NOT AN OPTION", ist, ien, ist_int, ien_int)

def calc_num_X_somy_bins(out_bin_df, freq_df, index, cell_n):
    #print out_bin_df["CN"]
    state_count = out_bin_df[out_bin_df["CN"] == int(STATE)].count().CN 
    total_bins = out_bin_df.shape[0]

    freq_df.loc[index] = [ID, cell_n, state_count, total_bins, (state_count/total_bins)]
    #print "freq_bf: ", freq_df

###########################
# USAGE
if len(sys.argv) != 7:
  sys.stderr.write("Usage: " + sys.argv[0] + " <0 or 1 (0-ANEUFINDER_INPUT_BED_FILE,  1-GINKGO_INPUT_TSV_FILE_LIST, e.g. '../file1.tsv,../file2.tsv,../file3.tsv')>  <DIR_INPUT_FILE(S)> <KB (e.g. 50000)> <ID (AHMM ADNACOPY GFIXCP GVARCP)>  <X-somy (e.g. 2.0)>  <OUTPUT_FILE_NAME>\n")
  sys.exit(1)
###########################

# ARGUMENTS
ANEU_OR_GINK = int(sys.argv[1])
DATA_DIR_FILE = sys.argv[2]
KB = int(sys.argv[3])
ID = sys.argv[4]
STATE = str(sys.argv[5])
#if ".0" not in STATE:
#      STATE = STATE+".0"
OUTPUT_FILE = str(sys.argv[6])

if __name__ == '__main__':
  # .BED.GZ (ANEUF) PROCESSING
  if ANEU_OR_GINK == 0: 
    sys.exit("This is only processing Ginkgo FIXED data files")
  
  # .TSV (GINKGO) PROCESSING
  elif ANEU_OR_GINK == 1: 
    print "state: ", STATE, type(STATE)
    c_idx = 0 # keep track of the number of cells for the freq_X-somy file.
    freq_X_somy_bins_df_gink = pd.DataFrame(columns=('ID', 'CELL_NAME', 'NUM_X-SOMY_BINS', 'NUM_TOT_BINS', 'NORM_X-SOMY_BINS'))
    
    # Get .tsv into pandas dataframe
    print "Processing .tsv: ", DATA_DIR_FILE
    big_ginkgo_df = pd.read_csv(DATA_DIR_FILE,sep="\t")

    # List of cells
    list_cells = list(big_ginkgo_df)
    print "before list cells ", list_cells
    for i in list_cells:
          if "merged" in i:
                list_cells = [i]
    print "list_cells", list_cells

    for cell_name_g in list_cells:
          c_idx+=1
          # Get cell specific dataframe and change cell_name col to "CN"
          cell_ginkgo_df = big_ginkgo_df[['CHR', 'START', 'END', cell_name_g]] # create dataframe for the cell
          cell_ginkgo_df['ID'] = ID
          cell_ginkgo_df= cell_ginkgo_df.rename(columns = {cell_name_g: 'CN'})
          cell_ginkgo_df['CELL_NAME'] = cell_name_g
          cell_ginkgo_df = cell_ginkgo_df[['CHR', 'START', 'END', 'ID', 'CELL_NAME', 'CN']]

          print "Already Binned cell: ", cell_name_g         
          cell_ginkgo_df.to_csv(OUTPUT_FILE+"_ID_"+ID+"_cell_"+cell_name_g+"_"+str(KB)+"size__bin_bed_or_tsv.csv")
         # print "cell df: ", cell_ginkgo_df 
        
          #output_bin_df = bin_data(cell_ginkgo_df, cell_name_g) # bin current cell
            
          # Calling calc_num_X_somy_bins to get normalized number of X-somy bins for each cell (output is 1 fil for all cells)
          print "\nCalling calc_num_X-somy_bins() for: ", cell_name_g
          calc_num_X_somy_bins(cell_ginkgo_df, freq_X_somy_bins_df_gink, c_idx, cell_name_g) #c_idx used to index the output freq_X-somy_bins_df
    
    freq_X_somy_bins_df_gink.to_csv(OUTPUT_FILE+"_ID_"+ID+"_"+str(KB)+"size_"+"frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv")
    print "Saved Ginkgo freq. X-somy .csv file as: ", OUTPUT_FILE+"_ID_"+ID+"_"+str(KB)+"size_"+"frequency_"+STATE+"-somy_bins__bin_bed_or_tsv.csv"
          
  else:
        sys.exit("MUST ENTER 0 or 1 (0-ANEUFINDER_INPUT_BED_FILE,  1-GINKGO_INPUT_TSV_FILE_LIST, e.g. '../file1.tsv,../file2.tsv,../file3.tsv') ")
