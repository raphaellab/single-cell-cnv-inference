#!/usr/bin/python
#
# make_one_violin.py : Creates one merged set of violin plots
#	
# Ashley Conard

import seaborn as sns
import matplotlib.pyplot as plt
import sys
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import os
 
def violin_plots(data_f,M_CELLS):
	sns.set_style("whitegrid")
	sns.plt.title("Difference Between Joint and Independent Methods: "+str(M_CELLS)+" Cells, 400 Bins ")
	plt.xlabel('Clones')
	sns.plt.ylabel('Averaged Manhattan Distance')
	#sns.plt.ylim(0, 0.2)
	#sns.plt.yticks(0.0, 0.2, 0.01)
	ax = sns.violinplot(x="Number Clones",y="Averaged Manhattan Distance",data=data_f,hue="Method",palette="muted",legend=False) #inner="quartile"
	sns.plt.legend(loc='lower right')

	#sns.plt.show()
	sns.plt.savefig('violin_plot_%dm_together.pdf' % (M_CELLS))
	
def read_file(input_file):
	joint_list = []
	indep_list = []
	j_i_list = []
	clones = []
	label = []

	with open(input_file,'r') as f:
		FLIP=True
		for line in f:
			s = line.strip('\n').split('\t')
			print s
			if s[0]== '60':
				if FLIP:
					M_CELLS = int(s[0])
					K_CLONES = int(s[1])
					FLIP = False
					print "F false - M_CELLS ", M_CELLS, "K_CLONES ", K_CLONES
				else:
					j_i_list+=joint_list+indep_list
					label.extend(['Jointly Inferred']*len(joint_list))
					label.extend(['Independently Inferred']*len(indep_list))
					clones.extend([K_CLONES]*len(joint_list+indep_list))
					joint_list = []
					indep_list = []
					print "M_CELLS ", M_CELLS, "K_CLONES ", K_CLONES, j_i_list
					M_CELLS = int(s[0])
					K_CLONES = int(s[1])
			else:
				joint_list.append(float(s[0]))
				indep_list.append(float(s[1]))
	
	# last set (60m and 20k)	
	j_i_list+=joint_list+indep_list
	label.extend(['Jointly Inferred']*len(joint_list))
	label.extend(['Independently Inferred']*len(indep_list))
	clones.extend([K_CLONES]*len(joint_list+indep_list))

	df = pd.DataFrame({'Averaged Manhattan Distance':j_i_list,'Method':label,'Number Clones':clones})
	print(df.to_string())
	#full_list = joint_dist+indep_dist
	#label = (['Jointly Inferred'] *len(joint_dist)) + (['Independently Inferred']*len(indep_dist))			
	#print "label",label
	#print "full_list", full_list

	return df,M_CELLS

###########################
# USAGE
if (len(sys.argv) !=2):
    sys.stderr.write("Usage: " + sys.argv[0] +"<all_together.txt>\n")
    sys.exit(1)

# ASSIGNMENTS
FILE = str(sys.argv[1])
#M_CELLS = int(sys.argv[2])
#K_CLONES = int(sys.argv[3])
#N_BINS = int(sys.argv[4])

if __name__ == '__main__':
	data_frame,M_CELLS = read_file(FILE)
	violin_plots(data_frame,M_CELLS)

