#!/usr/bin/python
#
# plot_ploidy_scatter.py : plot the ploidy calls for AneuFinder vs. Ginkgo in scatter plot. 
# 	input : must be output df csv from compare_methods.py or any df with df['PLOIDY_ANEUF'] and df['PLOIDY_GINKGO']. 
#	output : scatter plot where each dot is a cell's ploidy call between Ginkgo and AneuFinder
#
# Created: Ashley Conard
# Last Update: June 20

from __future__ import division
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import pandas as pd
import os
import numpy as np
from pandas import DataFrame
import warnings

###########################
# USAGE
if len(sys.argv) != 4:
  sys.stderr.write("Usage: " + sys.argv[0] + " PERCENTDIST_MANHDIST.tsv_file (from comparing_methods.py output) NAME_OF_CHANGE_COLOR_CELL PDF_FILE_OUTPUTNAME\n")
  sys.exit(1)

###########################
# ASSIGNMENTS
FILE = str(sys.argv[1])
CHANGECOLOR = str(sys.argv[2])
CELL_NAME = str(sys.argv[3])

###########################

def change_color(row):
    #print row['CELL'].strip(" ")# , row['CELL']== 'SRR3082330_1.fastq.gz', type(row['CELL'])
    if row['CELL'].strip(" ") == CHANGECOLOR: #row['CELL'] == str(CHANGECOLOR):
    	return 'R'
    else:
    	return 'B'

if __name__ == '__main__':
	df = pd.read_csv(FILE, sep='\t')
	df.columns = [x.strip().replace(' ', '_') for x in df.columns]
	df['Label'] = df.apply(change_color, axis=1)
	#print df

	fig, ax = plt.subplots()
	ax.scatter(df['PLOIDY_GINK'], df['PLOIDY_ANEUF'], c=df['Label'], s=60, edgecolor='black')

	plt.title('Ploidy for 38 Normal Cells Determined by AneuFinder vs. Ginkgo for Patient T5.')
	plt.xlabel('Ginkgo Ploidy')
	plt.ylabel('AneuFinder Ploidy')
	
	plt.savefig('%s_ploidy.pdf' % (CELL_NAME))
	plt.show()
	#ax = df.plot(kind='scatter', x='PLOIDY_GINK', y='PLOIDY_ANEUF');
	#groups = df.groupby('label')
	sys.exit()
