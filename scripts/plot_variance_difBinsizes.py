#!/usr/bin/python
#############################
#
# plot_variance_difBinsizes.py: bins .bed or .tsv files
# created by : Ashley Conard
# last updated: Aug. 31, 2017
#
#############################

import seaborn as sns
import matplotlib.pyplot as plt
import sys
import warnings
import pandas as pd
warnings.filterwarnings("ignore")
import numpy as np
from os import listdir
from os.path import isfile, join
import os

def reformat_df(data, binsize):
    df = pd.read_csv(data)
    len_rows = df.shape[0]
    df['BINSIZE'] = [binsize] * len_rows
    cell_name = list(df)[4]
    df.rename(columns={list(df)[4]:'READ_COUNTS'}, inplace=True)
    df['COVERAGE'] = df.apply(calc_coverage, axis=1) #df['COVERAGE'] = (df['BINSIZE']/binsize)*76
    return df
    
def calc_coverage(row):
    return (row['READ_COUNTS']/row['BINSIZE'])*76

def get_sd(df):
    df[np.abs(df.Data-df.Data.mean())<=(3*df.Data.std())] #keep only the ones that are within +3 to -3 standard deviations in the column 'Data'.
    df[~(np.abs(df.Data-df.Data.mean())>(3*df.Data.std()))] #or if you prefer the other way around

def plots(m_c_pd, RC_COV, LIM, VIOLIN_BOX): #LIM = NUMBER OR NA
    m_c_pd = m_c_pd.reset_index()
    list_max_vals = []
    
    # REPORT STATS WITH ALL DATA AND WITHOUT MAX VALUE
    if RC_COV == 'COVERAGE':
        print "Plotting Coverage"
        SNS_PLOT_TITLE = "Diploid Merged Cell Coverage Distribution Across Genome for Various Bin Sizes."
        OUTPUT_NAME = '_RCvar__plot_difBinsizes.pdf'
        print "Coverage Stats: \n", m_c_pd.groupby("BINSIZE").COVERAGE.describe().unstack()
    
        # Get all the max values for each bin size.
        list_max_per_numcel = m_c_pd.groupby(['BINSIZE'])['COVERAGE'].max()
        for index in list_max_per_numcel.index:
            list_max_vals.append(list_max_per_numcel[index]) #print index, list_max_per_binsize[index]
        
        #print list_max_vals
        print "\nAll data in df is", m_c_pd.shape
        temp_m_c_pd = m_c_pd[~m_c_pd['COVERAGE'].isin(list_max_vals)]# != list_max_vals)]     
        print "Without max values, df is now: ", temp_m_c_pd.shape
        print "\nCoverage Stats without max values for each num cell: \n", temp_m_c_pd.groupby("BINSIZE").COVERAGE.describe().unstack()
    
    elif RC_COV == 'READ_COUNTS':
        print "Plotting Read Counts"
        SNS_PLOT_TITLE = "Diploid Merged Cell Read Count Distribution Across Genome for Various Bin Sizes"
        OUTPUT_NAME ='_covVar__plot_difBinsizes.pdf'
        print "Read Count Stats: \n", m_c_pd.groupby("BINSIZE").READ_COUNTS.describe().unstack()
        
        # Get all the max values for each bin size.
        list_max_per_numcel = m_c_pd.groupby(['BINSIZE'])['READ_COUNTS'].max()
        for index in list_max_per_numcel.index:
            list_max_vals.append(list_max_per_numcel[index]) #print index, list_max_per_binsize[index]
        
        #print list_max_vals
        print "\nAll data in df is", m_c_pd.shape
        temp_m_c_pd = m_c_pd[~m_c_pd['READ_COUNTS'].isin(list_max_vals)]# != list_max_vals)]     
        print "Without max values, df is now: ", temp_m_c_pd.shape
        print "\nCoverage Stats without max values for each num cell size: \n", temp_m_c_pd.groupby("BINSIZE").READ_COUNTS.describe().unstack()
    
    else:
        sys.exit("ERROR: Neither READ_COUNTS nor COVERAGE specified")
    
    # REMOVE OUTLIERS TO PLOT
    if REMOVE_OUTLIERS: # OPTIONAL: Change from all to <22000 to <5000 for filtering.
        print "\nWill remove outliers (>65000 for RC and >15 for Coverage). This is max val: ", m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].COVERAGE, 'for bin size', m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].BINSIZE
        if RC_COV == 'READ_COUNTS':
            m_c_pd = m_c_pd[(m_c_pd['READ_COUNTS'] < 65000)]#& (m_c_pd['BINSIZE']!=1000000) & (m_c_pd['BINSIZE']!=500000) & (m_c_pd['BINSIZE']!=250000)]
        elif RC_COV == 'COVERAGE':
            m_c_pd = m_c_pd[(m_c_pd['COVERAGE'] < .4)]#m_c_pd.loc[m_c_pd['COVERAGE'].idxmax()].COVERAGE)]
    
    sns.set_style("whitegrid")
    fig, ax = plt.subplots()
    fig.set_size_inches(15, 15)# the size of A4 paper
    sns.plt.title(SNS_PLOT_TITLE, size = 20)
    plt.xticks(size = 20)
    plt.yticks(size = 20)
    
    if RC_COV == 'COVERAGE':
        sns.plt.ylabel('Coverage', size = 20)
    elif RC_COV == 'READ_COUNTS':
        sns.plt.ylabel('Read Counts', size = 20)  
    sns.plt.xlabel('Num. Cells', size = 20)
    
    # ZOOM into the plot.  
    if type(LIM) == int:
        plt.ylim(ymin=-100,ymax=6000) #OPTIONAL: Zoom into a specific part of plot
        
    # Violin or Boxplot
    if VIOLIN_BOX == 'boxplot':
        ax = sns.boxplot(x=m_c_pd['BINSIZE'],y=m_c_pd[RC_COV]) #ax = sns.boxplot(x=m_c_pd['BINSIZE'],y=m_c_pd['READ_COUNTS'])#, width=1, bw=.1)
    elif VIOLIN_BOX == 'violinplot':
        ax = sns.violinplot(x=m_c_pd['BINSIZE'],y=m_c_pd[RC_COV])
    
    # Save figure
    cell_n = cell_name.replace("binsXcells__read_depth_plot.csv","")
    plt.savefig(OUTPUT_DIR+"/"+cell_n+'_'+VIOLIN_BOX+'_'+OUTPUT_NAME)

    plt.show()


###########################
# USAGE
if len(sys.argv) != 8:
  sys.stderr.write("Usage: " + sys.argv[0] + " <DATA_DIR (dir of read_count_plot.py output (1 file per cell))> <OUTPUT_NAME> <OUTPUT_DIR> <RC_COV (COVERAGE or READ_COUNTS)> <LIM (NA or int> <VIOLIN_BOX (boxplot or violinplot)> <REMOVE_OUTLIERS (True or False)>\n")
  sys.exit(1)

###########################

# ARGUMENTS
DATA_DIR = str(sys.argv[1]) # DIRECTORY OF ALL BIN_SIZE FILES
#DATA_DIR = "/Users/ashleymaeconard/Desktop/singleCell/results_reproducible/T5_normal_2328cell_read_count_vectors_1MB-50KB/data/"
OUTPUT_NAME = str(sys.argv[2])# "2328cell_T5_normal_1MB-1kb"
OUTPUT_DIR = str(sys.argv[3])#"/Users/ashleymaeconard/Desktop/singleCell/results_reproducible/T5_normal_2328cell_read_count_vectors_1MB-50KB/"
# Options to alter
RC_COV = str(sys.argv[4])#'COVERAGE' # READ_COUNTS, COVERAGE
if RC_COV != 'COVERAGE' and 'READ_COUNTS':
    sys.exit("ERROR: Neither READ_COUNTS nor COVERAGE specified")
LIM = sys.argv[5] # 'NA' OR int(INTEGER)
VIOLIN_BOX = str(sys.argv[6]) #'boxplot'# violinplot or boxplot
if VIOLIN_BOX != 'boxplot' and 'violinplot':
    sys.exit("ERROR: Neither boxplot nor violinplot specified")
REMOVE_OUTLIERS = str(sys.argv[7]) # True
if REMOVE_OUTLIERS != 'True' and REMOVE_OUTLIERS != 'False':
    sys.exit("ERROR: Neither True nor False specified")


if __name__ == '__main__':
    # MAIN
    counter = 0 # keeping track of the number of cells which have been processed.

    # Create list of all .bam files to transform into vectors of normalized read counts
    onlyfiles = [f for f in listdir(DATA_DIR) if isfile(join(DATA_DIR, f)) and "binsXcells__read_depth_plot" in f]
    print "Data to process: ", onlyfiles, "\n"

    if ".DS_Store" in onlyfiles:
        onlyfiles.remove(".DS_Store")

    # DF to fill with vectors of normalized read counts across all cells in the input directory
    master_df = pd.DataFrame(columns=('CHR', 'START', 'END', 'READ_COUNTS', 'BINSIZE'))

    for cell_name in onlyfiles:
        counter+=1
        bin_size = float(cell_name.split("_")[-6])
        print bin_size
        print "File # ", counter ," of ", len(onlyfiles),". Num Cells ",len(onlyfiles), " in DIR+name: ", DATA_DIR+cell_name, "\n" # file and filepath
        
        # Reformat df
        df_reformatted = reformat_df(DATA_DIR+cell_name, bin_size)
        
        # Append to master_df
        master_df = master_df.append(df_reformatted)
    
    # CREATING PLOTS
    master_df.BINSIZE = master_df.BINSIZE.astype(float)
    master_df = master_df.sort_values('BINSIZE', ascending=True)
    master_df.to_csv(OUTPUT_DIR+"/"+OUTPUT_NAME+"_"+RC_COV+"__plot_difBinsizes.csv")
    plots(master_df, RC_COV, LIM, VIOLIN_BOX)