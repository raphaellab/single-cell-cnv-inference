
# coding: utf-8

# In[8]:


#!/usr/bin/python

from __future__ import division
#import seaborn as sns
from collections import Counter
import subprocess
import matplotlib.pyplot as plt
import numpy as np
import re
import pandas as pd
import pybedtools as pyb
from pybedtools import BedTool #https://daler.github.io/pybedtools/autodocs/pybedtools.bedtool.BedTool.html
import sys
import os
from os import listdir
from cStringIO import StringIO
import itertools
from os.path import isfile, join
import matplotlib
from scipy.stats import mode
import glob
from IPython.display import HTML
from pylab import rcParams
import scipy as sp

###########################
# USAGE
if len(sys.argv) != 5:
    sys.stderr.write("Usage: " + sys.argv[0] + " <CLONE (clone1 or clone2)> <MIN_BIN_SIZE (e.g. 250000)> <METHOD (AneuCP, AneuHMM, Ginkgo)> <KB_BIN_SIZE (250kb)>\n")
    sys.exit()

###########################
# ASSIGNMENTS
CLONE = str(sys.argv[1])
MIN_BIN_SIZE = str(sys.argv[2])
METHOD = str(sys.argv[3]) 
KB_BIN_SIZE = str(sys.argv[4])

if METHOD == 'AneuCP':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/clone1_matPat/"+
               "aneuFout/"+KB_BIN_SIZE+"_"+CLONE+"/BROWSERFILES/method-dnacopy/*")][0]
    print "Processing ", naam, " for AneuFinder Change Point"   
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'AneuHMM':
    naam = [name for name in glob.glob("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/clone1_matPat/"+
               "aneuFout/"+KB_BIN_SIZE+"_"+CLONE+"/BROWSERFILES/method-HMM/*")][0]
    print "Processing ", naam, " for AneuFinder HMM"  
    DATA = BedTool(naam)
    DATA = DATA.to_dataframe()

elif METHOD == 'Ginkgo':
    DATA = pd.read_csv("/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/"+KB_BIN_SIZE+"_"+CLONE+".tsv", sep='\t') # 38normal_cells_nochrY_variable_normalized_counts_10kb.tsv #38normal_cells_nochrY_fixed_CN_250kb.tsv
#OUTPUT_NAME = "38_normal_10KB_bins_wrong_right__distribution_non-2_copy_number.csv"
#df.replace({r'\r\n': ''},regex=True);

#### No need to change below ####
OUTPUT_NAME = "__precision_recall.csv"

# Events to check
if CLONE == 'clone1':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone1_seed14.csv")
elif CLONE == 'clone2':
    clone_events = pd.read_csv("/n/fs/ragr-data/projects/scDNA/scripts/clone2_seed23.csv")

# Output Directory
if METHOD == "Ginkgo":
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/ginkout/precision_recall_"+CLONE+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
else:
    OUTPUTDIR = "/n/fs/ragr-data/projects/scDNA/simulations_rsvsim_aug2017/"+CLONE+"_matPat/aneuFout/precision_recall_"+CLONE+"_"+str(MIN_BIN_SIZE)+"_"+METHOD+"/"
if not os.path.exists(OUTPUTDIR):
    os.makedirs(OUTPUTDIR)


def create_indiv_bed_files():
    list_cell_names = []
    list_cell_bed_files = []
    
    # get indices for how to split .bed file into multiple
    df_names = DATA[DATA['chrom'].str.contains("track")] # lines with name
    list_lines = df_names['chrom'].values.tolist() # list of all lines with name
    list_indices_cells = df_names.index.tolist()
    list_indices_cells.append(DATA.shape[0]) # get of the file
    
    # get names of cells
    for line in list_lines: 
        line_s = line.split(" ")
        list_cell_names.append([ i for i in line_s if "_" in i][0].strip('"')) # extracted names of all cells in order of bed file
     
    # split each cell into its own bed file
    for idx, i in  enumerate(list_indices_cells[:-1]):
        curr_cell_bed = DATA[list_indices_cells[idx]: list_indices_cells[idx+1]]
        curr_cell_bed = curr_cell_bed.drop(curr_cell_bed.index[[0]]) # drop weird title line
        curr_cell_bed = curr_cell_bed.drop(["score",'strand','thickStart','thickEnd','itemRgb'], axis=1) # drop unneeded cols
        curr_cell_bed.reset_index()
        curr_cell_bed['name'] = curr_cell_bed['name'].map(lambda x: x.rstrip('-somy'))# remove -somy
        curr_cell_bed['start'] = curr_cell_bed['start'].astype(int)
        curr_cell_bed['end'] = curr_cell_bed['end'].astype(int)
                
        # saving .bed file
        print idx, ": Saving sep"+str(list_cell_names[idx])+"_"+METHOD+".bed"
        list_cell_bed_files.append("sep"+str(list_cell_names[idx])+"_"+METHOD+".bed")
        curr_cell_bed.to_csv(OUTPUTDIR+"/"+"sep"+str(list_cell_names[idx])+"_"+METHOD+".bed",sep="\t", index=False, header=False)
    
    return list_cell_names, list_cell_bed_files

def check_each_chrom(df_cell_chrom, df_clone_event_chrom,TP,TN,FP,FN):
    if df_clone_event_chrom.empty:
        print "chrom does not have any inserted events:", df_clone_event_chrom
        print df_cell_chrom
        for idx, row in df_cell_chrom.iterrows():
            if row['CN']==2:
                TN+=1
            elif row['CN']!=2:
                FP+=1
    else:
        df_cell_chrom['ACCNTD'] =  np.nan
        df_clone_event_chrom['ACCNTD'] =  np.nan
        df_cell_chrom = df_cell_chrom.reset_index(); df_clone_event_chrom = df_clone_event_chrom.reset_index()
        print "events\n",df_clone_event_chrom
        print "cell\n",df_cell_chrom

        # for all inserted events
        for idx_evt, row_evt in df_clone_event_chrom.iterrows():
            num_matches=0 # see how many segments match with the inserted events (so to choose the largest one).
            print "index_event", idx_evt
            # for all called events by method
            for idx, row in df_cell_chrom.iterrows(): 
                print "\n TO ASSESS:", row['START'],"<=",row_evt['START']+row_evt['EVENT_SIZE']-1, "&", row_evt['START'],"<=", row['END'],"\n"

                # if overlap 
                # if (row.start <= (row_evt.START+row_evt.EVENT_SIZE+MIN_BIN_SIZE)) & (row_evt.START-MIN_BIN_SIZE <= row.end): # x1 <= y2 && y1 <= x2
                if ((row['START'] <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <= row['END'])):
                    num_matches+=1
                    df_clone_event_chrom = df_clone_event_chrom.set_value(idx_evt,'ACCNTD',0)
                    print df_clone_event_chrom
                    print "\n in interval"
                    # initiate overlap_df

                    if num_matches == 1: 
                        print "num matches == 1"
                        overlap_df = pd.DataFrame(columns=['CHR','START','END','CN','ACCNTD'])
                        overlap_df = overlap_df.append(row, ignore_index=True)
                        print "overlap\n", overlap_df

                        # if the next does not exist or does not overlap
                        if (df_cell_chrom.shape[0]-1 == idx):
                            print "next1 is not overlap - find_event()"
                            TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                            break
                        elif (((df_cell_chrom.iloc[idx+1].START <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <=df_cell_chrom.iloc[idx+1].END)) == False):
                            print "next2 is not overlap - find_event()"
                            TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                            break
                        else:
                            print "next is another overlap"
                    elif (df_cell_chrom.shape[0]-1 == idx):
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        print "overlap added to", overlap_df
                        print "at end of df, next is not overlap - find_event()"
                        TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                        break
                    elif (((df_cell_chrom.iloc[idx+1].START <= (row_evt['START']+row_evt['EVENT_SIZE']-1)) & (row_evt['START'] <=df_cell_chrom.iloc[idx+1].END)) == False) or (df_cell_chrom.shape[0] == idx): # if next row does not overlap - interval is complete
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        print "overlap added to", overlap_df
                        print "next3 is not overlap - find_event()"
                        TP,TN,FP,FN = find_event(overlap_df, row_evt,TP,TN,FP,FN, df_cell_chrom)
                        break

                    else: # keep adding to dataframe
                        overlap_df = overlap_df.append(row, ignore_index=True) #{'CHR':row['CHR'],'START':row['START'],'END','CN','ACCNTD'}
                        print "overlap added to\n", overlap_df
                else:
                    print "\n not in interval"

        print "transformed events\n",df_clone_event_chrom
        print "transformed cell\n",df_cell_chrom

        nan_df_cell_chrom = nans(df_cell_chrom)
        if not nan_df_cell_chrom.empty:
            for idx,row in nan_df_cell_chrom.iterrows():
                if row['CN']==2:
                    TN+=1
                elif row['CN']!=2:
                    FP+=1
    print "TPTN...", TP, TN, FP, FN       
    return TP,TN,FP,FN
    

def find_event(cell_df_inevent, row_evt, TP,TN,FP,FN, cell_df_chrom):
    print "\n\nCELL_DF\n",cell_df_inevent
    print "row_event\n",row_evt
    #count_once = 0
    
    for idx, row in cell_df_inevent.iterrows():  
        t_TP=TP;t_TN=TN;t_FP=FP;t_FN=FN
        # If only one overlapping segment
        if cell_df_inevent.shape[0] == 1:
            if (row_evt['MAX_CN'] <= 1) and (row['CN'] <= 1): # TP del
                print "TP"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                TP+=1
            elif (row_evt['MAX_CN'] >= 3) and (row['CN'] >= 3): # TP amp
                print "TP"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                TP+=1
            elif (row_evt['MAX_CN'] == 2) and (row['CN'] == 2): # TN diploid
                print "SHOULD NOT HAPPEN - TN"
                sys.exit()
                #cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                #TN+=1
            elif (row_evt['MAX_CN'] <= 1) and (row['CN'] == 2):
                print "FN"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FN+=1
            elif (row_evt['MAX_CN'] >= 1) and (row['CN'] == 2):
                print "FN"
                cell_df_chrom.loc[(cell_df_chrom.START==row['START']) & (cell_df_chrom.END==row['END']) & (cell_df_chrom['CN']==row['CN']), 'ACCNTD'] = 0
                FN+=1
            else:
                print("ERROR")

        # Multiple overlaping segements with one inserted event. Get all called amp or del events (depending on truth) for that region. 
        #   All else will be considered FP (if was amp but called del & vice versa) or FN (if called 2) later
        elif cell_df_inevent.shape[0] > 1:
            #count_once+=1
            print "MULTIPLE SEGMENTS OVERLAP WITH ONE EVENT"
            if (row_evt['MAX_CN'] <=1) and any(cell_df_inevent['CN'] <=1): # true dels
                selected = cell_df_inevent[cell_df_inevent['CN'] <= 1]
                #if count_once==1:
                TP+=1
                for it, row_s in selected.iterrows():
                    cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
            elif (row_evt['MAX_CN'] >= 3) and any(cell_df_inevent['CN'] >=3): # true amps
                selected = cell_df_inevent[cell_df_inevent['CN'] <= 1]
                #if count_once==1:
                TP+=1
                for it, row_s in selected.iterrows():
                    cell_df_chrom.loc[(cell_df_chrom.START==row_s['START'])&(cell_df_chrom.END==row_s['END']), 'ACCNTD'] = 0
                print "accounted for AMP:\n", cell_df_chrom
            #elif any(row_evt['MAX_CN'] >= 3) and cell_df_inevent['CN'] <=
            elif (row_evt['MAX_CN'] >= 3) and any(cell_df_inevent['CN'] == 2): # this will be hit if the others are not - so ALL are 2 
                #if count_once==1:
                FN+=1
                print "LOOK"
            elif (row_evt['MAX_CN'] <= 1) and any(cell_df_inevent['CN'] == 2): # this will be hit if the others are not - so ALL are 2
                #if count_once==1:
                FN+=1
                print "LOOK"
                
            else:
                print "another case..."
                sys.exit()     

    print "before: ", t_TP,t_TN,t_FP,t_FN
    print "cont...", TP, TN, FP, FN
    if abs((t_TP+t_TN+t_FP+t_FN)-(TP+TN+FP+FN)) > 1:
        print "ERROR - ADDED MORE THAN 1 FOR EACH SEGMENT."
        sys.exit()
    
    return TP, TN, FP, FN

#### MAIN ####
if __name__ == '__main__':
    # CREATE MASTER INTERVAL DATAFRAME: DF_MASTER
    chromosome = ["chr" + str(i) for i in range(1,23)]# chromosome
    chromosome.append('chrX'); chromosome.append("chrY")

    # Ginkgo processing df_master
    if (METHOD == 'Ginkgo'):
        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                #for ind, row in DATA:
                cur_chrom_DATA = DATA[DATA['CHR']=='chr1']
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                #df_res.append(res)
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_start_ends_chrom = clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist()
                    list_start_ends_chrom_min1 = [x - 1 for x in list_start_ends_chrom]
                    list_st_ens.append(list_start_ends_chrom+list_start_ends_chrom_min1)

                cur_chrom_DATA = DATA[DATA['CHR']==chro]
                #for ind, row in DATA:
                list_st_ens.append(cur_chrom_DATA['START'].tolist()+cur_chrom_DATA['END'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        #df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        df_master = df_master[abs(df_master.START-df_master.END)>1]
        print "size df_master: ",df_master.shape   

    # AneuFinder processing df_master
    else:
        list_names_cells, list_bed_files_cells = create_indiv_bed_files()

        for chro in chromosome: # for each chromosome
            list_st_ens = [] # new list for each chromosome
            if chro == 'chr1':
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file chromosome 1
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create df_master with chr1
                df_master = pd.DataFrame(list_all_intervals)
                df_master.insert(0, 'CHR', 'chr1')
                df_master.columns = ['CHR','START', 'END']

            else: # add all other chromosome intervals to df_master
                #df_res.append(res)
                if chro in clone_events.CHR.values: # for inserted events
                    clone_events_chro = clone_events[clone_events['CHR']==chro]
                    clone_events_chro_list_ends = clone_events_chro['START']+clone_events_chro['EVENT_SIZE'].tolist()
                    list_st_ens.append(clone_events_chro['START'].tolist()+ clone_events_chro_list_ends.tolist())

                for bed_file in list_bed_files_cells: # for each bed file all other chromosomes
                    df_cur = BedTool(OUTPUTDIR+"/"+bed_file)
                    df_cur = df_cur.to_dataframe()
                    df_cur_chrom = df_cur[df_cur['chrom']== chro]
                    list_st_ens.append(df_cur_chrom['start'].tolist()+df_cur_chrom['end'].tolist())

                # create intervals for dataframe
                list_st_ens = list(itertools.chain.from_iterable(list_st_ens))
                list_et_ens_sorted_set = sorted(set(list_st_ens))
                list_all_intervals = list(zip(list_et_ens_sorted_set[:-1], list_et_ens_sorted_set[1:]))

                # create dataframe for current chrom to add to df_master
                df_toadd = pd.DataFrame(list_all_intervals)
                df_toadd.insert(0, 'CHR', chro)
                df_toadd.columns = ['CHR','START', 'END']
                df_master = df_master.append(df_toadd,ignore_index=True)

        df_master["END"] = df_master["END"].apply(lambda x: x - 1)   
        print "size df_master: ",df_master.shape 

    # ITERATE THROUGH EACH CELL BED TO PRODUCE INTEGRATED SEGMENT BED FILE OF ALL CELLS
    if (METHOD == 'AneuHMM') or (METHOD == 'AneuCP'):
        for idex, cell_bed in enumerate(list_bed_files_cells):
            print cell_bed
            print list_names_cells[idex],'\n'

            sep_cell_bed = pd.read_csv(OUTPUTDIR+"/"+cell_bed, sep="\t", header=None)
            sep_cell_bed.columns =['CHR', 'START','END','CN']
            sep_cell_bed["END"] = sep_cell_bed["END"].apply(lambda x: x - 1)

            for chro in chromosome:
                cur_master = df_master[df_master['CHR']==chro] # subset chrom from df_master
                cur_cell = sep_cell_bed[sep_cell_bed['CHR']==chro] # subset chrom from current cell bed

                for idx, row in cur_master.iterrows(): # go through df_master intervals for that chrom
                    num_matches=0 # master interval should only match once!
                    for idx_cr_cell, row_cr_cell in cur_cell.iterrows(): # go through each cell bed for that chrom
                        if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                            num_matches+=1
                            if METHOD != 'Ginkgo':
                                if "zero" in row_cr_cell['CN']:
                                    print "Converting zero-inflation"
                                    row_cr_cell['CN'] = 0
                            df_master.loc[idx, list_names_cells[idex]] = int(row_cr_cell['CN'])
                            # if master interval is covered more than once in cur_cell-> flag to take a look
                            if num_matches>1:
                                sys.exit("ERROR", num_matches)

        # RETURNING NAN ROWS <- INDICATING THAT A MASTER INTERVAL WAS NOT FILLED (SHOULD NOT OCCUR) 
        # if master interval is NOT coverged - NAN -> check upon running
        nans = lambda df: df[df.isnull().any(axis=1)]
        nan_df = nans(df_master)
        print "NAN DF_MASTER SHAPE: \n",  nan_df.shape
        if nan_df.empty:
            df_master.to_csv(OUTPUTDIR+"/"+CLONE+"_"+METHOD+"_integrated_beds_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, index=False) 
        else:
            print nan_df
            sys.exit("Must fix this")

    # Processing Ginkgo
    else:
        print "There are ", len(list(DATA)[3:]), " cells to process."

        for cell in list(DATA)[3:]:
            print "Processing: ", cell
            cur_g_cell = DATA[['CHR','START','END',cell]]
            #cur_g_cell.columns =['CHR', 'START','END','CN']

            for chro in chromosome: # 
                cur_master = df_master[df_master['CHR']==chro] # subset chrom from df_master
                cur_cell = cur_g_cell[cur_g_cell['CHR']==chro] # subset chrom from current cell bed

                for idx, row in cur_master.iterrows(): # go through df_master intervals for that chrom
                    num_matches=0 # master interval should only match once!
                    for idx_cr_cell, row_cr_cell in cur_cell.iterrows(): # go through each cell bed for that chrom
                        if (row['START'] <= (row_cr_cell['END'])) & (row_cr_cell['START'] <= row['END']): # in interval
                            num_matches+=1
                            df_master.loc[idx, cell] = row_cr_cell[cell]
                            # if master interval is covered more than once in cur_cell-> flag to take a look
                            if num_matches>1:
                                sys.exit("ERROR", num_matches)

        list_names_cells = list(df_master)[3:]

        # RETURNING NAN ROWS <- INDICATING THAT A MASTER INTERVAL WAS NOT FILLED (SHOULD NOT OCCUR) 
        # if master interval is NOT coverged - NAN -> check upon running
        nans = lambda df: df[df.isnull().any(axis=1)]
        nan_df = nans(df_master)
        print "NAN DF_MASTER SHAPE: \n",  nan_df.shape
        if nan_df.empty:
            df_master.to_csv(OUTPUTDIR+"/"+CLONE+"_"+METHOD+"_integrated_beds_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, index=False) 
        else:
            print nan_df
            sys.exit("Must fix this")


    # Calculate Precision and Recall
    with open(OUTPUTDIR+"/"+"accuracyVals_"+CLONE+"_"+METHOD+"_"+str(MIN_BIN_SIZE)+OUTPUT_NAME, 'w') as f:
        f.write("Cell,TP,TN,FP,FN\n")
        for idx, i in  enumerate(list_names_cells):
            print "Currently counting TP,TN,FP,FN for: ", i
            curr_cell = df_master[['CHR','START','END',i]]
            curr_cell = curr_cell.rename(columns={i:'CN'})
            if METHOD != 'Ginkgo':
                if curr_cell['CN'].dtype != np.float64:
                    print "Converting zero-inflation"
                    curr_cell['CN'] = curr_cell.CN.replace({"zero-inflation":0})
            curr_cell['CN'] = curr_cell['CN'].astype(int)
            TP=TN=FN=FP=0
            for chro in chromosome:
                curr_cell_chrom = curr_cell[curr_cell['CHR']==chro]
                TP,TN,FP,FN = check_each_chrom(curr_cell_chrom,clone_events[clone_events.CHR==chro],TP,TN,FP,FN)
                print "EVENTS: ", TP,TN,FP,FN
            f.write(i+","+str(TP)+","+str(TN)+","+str(FP)+","+str(FN)+"\n")


