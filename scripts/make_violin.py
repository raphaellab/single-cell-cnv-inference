#!/usr/bin/python
#
# make_violin.py : Creates two violin plots (fixed m and k and n)
#	
# Ashley Conard

import seaborn as sns
import matplotlib.pyplot as plt
import sys
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import os
 
def violin_plots(x,y):
	print "x ", x
	print "y ", y

	sns.set_style("whitegrid")
	sns.plt.title("Average Difference: "+str(M_CELLS)+" Cells "+str(K_CLONES)+" Clones "+ str(N_BINS)+" Bins ")
	plt.xlabel('Methods')
	sns.plt.ylabel('Averaged Manhattan Distance')
	sns.plt.ylim(0, 0.2)
	#sns.plt.yticks(0.0, 0.2, 0.01)
	ax = sns.violinplot(x=y,y=x) #inner="quartile"
	sns.plt.savefig('violin_plot_%dm_%dk_%dn.png' % (M_CELLS, K_CLONES, N_BINS))
	#sns.plt.show()

def read_file(input_file):
	joint_dist = []
	indep_dist = []
	label = []

	with open(input_file,'r') as f:
		for line in f:
			s = line.strip('\n').split('\t')
			joint_dist.append(float(s[0]))
			indep_dist.append(float(s[1]))

	full_list = joint_dist+indep_dist
	label = (['Jointly Inferred'] *len(joint_dist)) + (['Independently Inferred']*len(indep_dist))			
	#print "label",label
	#print "full_list", full_list

	return full_list, label 

###########################
# USAGE
if (len(sys.argv) !=5):
    sys.stderr.write("Usage: " + sys.argv[0] +"<all.txt> (with summed and normalized distance between all points) joint 'tab' indep, <NUM_CELLS>  <K_CLONES> <N_BINS>\n")
    sys.exit(1)

# ASSIGNMENTS
FILE = str(sys.argv[1])
M_CELLS = int(sys.argv[2])
K_CLONES = int(sys.argv[3])
N_BINS = int(sys.argv[4])

if __name__ == '__main__':
	x_data,y_label = read_file(FILE)
	violin_plots(x_data, y_label)


